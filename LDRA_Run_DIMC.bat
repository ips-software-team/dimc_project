@echo off
set ATOLLIC=C:\Program Files (x86)\Atollic\TrueSTUDIO for STM32 9.1.0\
set tbed=C:\LDRA_Toolsuite
set tlp=C:\LDRA_Toolsuite\Compiler_spec\Atollic\Stm32\
set path=%tbed%;%ATOLLIC%ARMTools\bin;%ATOLLIC%Servers\ST-Link_gdbserver;C:\Program Files (x86)\Atollic\TrueSTUDIO for STM32 9.1.0\Servers\J-Link_gdbserver;%PATH%
set conf=Debug
set port=2331
set addr=127.0.0.1
set device=STM32F427VG
set server=ST-LINK_gdbserver
set exe=DIMC_01_00.elf
set script=run.gdb
set res=history.exh

rem delete any existing file
rem ========================
if exist %conf% cd /d %conf%
if exist %res% del /F %res%

if not exist %exe% (
  echo ERROR: Failed to locate the executable: %exe%
  echo        Please ensure that the project is built
  echo.
  ping localhost -n 6 >nul
  exit /B 1
)

rem create script
rem =============
echo # gdb script to capture execution history > %SCRIPT%
echo # ======================================= >> %SCRIPT%
echo. >> %SCRIPT%
echo target remote %addr%:%port% >> %SCRIPT%
echo. >> %SCRIPT%
echo # Set character encoding >> %SCRIPT%
echo set host-charset CP1252 >> %SCRIPT%
echo set target-charset CP1252 >> %SCRIPT%
echo. >> %SCRIPT%
echo set pagination off >> %SCRIPT%
echo set logging file %res% >> %SCRIPT%
echo set logging redirect on >> %SCRIPT%
echo. >> %SCRIPT%
echo file %exe% >> %SCRIPT%
echo load >> %SCRIPT%
echo monitor reset >> %SCRIPT%
echo. >> %SCRIPT%
echo # Enable Debug connection in low power modes (DBGMCU-^>CR) >> %SCRIPT%
echo set *0xE0042004 = (*0xE0042004) ^| 0x7 >> %SCRIPT%
echo. >> %SCRIPT%
echo delete breakpoint >> %SCRIPT%
echo set breakpoint auto-hw >> %SCRIPT%
echo. >> %SCRIPT%
echo break ldra_upload >> %SCRIPT%
echo commands 1 >> %SCRIPT%
echo   printf "LDRA->%%s\n", ldra_message >> %SCRIPT%
echo   if ldra_exit_reached == 1 >> %SCRIPT%
echo     set logging off >> %SCRIPT%
echo     delete breakpoint >> %SCRIPT%
echo     quit >> %SCRIPT%
echo   end >> %SCRIPT%
echo   continue >> %SCRIPT%
echo end >> %SCRIPT%
echo. >> %SCRIPT%
echo set logging on >> %SCRIPT%
echo continue >> %SCRIPT%


rem Start the gdbserver
rem ===================
set PROCESS=ST-LINK_gdbserver.exe
start "gdbserver" /min ST-LINK_gdbserver.exe -p %port% -r 15 -d -cp "C:\Program Files (x86)\Atollic\TrueSTUDIO for STM32 9.1.0\Servers\STM32CubeProgrammer\bin"


rem run gdb using a script
rem ======================
@echo on
arm-atollic-eabi-gdb --quiet --command=%SCRIPT%
@echo off


rem Kill the gdbserver
rem ==================
for /F %%x in ('tasklist /NH /FI "IMAGENAME eq %PROCESS%"') do (
  if %%x == %PROCESS% taskkill /F /IM %PROCESS% > nul
)


if exist %res% (
  rem remove any lines that don't start with LDRA->
  rem =============================================
  "%tbed%\Utils\Python\python.exe" "%tlp%Strip_EXH.py" %res%

  rem Finally view the generated file
  rem ===============================
  TBbrowse %res%
) else (
  echo ERROR: Failed to locate file: %res%
  echo        It could be that the project has not been built with instrumentation
  echo        Or that the target is not connected
  echo.
  ping localhost -n 6 >nul
  exit /B 1
)
