/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : CTRY
**
** Filename  : main.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 11-07-2020
**
** Description: Header for main.c file
** 		        This file contains the common defines of the application
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 11-07-2020 -- Added define
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/

#ifndef __MAIN_H
#define __MAIN_H

/* -----Includes -------------------------------------------------------------------*/
#include <gipsy_hal_def.h>


/* -----Definitions ----------------------------------------------------------------*/
#define _DIMC_ /*!< Module Name */
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/


/* -----Function prototypes --------------------------------------------------------*/
void Error_Handler(void);
extern void SystemInit(void);


#endif /* __MAIN_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
