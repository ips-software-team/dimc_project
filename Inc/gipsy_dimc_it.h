/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gipsy_dimc_it.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 28-10-2020
**
** Description: Header for gipsy_dimc_it.c - This file contains the headers of the interrupt handlers.
**
*/


/* Revision Log:
**
** Rev 1.0  : 28-10-20 -- Original version created.
**
**************************************************************************************
*/



/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GIPSY_DIMC_IT_H
#define GIPSY_DIMC_IT_H

/* -----Includes -------------------------------------------------------------------*/

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void SysTick_Handler(void);
void USART1_IRQHandler(void);
void CAN1_RX0_IRQHandler(void);

#ifdef _DEBUG_
void USART3_IRQHandler(void);
#endif

#endif /* GIPSY_DIMC_IT_H */

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
