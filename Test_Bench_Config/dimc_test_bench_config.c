/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : dimc_test_bench_config.c
** Author    : Omer Geron
** Revision  : 1.0
** Updated   : 30-01-2022
**
** Description: - This file contains recovery functions from unsupported HW tests in the DIMC Test-Bench setup.
**
** Notes:
**
**
*/


/* Revision Log:
**
** Rev 1.0  : 30-01-2022 -- Original version created.
** **************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "module_init_dimc.h"


/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** DIMC_Pbit_Recovery - Set the DIAC HW test result to 'OK'.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void DIMC_Pbit_Recovery(void)
{
	g_SysStatus.DIMC_PBIT_Failure_Desc &= 0xDF;
	g_SysTestsStatus.DIMC_Ch_A_Test_Status = TEST_OK;
}



/*************************************************************************************
** DIMC_System_PBIT_Recovery - Set the CTRYs motor_oper input test result to 'OK'.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void DIMC_System_PBIT_Recovery(void)
{
	for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM ; ctry_num++)
	{
		g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status = TEST_OK;
		g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status = TEST_OK;
		g_SysStatus.CTRY_PBIT_Status[ctry_num].PBIT_Status = 0x00U;
		g_SysStatus.CTRY_Status[ctry_num].failed_to_resp = 0x00U;
		g_CTRYs_SW_Ver[ctry_num] = 0x00U;
	}
	g_SysTestsStatus.CTRYs_SW_Ver_Mismatch_Test_Status = 0x00U;
	g_SysStatus.DIMC_PBIT_Failure_Desc = 0x00U;
}


/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
