/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : dimc_test_bench_config.c
** Author    : Omer Geron
** Revision  : 1.0
** Updated   : 30-01-2022
**
** Description: - .h file for dimc_test_bench_config.c file.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 30-01-2022 -- Original version created.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TEST_BENCH_CONFIG_H
#define TEST_BENCH_CONFIG_H



/* -----Function prototypes -----------------------------------------------------*/
void DIMC_Pbit_Recovery(void);
void DIMC_System_PBIT_Recovery(void);


#endif /*TEST_BENCH_CONFIG_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
