@echo off
set ATOLLIC=C:\Program Files (x86)\Atollic\TrueSTUDIO for STM32 9.1.0\
set PATH=%ATOLLIC%ide;%PATH%

set ROOT=C:\Projects\AIF_IPS_GHTP\
set PROJ=Atollic_STM32F427_DIMC
set WORK=C:\LDRA_Workarea\Examples\Tlps\Atollic\Stm32_Workspace
set CFG=Debug

if not exist "C:\Program Files (x86)\Atollic\TrueSTUDIO for STM32 9.1.0\ide\headless.bat" (
  echo ERROR: Failed to locate the executable: headless.bat
  echo        Please reinstall the TLP and select the correct path of where Atollic TrueSTUDIO for STM32 is located
  echo.
  pause
  exit /B 1
)

@echo on
call headless.bat -data %WORK% -cleanBuild %PROJ%/%CFG%

if errorlevel 1 pause
 pause