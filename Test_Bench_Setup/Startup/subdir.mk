################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../Startup/startup_stm32f427xx.s 

OBJS += \
./Startup/startup_stm32f427xx.o 


# Each subdirectory must supply rules for building sources it contributes
Startup/%.o: ../Startup/%.s
	arm-atollic-eabi-gcc -c -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D_DIMC_ -D__packed=__attribute__((__packed__)) -D__weak=__attribute__((weak)) -DSTM32F427xx -DUSE_HAL_DRIVER -g -Wa,--warn -x assembler-with-cpp -specs=nano.specs -o "$@" "$<"

