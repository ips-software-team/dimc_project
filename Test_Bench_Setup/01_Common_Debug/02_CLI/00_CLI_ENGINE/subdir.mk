################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_engine.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_general.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_io.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_msg.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_parser.c 

OBJS += \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_engine.o \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_general.o \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_io.o \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_msg.o \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_parser.o 

C_DEPS += \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_engine.d \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_general.d \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_io.d \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_msg.d \
./01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_parser.d 


# Each subdirectory must supply rules for building sources it contributes
01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_engine.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_engine.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_general.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_general.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_io.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_io.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_msg.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_msg.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_parser.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/01_Common_Debug/02_CLI/00_CLI_ENGINE/cli_parser.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"

