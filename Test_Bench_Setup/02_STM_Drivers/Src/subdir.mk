################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_adc.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_can.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_cortex.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_gpio.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_i2c_dimc.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_rcc.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_rtc.c \
C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_uart.c 

OBJS += \
./02_STM_Drivers/Src/gipsy_hal_adc.o \
./02_STM_Drivers/Src/gipsy_hal_can.o \
./02_STM_Drivers/Src/gipsy_hal_cortex.o \
./02_STM_Drivers/Src/gipsy_hal_gpio.o \
./02_STM_Drivers/Src/gipsy_hal_i2c_dimc.o \
./02_STM_Drivers/Src/gipsy_hal_rcc.o \
./02_STM_Drivers/Src/gipsy_hal_rtc.o \
./02_STM_Drivers/Src/gipsy_hal_uart.o 

C_DEPS += \
./02_STM_Drivers/Src/gipsy_hal_adc.d \
./02_STM_Drivers/Src/gipsy_hal_can.d \
./02_STM_Drivers/Src/gipsy_hal_cortex.d \
./02_STM_Drivers/Src/gipsy_hal_gpio.d \
./02_STM_Drivers/Src/gipsy_hal_i2c_dimc.d \
./02_STM_Drivers/Src/gipsy_hal_rcc.d \
./02_STM_Drivers/Src/gipsy_hal_rtc.d \
./02_STM_Drivers/Src/gipsy_hal_uart.d 


# Each subdirectory must supply rules for building sources it contributes
02_STM_Drivers/Src/gipsy_hal_adc.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_adc.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_can.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_can.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_cortex.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_cortex.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_gpio.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_gpio.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_i2c_dimc.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_i2c_dimc.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_rcc.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_rcc.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_rtc.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_rtc.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"
02_STM_Drivers/Src/gipsy_hal_uart.o: C:/Projects/AIF_IPS_GHTP/IPS_STM32F4/02_STM_Drivers/Src/gipsy_hal_uart.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"

