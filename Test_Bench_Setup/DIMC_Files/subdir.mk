################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../DIMC_Files/can_protocol_dimc.c \
../DIMC_Files/cbit_dimc.c \
../DIMC_Files/crc16.c \
../DIMC_Files/diac_mngmnt.c \
../DIMC_Files/exflash_dimc.c \
../DIMC_Files/fault_management_dimc.c \
../DIMC_Files/flash_integrity_test.c \
../DIMC_Files/gpio_control_dimc.c \
../DIMC_Files/irs_protocol_dimc.c \
../DIMC_Files/logger_dimc.c \
../DIMC_Files/mcu_config_dimc.c \
../DIMC_Files/module_init_dimc.c \
../DIMC_Files/pbit_dimc.c \
../DIMC_Files/periodic_dimc.c \
../DIMC_Files/system_params_dimc.c \
../DIMC_Files/voltage_meas_dimc.c \
../DIMC_Files/wdog.c 

OBJS += \
./DIMC_Files/can_protocol_dimc.o \
./DIMC_Files/cbit_dimc.o \
./DIMC_Files/crc16.o \
./DIMC_Files/diac_mngmnt.o \
./DIMC_Files/exflash_dimc.o \
./DIMC_Files/fault_management_dimc.o \
./DIMC_Files/flash_integrity_test.o \
./DIMC_Files/gpio_control_dimc.o \
./DIMC_Files/irs_protocol_dimc.o \
./DIMC_Files/logger_dimc.o \
./DIMC_Files/mcu_config_dimc.o \
./DIMC_Files/module_init_dimc.o \
./DIMC_Files/pbit_dimc.o \
./DIMC_Files/periodic_dimc.o \
./DIMC_Files/system_params_dimc.o \
./DIMC_Files/voltage_meas_dimc.o \
./DIMC_Files/wdog.o 

C_DEPS += \
./DIMC_Files/can_protocol_dimc.d \
./DIMC_Files/cbit_dimc.d \
./DIMC_Files/crc16.d \
./DIMC_Files/diac_mngmnt.d \
./DIMC_Files/exflash_dimc.d \
./DIMC_Files/fault_management_dimc.d \
./DIMC_Files/flash_integrity_test.d \
./DIMC_Files/gpio_control_dimc.d \
./DIMC_Files/irs_protocol_dimc.d \
./DIMC_Files/logger_dimc.d \
./DIMC_Files/mcu_config_dimc.d \
./DIMC_Files/module_init_dimc.d \
./DIMC_Files/pbit_dimc.d \
./DIMC_Files/periodic_dimc.d \
./DIMC_Files/system_params_dimc.d \
./DIMC_Files/voltage_meas_dimc.d \
./DIMC_Files/wdog.d 


# Each subdirectory must supply rules for building sources it contributes
DIMC_Files/%.o: ../DIMC_Files/%.c
	arm-atollic-eabi-gcc -c "$<" -mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16 -std=gnu11 -D__packed=__attribute__((__packed__)) -D_TEST_BENCH_CONFIGURATION_ -D_WDOG_TEST_DISABLE_ -D_FLASH_TEST_DISABLE_ -D__weak=__attribute__((__weak__)) -DSTM32F427xx -I../Inc -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\Test_Bench_Config" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\Atollic_STM32F427_DIMC\DIMC_Files\00_Includes" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\02_STM_Drivers\Inc" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\01_BSP\03_UART" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\02_CLI\00_CLI_ENGINE" -I"C:\Projects\AIF_IPS_GHTP\IPS_STM32F4\01_Common_Debug\04_DbgPrint" -O0 -g3 -fstack-usage -Wall -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -specs=nano.specs -o "$@"

