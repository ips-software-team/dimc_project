/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : main.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 26-08-2021
**
** Description: This file provides the application entry point
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2 	: 25-04-2021 -- Update due to LDRA remarks.
** Rev 1.3 	: 26-08-2021 -- Update due to LDRA remarks.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "main.h"
#include "module_init_dimc.h"
#include "periodic_dimc.h"
#include "gipsy_hal_def.h"



/* -----Definitions ----------------------------------------------------------------*/
/*!< Uncomment the following line if you need to relocate your vector Table in
     Internal SRAM. */
/* #define VECT_TAB_SRAM */
#define VECT_TAB_OFFSET  0x00 /*!< Vector Table base offset field.
                                   This value must be a multiple of 0x200. */
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
**
** main - The application entry point
**
**
** Params : None.
**
** Returns: int
** Notes:
**
*************************************************************************************/

int main(void)
{
  	DIMC_Module_Init();

	while (TRUE)
	{
		Periodic_Tasks_Scheduler();
	}

	return 0;
}


/*************************************************************************************
  ** SystemInit - Setup the microcontroller system: Initialize the FPU setting,
  **              vector table location and External memory configuration.
  **
  ** Params : None
  **
  ** Returns: None
  ** Notes:
  *************************************************************************************/
  void SystemInit(void)
  {
	  /* FPU settings ------------------------------------------------------------*/
	  SCB->CPACR |= (uint32_t)((3UL << (10UL*2UL))|(3UL << (11UL*2UL)));  /* set CP10 and CP11 Full Access */

	  /* Reset the RCC clock configuration to the default reset state ------------*/
	  /* Set HSION bit */
	  RCC->CR |= (uint32_t)0x00000001U;

	  /* Reset CFGR register */
	  RCC->CFGR = 0x00000000U;

	  /* Reset HSEON, CSSON and PLLON bits */
	  RCC->CR &= (uint32_t)0xFEF6FFFFU;

	  /* Reset PLLCFGR register */
	  RCC->PLLCFGR = 0x24003010U;

	  /* Reset HSEBYP bit */
	  RCC->CR &= (uint32_t)0xFFFBFFFFU;

	  /* Disable all interrupts */
	  RCC->CIR = 0x00000000U;

	  /* Configure the Vector Table location add offset address ------------------*/
	  SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */

  }

/*************************************************************************************
**
** Error_Handler - This function is executed in case of fatal error occurrence
**
**
** Params : None.
**
** Returns: None.
** Notes:
**
*************************************************************************************/

void Error_Handler(void)
{

  while(TRUE)
  {
	  __NOP();
  }

}



/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
