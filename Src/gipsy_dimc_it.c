/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gipsy_dimc_it.c
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 03-01-2021
**
** Description: This file provides the Interrupt Service Routines handlers
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
**
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/


#include "gipsy_dimc_it.h"
#include "gipsy_hal_rcc.h"
#include "gipsy_hal_can.h"
#include "mcu_config_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
**
** SysTick_Handler - This function handles System tick timer
**
**
** Params : None.
**
** Returns: None.
** Notes:
**
*************************************************************************************/
void SysTick_Handler(void)
{
  HAL_IncTick();
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Interrupt Handlers for the used peripherals.                               */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/


#ifdef _DEBUG_
/*************************************************************************************
**
** USART3_IRQHandler - This function handles USART3 global interrupt
**
**
** Params : None.
**
** Returns: None.
** Notes:
**
*************************************************************************************/
void USART3_IRQHandler(void)
{
  HAL_UART_IRQHandler(&g_HandleUart3Debug);
}
#endif
/*************************************************************************************
**
** USART1_IRQHandler - This function handles USART1 global interrupt
**
**
** Params : None.
**
** Returns: None.
** Notes:
**
*************************************************************************************/
void USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(&g_HandleUart1Irs);
}
/*************************************************************************************
**
** CAN1_RX0_IRQHandler - This function handles CAN1 RX0 interrupt request
**
**
** Params : None.
**
** Returns: None.
** Notes:
**
*************************************************************************************/
void CAN1_RX0_IRQHandler(void)
{
   HAL_CAN_IRQHandler();
}


/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE****/
