/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : logger_dimc.c
** Author    : Omer Geron
** Revision  : 1.3
** Updated   : 11-07-2021
**
** Description:	This file contains functions for writing and reading system's events in the data logger, as well as erasing the data logger.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 23-08-2020 -- Original version created.
** Rev 1.1 	: 18-10-2020 -- Logger erase is possible only in STBY mode.
** Rev 1.2	: 25-04-2021 -- Update due to LDRA remarks.
** Rev 1.3	: 11-07-2021 -- Added doxygen comments.
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "logger_dimc.h"
#include "exflash_dimc.h"
#include "gipsy_hal_rtc.h"
#include "irs_protocol_dimc.h"
#include "module_init_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define SIZE_OF_EVENT_IN_BYTES				16U 		/* 16 bytes */
#define NUMBER_OF_EVENTS_TO_READ			10U			/* Number of events to read */
#define SIZE_OF_EVENTS_BUFFER_IN_BYTES		160U		/* Number of bytes to read = 16*10 */
#define MAX_EVENT_INDEX						0x3E75U		/* 15,989 Max event index */


/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*!Specifies logger's status(empty/not empty) (Range: 0 - 1 | Units: None)*/
/*!\sa Set function: Init_Mem_Hal(), Proccess_Logger_Request_from_IRS(), Write_Event_To_Memory()*/
/*!\sa Get function: Get_Logger_Status()*/
uint8_t g_Logger_Status = LOGGER_EMPTY;

uint8_t Logger_Erase_Flag = FALSE;
ReadFromLogger_t StReadFromLogger = {.read_data = FALSE, .read_from_event = 0U};
uint8_t Event_buffer[SIZE_OF_EVENTS_BUFFER_IN_BYTES] = {0U};

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static void parse_event_to_bytes(eventTypeDef event, uint8_t *byte_buffer);

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Set_Logger_Erase_Flag -
** 		- Set logger erase flag to TRUE.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Set_Logger_Erase_Flag(void)
{
	Logger_Erase_Flag = TRUE;
}

/*************************************************************************************
** Get_Logger_Status -
** 		- Get logger status (empty or not_empty)
**
** Param : 	None.
**
** Returns: uint8_t logger status.
** Notes:
*************************************************************************************/
uint8_t Get_Logger_Status(void)
{
	return g_Logger_Status;
}

/*************************************************************************************
** Set_Read_Request -
** 		- Set read request by IRS.
**
** Param : 	uint8_t read_data - TRUE/FALSE
** 			uint16_t read_from_event - Index of the event to read from.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Set_Read_Request(uint8_t read_data, uint16_t read_from_event)
{
	StReadFromLogger.read_data = read_data;
	StReadFromLogger.read_from_event = read_from_event;
}

/*************************************************************************************
** Write_Event_To_Memory-
** 		- Receive event parameters and build an event (event struct defined in logger.h file)
** 		- Generate a time stamp for the event.
** 		- Parse the event struct into a byte buffer.
** 		- Write the event to memory.
** 		- Return operation result (write succeed / failed).
**
** Param : 	1. e_Event_Types event_type		-	event type.
** 			2. e_EVENT_CODE event_code.		- 	event code.
**			3. uint16_t fault_value.		-	fault value.
** 			4. e_UNIT_TYPES unit_type.		- 	unit type.
** 			5. uint8_t unit_id.				-	unit id.
** 			6. uint8_t *spare_bytes.		- 	array of 3 bytes.
**
** Returns: HAL_StatusTypeDef - operation result.
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Write_Event_To_Memory(e_EVENT_TYPES event_type, e_EVENT_CODE event_code, uint16_t fault_value, e_UNIT_TYPES unit_type, uint8_t unit_id, uint8_t *spare_bytes)
{
	/* Init local result var */
	HAL_StatusTypeDef eeprom_operation_result = HAL_OK;

	/* Init local vars */
	eventTypeDef new_event = {0U};
	uint8_t byte_buffer[SIZE_OF_EVENT_IN_BYTES] = {0U};
	RTC_TimeDate_t event_time_date = {0U};

	/* Set event time stamp */
	RTC_GetDateTime(&event_time_date);
	new_event.time_stamp[0U] = event_time_date.Day;
	new_event.time_stamp[1U] = event_time_date.Month;
	new_event.time_stamp[2U] = event_time_date.Year;
	new_event.time_stamp[3U] = event_time_date.Hours;
	new_event.time_stamp[4U] = event_time_date.Minutes;
	new_event.time_stamp[5U] = event_time_date.Seconds;

	/* Set event data fields */
	new_event.type_of_event 	= 	event_type;
	new_event.code_of_event 	= 	event_code;
	new_event.fault_value 		= 	fault_value;
	new_event.type_of_unit 		= 	unit_type;
	new_event.unit_id 			= 	unit_id;

	/* Set spare space data */
	new_event.spare[0U]		= 	spare_bytes[0U];
	new_event.spare[1U]		= 	spare_bytes[1U];
	new_event.spare[2U]		= 	spare_bytes[2U];

	/* Parse event to byte buffer */
	parse_event_to_bytes(new_event, byte_buffer);

	/* Write to EEPROM */
	eeprom_operation_result = Write_EEPROM(byte_buffer,SIZE_OF_EVENT_IN_BYTES);

	if(HAL_OK == eeprom_operation_result)
	{
		g_Logger_Status = LOGGER_NOT_EMPTY;
	}
	return eeprom_operation_result;
}

/*************************************************************************************
** Read_Events_From_Memory -
** 		- Read 10 events from a given event index in memory (see example below).
** 		- Return operation result (read succeed / failed).
**
** Param : 	uint16_t from_event_index		- index of event to start reading from.
**
** Returns: HAL_StatusTypeDef read_result 	- operation result.
**
** Notes:
** 		Event index start from 0.
**
** 		Example: Read_Events_From_Memory(3) -> 	Read 10 events from the 3rd event in memory,
**
*************************************************************************************/
HAL_StatusTypeDef Read_Events_From_Memory (uint16_t from_event_index)
{
	/* Init local variables */
	uint32_t address = 0U;
	HAL_StatusTypeDef eeprom_operation_result = {0};

	/* from_event_index is a valid value */
	if((MAX_EVENT_INDEX >= from_event_index) || (READ_FROM_LAST_EVENT == from_event_index))
	{
		/* Parse event index to address */
		if(MAX_NUMBER_OF_EVENTS >= from_event_index)
		{
			address = (NUMBER_OF_EVENTS_TO_READ + from_event_index) * SIZE_OF_EVENT_IN_BYTES;
		}
		else
		{
			address = READ_FROM_TOP_OF_MEMORY;
		}

		/* Read data from EEPROM to byte buffer */
		eeprom_operation_result = Read_EEPROM(Event_buffer, SIZE_OF_EVENTS_BUFFER_IN_BYTES, address);
	}

	/* The value of from_event_index is out of range */
	else
	{
		eeprom_operation_result = HAL_ERROR;
	}

	return eeprom_operation_result;
}

/*************************************************************************************
** Erase_Memory -
**			- Set "number_of_events_in_memory" to zero.
**			- Calls Erase_EEPROM function that reset the internal state of the EEPROM.
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Erase_Memory(void)
{
	return Erase_EEPROM();
}

/*************************************************************************************
** parse_event_to_bytes -
** 		- Parse event struct into byte buffer.
**
** Param : 	1. eventTypeDef event 	- The event that will be parsed to bytes array.
** 			2. uint8_t *byte_buffer - Pointer to the byte_buffer that will hold the data of the event.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void parse_event_to_bytes(eventTypeDef event, uint8_t *byte_buffer)
{
	/* Parse time stamp */
	for (uint8_t i = 0U ; i < 6U ; i++)
	{
		byte_buffer[i] = event.time_stamp[i];
	}

	/* Parse uint16_t event code into two uint8_t values */
	byte_buffer[7U] 		= 	(uint8_t)(event.code_of_event & 0x00FFU);					/* LSB */
	byte_buffer[8U] 		= 	(uint8_t)(((uint16_t)event.code_of_event >> 8U) & 0x00FFU);	/* MSB */

	/* Parse uint16_t fault value into two uint8_t values */
	byte_buffer[9U] 		= 	(uint8_t)(event.fault_value & 0x00FFU);					/* LSB */
	event.fault_value 		>>= 8U;
	byte_buffer[10U] 		= 	(uint8_t)(event.fault_value & 0x00FFU);					/* MSB */

	/* Parse the rest of the event fields to bytes */
	byte_buffer[6U] 	= 	event.type_of_event;
	byte_buffer[11U] 	= 	event.type_of_unit;
	byte_buffer[12U] 	= 	event.unit_id;
	byte_buffer[13U]	=	event.spare[0U];
	byte_buffer[14U]	=	event.spare[1U];
	byte_buffer[15U]	=	event.spare[2U];
}

/*************************************************************************************
** Proccess_Logger_Request_from_IRS -
** 		- Process logger request from IRS.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Proccess_Logger_Request_from_IRS(void)
{
	/* init local variable */
	HAL_StatusTypeDef eeprom_operation_result = {0U};

	/* Perform Logger erasure after getting request via IRS communication port */
	/*Rev 1.1 - logger erase is possible only in STBY mode */
	if((TRUE == Logger_Erase_Flag) && (E_STBY_MODE == g_SysStatus.Sys_Mode))
	{
		eeprom_operation_result = Erase_Memory();
		if (HAL_OK == eeprom_operation_result)
		{
			uint8_t spare_data[3] = {0U};
			eeprom_operation_result = Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_ERASE_LOGGER_IRS_COMMAND_RECEIVED, 0x00U, E_UNIT_TYPE_IPS, 0X01, spare_data);
			if (HAL_OK == eeprom_operation_result)
			{
				g_Logger_Status = LOGGER_EMPTY;
				Logger_Erase_Flag = FALSE;
				IRS_Send_Msg_Maintenance_Report();
			}
		}
	}

	/* Read data from logger and send it via IRS communication port */
	if(	(TRUE == StReadFromLogger.read_data) &&\
		((MAX_EVENT_INDEX >= StReadFromLogger.read_from_event) || (READ_FROM_LAST_EVENT == StReadFromLogger.read_from_event )))
	{
		/* Read data from logger to array */
		eeprom_operation_result = Read_Events_From_Memory(StReadFromLogger.read_from_event);
		if(HAL_OK == eeprom_operation_result)
		{
			/* Pass array pointer and array size to IRS message combine procedure */
			IRS_Send_Msg_Log_Data_Report(Event_buffer, SIZE_OF_EVENTS_BUFFER_IN_BYTES);
		}

		/* Set read_data back to FALSE */
		StReadFromLogger.read_data = FALSE;
	}
	else
	{
		StReadFromLogger.read_data = FALSE;
		StReadFromLogger.read_from_event = 0U;
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
