/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : mcu_config_dimc.c
** Author    : Leonid Savchenko
** Revision  : 1.5
** Updated   : 11-07-2021
**
** Description: This file contains procedures for setup and initialization of used MCU's Pins,
** 		        clock and internal peripherals
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
**
** Rev 1.1	: 02-09-2020 -- HAL_StatusTypeDef can_status variable added in Board_Periph_Config(void) function.
** 						- Get can_status variable in Board_Periph_Config(void) function.
** 						- set can_status in system_status variable in Board_Periph_Config(void) function.
**
** Rev 1.2	: 17-09-2020 -- The loopback test is updated directly in g_SystemStatus struct.
** 						- No need Set & Get can_status (Rev 1.1 can be ignored).
** Rev 1.3	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.4  : 01-07-2021 -- Updated due to LDRA remarks.
** Rev 1.5  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* -----Includes -------------------------------------------------------------------*/
#include "mcu_config_dimc.h"

#include "irs_protocol_dimc.h"
#include "exflash_dimc.h"
#include "gipsy_hal_cortex.h"
#include "gipsy_hal_adc.h"
#include "gipsy_hal_can.h"
#include "gipsy_hal_uart.h"
#include "gipsy_hal_rcc.h"
#include "wdog.h"
#ifdef _DEBUG_
#include "uart.h"
#endif
/* -----Definitions ----------------------------------------------------------------*/
#define PWR_REGULATOR_VOLTAGE_SCALE1         PWR_CR_VOS             /* Scale 1 mode(default value at reset): the maximum value of fHCLK is 168 MHz.*/

/********************  Bit definition for PWR_CR register  ********************/
#define PWR_CR_VOS_POS         (14U)
#define PWR_CR_VOS_MSK         (0x3UL << PWR_CR_VOS_POS)                        /*!< 0x0000C000 */
#define PWR_CR_VOS             PWR_CR_VOS_MSK                                  /*!< VOS[1:0] bits (Regulator voltage scaling output selection) */

/*I2C Buses definitions*/
#define I2C_DUTYCYCLE_2   			0x00000000U
#define I2C_OWNADDRESS1    			0x00000000U
#define I2C_ADDRESSINGMODE_7BIT  	0x00004000U
#define I2C_DUALADDRESS_DISABLE  	0x00000000U
#define I2C_OWNADDRESS2    			0x00000000U
#define I2C_GENERALCALL_DISABLE 	0x00000000U
#define I2C_NOSTRETCH_DISABLE 		0x00000000U

#define I2C_2_SPEED      400000U  /*400Khz*/

#define I2C_OWNADDRESS1_EXFLASH    			0x0000000AU
#define I2C_OWNADDRESS2_EXFLASH     		0x000000FEU

#define CAN1_IT_PREEMPT_PRIORITY  5U
#define CAN1_IT_SUB_PRIORITY      0U

/*USART Definitions*/

#define UART_WORDLENGTH_9B  	0x00001000U
#define UART_STOPBITS_1     	0x00000000U
#define UART_PARITY_ODD         (0x00000400U | 0x00000200U)
#define UART_MODE_TX_RX         (0x00000008U | 0x00000004U)
#define UART_HWCONTROL_NONE		0x00000000U
#define UART_OVERSAMPLING_16	0x00000000U


/********************  Bit definition for RCC_AHB1ENR register  ***************/
#define RCC_AHB1ENR_GPIOAEN_POS            (0U)
#define RCC_AHB1ENR_GPIOAEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOAEN_POS)   /*!< 0x00000001 */
#define RCC_AHB1ENR_GPIOAEN                RCC_AHB1ENR_GPIOAEN_MSK
#define RCC_AHB1ENR_GPIOBEN_POS            (1U)
#define RCC_AHB1ENR_GPIOBEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOBEN_POS)   /*!< 0x00000002 */
#define RCC_AHB1ENR_GPIOBEN                RCC_AHB1ENR_GPIOBEN_MSK
#define RCC_AHB1ENR_GPIOCEN_POS            (2U)
#define RCC_AHB1ENR_GPIOCEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOCEN_POS)   /*!< 0x00000004 */
#define RCC_AHB1ENR_GPIOCEN                RCC_AHB1ENR_GPIOCEN_MSK
#define RCC_AHB1ENR_GPIODEN_POS            (3U)
#define RCC_AHB1ENR_GPIODEN_MSK            (0x1UL << RCC_AHB1ENR_GPIODEN_POS)   /*!< 0x00000008 */
#define RCC_AHB1ENR_GPIODEN                RCC_AHB1ENR_GPIODEN_MSK
#define RCC_AHB1ENR_GPIOEEN_POS            (4U)
#define RCC_AHB1ENR_GPIOEEN_MSK            (0x1UL << RCC_AHB1ENR_GPIOEEN_POS)   /*!< 0x00000010 */
#define RCC_AHB1ENR_GPIOEEN                RCC_AHB1ENR_GPIOEEN_MSK
#define RCC_AHB1ENR_CRCEN_POS              (12U)
#define RCC_AHB1ENR_CRCEN_MSK              (0x1UL << RCC_AHB1ENR_CRCEN_POS)     /*!< 0x00001000 */
#define RCC_AHB1ENR_CRCEN                  RCC_AHB1ENR_CRCEN_MSK

/********************  Bit definition for RCC_APB1ENR register  ***************/
#define RCC_APB1ENR_TIM3EN_POS             (1U)
#define RCC_APB1ENR_TIM3EN_MSK             (0x1UL << RCC_APB1ENR_TIM3EN_POS)    /*!< 0x00000002 */
#define RCC_APB1ENR_TIM3EN                 RCC_APB1ENR_TIM3EN_MSK
#define RCC_APB1ENR_TIM4EN_POS             (2U)
#define RCC_APB1ENR_TIM4EN_MSK             (0x1UL << RCC_APB1ENR_TIM4EN_POS)    /*!< 0x00000004 */
#define RCC_APB1ENR_TIM4EN                 RCC_APB1ENR_TIM4EN_MSK
#define RCC_APB1ENR_USART3EN_POS           (18U)
#define RCC_APB1ENR_USART3EN_MSK           (0x1UL << RCC_APB1ENR_USART3EN_POS)  /*!< 0x00040000 */
#define RCC_APB1ENR_USART3EN               RCC_APB1ENR_USART3EN_MSK
#define RCC_APB1ENR_I2C1EN_POS             (21U)
#define RCC_APB1ENR_I2C1EN_MSK             (0x1UL << RCC_APB1ENR_I2C1EN_POS)    /*!< 0x00200000 */
#define RCC_APB1ENR_I2C1EN                 RCC_APB1ENR_I2C1EN_MSK
#define RCC_APB1ENR_I2C2EN_POS             (22U)
#define RCC_APB1ENR_I2C2EN_MSK             (0x1UL << RCC_APB1ENR_I2C2EN_POS)    /*!< 0x00400000 */
#define RCC_APB1ENR_I2C2EN                 RCC_APB1ENR_I2C2EN_MSK
#define RCC_APB1ENR_I2C3EN_POS             (23U)
#define RCC_APB1ENR_I2C3EN_MSK             (0x1UL << RCC_APB1ENR_I2C3EN_POS)    /*!< 0x00800000 */
#define RCC_APB1ENR_I2C3EN                 RCC_APB1ENR_I2C3EN_MSK
#define RCC_APB1ENR_CAN1EN_POS             (25U)
#define RCC_APB1ENR_CAN1EN_MSK             (0x1UL << RCC_APB1ENR_CAN1EN_POS)    /*!< 0x02000000 */
#define RCC_APB1ENR_CAN1EN                 RCC_APB1ENR_CAN1EN_MSK
#define RCC_APB1ENR_PWREN_POS              (28U)
#define RCC_APB1ENR_PWREN_MSK              (0x1UL << RCC_APB1ENR_PWREN_POS)     /*!< 0x10000000 */
#define RCC_APB1ENR_PWREN                  RCC_APB1ENR_PWREN_MSK

/********************  Bit definition for RCC_APB2ENR register  ***************/
#define RCC_APB2ENR_TIM1EN_POS             (0U)
#define RCC_APB2ENR_TIM1EN_MSK             (0x1UL << RCC_APB2ENR_TIM1EN_POS)    /*!< 0x00000001 */
#define RCC_APB2ENR_TIM1EN                 RCC_APB2ENR_TIM1EN_MSK
#define RCC_APB2ENR_USART1EN_POS           (4U)
#define RCC_APB2ENR_USART1EN_MSK           (0x1UL << RCC_APB2ENR_USART1EN_POS)  /*!< 0x00000010 */
#define RCC_APB2ENR_USART1EN               RCC_APB2ENR_USART1EN_MSK
#define RCC_APB2ENR_ADC1EN_POS             (8U)
#define RCC_APB2ENR_ADC1EN_MSK             (0x1UL << RCC_APB2ENR_ADC1EN_POS)    /*!< 0x00000100 */
#define RCC_APB2ENR_ADC1EN                 RCC_APB2ENR_ADC1EN_MSK
/* -----Macros ---------------------------------------------------------------------*/
/* @brief  macros configure the main internal regulator output voltage.
  * @param  _REGULATOR_ specifies the regulator output voltage to achieve
  *         a tradeoff between performance and power consumption when the device does
  *         not operate at the maximum frequency (refer to the datasheets for more details).
  *          This parameter can be one of the following values:
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE1: Regulator voltage output Scale 1 mode
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE2: Regulator voltage output Scale 2 mode
  *            @arg PWR_REGULATOR_VOLTAGE_SCALE3: Regulator voltage output Scale 3 mode
  * @retval None
  */
#define HAL_PWR_VOLTAGESCALING_CONFIG(_REGULATOR_) do {                                                     \
                                                            __IO uint32_t tmpreg = 0x00U;                        \
                                                            MODIFY_REG(PWR->CR, PWR_CR_VOS, (_REGULATOR_));   \
                                                            /* Delay after an RCC peripheral clock enabling */  \
                                                            tmpreg = READ_BIT(PWR->CR, PWR_CR_VOS);             \
                                                          } while(FALSE)

#define HAL_RCC_PWR_CLK_ENABLE()     do { \
                                        __IO uint32_t tmpreg = 0x00U; \
                                        SET_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);\
                                        /* Delay after an RCC peripheral clock enabling */ \
                                        tmpreg = READ_BIT(RCC->APB1ENR, RCC_APB1ENR_PWREN);\
                                          } while(FALSE)


/*Enable the FLASH data cache*/
#define HAL_FLASH_DATA_CACHE_ENABLE()  (FLASH->ACR |= FLASH_ACR_DCEN)

/*Enable the FLASH instruction cache*/
#define HAL_FLASH_INSTRUCTION_CACHE_ENABLE()  (FLASH->ACR |= FLASH_ACR_ICEN)

/*Enable the FLASH prefetch buffer*/
#define HAL_FLASH_PREFETCH_BUFFER_ENABLE()  (FLASH->ACR |= FLASH_ACR_PRFTEN)

/* -----Global variables -----------------------------------------------------------*/

/*Global Peripherals handlers*/
/*!Specifies I2C_2 bus parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: exflash_i2c_init(),Read_EEPROM(), read_mem_hal_status_from_EEPROM(), Write_EEPROM(), write_mem_hal_status_to_EEPROM()*/
/*!\sa Get function: exflash_i2c_init(),Read_EEPROM(), read_mem_hal_status_from_EEPROM(), Write_EEPROM(), write_mem_hal_status_to_EEPROM()*/
I2C_HandleTypeDef     g_HandleI2C2_ExFlash = {0U};

/*!Specifies USART1 bus parameters (Range: var type | Units: see var declaration)*/
/*!\sa Set function: irs_comm_usart1_init(), irs_message_transmit(), IRS_UART1_Serial_Init(), USART1_IRQHandler*/
/*!\sa Get function: irs_comm_usart1_init(), irs_message_transmit(), IRS_UART1_Serial_Init(), USART1_IRQHandler*/
UART_HandleTypeDef    g_HandleUart1Irs = {0U};


#ifdef _DEBUG_
UART_HandleTypeDef    g_HandleUart3Debug;
#endif


/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static void system_clock_config(void);
static void irs_comm_usart1_init(void);
static void exflash_i2c_init(void);
static void mcu_gpio_pins_init_dimc(void);


#ifdef _DEBUG_
static void debug_uart_usart3_init(void);
#endif
/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** MCU_Periph_Config - Initialization of MCUs peripherals
** This function initializes and configures  MCUs peripherals
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void MCU_Periph_Config(void)
{
	/* HAL_INIT - Reset of all peripherals, Initializes the Flash interface. */
	/* INSTRUCTION_CACHE_ENABLE */
	HAL_FLASH_INSTRUCTION_CACHE_ENABLE();

	/* DATA_CACHE_ENABLE */
	HAL_FLASH_DATA_CACHE_ENABLE();

	/* PREFETCH_ENABLE */
	HAL_FLASH_PREFETCH_BUFFER_ENABLE();

	/* Set Interrupt Group Priority */
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	/*HAL Init Ends*/

	system_clock_config(); /* Configure the system clock */



	/* Initialize all configured peripherals */
	mcu_gpio_pins_init_dimc();
	exflash_i2c_init();
	HAL_ADC_Init();
	irs_comm_usart1_init();
	IRS_UART1_Serial_Init();

#ifdef _DEBUG_
	debug_uart_usart3_init();
	UART3_Serial_Init();
#endif
}


/*************************************************************************************
** system_clock_config - System Clock Configuration
** 						 Configures and enables internal MCU's clock mechanism
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
static void system_clock_config(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Configure the main internal regulator output voltage*/
	HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/*Initialization of the oscillators */
	hal_status |= HAL_RCC_OscConfig();

	/*Initialization of the CPU, AHB and APB busses clocks*/
	hal_status |= HAL_RCC_ClockConfig();

	/*Initialization of RTC bus clock*/
	hal_status |= HAL_RCCEx_PeriphCLKConfig();

	/*In case of clocks initialization error -> Enter infinite loop */
	if(HAL_OK != hal_status)
	{
		Error_Handler();
	}

	/*Initialization of Peripherals clocks*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOAEN);/*Enable clock for GPIO_A port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOBEN);/*Enable clock for GPIO_B port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOCEN);/*Enable clock for GPIO_C port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIODEN);/*Enable clock for GPIO_D port*/
	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_GPIOEEN);/*Enable clock for GPIO_E port*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C2EN);/*Enable clock for I2C_2 bus*/
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_CAN1EN);/*Enable clock for CAN1 bus*/
	SET_BIT(RCC->APB2ENR, RCC_APB2ENR_ADC1EN);/*Enable clock for ADC1*/

	SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN);/*Enable clock for UART_1 bus*/

	SET_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN);/*Enable CRC HW peripheral*/

#ifdef _DEBUG_
	SET_BIT(RCC->APB1ENR, RCC_APB1ENR_USART3EN);/*Enable clock for UART_3 bus*/
#endif

}

/*************************************************************************************
** MCU_CAN_Interface_Init - Initialization and configuration of CAN1 communication interface
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef MCU_CAN_Interface_Init(void)
{
	 /* NVIC configuration for CAN1 Reception complete interrupt */
	 HAL_NVIC_SetPriority(CAN1_RX0_IRQn, CAN1_IT_PREEMPT_PRIORITY, CAN1_IT_SUB_PRIORITY);
	 HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);

	 /*CAN peripheral :Init, Start, Enable interrupts and perform LoopBack Test*/
	 return HAL_CAN_Init();
}


/*************************************************************************************
** exflash_i2c_init - Initialization and configuration of I2C bus communication interface
**                    for interfacing External Flash data logger
**
** Params : None.
**
** Returns: None
** Notes:
*************************************************************************************/
static void exflash_i2c_init(void)
{
	g_HandleI2C2_ExFlash.Instance =             I2C2;
	g_HandleI2C2_ExFlash.Init.ClockSpeed =      I2C_2_SPEED; /*400KHz*/
	g_HandleI2C2_ExFlash.Init.DutyCycle =       I2C_DUTYCYCLE_2;
	g_HandleI2C2_ExFlash.Init.OwnAddress1 =     I2C_OWNADDRESS1_EXFLASH;
	g_HandleI2C2_ExFlash.Init.AddressingMode =  I2C_ADDRESSINGMODE_7BIT;
	g_HandleI2C2_ExFlash.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
	g_HandleI2C2_ExFlash.Init.OwnAddress2 =     I2C_OWNADDRESS2_EXFLASH;
	g_HandleI2C2_ExFlash.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
	g_HandleI2C2_ExFlash.Init.NoStretchMode =   I2C_NOSTRETCH_DISABLE;

	/*Initialize I2C2 peripheral*/
	HAL_I2C_Init(&g_HandleI2C2_ExFlash);
}

/*************************************************************************************
** irs_comm_usart1_init - Initialization and configuration of USART1 bus communication
**                        interface for RS-422 communication interface
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void irs_comm_usart1_init(void)
{
	g_HandleUart1Irs.Instance = USART1;
	g_HandleUart1Irs.Init.BaudRate = 38400U;  /*IRS communication speed*/
	g_HandleUart1Irs.Init.WordLength = UART_WORDLENGTH_9B; /*8 bits of data + odd parity*/
	g_HandleUart1Irs.Init.StopBits = UART_STOPBITS_1;
	g_HandleUart1Irs.Init.Parity = UART_PARITY_ODD;
	g_HandleUart1Irs.Init.Mode = UART_MODE_TX_RX;
	g_HandleUart1Irs.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	g_HandleUart1Irs.Init.OverSampling = UART_OVERSAMPLING_16;



	/* USART1 interrupt Init */
	HAL_NVIC_SetPriority(USART1_IRQn, 3U, 0U);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	HAL_UART_Init(&g_HandleUart1Irs);
}


/*************************************************************************************
** mcu_gpio_pins_init_dimc - Initialization and configuration of GPIO's
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void mcu_gpio_pins_init_dimc(void)
{
	GPIO_InitTypeDef gpio_init_struct = {0U};
	HAL_StatusTypeDef hal_status = HAL_OK;

	/**USART1 GPIO Configuration
	    PA9     ------> RS422_TX0_Pin
	    Pa10    ------> RS422_RX0_Pin
	 */
	gpio_init_struct.Pin = RS422_TX0_PIN|RS422_RX0_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF7_USART1;
	hal_status |= HAL_GPIO_Init(RS422_SER_PORT, &gpio_init_struct);

	/*RE/DE Output pins Initialization*/
	gpio_init_struct.Pin = RS422_DE0_PIN|RS422_RE0_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(RS422_SER_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(RS422_SER_PORT, RS422_DE0_PIN, E_GPIO_PIN_SET);
	HAL_GPIO_WritePin(RS422_SER_PORT, RS422_RE0_PIN, E_GPIO_PIN_RESET);

	/**I2C2 GPIO Configuration
	    PB10     ------> EX_FLASH_SCL_Pin
	    PB11     ------> EX_FLASH_SDA_Pin
	 */
	gpio_init_struct.Pin = EX_FLASH_SCL_PIN|EX_FLASH_SDA_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_OD;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF4_I2C2;
	hal_status |= HAL_GPIO_Init(EX_FLASH_PORT, &gpio_init_struct);

	/*ExFlash Write Protect Output Pin Initialization*/
	gpio_init_struct.Pin = EX_FLASH_WP_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(EX_FLASH_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(EX_FLASH_PORT, EX_FLASH_WP_PIN, E_GPIO_PIN_RESET);

	/*ADC1 GPIO Configuration*/
	/*PA4     ------> ADC1_IN4--->Voltage_5VDC*/
	gpio_init_struct.Pin =  V5VDCLVL_PIN;
	gpio_init_struct.Mode = GPIO_MODE_ANALOG;
	gpio_init_struct.Pull = GPIO_NOPULL;
	hal_status |= HAL_GPIO_Init(ADC1_PORT, &gpio_init_struct);

	/* CAN1 TX GPIO pin configuration */
	gpio_init_struct.Pin = TXCAN_1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Alternate =  GPIO_AF9_CAN1;
	hal_status |= HAL_GPIO_Init(CAN_PORT, &gpio_init_struct);

	/* CAN1 RX GPIO pin configuration */
	gpio_init_struct.Pin = RXCAN_1_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Alternate =  GPIO_AF9_CAN1;
	hal_status |= HAL_GPIO_Init(CAN_PORT, &gpio_init_struct);

	/*General Purpose Outputs Initialization------------------------*/

	/*Init Debug LEDs Outputs*/
	gpio_init_struct.Pin = SW_RUN_LED_PIN | FLT_LED_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(LED_PORT, &gpio_init_struct);
	/*Set GPIOs default state*/
	HAL_GPIO_WritePin(LED_PORT, SW_RUN_LED_PIN | FLT_LED_PIN, E_GPIO_PIN_SET);


	/*BIT Operate Output Pins Initialization*/
	gpio_init_struct.Pin = CPU_BIT_OPERATE_1_PIN|CPU_BIT_OPERATE_2_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(BIT_OPERATE_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(BIT_OPERATE_PORT, CPU_BIT_OPERATE_1_PIN|CPU_BIT_OPERATE_2_PIN, E_GPIO_PIN_SET);

	/*Cut Of Power Output Pins Initialization*/
	gpio_init_struct.Pin = COP_TTL_1_PIN|COP_TTL_2_PIN|COP_TTL_3_PIN|COP_TTL_4_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(COP_TTL_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(COP_TTL_PORT, COP_TTL_1_PIN|COP_TTL_2_PIN|COP_TTL_3_PIN|COP_TTL_4_PIN, E_GPIO_PIN_RESET);

	/*Fault Channels Output Pins Initialization*/
	gpio_init_struct.Pin = CHANNEL_A_FAULT_TTL_PIN|CHANNEL_C_FAULT_TTL_PIN|CHANNEL_B_FAULT_TTL_1_PIN|CHANNEL_B_FAULT_TTL_2_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CHANNEL_FAULT_TTL_OUT_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(CHANNEL_FAULT_TTL_OUT_PORT, CHANNEL_A_FAULT_TTL_PIN|CHANNEL_C_FAULT_TTL_PIN|CHANNEL_B_FAULT_TTL_1_PIN|CHANNEL_B_FAULT_TTL_2_PIN, E_GPIO_PIN_RESET);


	/*External Watch-Dog strobe Output Pin Initialization*/
	gpio_init_struct.Pin = WDI_PIN;
	gpio_init_struct.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(WDI_PORT, &gpio_init_struct);
	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(WDI_PORT, WDI_PIN, E_GPIO_PIN_RESET);


	/*General Purpose Inputs Initialization------------------------*/

	/*Fault Channels Input Pins Initialization*/
	gpio_init_struct.Pin = CHANNEL_A_FAULT_TTL_1_PIN|CHANNEL_A_FAULT_TTL_2_PIN|CHANNEL_A_FAULT_TTL_3_PIN|CHANNEL_A_FAULT_TTL_4_PIN|\
			CHANNEL_C_FAULT_TTL_1_PIN|CHANNEL_C_FAULT_TTL_2_PIN|CHANNEL_C_FAULT_TTL_3_PIN|CHANNEL_C_FAULT_TTL_4_PIN|\
			CHANNEL_C_FAULT_TTL_5_PIN|CHANNEL_C_FAULT_TTL_6_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(CHANNEL_FAULT_TTL_IN_PORT, &gpio_init_struct);

	/*OPER Input Pins Initialization*/
	gpio_init_struct.Pin = OPER_TTL_1_PIN|OPER_TTL_2_PIN|OPER_1_2_TTL_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_NOPULL;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(OPER_TTL_PORT, &gpio_init_struct);

	/*IBIT Input Pin Initialization*/
	gpio_init_struct.Pin = IBIT_TTL_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(IBIT_TTL_PORT, &gpio_init_struct);

	/*Fault Sense Input Pins Initialization*/
	gpio_init_struct.Pin = FAIL_A_SENS_TTL_PIN|FAIL_B_SENS_TTL_PIN|FAIL_C_SENS_TTL_PIN;
	gpio_init_struct.Mode = GPIO_MODE_INPUT;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_LOW;
	hal_status |= HAL_GPIO_Init(FAIL_SENS_TTL_PORT, &gpio_init_struct);

	/*In case of GPIO initialization error -> Enter infinite loop */
	if(HAL_OK != hal_status)
	{
		Error_Handler();
	}
}

#ifdef _DEBUG_
/*************************************************************************************
** debug_uart_usart3_init - Initialization and configuration of USART bus
**                          communication interface for DEBUG port interface
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void debug_uart_usart3_init(void)
{
	GPIO_InitTypeDef gpio_init_struct = {0U};
	HAL_StatusTypeDef hal_status = HAL_OK;

	g_HandleUart3Debug.Instance = USART3;
	g_HandleUart3Debug.Init.BaudRate = (uint32_t)115200U;
	g_HandleUart3Debug.Init.WordLength = UART_WORDLENGTH_9B;
	g_HandleUart3Debug.Init.StopBits = UART_STOPBITS_1;
	g_HandleUart3Debug.Init.Parity = UART_PARITY_ODD;
	g_HandleUart3Debug.Init.Mode = UART_MODE_TX_RX;
	g_HandleUart3Debug.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	g_HandleUart3Debug.Init.OverSampling = UART_OVERSAMPLING_16;

	/*Configuration of hardware resources used by USART3*/
	/**USART3 GPIO Configuration
	    PC10     ------> U3_TX_Pin
	    PC11     ------> U3_RX_Pin
	 */
	gpio_init_struct.Pin = U3_TX_PIN|U3_RX_PIN;
	gpio_init_struct.Mode = GPIO_MODE_AF_PP;
	gpio_init_struct.Pull = GPIO_PULLUP;
	gpio_init_struct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	gpio_init_struct.Alternate = GPIO_AF7_USART3;
	hal_status = HAL_GPIO_Init(DEBUG_UART_PORT, &gpio_init_struct);

	/* UART3 interrupt Init */
	HAL_NVIC_SetPriority(USART3_IRQn, 6U, 0U);
	HAL_NVIC_EnableIRQ(USART3_IRQn);
	HAL_UART_Init(&g_HandleUart3Debug);
}
#endif
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
