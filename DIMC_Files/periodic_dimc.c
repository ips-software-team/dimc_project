/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : periodic_dimc.c
** Author    : Omer Geron
** Revision  : 2.8
** Updated   : 28-05-2023
**
** Description: This file contains periodic scheduling procedure.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 24-11-2020 -- Original version created.
**
** Rev 1.1	: 29-11-2020 -- Fix  typo error ('==' -> '!=') in ibit function (marked in code)
**
** Rev 1.2	: 03-12-2020 -- Update actuator maintenance report struct variable name now: 'g_Maint_ActuatorData'  (there were 2 different variables for the maint' report).
** 					 	  Place get_ctrys_statusfunction in the periodic task scheduler.
** Rev 1.3	: 03-01-2021 -- Updated due to verification remarks.
**
** Rev 1.4	: 25-04-2021 -- Update Discretes monitoring function.
** 						  Update due to verification remarks.
** 						  Updates according to LDRA remarks.
**
** Rev 1.5	: 06-07-2021 -- Update "get_ctry_status" function.
** 						  Removed support for IRS safe mode entry request.
** 						  Update the actuators monitor function:
** 						  	- All CTRYs current cunsomption will be monitored all athe time to verify that only the required actuators are activated.
** 						  Update the opration mang and ibit activation function, no need to perform monitoring inside these functions.
** 						  Update the Timer and oper_handler structures.
** 						  Update the manual actuator activation function and ibit function.
** Rev 1.6  : 11-07-2021 -- Added doxygen comments
** Rev 1.7	: 16-08-2021 -- Update the function "monitor actuators".
** Rev 1.8	: 23-08-2021 -- Update function "get_ctrys_status" due to verification remarks.
** Rev 1.9	: 09-09-2021 -- Update comment in function "handle_irs_actuator_activation_command".
** Rev 2.0	: 05-10-2021 -- Integration updates.
** Rev 2.1	: 19-01-2022 -- IBIT limits update.
** Rev 2.2	: 30-01-2022 -- Update the function "operation_manager" - fixing PR: DIMC-29.
** 							Update the function "compute_ctry_temp_avrg" - fixing PR: DIMC-25.
**							Update the function "monitor_actuators".
** Rev 2.3	: 12-05-2022 -- Fixing IAI IBIT entry after operation - DIMC Ver: 01_10.
** 							Logging DIMC startup completed.
** Rev 2.4	: 25-05-2022 -- Update operation mode exit method due to IAI integration.
** Rev 2.5	: 17-08-2022 -- Sending the "All Actuators Off" command before entering OPER mode.
** 							Extend OPER enrty delay to 500ms.
** 							Update discrete monitor period to 10ms.
** 							Send "ALL ACTUATORS OFF" command when exiting OPER mode.
** 							Logging the first cycle of actuator activation in OPER mode.
** 							Change IBIT actuators activation command to "Manual Activation".
** Rev 2.6 : 25-08-2022 --  Extend actuators activation time to 2.8 seconds during IBIT mode.
** 							Extend actuators interavl time to 3.4 seconds during IBIT mode.
** 							Update 'monitor_actuators' function.
** Rev 2.7 : 07-05-2023 --  Extened OPER entry delay to 2000ms.
** 							Disable the idle CTRY current consumption monitoring.
** 							Removed changes of version 2.5:
** 								- removed the send "ALL ACTUATORS OFF" command when exiting OPER mode.
** 								- removed the send "ALL ACTUATORS OFF" command before entering OPER mode.
** 							Update actuators' monitor function.
** 							Updating code to improve code coverage results.
** Rev 2.8	: 28-05-2023 --	Update varaible name from 'act_de_activation_fl' to 'com_lost_during_act_activation':
** 								- Update the function 'monitor_actuators'.
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "periodic_dimc.h"
#include "gpio_control_dimc.h"
#include "irs_protocol_dimc.h"
#include "logger_dimc.h"
#include "wdog.h"
#include "can_protocol_dimc.h"
#include "fault_management_dimc.h"
#include "cbit_dimc.h"
#include "module_init_dimc.h"
#include "system_params_dimc.h"
#include "gipsy_hal_rcc.h"
#include "mcu_config_dimc.h"
#include "module_init_dimc.h"


/* -----Definitions ----------------------------------------------------------------*/

/* oper_handler definitions */
#define NUMBER_OF_CTRYS_PAIRS						(uint8_t) 10U
#define NA_ACT_PORT_NUM								(uint8_t) 0x07U
#define NO_ACTIVE_CTRY								(uint8_t) 0xFFU;
#define MAX_RETRY_NUM								(uint8_t) 2U

/* CAN BUS COMM time definitions */
#define CTRY_MAX_RESPONSE_TIME						(uint8_t) 250U
#define	CAN_MSG_INTERVAL							(uint8_t) 20U
#define RETRY_TIMEOUT								(uint8_t) 50U
#define CAN_TX_DELAY								(uint8_t) 5U

/* IBIT definitions */
#define IBIT_ACTUATOR_INTERVAL_MILLIS				(uint16_t) 3400U
#define IBIT_ACTUATOR_OPERATION_MILLIS				(uint16_t) 2800U
#define IBIT_SAMPLE_DELAY_AFTER_OPER_MODE			(uint16_t) 2000U



/* Actuator monitor definitions */
#define	ACT_MONITOR_WAIT_PERIOD_MILLIS				(uint16_t) 1500U
#define	ACT_MONITOR_INTERVAL_MILLIS					(uint16_t) 1000U
#define ACTIVATION_LOG_ARRAY_SIZE					(uint8_t)  200U /*File Rev 2.3 */

/* Periodic timing definitions */
#define START_OPERATION_DELAY_MILLIS				(uint16_t) 2000U
#define DISCRETE_INPUT_SAMPLE_PERIOD_MILLIS     	(uint8_t)  10U
#define SW_LED_TOGGLE_PERIOD_MILLIS					(uint16_t) 500U
#define IRS_OPER_MSG_SEND_PERIOD_MILLIS      		(uint16_t) 1000U
#define CBIT_TEST_PERIOD_MILLIS				   		(uint16_t) 1000U

/* Operation timing definition */
#define MAN_ACTUATOR_ACTIVATION_MILLIS     	   		(uint16_t) 4400U
#define IPS_ACTUATOR_INTERVAL_MILLIS				(uint16_t) 5000U
#define IPS_ACTUATOR_ON_DURATION_MILLIS				(uint16_t) 4400U

/* General definitios */
#define SECOND										(uint16_t) 1000U

/* Actuators Monitoring Definitions */
#define PASS			(uint8_t)1U
#define NOT_PASS		(uint8_t)0U
#define MAX_NUMBER_OF_MEASURMENTS		(uint8_t)2U


/* - Operation status - */
/* Used to identify new operation or IBIT sessions */
typedef enum
{
	E_IPS_ON,
	E_IPS_OFF,
	E_IBIT_TEST_ON,
	E_IBIT_TEST_OFF,
	E_NO_ACTIVE_ACTUATORS
}e_Oper_Status;



/* Actuator activation state (ON / OFF) */
typedef enum
{
	E_ACT_ON,
	E_ACT_OFF
}e_Activation_State;


/* -----Macros ---------------------------------------------------------------------*/
#define GET_ELAPSED_TIME(timer)							(HAL_GetTick() - (timer))
#define GET_NUMBER_OF_ACTUATORS()						oper_handler.ctry_table[oper_handler.current_ctry_pair*2U].number_of_actuators
#define GET_R_CTRY_ID()									oper_handler.ctry_table[oper_handler.current_ctry_pair*2U].ctry_id
#define GET_L_CTRY_ID()									oper_handler.ctry_table[(oper_handler.current_ctry_pair*2U) + 1U].ctry_id
#define GET_R_ACTUATOR_NUMBER()							oper_handler.ctry_table[oper_handler.current_ctry_pair*2U].actuator_table[oper_handler.current_act] -1U
#define GET_L_ACTUATOR_NUMBER()							oper_handler.ctry_table[(oper_handler.current_ctry_pair*2U) + 1U].actuator_table[oper_handler.current_act] -1U
#define CTRY_ON_RIGHT_WING(ctry_num)					(0U == (ctry_num) % 2U)

/* -----Global variables -----------------------------------------------------------*/
uint8_t Spare_Bytes[3U] = {0U};					/* Byte array that used for writing events to the data logger */
uint8_t Data_Buf[7U] 	= {0U};					/* Used to send CAN BUS messages */
e_SystemModesTypes Next_Sys_Mod = E_STBY_MODE; 	/* Used to hold the next system mode when transitioning between modes */
uint32_t OPER_Current_Tick = {0U};				/* Holds the current tick during OPER mode. Used to count a delay before sampling the IBIT channel after exiting OPER mode -  file rev: 2.3 */


/* File Rev 2.3 */
uint8_t Act_log_status[ACTIVATION_LOG_ARRAY_SIZE] = {0U};



/*!Specifies Actuator maintenance report struct (Range: var type | Units: see var declaration)*/
/*!\sa Set function: compute_ctry_temp_avrg(), monitor_actuators()*/
/*!\sa Get function: IRS_Send_Msg_Maintanance_Actuator_Data(), monitor_actuators()*/
Actuator_Maint_Data_TypeDef g_Maint_ActuatorData={0U};



/*!Specifies Manual actuator activation struct (Range: var type | Units: see var declaration)*/
/*!\sa Set function: enter_safe_mode(), handle_irs_actuator_activation_command(), irs_execute_recieved_command()*/
/*!\sa Get function: enter_safe_mode(), handle_irs_actuator_activation_command(), irs_execute_recieved_command(), monitor_actuators(), Periodic_Tasks_Scheduler()*/
manual_act_activation_TypeDef g_Actuator_Man_Req = {0U};


/* - CTRY operation data table - */
/* Contaions data related to the activation sequence during OPER or IBIT modes */
struct ctry_operation_data
{
	uint8_t actuator_table[6U];
	uint8_t number_of_actuators;
	uint8_t ctry_id;
	uint16_t actuator_id_table[6U];
}ctry_operation_data_table[CTRY_MAX_NUM] =
{
	CTRY_130H, CTRY_131H,
	CTRY_132H, CTRY_133H,
	CTRY_134H, CTRY_135H,
	CTRY_136H, CTRY_137H,
	CTRY_138H, CTRY_139H,
	CTRY_140H, CTRY_141H,
	CTRY_142H, CTRY_143H,
	CTRY_144H, CTRY_145H,
	CTRY_146H, CTRY_147H,
	CTRY_148H, CTRY_149H,
};



/* - Operation Handler - */
/* Used to control and monitor the active actuators */
struct Operation_handler
{
	const struct ctry_operation_data *ctry_table;
	uint8_t current_act;								/* Used to monitor the current position of the activation sequence */
	uint8_t current_ctry_pair;
	uint32_t start_oper_time;
	uint32_t last_activation_time;
	e_Oper_Status oper_status;
	e_Activation_State act_state;
	uint8_t active_ctry[2U];
}oper_handler =
{
		.ctry_table = ctry_operation_data_table,
		.current_act = 0U,
		.current_ctry_pair = 0U,
		.last_activation_time = 0U,
		.start_oper_time = 0U,							/* Used to delay the activation for 50 milliseconds (until the OPER signal will be stable)*/
		.oper_status = E_NO_ACTIVE_ACTUATORS,			/* Used to identify new operation or IBIT sessions */
		.act_state = E_ACT_OFF,							/* Used to identify if there are any running actuators */
		.active_ctry = {0xFFU, 0xFFU},
};



/* Operation timer - Holds timing data for the periodic task manager */
struct Operation_timer
{
	uint32_t last_disc_smp_time;
	uint32_t last_oper_msg_transmit_time;
	uint32_t last_led_toggle_time;
	uint32_t last_cbit_test_time;
	uint32_t last_monitor_time[CTRY_MAX_NUM];
	uint32_t ctry_on_time[CTRY_MAX_NUM];
	uint32_t ctry_off_time[CTRY_MAX_NUM];
}Timer = {0U};



/* Discrete states - hold the state of the discrete OPERATION and IBIT channels
 * 	used to identify changes in the state of the channels */
struct discrete_ch_state
{
	uint8_t oper_channels;
	uint8_t ibit_channel;
	uint32_t ibit_click_time;
	uint8_t ibit_tr; 				/* Mark IBIT transition */
}Disc_Prev_State =
{
		.oper_channels = 0xFFU,
		.ibit_channel = 0xFFU,
		.ibit_click_time = 0x00U,
		.ibit_tr = FALSE
};

struct discrete_ch_state Disc_Curr_State  =
{
		.oper_channels = 0xFFU,
		.ibit_channel = 0xFFU,
		.ibit_click_time = 0x00U,
		.ibit_tr = FALSE
};




/* CTRYs status handler - used to get CTRYs status and monitor the response time for the request  */
struct ctry_status_hal
{
	uint8_t current_ctry;
	uint8_t status_req_sent;
	uint8_t number_of_attempts[20U];
	uint32_t responded_ctrys;
	uint32_t status_req_start_time;
	uint32_t last_req_sent_time;
}Ctry_Status_Hal =
{
		.current_ctry = 0x00U,
		.status_req_sent = FALSE,
		.status_req_start_time = 0x00U,
		.responded_ctrys = 0x00U,
		.number_of_attempts = {0U},
		.last_req_sent_time = 0x00U
};
/* -----External variables ---------------------------------------------------------*/
extern uint32_t g_Sys_OPER_Time_Sec;
extern uint32_t g_Sys_Acc_OPER_Time_Sec;

/* -----Static Function prototypes -------------------------------------------------*/

static HAL_StatusTypeDef handle_irs_actuator_activation_command(void);
static HAL_StatusTypeDef ibit_test(void);
static HAL_StatusTypeDef operation_manager(void);
static HAL_StatusTypeDef monitor_discrete_channels(void);
static HAL_StatusTypeDef compute_ctry_temp_avrg (uint8_t ctry_id);
static HAL_StatusTypeDef monitor_actuators(void);
static void get_ctrys_status(void);

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Periodic_Tasks_Scheduler -
** 		- perform periodic task scheduling according to the IPS operation mode ("INIT", "STBY", "OPER", "IBIT", "SAFE"):
** 			- Trigger WDG.
** 			- Sample discrete inputs.
** 			- Toggle LED light.
** 			- Send operational status to the UAV.
** 			- Process UAV commands.
** 			- Perform CBIT test.
** 			- Operation mode handler.
** 			- IBIT test handler.
** 			- Handle manual actuator activation (from IRS commands).
** 			- Handle the system's faults.
** 			- Monitor the CTRYs status.
**
** Param: None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Periodic_Tasks_Scheduler(void)
{
	/* Local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	e_SystemModesTypes prev_sys_mode = E_INIT_MODE;

	/* Log system startup event */
	g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_DIMC_STARTUP_COMPLETED,0x00U,E_UNIT_TYPE_DIMC,DIMC_ID,Spare_Bytes);

	/* Enter STBY Mode */
	if (E_INIT_MODE == g_SysStatus.Sys_Mode)
	{
		/* Send TURN OFF command before transition from 'INIT' mode to 'STBY' mode */
		CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);

		/* Change System Mode to 'STBY' */
		g_SysStatus.Sys_Mode = E_STBY_MODE;
	}

	/* Else, current system mode is 'SAFE_MODE' */

	while(TRUE)
	{
		/* Trigger WDG */
		WDOG_Trigger();

		/* Detect entry to SAFE mode - update oper_handler */
		if ((E_SAFE_MODE == g_SysStatus.Sys_Mode) && (E_SAFE_MODE != prev_sys_mode))
		{
			/* Update previous system mode */
			prev_sys_mode = E_SAFE_MODE;

			/* Update oper_handler */
			oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
			oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;

			/* Update all CTRYs off time */
			for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM; ctry_num++)
			{
				Timer.ctry_off_time[ctry_num] = HAL_GetTick();
			}
		}

		/* Handle IPS Activation */
		if (E_OPER_MODE == g_SysStatus.Sys_Mode)
		{
			hal_status |= operation_manager();
		}

		/* Handle IBIT Activation */
		if (E_IBIT_MODE == g_SysStatus.Sys_Mode)
		{
			hal_status |= ibit_test();
		}

		/* Get CTRYs statuses */
		get_ctrys_status();

		/* Sample Discrete Channels */
		if(DISCRETE_INPUT_SAMPLE_PERIOD_MILLIS <= GET_ELAPSED_TIME(Timer.last_disc_smp_time))
		{
			hal_status |= monitor_discrete_channels();
			Timer.last_disc_smp_time = HAL_GetTick();
		}

		/* Monitor Actuators */
		hal_status |= monitor_actuators();

		/* Handle system's faults */
		Fault_Management();

		/* Toggle LED */
		if(SW_LED_TOGGLE_PERIOD_MILLIS <= GET_ELAPSED_TIME(Timer.last_led_toggle_time))
		{
			Manage_OnBoard_Leds();
			Timer.last_led_toggle_time = HAL_GetTick();
		}

		/* Send Operational Status */
		if(IRS_OPER_MSG_SEND_PERIOD_MILLIS <= GET_ELAPSED_TIME(Timer.last_oper_msg_transmit_time))
		{
			IRS_Send_Msg_Operational_Status_Report();
			Timer.last_oper_msg_transmit_time = HAL_GetTick();
		}

		/* Perform CBIT Test */
		if (((E_STBY_MODE == g_SysStatus.Sys_Mode) || (E_OPER_MODE == g_SysStatus.Sys_Mode) || (E_IBIT_MODE == g_SysStatus.Sys_Mode)) &&\
			(CBIT_TEST_PERIOD_MILLIS <= GET_ELAPSED_TIME(Timer.last_cbit_test_time)))
		{
			Perform_CBIT_Test();
			Timer.last_cbit_test_time = HAL_GetTick();
		}

		/* Process IRS Commands */
		Parse_IRS_Command();
		Proccess_Logger_Request_from_IRS();

		if ((E_STBY_MODE == g_SysStatus.Sys_Mode) && (E_NO_REQ != g_Actuator_Man_Req.request_status))
		{
			hal_status |= handle_irs_actuator_activation_command();
		}

		if(HAL_OK != hal_status)
		{
			g_SysStatus.SW_Erros = TRUE;
		}
	}
}

/*************************************************************************************
** monitor_discrete_channels -
** 		- Get the state of the discrete channels and change the operation modes accordingly.
** 			- Get the state of the OPER and IBIT channels.
** 			- Get the state of the FLT channels A and C.
** 			- Get the state of the FLT sens channels
** 			- Log the change of system modes in the data logger.
**
** Param:  None.
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef monitor_discrete_channels(void)
{
	/* Init Local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef logger_status = HAL_OK;
	uint8_t discrete_st = 0x00U;	/* Used to set the discrete channels value in g_SysStatus */
	uint8_t exit_ibit = FALSE;
	uint8_t ch_A = 0x00U;
	uint8_t ch_C = 0x00U;
	uint8_t flt_sens = 0x00U;
	uint8_t oper_ch = 0x00U;
	uint8_t oper_1_2 = 0x00U;


	/* Sample "IBIT" and "OPER" channel only if not in "SAFE" mode */
	if(E_SAFE_MODE != g_SysStatus.Sys_Mode)
	{
		/* Set discrete channels previous state */
		Disc_Prev_State.ibit_channel = Disc_Curr_State.ibit_channel;
		Disc_Prev_State.oper_channels = Disc_Curr_State.oper_channels;

		/* Get discrete channels current state */
		hal_status |=  Read_OPER_Inputs(&Disc_Curr_State.oper_channels, &oper_ch, &oper_1_2);
		Disc_Curr_State.ibit_channel = Read_IBIT_Input();

		/* Set time stamp for IBIT state changing from 'OFF' to 'ON' */
		if ((E_IBIT_ON == Disc_Curr_State.ibit_channel) && (E_IBIT_OFF == Disc_Prev_State.ibit_channel) && (IBIT_SAMPLE_DELAY_AFTER_OPER_MODE <= GET_ELAPSED_TIME(OPER_Current_Tick)))
		{
			Disc_Curr_State.ibit_click_time = HAL_GetTick();
			Disc_Curr_State.ibit_tr = TRUE;
		}

		/* Set discrete operation and IBIT channels to discrete_st variable */
		discrete_st |= (oper_ch & 0x03U);
		discrete_st |= (Disc_Curr_State.ibit_channel << 2U) & 0x04U;

		if (E_STBY_MODE == g_SysStatus.Sys_Mode)
		{
			/* Check Operation entry conditions */
			if((E_OPER_ON == Disc_Curr_State.oper_channels) && (E_OPER_OFF == Disc_Prev_State.oper_channels) && (E_ACT_OFF == oper_handler.act_state))
			{
				/* Change system mode */
				g_SysStatus.Sys_Mode = E_OPER_MODE;

				/* Start operation time counter */
				g_Sys_OPER_Time_Sec = HAL_GetTick();

				/* Set the starting time of the operation */
				oper_handler.start_oper_time = HAL_GetTick();

				/* Write event */
				logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_START_RECEIVED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
			}

			/* Check IBIT entry conditions */
			else if((E_IBIT_ON == Disc_Curr_State.ibit_channel) && (TRUE == Disc_Curr_State.ibit_tr) && (2000U <= GET_ELAPSED_TIME(Disc_Curr_State.ibit_click_time)) && (0x03U != oper_ch) && (E_ACT_OFF == oper_handler.act_state))
			{
				/* Reset channel transition flag */
				Disc_Curr_State.ibit_tr = FALSE;

				/* Change system mode */
				g_SysStatus.Sys_Mode = E_IBIT_MODE;

				/* Write event */
				logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_START_RECEIVD, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
			}
		}

		else if (E_OPER_MODE == g_SysStatus.Sys_Mode)
		{
			/* File Rev: 2.3 */
			OPER_Current_Tick = HAL_GetTick();
			/* -- */

			/* Check Operation mode exit conditions */
			if ((E_OPER_OFF == Disc_Curr_State.oper_channels) && (E_IPS_ON == oper_handler.oper_status))
			{

				/* Update oper_handler data fields */
				oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
				oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
				Timer.ctry_off_time[GET_R_CTRY_ID() - 1U] = HAL_GetTick();
				Timer.ctry_off_time[GET_L_CTRY_ID() - 1U] = HAL_GetTick();

				/* Send TURN OFF command */
				CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);

				/* Update the state of the actuators */
				oper_handler.act_state = E_ACT_OFF;

				/* Change oper_handler status */
				oper_handler.oper_status = E_IPS_OFF;

				/* Write event */
				logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_ABORT_RECEIVED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
			}
		}

		else if (E_IBIT_MODE == g_SysStatus.Sys_Mode)
		{
			/* Check IBIT exit conditions */
			if ((E_IBIT_OFF == Disc_Curr_State.ibit_channel) && (E_IBIT_TEST_ON == oper_handler.oper_status))
			{
				exit_ibit = TRUE;
				Next_Sys_Mod = E_STBY_MODE;
			}
			if ((E_OPER_ON == Disc_Curr_State.oper_channels) && (E_IBIT_TEST_ON == oper_handler.oper_status))
			{
				exit_ibit = TRUE;
				Next_Sys_Mod = E_OPER_MODE;
			}
			if (TRUE == exit_ibit)
			{
				if (E_ACT_ON == oper_handler.act_state)
				{
					/* TURN OFF running actuators */
					CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);

					/* Update oper_handler data fields */
					oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
					oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
					Timer.ctry_off_time[GET_R_CTRY_ID() - 1U] = HAL_GetTick();
					Timer.ctry_off_time[GET_L_CTRY_ID() - 1U] = HAL_GetTick();

					/* Update the state of the actuators */
					oper_handler.act_state = E_ACT_OFF;
				}

				/* Change oper_handler status */
				oper_handler.oper_status = E_IBIT_TEST_OFF;

				/* Update IBIT test status */
				g_IBIT_Result.IBIT_status = E_IBIT_ABORTED;

				/* Write event */
				logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_ABORT_RECEIVED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
			}
		}
	}

	/* Sample fault channels A & C */
	Read_FLT_Channel_A_C_Inputs(&ch_A, &ch_C);
	g_SysStatus.ChannelA_FaultsStatus = ch_A;
	g_SysStatus.ChannelC_FaultsStatus = ch_C;

	/* Sample discrete sense channels */
	flt_sens = Read_FLT_Sense_Inputs();
	flt_sens = ((uint8_t)(~flt_sens) & 0x07U);
	discrete_st |= (flt_sens << 3U);

	/* Update the global discrete status */
	g_SysStatus.DiscretesStatus = discrete_st;

	if (HAL_OK != logger_status)
	{
		g_SysStatus.Logger_FL = TRUE;
	}

	return hal_status;
}

/*************************************************************************************
** handle_irs_actuator_activation_command -
** 		- Handle manual actuator activation request:
** 			- Get the actuator status ('FL' / 'OK') and its activation speed .
** 			- Send TURN ON command to the specific actuator.
** 			- Send TURN OFF command after 4.4 seconds of activation.
** 			- Get the CTRY's temperatures.
** 			- Send Actuator Maintenance Report the the UAV.
**
** Param:  None.
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef handle_irs_actuator_activation_command(void)
{
	/* Init local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t ctry_number	= 0x00U;
	uint16_t act_id 	= 0x00U;
	uint8_t act_num 	= 0x00U;
	uint8_t act_port	= 0x00U;
	uint8_t l_ctry_id 	= 0x00U;
	uint8_t r_ctry_id 	= 0x00U;
	uint8_t l_act_port 	= 0x00U;
	uint8_t r_act_port 	= 0x00U;
	uint8_t act_speed = 0x00U;
	e_ActuatorStatus act_status = E_ACT_FL;

	/* Process new request */
	if (E_NEW_REQ == g_Actuator_Man_Req.request_status)
	{
		/* Find the corresponded CTRY */
		hal_status = Get_Actuator_Id(ctry_number, act_num, &act_id);

		while ((act_id != g_Actuator_Man_Req.actuator_id) && (HAL_OK == hal_status))
		{
			act_num++;
			if(E_ACT_MAX_NUM == act_num)
			{
				ctry_number++;
				act_num = 0U;
			}
			hal_status = Get_Actuator_Id(ctry_number, act_num, &act_id);
		}

		/* Get actuator's status */
		act_port = act_num+1U;
		hal_status |= Get_Actuator_Status(ctry_number,act_port-1,&act_status);

		/* Get activation speed */
		hal_status |= Get_Actuator_Speed(ctry_number,act_port-1,&act_speed,E_ACT_MANUAL);

		/* Right wing actuator selected */
		if(CTRY_ON_RIGHT_WING(ctry_number))
		{
			r_ctry_id = oper_handler.ctry_table[ctry_number].ctry_id;
			r_act_port = act_port;
			l_ctry_id = r_ctry_id + 1U;
			l_act_port = NA_ACT_PORT_NUM;
		}

		/* Left wing actuator selected */
		else
		{
			l_ctry_id = oper_handler.ctry_table[ctry_number].ctry_id;
			l_act_port = act_port;
			r_ctry_id = l_ctry_id - 1U;
			r_act_port = NA_ACT_PORT_NUM;
		}

		if ((HAL_OK == hal_status) && (E_ACT_OK == act_status))
		{
			/* Set data_buf for the CAN_BUS msg format */
			Data_Buf[0U] = l_ctry_id;
			Data_Buf[1U] = l_act_port;
			Data_Buf[2U] = act_speed;
			Data_Buf[3U] = r_ctry_id;
			Data_Buf[4U] = r_act_port;
			Data_Buf[5U] = act_speed;
			Data_Buf[6U] = (uint8_t)E_ACT_MANUAL;

			/* Send Activation Command */
			if ((E_SAFE_MODE != g_SysStatus.Sys_Mode) && (E_ACT_OFF == oper_handler.act_state))
			{
				CAN_Message_Transmit(ID_ACTUATOR_ON_REQ, CAN_BROADCAST_ADDR, Data_Buf);
				oper_handler.act_state = E_ACT_ON;

				/* Update oper_handler  */
				oper_handler.active_ctry[0U] = ctry_number;
				Timer.ctry_on_time[ctry_number] = HAL_GetTick();
			}

			/* Update request parameters */
			g_Actuator_Man_Req.start_time = HAL_GetTick();
			g_Actuator_Man_Req.request_status = E_ACTIVE_REQ;
			g_Actuator_Man_Req.act_number = (act_port - 1U);
			g_Actuator_Man_Req.ctry_num = ctry_number;
		}
		else
		{
			g_Actuator_Man_Req.request_status = E_NO_REQ;
		}
	}

	/* Terminate current active request */
	if((E_ACTIVE_REQ == g_Actuator_Man_Req.request_status) && (MAN_ACTUATOR_ACTIVATION_MILLIS <= GET_ELAPSED_TIME(g_Actuator_Man_Req.start_time)))
	{
		/* Send TURN OFF command */
		CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);
		oper_handler.act_state = E_ACT_OFF;

		/* Update oper_handler */
		oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
		Timer.ctry_off_time[g_Actuator_Man_Req.ctry_num] = HAL_GetTick();

		/* Send DRV BRD TEMP Req */
		CAN_Message_Transmit(ID_DRV_BRD_TEMP_REQ, g_Actuator_Man_Req.ctry_num+1U, Data_Buf);
		HAL_Delay(CAN_TX_DELAY);

		/* Send CPU BRD TEMP Req */
		CAN_Message_Transmit(ID_CPU_BRD_TEMP_REQ, g_Actuator_Man_Req.ctry_num+1U, Data_Buf);
		HAL_Delay(CAN_TX_DELAY);

		/* Update request parameters */
		g_Actuator_Man_Req.request_status = E_NO_REQ;

		/* Set temperature measurements in the the maintenance report */
		hal_status |= compute_ctry_temp_avrg(g_Actuator_Man_Req.ctry_num);

		/* Send activation result to UAV */
		IRS_Send_Msg_Maintanance_Actuator_Data();
	}
	return hal_status;
}

/*************************************************************************************
**
** ibit_test(void)
** 		Perform Initiated-BIT.
** 		- Test channel C inputs.
** 		- Run actuators for 2 seconds, only 2 actuators at the same time.
** 		- Send IBIT Test Result to the UAV.
**
** Param : 	None.
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef ibit_test(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef logger_status = HAL_OK;
	uint8_t turn_on_act = FALSE;
	uint8_t r_act_port = 0x00U;
	uint8_t l_act_port = 0x00U;
	uint8_t r_act_speed  = 0x00U;
	uint8_t l_act_speed  = 0x00U;
	uint8_t r_ctry_num = 0x00U;
	uint8_t l_ctry_num = 0x00U;
	e_ActuatorStatus r_act_status = E_ACT_OK;
	e_ActuatorStatus l_act_status = E_ACT_OK;
	e_CtryStatus r_ctry_status = E_CTRY_OK;
	e_CtryStatus l_ctry_status = E_CTRY_OK;

	/* New IBIT operation */
	if(E_NO_ACTIVE_ACTUATORS == oper_handler.oper_status)
	{
		logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_STARTED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
		oper_handler.oper_status = E_IBIT_TEST_ON;
		turn_on_act = TRUE;

		/* Reset previous IBIT test result */
		g_IBIT_Result.DIMC_IBIT_Res = 0x00U;
		for (uint8_t ctry_n = 0U; ctry_n < CTRY_MAX_NUM; ctry_n++)
		{
			g_IBIT_Result.CTRYs_IBIT_Results[ctry_n] = 0x00U;
		}

		/* Set IBIT test result struct */
		g_IBIT_Result.IBIT_status = E_IBIT_IN_PROCESS;
		g_IBIT_Result.DIMC_IBIT_Res |= (g_SysTestsStatus.DIMC_Logger_Test_Status & 0x01U);
		g_IBIT_Result.DIMC_IBIT_Res |= ((g_SysTestsStatus.DIMC_Volt_Test_Status << 1U) & 0x02U);
		g_IBIT_Result.DIMC_IBIT_Res |= ((g_SysTestsStatus.DIMC_Can_Bus_Init_Test_Status << 2U) & 0x04U);
		g_IBIT_Result.DIMC_IBIT_Res |= ((g_SysTestsStatus.CTRYs_SW_Ver_Mismatch_Test_Status << 3U) & 0x08U);
		g_IBIT_Result.DIMC_IBIT_Res |= ((g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status << 4U) & 0x10U);

		/* Executed only in operatioal version */
#ifndef _TEST_BENCH_CONFIGURATION_
		/* Test channel C */
		if (TEST_FL == Test_CH_C_Inputs())
		{
			g_IBIT_Result.DIMC_IBIT_Res |= (((uint8_t)0x01U << 4U) & 0x10U);
			g_SysTestsStatus.DIMC_Ch_C_Test_Status = TEST_FL;
		}
#endif
	}

	/* Update IBIT status and turn off current active actuators */
	else if ((E_ACT_ON == oper_handler.act_state) && (IBIT_ACTUATOR_OPERATION_MILLIS <= GET_ELAPSED_TIME(oper_handler.last_activation_time)))
	{
		l_ctry_num = GET_L_CTRY_ID()- 1U; /* CTRY number defined as CTRY ID -1 */
		r_ctry_num = GET_R_CTRY_ID()- 1U; /* CTRY number defined as CTRY ID -1 */

		/* Update IBIT status according to the CTRYs monitoring test */
		if ((0x00U != g_IBIT_Result.CTRYs_IBIT_Results[l_ctry_num]) || (0x00U != g_IBIT_Result.CTRYs_IBIT_Results[r_ctry_num]))
		{
			g_IBIT_Result.IBIT_status = E_IBIT_FAILED;
		}

		/* Send TURN OFF command */
		CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);

		/* Update oper_handler data fields*/
		oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
		oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
		Timer.ctry_off_time[(GET_R_CTRY_ID() - 1U)] = HAL_GetTick();
		Timer.ctry_off_time[(GET_L_CTRY_ID() - 1U)] = HAL_GetTick();

		HAL_Delay(CAN_TX_DELAY);

		/* Update the state of the actuators */
		oper_handler.act_state = E_ACT_OFF;
	}

	/* Turn on the next actuators pair in the sequence */
	else if ((IBIT_ACTUATOR_INTERVAL_MILLIS <= GET_ELAPSED_TIME(oper_handler.last_activation_time)) && (E_IBIT_TEST_ON == oper_handler.oper_status))
	{
		turn_on_act = TRUE;
		oper_handler.current_act++;

		/* Move to the next CTRY in the sequence */
		if(GET_NUMBER_OF_ACTUATORS() <= oper_handler.current_act)
		{
			oper_handler.current_ctry_pair++;
			oper_handler.current_act = 0U;
		}

		/* IBIT complete */
		if ((NUMBER_OF_CTRYS_PAIRS <= oper_handler.current_ctry_pair) && (E_SAFE_MODE != g_SysStatus.Sys_Mode))
		{
			/* Exit Ibit mode */
			g_SysStatus.Sys_Mode = E_STBY_MODE;
			turn_on_act = FALSE;

			/* Reset oper_handler */
			oper_handler.oper_status 			= E_NO_ACTIVE_ACTUATORS;
			oper_handler.current_act 			= 0x00U;
			oper_handler.current_ctry_pair 		= 0x00U;
			oper_handler.last_activation_time 	= 0x00U;

			/* Update IBIT test status */
			if (E_IBIT_FAILED != g_IBIT_Result.IBIT_status)
			{
				g_IBIT_Result.IBIT_status = E_IBIT_PASSED;
			}

			/* Write event */
			logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_COMPLETE, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
		}
	}

	/* End of IBIT test - IBIT aborted */
	else if ((E_IBIT_TEST_OFF == oper_handler.oper_status) && (E_SAFE_MODE != g_SysStatus.Sys_Mode))
	{
		/* Change system mode: 'IBIT' -> 'STBY' */
		g_SysStatus.Sys_Mode = Next_Sys_Mod;

		oper_handler.oper_status 			= E_NO_ACTIVE_ACTUATORS;
		oper_handler.current_act 			= 0x00U;
		oper_handler.current_ctry_pair 		= 0x00U;
		oper_handler.last_activation_time 	= 0x00U;

		/* Write event */
		logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_ABORTED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);

		if(E_OPER_MODE == g_SysStatus.Sys_Mode)
		{
			/* Write event */
			logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_START_RECEIVED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
		}
	}

	/* Turn on Actuators */
	if ((E_SAFE_MODE != g_SysStatus.Sys_Mode) && (TRUE == turn_on_act))
	{
		/* Set timing data */
		oper_handler.last_activation_time = HAL_GetTick();

		/* Set actuators ports */
		l_act_port = GET_L_ACTUATOR_NUMBER() +1U;	/* Act_port defined as act_numer +1 */
		r_act_port = GET_R_ACTUATOR_NUMBER() +1U;	/* Act_port defined as act_numer +1 */

		/* Update oper_handler data fields*/
		oper_handler.active_ctry[0U] = GET_R_CTRY_ID() - 1U;
		oper_handler.active_ctry[1U] = GET_L_CTRY_ID() - 1U;
		Timer.ctry_on_time[(GET_R_CTRY_ID() - 1U)] = HAL_GetTick();
		Timer.ctry_on_time[(GET_L_CTRY_ID() - 1U)] = HAL_GetTick();

		/* Get actuators statuses */
		hal_status |= Get_Actuator_Status((GET_R_CTRY_ID() - 1U), (r_act_port - 1U), &r_act_status);
		hal_status |= Get_Actuator_Status((GET_L_CTRY_ID() - 1U), (l_act_port - 1U), &l_act_status);

		/* Get CTRYs statuses */
		hal_status |= Get_CTRY_Status((GET_R_CTRY_ID() - 1U),&r_ctry_status);
		hal_status |= Get_CTRY_Status((GET_L_CTRY_ID() - 1U),&l_ctry_status);

		/* Get actuators speed */
		hal_status |= Get_Actuator_Speed((GET_L_CTRY_ID() - 1U), (l_act_port - 1U), &l_act_speed, E_ACT_IBIT);
		hal_status |= Get_Actuator_Speed((GET_R_CTRY_ID() - 1U), (r_act_port - 1U), &r_act_speed, E_ACT_IBIT);

		/* Verify right CTRY and actuator statuses are OK */
		if ((E_ACT_FL == r_act_status) || (E_CTRY_FL == r_ctry_status))
		{
			r_act_port = NA_ACT_PORT_NUM;
			oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
		}

		/* Verify left CTRY and actuator statuses are OK */
		if ((E_ACT_FL == l_act_status) || (E_CTRY_FL == l_ctry_status))
		{
			l_act_port = NA_ACT_PORT_NUM;
			oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
		}

		/* Set data_buf for CAN_BUS msg format */
		Data_Buf[0U] = GET_L_CTRY_ID();
		Data_Buf[1U] = l_act_port;
		Data_Buf[2U] = l_act_speed;
		Data_Buf[3U] = GET_R_CTRY_ID();
		Data_Buf[4U] = r_act_port;
		Data_Buf[5U] = r_act_speed;
		Data_Buf[6U] = (uint8_t)E_ACT_IBIT;

		CAN_Message_Transmit(ID_ACTUATOR_ON_REQ, CAN_BROADCAST_ADDR, Data_Buf);
		HAL_Delay(CAN_TX_DELAY);

		/* Update the state of the actuators */
		oper_handler.act_state = E_ACT_ON;
	}

	if (HAL_OK != logger_status)
	{
		g_SysStatus.Logger_FL = TRUE;
	}

	return hal_status;
}


/*************************************************************************************
**
** operation_manager(void)
** 		Handles all the activities related to the OPERATION mode:
** 		 - Control and schedule the actuators activation sequence.
**
** Param : 	None.
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef operation_manager(void)
{
	/* Local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef logger_status = HAL_OK;
	uint8_t turn_on_act = FALSE;
	uint8_t r_act_port = 0x00U;
	uint8_t l_act_port = 0x00U;
	uint8_t r_act_speed = 0x00U;
	uint8_t l_act_speed = 0x00U;
	e_ActuatorStatus r_act_status = E_ACT_OK;
	e_ActuatorStatus l_act_status = E_ACT_OK;
	e_CtryStatus r_ctry_status = E_CTRY_OK;
	e_CtryStatus l_ctry_status = E_CTRY_OK;

	/* New De-Icing operation */
	if((E_NO_ACTIVE_ACTUATORS == oper_handler.oper_status) && (START_OPERATION_DELAY_MILLIS <= GET_ELAPSED_TIME(oper_handler.start_oper_time)))
	{
		/* Write event */
		logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_MODE_STARTED, 0x00U , E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);
		oper_handler.oper_status = E_IPS_ON;
		turn_on_act = TRUE;
	}

	/* Turn off current active actuators */
	else if ((E_ACT_ON == oper_handler.act_state) && (IPS_ACTUATOR_ON_DURATION_MILLIS <= GET_ELAPSED_TIME(oper_handler.last_activation_time)))
	{
		/* Send TURN OFF command */
		CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, Data_Buf);
		HAL_Delay(CAN_TX_DELAY);

		/* Update the state of the actuators */
		oper_handler.act_state = E_ACT_OFF;

		/* Update oper_handler data fields*/
		oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
		oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
		Timer.ctry_off_time[GET_R_CTRY_ID() - 1U] = HAL_GetTick();
		Timer.ctry_off_time[GET_L_CTRY_ID() - 1U] = HAL_GetTick();
	}

	/* Turn on the next actuators in the sequence */
	else if ((IPS_ACTUATOR_INTERVAL_MILLIS <= GET_ELAPSED_TIME(oper_handler.last_activation_time)) && (E_IPS_ON == oper_handler.oper_status))
	{
		turn_on_act = TRUE;
		oper_handler.current_act++;

		/* Move to the next CTRY  */
		if(GET_NUMBER_OF_ACTUATORS() <= oper_handler.current_act)
		{
			oper_handler.current_ctry_pair++;
			oper_handler.current_act = 0U;
		}

		/* Restart operation sequence */
		if(NUMBER_OF_CTRYS_PAIRS <= oper_handler.current_ctry_pair)
		{
			oper_handler.current_act = 0U;
			oper_handler.current_ctry_pair = 0U;
		}
	}

	/* Operation terminated */
	else if ((E_IPS_OFF == oper_handler.oper_status) && (E_SAFE_MODE != g_SysStatus.Sys_Mode))
	{
		/* Write event */
		logger_status |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_MODE_TERMINATED, 0x00U, E_UNIT_TYPE_IPS, IPS_ID, Spare_Bytes);

		/* Stop operation time counter */
		g_Sys_OPER_Time_Sec = ((HAL_GetTick() - g_Sys_OPER_Time_Sec) / SECOND);

		/* Add to the accumulated operation time */
		g_Sys_Acc_OPER_Time_Sec += g_Sys_OPER_Time_Sec;

		/* Change system mode: 'OPER' -> 'STBY' */
		g_SysStatus.Sys_Mode = E_STBY_MODE;

		/* Reset oper_handler */
		oper_handler.oper_status 			= E_NO_ACTIVE_ACTUATORS;
		oper_handler.current_act 			= 0x00U;
		oper_handler.current_ctry_pair 		= 0x00U;
		oper_handler.last_activation_time 	= 0x00U;

		/* Reset actuators logging statuses */
		for(uint8_t act_id = 0; act_id < ACTIVATION_LOG_ARRAY_SIZE; act_id++)
		{
			/* Reset operation log array */
			Act_log_status[act_id] = FALSE;
		}
	}


	/* Send 'turn on' command to the CTRYs */
	if ((E_SAFE_MODE != g_SysStatus.Sys_Mode) && (TRUE == turn_on_act) && (E_ACT_OFF == oper_handler.act_state))
	{
		/* Set timing data */
		oper_handler.last_activation_time = HAL_GetTick();

		/* Set actuators ports */
		l_act_port = GET_L_ACTUATOR_NUMBER() + 1U;	/* Act_port defined as act_numer +1 */
		r_act_port = GET_R_ACTUATOR_NUMBER() + 1U;	/* Act_port defined as act_numer +1 */

		/* Get actuators statuses */
		hal_status |= Get_Actuator_Status((GET_R_CTRY_ID() - 1U), (r_act_port - 1U), &r_act_status);
		hal_status |= Get_Actuator_Status((GET_L_CTRY_ID() - 1U), (l_act_port - 1U), &l_act_status);

		/* Get CTRYs statuses */
		hal_status |= Get_CTRY_Status((GET_R_CTRY_ID() - 1U),&r_ctry_status);
		hal_status |= Get_CTRY_Status((GET_L_CTRY_ID() - 1U),&l_ctry_status);

		/* Get actuators speed */
		hal_status |= Get_Actuator_Speed((GET_L_CTRY_ID() - 1U), (l_act_port - 1U), &l_act_speed, E_ACT_OPER);
		hal_status |= Get_Actuator_Speed((GET_R_CTRY_ID() - 1U), (r_act_port - 1U), &r_act_speed, E_ACT_OPER);

		/* Update oper_handler data fields*/
		oper_handler.active_ctry[0U] = GET_R_CTRY_ID() - 1U;
		oper_handler.active_ctry[1U] = GET_L_CTRY_ID() - 1U;
		Timer.ctry_on_time[GET_R_CTRY_ID() - 1U] = HAL_GetTick();
		Timer.ctry_on_time[GET_L_CTRY_ID() - 1U] = HAL_GetTick();

		/* Verify right CTRY and actuator statuses are OK */
		if ((E_ACT_FL == r_act_status) || (E_CTRY_FL == r_ctry_status))
		{
			r_act_port = NA_ACT_PORT_NUM;
			oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
		}

		/* Verify left CTRY and actuator statuses are OK */
		if ((E_ACT_FL == l_act_status) || (E_CTRY_FL == l_ctry_status))
		{
			l_act_port = NA_ACT_PORT_NUM;
			oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
		}

		if (HAL_OK == hal_status)
		{
			/* Set data_buf for CAN_BUS msg format */
			Data_Buf[0U] = GET_L_CTRY_ID();
			Data_Buf[1U] = l_act_port;
			Data_Buf[2U] = l_act_speed;
			Data_Buf[3U] = GET_R_CTRY_ID();
			Data_Buf[4U] = r_act_port;
			Data_Buf[5U] = r_act_speed;
			Data_Buf[6U] = (uint8_t)E_ACT_OPER;

			CAN_Message_Transmit(ID_ACTUATOR_ON_REQ, CAN_BROADCAST_ADDR, Data_Buf);
			HAL_Delay(CAN_TX_DELAY);

			/* Update the state of the actuators */
			oper_handler.act_state = E_ACT_ON;
		}
	}

	if(HAL_OK != logger_status)
	{
		g_SysStatus.Logger_FL = TRUE;
	}

	return hal_status;
}
/*************************************************************************************
** get_ctrys_status(void)
** 		- Send status request to one CTRY at a time (0-19).
** 		- Check if each CTRY respond during the 250 milliseconds time limit.
** 		- Marked CTRYs that failed to respond.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
static void get_ctrys_status(void)
{
	/* Local variables */
	uint8_t ctry_number = 0x00U;
	uint32_t msg_tx_time = 0x00U;
	uint32_t msg_rx_time = 0x00U;

	/* Get CTRY data */
	ctry_number = Ctry_Status_Hal.current_ctry;
	msg_tx_time = g_SysStatus.CTRY_Status[ctry_number].transmit_time;
	msg_rx_time = g_SysStatus.CTRY_Status[ctry_number].respond_time;

	/* Send status request if CTRY is responded to last request and if the time interval has passed */
	if ((FALSE == g_SysStatus.CTRY_Status[ctry_number].failed_to_resp) && (CAN_MSG_INTERVAL < GET_ELAPSED_TIME(Ctry_Status_Hal.last_req_sent_time)))
	{
		/* Send a status request to the CTRY */
		if (msg_rx_time >= msg_tx_time)
		{
			/* Send msg */
			CAN_Message_Transmit(ID_CTRY_STATUS_REQ, ctry_number+1U, Data_Buf);

			/* Set msg transmit time */
			Ctry_Status_Hal.last_req_sent_time = HAL_GetTick();
			g_SysStatus.CTRY_Status[ctry_number].transmit_time = Ctry_Status_Hal.last_req_sent_time;

			/* Increment the number of attempt to get CTRY status */
			Ctry_Status_Hal.number_of_attempts[ctry_number] = 0x01U;
		}

		/* CTRY failed to respond to the first status request */
		else if ((RETRY_TIMEOUT < GET_ELAPSED_TIME(msg_tx_time)) && (MAX_RETRY_NUM > Ctry_Status_Hal.number_of_attempts[ctry_number]))
		{
			/* Send msg */
			CAN_Message_Transmit(ID_CTRY_STATUS_REQ, ctry_number+1U, Data_Buf);

			/* Set msg transmit time */
			Ctry_Status_Hal.last_req_sent_time = HAL_GetTick();
			g_SysStatus.CTRY_Status[ctry_number].transmit_time = Ctry_Status_Hal.last_req_sent_time;

			/* Increment the number of attempt to get CTRY status */
			Ctry_Status_Hal.number_of_attempts[ctry_number]++;
		}

		/* CTRY failed to respond to the second request */
		else if ((MAX_RETRY_NUM == Ctry_Status_Hal.number_of_attempts[ctry_number]) && (CTRY_MAX_RESPONSE_TIME < msg_tx_time - msg_rx_time))
		{
			/* Set current CTRY as 'FL' */
			g_SysStatus.CTRY_Status[ctry_number].failed_to_resp = TRUE;
		}

		/* Increment current CTRY */
		Ctry_Status_Hal.current_ctry++;

		/* Reset sequence */
		if (Ctry_Status_Hal.current_ctry == CTRY_MAX_NUM)
		{
			Ctry_Status_Hal.current_ctry = 0x00U;
		}

	}

	else if (TRUE == g_SysStatus.CTRY_Status[ctry_number].failed_to_resp)
	{
		/* Increment current CTRY */
		Ctry_Status_Hal.current_ctry++;

		/* Reset sequence */
		if (Ctry_Status_Hal.current_ctry == CTRY_MAX_NUM)
		{
			Ctry_Status_Hal.current_ctry = 0x00U;
		}
	}
}

/*************************************************************************************
** compute_ctry_temp_avrg
** 		- Compute the average temperature of the 4 sensors on the CPU board and the average temperature of the 4 sensors on the driver board.
** 		- Place the results in the actuator's maintenance report structure.
**
** Param : 	uint8_t ctry_id - the id of the CTRY that beaning monitor (0-19).
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef compute_ctry_temp_avrg (uint8_t ctry_id)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	int16_t cpu_temp_sum = 0x00U;
	int16_t dvr_temp_sum = 0x00U;
	int8_t sign = 0x00U;

	if (ctry_id > CTRY_MAX_NUM)
	{
		hal_status = HAL_ERROR;
	}

	else
	{
		for(uint8_t i = 0x00U; i < MAX_TEMP_SENSORS_ON_BOARD ; i++)
		{
			/* Sum CPU measured temperatures (msb = 0 --> sign = +) */
			sign = ((g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Senors_Temperature[i] >> 7U) & 0x01U);
			if(0x00U == sign)
			{
				sign = 1U;
			}
			else
			{
				sign = -1;
			}
			cpu_temp_sum += ((g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Senors_Temperature[i] & 0x7FU) * sign);

			/* Sum DVR measured temperatures (msb = 0 --> sign = +) */
			sign = ((g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Senors_Temperature[i] >> 7U) & 0x01U);
			if(0x00U == sign)
			{
				sign = 1U;
			}
			else
			{
				sign = -1;
			}
			dvr_temp_sum += ((g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Senors_Temperature[i] & 0x7FU) * sign);
		}

		/* Compute average */
		g_Maint_ActuatorData.TemperatureCpu = (int8_t)(cpu_temp_sum/MAX_TEMP_SENSORS_ON_BOARD);
		g_Maint_ActuatorData.TemperatureDrv = (int8_t)(dvr_temp_sum/MAX_TEMP_SENSORS_ON_BOARD);
	}
	return hal_status;
}





/*************************************************************************************
** monitor_actuators - Monitor active actuator's vibration power and current consumption.
** 							- monitor unactive CTRY's current consumption.
**							- Set the actuator's status to 'FL' if its operating out of its limits.
**							- Verify that the actuator de-activated successfully
**
** Param:  	None.
**
** Returns: HAL_StatusTypeDef - Method's status.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef monitor_actuators(void)
{
	/* Local Variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t act_num = 0x00U;
	uint16_t act_id = 0x0U;
	uint8_t ctry_vibration_G = 0x00U;
	uint8_t ctry_current_A = 0x00U;
	uint8_t log_act_id = 0x00;				/* File Rev 2.3 */
	uint16_t log_flt_value = 0x00U;

	for(uint8_t ctry_id = 0U; ctry_id <CTRY_MAX_NUM; ctry_id++)
	{
		/* Get the CTRY's operation data */
		ctry_vibration_G = g_SysStatus.CTRY_Status[ctry_id].VibrationValue;
		ctry_current_A = g_SysStatus.CTRY_Status[ctry_id].CurrentValue;

		/* Monitor CTRY with active actuator */
		if (((oper_handler.active_ctry[0U] == ctry_id) || (oper_handler.active_ctry[1U] == ctry_id))						&& 		/* One of the CTRY's actuators is active */
			(ACT_MONITOR_WAIT_PERIOD_MILLIS + Timer.ctry_on_time[ctry_id] <= g_SysStatus.CTRY_Status[ctry_id].respond_time) && 		/* The CTRY's status is updated */
			(ACT_MONITOR_INTERVAL_MILLIS <= GET_ELAPSED_TIME(Timer.last_monitor_time[ctry_id]))								&&		/* Last monitor of this CTRY was at least 1 second ago */
			(E_SAFE_MODE != g_SysStatus.Sys_Mode))																					/* System mode is not SAFE */
		{
			/* Get the active actuator number - Manual opeation */
			if (E_ACTIVE_REQ == g_Actuator_Man_Req.request_status)
			{
				act_num = g_Actuator_Man_Req.act_number;
			}

			/* Get the active actuator number - IBIT\OPER mode, right actuator */
			else if (CTRY_ON_RIGHT_WING(ctry_id))
			{
				act_num = GET_R_ACTUATOR_NUMBER();
			}

			/* Get the active actuator number - IBIT\OPER mode, left actuator */
			else
			{
				act_num = GET_L_ACTUATOR_NUMBER();
			}

			/* Get the actuator's ID */
			hal_status |= Get_Actuator_Id(ctry_id, act_num, &act_id);
			g_SysStatus.CTRY_Status[ctry_id].fl_act_id = (uint8_t)(act_id - 0x200U);

			/* Reset previous maintenance report data */
			g_Maint_ActuatorData.TestsResults = 0U;

			/* Set the actuator's maintenance report */
			g_Maint_ActuatorData.Actuator_ID = act_id;
			g_Maint_ActuatorData.VibrationValue = ctry_vibration_G;
			g_Maint_ActuatorData.CurrConsumption = ctry_current_A;

			/* Set the actuator id for logging the operation values */
			log_act_id = g_SysStatus.CTRY_Status[ctry_id].fl_act_id;


			/* OPER Mode - Log the first cycle of activation */
			if (E_OPER_MODE == g_SysStatus.Sys_Mode)
			{
				/* Log the actuator operation values */
				if(FALSE == Act_log_status[log_act_id])
				{
					/* Log measured values */
					log_flt_value = ((uint16_t)ctry_vibration_G);
					log_flt_value <<= 8U;
					log_flt_value |= (0xFFU & ctry_current_A);

					/* Write to logger */
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_ACTUAOR_MONITOR, log_flt_value,E_UNIT_TYPE_ACTUATOR,log_act_id,Spare_Bytes);
					Act_log_status[log_act_id] = TRUE;
				}
			}

			/* Set monitor time */
			Timer.last_monitor_time[ctry_id] = HAL_GetTick();
		}

		/* CTRY is active but not responde to status request */
		else if (((oper_handler.active_ctry[0U] == ctry_id) || (oper_handler.active_ctry[1U] == ctry_id)) &&\
				 (TRUE == g_SysStatus.CTRY_Status[ctry_id].failed_to_resp))
		{
			g_SysStatus.CTRY_Status[ctry_id].com_lost_during_act_activation = TRUE;

			/* Current active CTRY will be disabled by the flt handler - update oper_handler */
			if (oper_handler.active_ctry[0U] == ctry_id)
			{
				oper_handler.active_ctry[0U] = NO_ACTIVE_CTRY;
			}
			else
			{
				oper_handler.active_ctry[1U] = NO_ACTIVE_CTRY;
			}
		}
	}
	return hal_status;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
