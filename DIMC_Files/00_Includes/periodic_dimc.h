/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : periodic_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.5
** Updated   : 14-07-2021
**
** Description: Header for periodic_dimc.c file
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
**
** Rev 1.1	: 03-12-2020 -- Update actuator maintenance report struct now: 'Actuator_Maint_Data_t'  (there were 2 different struct).
** Rev 1.2	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.3	: 29-04-2021 -- Update the Actuator's map.
** Rev 1.4  : 04-07-2021 -- Update 'manual_act_activation_TypeDef' struct and move define values to .c file
** Rev 1.5  : 14-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _PERIODIC_DIMC_H_
#define _PERIODIC_DIMC_H_

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

#define CTRY_130H_ACT_ID_TABLE			{0x200U, 0x202U, 0x204U, 0x206}
#define CTRY_131H_ACT_ID_TABLE			{0x201U, 0x203U, 0x205U, 0x207}
#define CTRY_132H_ACT_ID_TABLE			{0x208U, 0x210U}
#define CTRY_133H_ACT_ID_TABLE			{0x209U, 0x211U}
#define CTRY_134H_ACT_ID_TABLE			{0x212U, 0x214U, 0x216U}
#define CTRY_135H_ACT_ID_TABLE			{0x213U, 0x215U, 0x217U}
#define CTRY_136H_ACT_ID_TABLE			{0x218U, 0x220U, 0x222U, 0x224U}
#define CTRY_137H_ACT_ID_TABLE			{0x219U, 0x221U, 0x223U, 0x225U}
#define CTRY_138H_ACT_ID_TABLE			{0x226U, 0x228U, 0x230U, 0x232U, 0x234U}
#define CTRY_139H_ACT_ID_TABLE			{0x227U, 0x229U, 0x231U, 0x233U, 0x235U}
#define CTRY_140H_ACT_ID_TABLE			{0x236U, 0x238U, 0x240U, 0x242U, 0x244U}
#define CTRY_141H_ACT_ID_TABLE			{0x237U, 0x239U, 0x241U, 0x243U, 0x245U}
#define CTRY_142H_ACT_ID_TABLE			{0x246U, 0x248U, 0x250U, 0x252U, 0x254U, 0x256U}
#define CTRY_143H_ACT_ID_TABLE			{0x247U, 0x249U, 0x251U, 0x253U, 0x255U, 0x257U}
#define CTRY_144H_ACT_ID_TABLE			{0x258U, 0x260U, 0x262U, 0x264U, 0x266U, 0x268U}
#define CTRY_145H_ACT_ID_TABLE			{0x259U, 0x261U, 0x263U, 0x265U, 0x267U, 0x269U}
#define CTRY_146H_ACT_ID_TABLE			{0x270U, 0x272U, 0x274U, 0x276U, 0x278U, 0x280U}
#define CTRY_147H_ACT_ID_TABLE			{0x271U, 0x273U, 0x275U, 0x277U, 0x279U, 0x281U}
#define CTRY_148H_ACT_ID_TABLE			{0x282U, 0x284U, 0x286U, 0x288U, 0x290U, 0x292U}
#define CTRY_149H_ACT_ID_TABLE			{0x283U, 0x285U, 0x287U, 0x289U, 0x291U, 0x293U}

#define CTRY_130H		{.actuator_table = {4U,6U,1U,3U}, 		.number_of_actuators = 4U,		.ctry_id = 0x01U,		.actuator_id_table = CTRY_130H_ACT_ID_TABLE}
#define CTRY_131H		{.actuator_table = {6U,4U,3U,1U}, 		.number_of_actuators = 4U,		.ctry_id = 0x02U,		.actuator_id_table = CTRY_131H_ACT_ID_TABLE}
#define CTRY_132H		{.actuator_table = {6U,4U}, 			.number_of_actuators = 2U,		.ctry_id = 0x03U,		.actuator_id_table = CTRY_132H_ACT_ID_TABLE}
#define CTRY_133H		{.actuator_table = {4U,6U}, 			.number_of_actuators = 2U,		.ctry_id = 0x04U,		.actuator_id_table = CTRY_133H_ACT_ID_TABLE}
#define CTRY_134H		{.actuator_table = {1U,4U,3U}, 			.number_of_actuators = 3U,		.ctry_id = 0x05U,		.actuator_id_table = CTRY_134H_ACT_ID_TABLE}
#define CTRY_135H		{.actuator_table = {3U,6U,1U}, 			.number_of_actuators = 3U,		.ctry_id = 0x06U,		.actuator_id_table = CTRY_135H_ACT_ID_TABLE}
#define CTRY_136H		{.actuator_table = {1U,3U,4U,6U}, 		.number_of_actuators = 4U,		.ctry_id = 0x07U,		.actuator_id_table = CTRY_136H_ACT_ID_TABLE}
#define CTRY_137H		{.actuator_table = {3U,1U,6U,4U}, 		.number_of_actuators = 4U,		.ctry_id = 0x08U,		.actuator_id_table = CTRY_137H_ACT_ID_TABLE}
#define CTRY_138H		{.actuator_table = {3U,1U,5U,4U,6U}, 	.number_of_actuators = 5U,		.ctry_id = 0x09U,		.actuator_id_table = CTRY_138H_ACT_ID_TABLE}
#define CTRY_139H		{.actuator_table = {1U,3U,5U,6U,4U}, 	.number_of_actuators = 5U,		.ctry_id = 0x0AU,		.actuator_id_table = CTRY_139H_ACT_ID_TABLE}
#define CTRY_140H		{.actuator_table = {1U,3U,2U,6U,4U}, 	.number_of_actuators = 5U,		.ctry_id = 0x0BU,		.actuator_id_table = CTRY_140H_ACT_ID_TABLE}
#define CTRY_141H		{.actuator_table = {3U,1U,2U,4U,6U}, 	.number_of_actuators = 5U,		.ctry_id = 0x0CU,		.actuator_id_table = CTRY_141H_ACT_ID_TABLE}
#define CTRY_142H		{.actuator_table = {1U,3U,2U,5U,4U,6U}, .number_of_actuators = 6U,		.ctry_id = 0x0DU,		.actuator_id_table = CTRY_142H_ACT_ID_TABLE}
#define CTRY_143H		{.actuator_table = {3U,1U,2U,5U,6U,4U}, .number_of_actuators = 6U,		.ctry_id = 0x0EU,		.actuator_id_table = CTRY_143H_ACT_ID_TABLE}
#define CTRY_144H		{.actuator_table = {1U,3U,2U,5U,4U,6U}, .number_of_actuators = 6U,		.ctry_id = 0x0FU,		.actuator_id_table = CTRY_144H_ACT_ID_TABLE}
#define CTRY_145H		{.actuator_table = {3U,1U,2U,5U,6U,4U}, .number_of_actuators = 6U,		.ctry_id = 0x10U,		.actuator_id_table = CTRY_145H_ACT_ID_TABLE}
#define CTRY_146H		{.actuator_table = {1U,3U,2U,5U,4U,6U}, .number_of_actuators = 6U,		.ctry_id = 0x11U,		.actuator_id_table = CTRY_146H_ACT_ID_TABLE}
#define CTRY_147H		{.actuator_table = {3U,1U,2U,5U,6U,4U}, .number_of_actuators = 6U,		.ctry_id = 0x12U,		.actuator_id_table = CTRY_147H_ACT_ID_TABLE}
#define CTRY_148H		{.actuator_table = {1U,3U,2U,5U,4U,6U}, .number_of_actuators = 6U,		.ctry_id = 0x13U,		.actuator_id_table = CTRY_148H_ACT_ID_TABLE}
#define CTRY_149H		{.actuator_table = {3U,1U,2U,5U,6U,4U}, .number_of_actuators = 6U,		.ctry_id = 0x14U,		.actuator_id_table = CTRY_149H_ACT_ID_TABLE}

#define ACTUATOR_ID_MAX     0x0293U
#define ACTUATOR_ID_MIN     0x0200U
#define ACTUATOR_ID_NONE    0x0000U



/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Structure for Maintenance Data parameters*/
typedef struct
{
	uint16_t Actuator_ID;			/*!< Specifies checked actuator's ID value (Range: 0x0200 - 0x0293 | Unit: None)*/
	uint8_t TestsResults;			/*!< Specifies test results value (Range: 0 - 3 | Unit: None)*/
	uint8_t VibrationValue;			/*!< Specifies vibration value (Range: var type | Unit: G)*/
	uint8_t CurrConsumption;        /*!< Specifies current value (Range: var type | Unit: 0.1Amp)*/
	int8_t TemperatureCpu;			/*!< Specifies current value (Range: var type | Unit: Celsius Deg)*/
	int8_t TemperatureDrv;			/*!< Specifies current value (Range: var type | Unit: Celsius Deg)*/
}Actuator_Maint_Data_TypeDef;

/*! Enumerator for actuator activation request status description*/
typedef enum
{
	E_NO_REQ = 0U,				/*!<Val:0 -  No request			 	 */
	E_NEW_REQ,					/*!<Val:0 -  New request			 */
	E_ACTIVE_REQ				/*!<Val:0 -  Active request			 */
}e_Act_req_Status;


/*! Enumerator for Actuator's Operation type description (Used to specify the operation type in CAN msg)*/
typedef enum
{
	E_ACT_OPER = 0U,			/*!<Val:0 -  OPER activation		 	 */
	E_ACT_IBIT,					/*!<Val:1 -  IBIT activation		 	 */
	E_ACT_MANUAL				/*!<Val:2 -  Manual activation		 	 */
}e_Actuator_OperType;


/*! Structure for manual actuator activation parameters*/
typedef struct
{
	uint8_t act_number;					/*!< Specifies activated actuator's number value (Range: 0 - 5 | Unit: None)	*/
	uint16_t actuator_id;				/*!< Specifies activated actuator's ID value (Range: 0x0200 - 0x0293 | Unit: None)	*/
	uint8_t ctry_num;					/*!< Specifies CTRY's number value (Range: 0 - 19 | Unit: None)					*/
	uint32_t start_time;				/*!< Specifies activation start time value (Range: var tye | Unit: milliseconds)*/
	e_Act_req_Status request_status;	/*!< Specifies activation request status value (Range: 0 - 2 | Unit: None)		*/
}manual_act_activation_TypeDef;



/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern manual_act_activation_TypeDef g_Actuator_Man_Req;
extern Actuator_Maint_Data_TypeDef g_Maint_ActuatorData;
/* -----Function prototypes --------------------------------------------------------*/
void Periodic_Tasks_Scheduler(void);

#endif /*_PERIODIC_DIMC_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/

