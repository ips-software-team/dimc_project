/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : exflash_dimc.h
** Author    : Omer Geron
** Revision  : 1.2
** Updated   : 11-07-2021
**
** Description: Header for exflash_dimc.c file
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 23-08-2020 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef EXFLASH_DIMC_H
#define EXFLASH_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"

/* -----Definitions ----------------------------------------------------------------*/
#define READ_FROM_TOP_OF_MEMORY		0xFFFFFFFFU		/* Read request is from the top of the memory */

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/



/*! Enumerator for External Flash BIT status*/
typedef enum
{
	E_EXFLASH_BIT_PASS = 0,        /*!<Val:0 -  Test Passed		*/
	E_EXFLASH_BIT_FAIL             /*!<Val:1 -  Test Failed		*/
}e_ExFlash_BIT_RESULT;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
HAL_StatusTypeDef Write_EEPROM(uint8_t *pData, uint8_t number_of_bytes_to_write);
HAL_StatusTypeDef Read_EEPROM(uint8_t *pData, uint16_t number_of_bytes_to_read, uint32_t address);
HAL_StatusTypeDef Erase_EEPROM(void);
HAL_StatusTypeDef Init_Mem_Hal(void);
e_ExFlash_BIT_RESULT Cbit_Mem_Test(void);
e_ExFlash_BIT_RESULT Pbit_Mem_Test(void);



#endif /*EXFLASH_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
