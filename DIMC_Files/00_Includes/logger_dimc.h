/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : logger_dimc.h
** Author    : Omer Geron
** Revision  : 2.2
** Updated   : 11-09-2023
**
** Description: Header for logger_dimc.c file
** 				This file contains functions prototype for the logger_dimc.c file and,
** 				all the System's event codes, unit types and relevat event propeties used to log system data in the data logger.
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 23-08-2020 -- Original version created.
** Rev 1.1	: 26-10-2020 -- "E_FC_CAN_BUS_COMM_FAIL" changed to: "E_FC_CTRAY_CAN_BUS_COMM_FAIL" (0x4001U)
** Rev 1.2	: 03-01-2021 -- Added new fault codes.
** 						  Updated due to verification remarks.
** Rev 1.3	: 13-04-2021 -- Added new fault codes.
** Rev 1.4	: 18-04-2021 -- Added over_temp fault_code.
** Rev 1.5	: 07-07-2021 -- Update fault codes.
** Rev 1.6  : 14-07-2021 -- Added doxygen comments.
** Rev 1.7	: 16-08-2021 -- Update fault codes.
** Rev 1.8	: 12-05-2022 -- Update fault codes.
** Rev 1.9	: 26-05-2022 -- Fix typo in fault code description.
** Rev 2.0  : 07-05-2023 -- Update fault codes.
** Rev 2.1	: 28-05-2023 -- Update fault codes.
** Rev 2.2	: 11-09-2023 -- Update fault codes.
**************************************************************************************
*/
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef LOGGER_DIMC_H
#define LOGGER_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/


#define LOGGER_EMPTY 			0U
#define LOGGER_NOT_EMPTY 		1U

#define MAX_NUMBER_OF_EVENTS 	0x3E7FU		/* 15,999 */
#define READ_FROM_LAST_EVENT	0xFFFFU		/* Special mark to note that the read request is from the current top of the memory */


/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Structure for the logger's data read parameters*/
typedef struct
{
	uint8_t  read_data;			/*!< Specifies read request status (TRUE \ FALSE) (Range: 0 - 1 | Unit: None)                  */
	uint16_t read_from_event;	/*!< Specifies index of event to read from (Range: 0xFFFF - 0 | Unit: None)                    */
}ReadFromLogger_t;



/* --- Event Data Fields Enums --- */

/*! Enumerator for event types*/
typedef enum{
	E_NA_EVENT_TYPE 	= 0x00U,	/*!<Val:0 -  NA event			*/
	E_SYSTEM_EVENT 		= 0x01U,	/*!<Val:1 -  System event		*/
	E_FAULT 			= 0x02U		/*!<Val:2 -  Fault event		*/
}e_EVENT_TYPES;


/*! Enumerator for event status types*/
typedef enum{
	E_VALID_EVENT = 0U,         /*!<Val:0 -  Valid event			*/
	E_EMPTY_EVENT               /*!<Val:1 -  Empty event			*/
}e_EVENT_STATUS;



/*! Enumerator for IPS units type for event logging */
typedef enum{
	E_UNIT_TYPE_IPS 		= 0x01U,		/*!<Val:1 -  IPS	 	*/
	E_UNIT_TYPE_DIMC 		= 0x02U,		/*!<Val:2 -  DIMC	 	*/
	E_UNIT_TYPE_DIAC 		= 0x03U,		/*!<Val:3 -  DIAC	 	*/
	E_UNIT_TYPE_CTRY 		= 0x04U,		/*!<Val:4 -  CTRY 		*/
	E_UNIT_TYPE_ACTUATOR 	= 0x05U			/*!<Val:5 -  Actuator	*/
}e_UNIT_TYPES;





/* Note:
 * FC: Fault code
 * EC: Event code
 */
/*! Enumerator for event codes */
typedef enum{
		/* IPS Faults and Events */
		E_FC_GIPSY_SYSTEM_REPORT 						= 0x1000U,                   /*!<Val:0x1000 -  IPS system report	 				*/
		E_FC_CHANNEL_A_FAULT_REPORT 					= 0x1001U,                   /*!<Val:0x1001 -  Channel A fault report	 			*/
		E_FC_CHANNEL_B_FAULT_REPORT						= 0x1002U,                   /*!<Val:0x1002 -  Channel B fault report	 			*/
		E_FC_CHANNEL_C_FAULT_REPORT 					= 0x1003U,                   /*!<Val:0x1003 -  Channel C fault report 				*/
		E_EC_OPERATION_START_RECEIVED 					= 0x1004U,                   /*!<Val:0x1004 -  Operation start command received		*/
		E_EC_OPERATION_ABORT_RECEIVED 					= 0x1005U,                   /*!<Val:0x1005 -  Operation abort command received	 	*/
		E_EC_OPERATION_MODE_STARTED 					= 0x1006U,                   /*!<Val:0x1006 -  Operation start	 					*/
		E_EC_OPERATION_MODE_TERMINATED 					= 0x1007U,                   /*!<Val:0x1007 -  Operation terminated	 				*/
		E_EC_IBIT_START_RECEIVD 						= 0x1008U,                   /*!<Val:0x1008 -  IBIT start command received			*/
		E_EC_IBIT_ABORT_RECEIVED 						= 0x1009U,                   /*!<Val:0x1009 -  IBIT abort command received	 		*/
		E_EC_IBIT_STARTED 								= 0x100AU,                   /*!<Val:0x100A -  IBIT start	 						*/
		E_EC_IBIT_ABORTED 								= 0x100BU,                   /*!<Val:0x100B -  IBIT aborted	 						*/
		E_EC_IBIT_COMPLETE 								= 0x100CU,                   /*!<Val:0x100C -  IBIT complete	 					*/
		E_FC_IBIT_FAILED 								= 0x100DU,                   /*!<Val:0x100D -  IBIT failed 							*/
		E_EC_ERASE_LOGGER_IRS_COMMAND_RECEIVED 			= 0x100EU,                   /*!<Val:0x100E -  Erase logger command received		*/
		E_FC_OPERATION_1_DISC_IS_ALWAYS_ACTIVE			= 0x100FU,                   /*!<Val:0x100F -  Operation channel 1 is always active	*/
		E_FC_OPERATION_2_DISC_IS_ALWAYS_ACTIVE			= 0x1010U,                   /*!<Val:0x1010 -  Operation channel 2 is always active	*/
		E_FC_IBIT_DISC_IS_ALWAYS_ACTIVE					= 0x1011U,                   /*!<Val:0x1011 -  IBIT channel is always active		*/
		E_EC_SAFE_MODE_ENTRY							= 0x1013U,                   /*!<Val:0x1013 -  Safe mode entry						*/
		E_FC_CTRAYS_IDENTIFICATION_FAILED				= 0x1014U,                   /*!<Val:0x1014 -  CTRYs identification failed  	 	*/
		E_FC_CTRYS_SW_VERSION_TEST_FAIL					= 0x1015U,                   /*!<Val:0x1015 -  CTRYs SW version test failure	 	*/
		E_FC_CTRYS_PBIT_DUPLICATE_ST_RESP				= 0x1016U,                   /*!<Val:0x1016 -  Duplicate response for PBIT requet	*/
		E_FC_CHANNEL_A_SENS_FAULT_REPORT 				= 0x1017U,                   /*!<Val:0x1017 -  Channel A sens fault report		 	*/
		E_FC_CHANNEL_B_SENS_FAULT_REPORT				= 0x1018U,                   /*!<Val:0x1018 -  Channel B sens fault report			*/
		E_FC_CHANNEL_C_SENS_FAULT_REPORT 				= 0x1019U,                   /*!<Val:0x1019 -  Channel C sens fault report		 	*/
		E_FC_IPS_RESET_REQUEST_RECEIVED 				= 0x101AU,                   /*!<Val:0x101A -  IPS Reset request received		 	*/
		E_FC_IPS_RESET_REQUEST_IGNORED 					= 0x101BU,                   /*!<Val:0x101B -  IPS Reset request ignored		 	*/
		E_FC_IPS_RESET_FAILED		 					= 0x101CU,                   /*!<Val:0x101C -  IPS Reset failed		 				*/

		E_FC_CHANNEL_A_TEST_FLT							= 0x101DU,                   /*!<Val:0x101D -  Channel A test failure 				*/
		E_FC_CHANNEL_B_TEST_FLT							= 0x101EU,                   /*!<Val:0x101E -  Channel B test failure 				*/
		E_FC_CHANNEL_C_TEST_FLT							= 0x101FU,                   /*!<Val:0x101F -  Channel C test failure 				*/

		/* DIMC Related faults */
		E_FC_DIMC_GENERAL_FAULT							= 0x2000U,                   /*!<Val:0x2000 -  DIMC general fault					*/
		E_FC_DIMC_INIT_FAILED							= 0x2001U,                   /*!<Val:0x2001 -  DIMC INIT failed						*/
		E_FC_CAN_BUS_INIT_FAILED						= 0x2002U,                   /*!<Val:0x2002 -  CAN BUS INIT failed				 	*/
		E_FC_DIMC_PBIT_FAILED							= 0x2003U,                   /*!<Val:0x2003 -  DIMC PBIT failed					 	*/
		E_FC_DIMC_CBIT_FAILED							= 0x2004U,                   /*!<Val:0x2004 -  DIMC CBIT failed					 	*/
		E_FC_DIMC_POWER_MONITOR_5VDC_FAILED				= 0x2005U,                   /*!<Val:0x2005 -  DIMC 5 volt failure				 	*/
		E_FC_CAN_BUS_ERROR_DETECTED						= 0x2006U,                   /*!<Val:0x2006 -  CAN BUS error						*/
		E_FC_UAV_422_COMM_ERROR							= 0x2007U,                   /*!<Val:0x2007 -  IRS-422 error			 			*/
		E_FC_UAV_422_COMM_CRC_FAILED					= 0x2008U,                   /*!<Val:0x2008 -  IRS-422 CRC error				 	*/
		E_FC_OPER_SIGNALS_INTEGRITY_TEST_FAILED			= 0x2009U,                   /*!<Val:0x2009 -  Operation channels test failed	 	*/
		E_FC_CAN_BUS_TRANSMIT_ERROR						= 0x200AU,                   /*!<Val:0x200A -  CAN BUS transmit error  				*/
		E_FC_DIMC_SANITY_CHECK_FAILURE					= 0x200BU,                   /*!<Val:0x200B -  At least 2 CTRYs reported 'Sanity check failure'	*/
		E_FC_DIMC_SW_ERROR								= 0x200CU,                   /*!<Val:0x200C -  DIMC SW error					 	*/
		E_EC_DIMC_STARTUP_COMPLETED						= 0x200DU,					 /*!<Val:0x200D -  DIMC startup completed			 	*/
		E_FC_DIMC_ACTIVATE_FLT_CH_A						= 0x200EU,                   /*!<Val:0x200E -  DIMC activate channel A				*/
		E_FC_DIMC_ACTIVATE_FLT_CH_B						= 0x200FU,                   /*!<Val:0x200F -  DIMC activate channel B				*/
		E_FC_DIMC_ACTIVATE_FLT_CH_C						= 0x2010U,                   /*!<Val:0x2010 -  DIMC activate channel C			 	*/

		/* DIAC Related faults */
		E_FC_DIAC_GENERAL_FAULT							= 0x3000U,                   /*!<Val:0x3000 -  DIAC general fault				 	*/
		E_FC_POWER_SUP_1_CHANNEL_A_FAULT_REPORT			= 0x3001U,                   /*!<Val:0x3001 -  Power supply 1 fault					*/
		E_FC_POWER_SUP_2_CHANNEL_A_FAULT_REPORT			= 0x3002U,                   /*!<Val:0x3002 -  Power supply 2 fault				 	*/
		E_FC_NO_POWER_OUTPUT_AT_POWER_SUP_1				= 0x3003U,                   /*!<Val:0x3003 -  No power from power supply 1 		*/
		E_FC_NO_POWER_OUTPUT_AT_POWER_SUP_2				= 0x3004U,                   /*!<Val:0x3004 -  No power from power supply 2	 		*/
		E_FC_POWER_SUP_1_CUT_OFF_FAIL					= 0x3005U,                   /*!<Val:0x3005 -  Power supply 1 cut off failed 		*/
		E_FC_POWER_SUP_2_CUT_OFF_FAIL					= 0x3006U,                   /*!<Val:0x3006 -  Power supply 2 cut off failed		*/
		E_FC_POWER_SUP_3_CHANNEL_A_FAULT_REPORT			= 0x3007U,                   /*!<Val:0x3007 -  Power supply 3 fault					*/
		E_FC_POWER_SUP_4_CHANNEL_A_FAULT_REPORT			= 0x3008U,                   /*!<Val:0x3008 -  Power supply 4 fault				 	*/
		E_FC_POWER_SUP_CUT_OFF							= 0x3009U,					 /*!<Val:0x3009 -  Power supply cut off					*/

		/* CTRAY Related faults */
		E_FC_CTRY_GENERAL_FAULT							= 0x4000U,                   /*!<Val:0x4000 -  CTRY general fault					*/
		E_FC_CTRY_CAN_BUS_COMM_FAIL						= 0x4001U,                   /*!<Val:0x4001 -  CTRY CAN BUS comunication failed		*/
		E_FC_CTRY_PBIT_FAILED							= 0x4002U,                   /*!<Val:0x4002 -  CTRY PBIT failed						*/
		E_FC_CTRY_CBIT_FAILED							= 0x4003U,                   /*!<Val:0x4003 -  CTRY CBIT failed						*/
		E_FC_CPU_BOARD_TEMP_HIGH						= 0x4004U,                   /*!<Val:0x4004 -  High temperature in CPU board		*/
		E_FC_CPU_BOARD_TEMP_LOW							= 0x4005U,                   /*!<Val:0x4005 -  Low temperature in CPU board			*/
		E_FC_DVR_BOARD_TEMP_HIGH						= 0x4006U,                   /*!<Val:0x4006 -  High temperature in DRV board		*/
		E_FC_DVR_BOARD_TEMP_LOW							= 0x4007U,                   /*!<Val:0x4007 -  Low temperature in DRV board			*/
		E_FC_CPU_BOARD_HEATER_FAIL						= 0x4008U,                   /*!<Val:0x4008 -  CPU board heater failed				*/
		E_FC_DVR_BORAD_HEATER_FAIL						= 0x4009U,                   /*!<Val:0x4009 -  DRV board heater failed				*/
		E_FC_OVER_CURRENT_DETECTED						= 0x400AU,                   /*!<Val:0x400A -  Over current consumption detected	*/
		E_FC_CTRAY_POWER_MONITOR_5VDC_FAILED			= 0x400BU,                   /*!<Val:0x400B -  CTRY 5 volt failure					*/
		E_FC_POWER_MONITOR_14_8_VDC_INPUT_FAILED		= 0x400CU,                   /*!<Val:0x400C -  CTRY 14.8 volt input failure			*/
		E_FC_POWER_MONITOR_14_8_VDC_MOTORS_FAILED		= 0x400DU,                   /*!<Val:0x400D -  CTRY 14.8 volt motor drivers input failure	*/
		E_FC_DRIVER_4_SPI_COMM_FAIL						= 0x400EU,                   /*!<Val:0x400E -  CTRY's driver 4 SPI communication failure */
		E_FC_DRIVER_5_SPI_COMM_FAIL						= 0x400FU,                   /*!<Val:0x400F -  CTRY's driver 5 SPI communication failure */
		E_FC_DRIVER_6_SPI_COMM_FAIL						= 0x4010U,                   /*!<Val:0x4010 -  CTRY's driver 6 SPI communication failure */
		E_FC_CTRY_INVALID_CAN_ADDRESS					= 0x4011U,                   /*!<Val:0x4011 -  Message received with wrong address	*/
		E_FC_CTRY_INVALID_CAN_ID						= 0x4012U,                   /*!<Val:0x4012 -  CAN BUS CTRY ID out of range			*/
		E_FC_CTRY_INVALID_MSG_TYPE						= 0x4013U,                   /*!<Val:0x4013 -  CAN BUS CTRY invalid message type	*/
		E_FC_CTRY_MOT_OPER_IN							= 0x4014U,	    			 /*!<Val:0x4014 -  CTRY's motor input test failed		*/
		E_FC_CTRY_OVER_TEMP								= 0x4015U,					 /*!<Val:0x4015 -  CTRY over temperature				*/
		E_FC_CTRY_NOT_RESP								= 0x4016U,                   /*!<Val:0x4016 -  CTRY not respond to CAN BUS message	*/
		E_FC_CTRY_SANITY_CHECK_FAILURE					= 0x4017U,                   /*!<Val:0x4017 -  CTRY reported sanity check failure	*/
		E_FC_CTRY_COM_LOST_DURING_ACT_ACTIVATION		= 0x4018U,                   /*!<Val:0x4018 -  CTRY COM lost during actuator activation	*/

		/* ACTUATOR Related faults */
		E_FC_ACTUATOR_DISABLED_BY_THE_CTRY				= 0x5000U,                   /*!<Val:0x5000 -  Actuator disabled by the ctry				*/
		E_FC_LOW_OPER_CURRENT							= 0x5001U,                   /*!<Val:0x5001 -  Actuator low operation current		*/
		E_FC_OVER_CURRENT								= 0x5002U,                   /*!<Val:0x5002 -  Actuator high operation current		*/
		E_FC_DISCONNECT_MOTOR							= 0x5003U,                   /*!<Val:0x5003 -  Actuator disconnect					*/
		E_FC_LOW_VIBRATION								= 0x5004U,                   /*!<Val:0x5004 -  Actuator low vibration				*/
		E_FC_HIGH_VIBRATION								= 0x5005U,                   /*!<Val:0x5005 -  Actuator high vibration				*/
		E_FC_WRONG_OPERATION_LIMITS						= 0x5006U,                   /*!<Val:0x5006 -  Actuator wrong operation limits		*/
		E_FC_DE_ACTIVATION_FLT							= 0x5007U,                   /*!<Val:0x5007 -  Actuator de activation failure		*/
		E_EC_ACTUAOR_MONITOR							= 0x5008U,					 /*!<Val:0x5008 -  Actuator Operation values			*/

		/* UAV Received Commands */
		E_EC_IRS_COMMAND_RECEIVED						= 0x6000U,                   /*!<Val:0x6000U -  IRS Command Received				*/
		E_EC_IRS_SW_VER_REPORT_REQ                      = 0x6001U,                   /*!<Val:0x6001U -  IRS SW versions report request received	*/
		E_EC_IRS_PBIT_RESULTS_REQ                       = 0x6002U,                   /*!<Val:0x6002U -  IRS PBIT report request recevied	*/
		E_EC_IRS_IBIT_RESULTS_REQ                       = 0x6003U,                   /*!<Val:0x6003U -  IRS IBIT report request recevied	*/
		E_EC_IRS_ACT_MAINTENANCE_REPORT_REQ             = 0x6004U,                   /*!<Val:0x6004U -  IRS ACT maintenance report request received */
		E_EC_IRS_MAINTENANCE_REPORT_REQ                 = 0x6005U,                   /*!<Val:0x6005U -  IRS maintenance report request received	*/
		E_EC_IRS_INVALID_DATA_REQ	                    = 0x6006U,                   /*!<Val:0x6006U -  IRS invalid data request received	*/
		E_EC_IRS_ACTUATOR_ACTIVATION_REQ	            = 0x6007U,                   /*!<Val:0x6007U -  IRS ACT activation request received */
		E_EC_IRS_SET_TIME_DATE_REQ                      = 0x6008U,                   /*!<Val:0x6008U -  IRS Set time date request receiced	*/
		E_EC_IRS_INVALID_COMMAND						= 0x6009U,					 /*!<Val:0x6009U -  IRS invalid command received		*/
		E_EC_IRS_MSG_RECEIVED_WITH_CRC_FLT				= 0x600AU,					 /*!<Val:0x600AU -  IRS message received with CRC FLT	*/
}e_EVENT_CODE;





/*! Structure for event's parameters*/
typedef struct
{
  uint8_t               	time_stamp[6U];     	    /*!< Specifies event's time [0](Range: 1 - 31 | Unit: day in a month)
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	[1](Range: 1 - 12 | Unit: month in a year)
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	[2](Range: 00 -99 | Unit: year (0=2000, 99=2099))
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	[3](Range: 0 - 23 | Unit: Hours)
  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	  	[4](Range: 0 - 59 | Unit: minutes)
	  	  	  	  	  	  	  														[5](Range: 0 - 59 | Unit: seconds)					*/

  e_EVENT_TYPES            	type_of_event;     			/*!< Specifies type of the event (Range: var type | Unit: None)					*/

  e_EVENT_CODE           	code_of_event;     			/*!< Specifies code of the event (Range: var type | Unit: None)					*/

  uint16_t					fault_value;				/*!< Specifies fault value (Range: var type | Unit: None)						*/

  e_UNIT_TYPES				type_of_unit;				/*!< Specifies type of unit (Range: var type | Unit: None)						*/

  uint8_t					unit_id;					/*!< Specifies unit ID (Range: 92 - 0 | Unit: None)							*/

  uint8_t					spare[3U];					/*!< Specifies spare bytes of the event record (Range: var type | Unit: None)	*/

}eventTypeDef;



/* -----Global variables -----------------------------------------------------------*/


/* -----External variables ---------------------------------------------------------*/

extern uint8_t g_Logger_Status;

/* -----Function prototypes --------------------------------------------------------*/
void Proccess_Logger_Request_from_IRS(void);
HAL_StatusTypeDef Write_Event_To_Memory(e_EVENT_TYPES event_type, e_EVENT_CODE event_code, uint16_t fault_value, e_UNIT_TYPES unit_type, uint8_t unit_id, uint8_t *spare_bytes);
HAL_StatusTypeDef Read_Events_From_Memory (uint16_t from_event_index);
HAL_StatusTypeDef Erase_Memory(void);
uint8_t Get_Logger_Status(void);
void Set_Read_Request(uint8_t read_data, uint16_t read_from_event);
void Set_Logger_Erase_Flag(void);

#endif /*LOGGER_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
