/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : crc16.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 03-01-2021
**
** Description: Header for crc16.c file.
**
** Notes:
*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 03-01-2021 -- Updated due to verification remarks.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CRC16_H
#define CRC16_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
uint16_t Calculate_CRC16(uint8_t *data, uint16_t length);
#endif /*_CRC16_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
