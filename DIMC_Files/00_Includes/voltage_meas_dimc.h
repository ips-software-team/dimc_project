/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : voltage_meas_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 11-07-2021
**
** Description: Header for voltage_meas_dimc.c file
**
** Notes:

*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 31-10-2020 -- Updated e_VoltageTestStatus enumerators, updated st_VoltageMeasParameters
** Rev 1.2	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.3	: 25-04-2021 -- Updated due to verification remarks.
** Rev 1.4  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef VOLTAGE_MEAS_DIMC_H
#define VOLTAGE_MEAS_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/*! Enumerator for voltage test and measurement status*/
typedef enum
{
	E_VOLT_TEST_OK = 0U,        /*!<Val:0 -  Voltage test OK 		*/
	E_VOLT_MEAS_FL = 1U,        /*!<Val:1 -  Voltage measurement Failed 	*/
	E_VOLT_TEST_FL = 2U			/*!<Val:2 -  Voltage test Failed 	*/
}e_VoltageTestStatus;

/*! Structure for voltage test parameters*/
typedef struct
{
	uint8_t              VoltageValue;            /*!< Specifies measured voltage value (Range: var type | Unit: 0.1 Volt)	*/
	e_VoltageTestStatus  VoltageTestResult;       /*!< Specifies voltage test status (Range: 0 - 1 | Unit: None)	        */
}st_VoltageMeasParameters;



/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_VoltageMeasParameters g_VoltageMeasurement;
/* -----Function prototypes --------------------------------------------------------*/
e_VoltageTestStatus Test_OnBoard_Voltage(void);

#endif /* VOLTAGE_MEAS_DIMC_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
