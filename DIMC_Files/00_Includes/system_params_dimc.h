/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : system_params_dimc.h
** Author    : Leonid Savchenko
** Revision  : 2.0
** Updated   : 01-06-2023
**
** Description: Header for system_params_dimc.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 16-03-2020 -- Original version created.
** Rev 1.1  : 21-10-2020 -- Added defines, added EquipedActuatorsNumber field to st_SysCtryActParams
** Rev 1.2  : 03-01-2021 -- Updated due to verification remarks.
** Rev 1.3	: 16-02-2021 -- The EquipedActuatorsNumber field in st_SysCtryActParams was removed.
** 						    Acutator's Id added to 'SysCtryParamTable' and a 'getter' function for this field.
** Rev 1.4	: 27-06-2021 -- Added IBIT speed parameter for each actuator.
** 						    Update the function "Get_Actuator_Speed".
** Rev 1.5  : 11-07-2021 -- Added doxygen comments
** Rev 1.6	: 25-07-2021 -- Update the header of function "Set_CTRY_All_Actuators_Status_FL".
** Rev 1.7	: 19-10-2021 -- Remove the header of the unused function "Get_Actuator_State".
** Rev 1.8	: 14-07-2022 -- Changing actuators Id format from hex numbers to decimal numbers.
** Rev 1.9	: 10-05-2023 -- IBIT limits definitions were added.
** Rev 2.0  : 01-06-2023 -- Update IBIT limits.
**************************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef SYSTEM_PARAMS_DIMC_H
#define SYSTEM_PARAMS_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
#include "diac_mngmnt.h"
#include "periodic_dimc.h"
/* -----Definitions ----------------------------------------------------------------*/
#define CTRY_MAX_NUM 		 20U
#define ALL_ACT_FL           0x3FU
#define ALL_ACT_OK           0x00U


/* Actuators IBIT limits */
#define IBIT_TEST_UPPER_CURRENT_LIMIT				(uint8_t) 150U
#define IBIT_TEST_LOWER_CURRENT_LIMIT				(uint8_t) 10U
#define IBIT_TEST_UPPER_VIBRATION_LIMIT				(uint8_t) 60U
#define IBIT_TEST_LOWER_VIBRATION_LIMIT				(uint8_t) 5U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for CTRY's actuators number*/
typedef enum
{
	E_ACT1 = 0U,              /*!<Val:0 -  Actuator #1 						*/
	E_ACT2,                   /*!<Val:1 -  Actuator #2 						*/
	E_ACT3,                   /*!<Val:2 -  Actuator #3 						*/
	E_ACT4,                   /*!<Val:3 -  Actuator #4 						*/
	E_ACT5,                   /*!<Val:4 -  Actuator #5 						*/
	E_ACT6,                   /*!<Val:5 -  Actuator #6 						*/
	E_ACT_MAX_NUM,            /*!<Val:6 -  Max number of CTRY's actuators	*/
}e_ActuatorsNum;

/*! Enumerator for actuator's installation status*/
typedef enum
{
	E_ACT_EQ = 0U,            /*!<Val:0 -  Actuator equipped 		*/
	E_ACT_NA,                 /*!<Val:1 -  Actuator not-equipped 	*/
}e_ActuatorState;

/*! Enumerator for actuator's operational status*/
typedef enum
{
	E_ACT_OK = 0U,           /*!<Val:0 -  Actuator OK 	    */
	E_ACT_FL,                /*!<Val:1 -  Actuator Fail 	*/
}e_ActuatorStatus;

/*! Enumerator for CTRY's operational status*/
typedef enum
{
	E_CTRY_OK = 0U,           /*!<Val:0 -  CTRY OK 			    */
	E_CTRY_FL,                /*!<Val:1 -  CTRY Fail 			*/
	E_CTRY_NA,                /*!<Val:2 -  CTRY Not Available   */
}e_CtryStatus;

/*! Enumerator for System's table monitoring test status*/
typedef enum
{
	E_MON_TBL_OK = 0U,        /*!<Val:0 -  Table monitoring passed*/
	E_MON_TBL_FL = 1U         /*!<Val:1 -  Table monitoring failed*/
}e_MonitTableStatus;

/*! Structure for actuator's limits parameters*/
typedef struct
{
	uint8_t CurrentMax[E_ACT_MAX_NUM];      /*!< Specifies current max limit value (Range: var type | Unit: 0.1 Amperes)*/
	uint8_t CurrentMin[E_ACT_MAX_NUM];      /*!< Specifies current min limit value (Range: var type | Unit: 0.1 Amperes)*/
	uint8_t AccelPowMax[E_ACT_MAX_NUM];     /*!< Specifies vibration max limit value (Range: var type | Unit: G)		*/
	uint8_t AccelPowMin[E_ACT_MAX_NUM];     /*!< Specifies vibration min limit value (Range: var type | Unit: G)		*/
}st_CtryActLimits;

/*! Structure for CTRY's actuators parameters*/
typedef struct
{
	e_ActuatorState  ActState[E_ACT_MAX_NUM];       /*!< Specifies actuators installation state (Range: 0 - 1 | Unit: None)                                  */
	uint8_t ActDefaultSpeed[E_ACT_MAX_NUM];         /*!< Specifies default speed value (Range: 0 - 100 | Unit: % PWM)                                        */
	uint8_t ActDefaultIBITSpeed[E_ACT_MAX_NUM];     /*!< Specifies IBIT default speed value (Range: 0 - 100 | Unit: % PWM)                                     */
	uint16_t ActId[E_ACT_MAX_NUM];                  /*!< Specifies actuators installation state (Range: 200 - 293 | Unit: None)                                  */
	st_CtryActLimits stActLimits;                   /*! Specifies structure for actuator's limits parameters (Range: var type | Unit: See struct declaration)*/
}st_SysCtryActParams;

/*! Structure for CTRY's and it's actuators statuses*/
typedef struct
{
	e_CtryStatus     CtryStatus;                       /*!< Specifies CTRY's operational status (Range: 0 - 2 | Unit: None)					    */
	e_ActuatorStatus  ActStatus[E_ACT_MAX_NUM];        /*!< Specifies each actuator's operational status (Range: 0 - 1 | Unit: None)			*/
	uint8_t          CtryActuatorsStatus;              /*!< Specifies CTRY's Actuators' operational status (Range: 0x00 - 0x3F | Unit: None)	*/
}st_SysCtryActStatus;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
HAL_StatusTypeDef Get_Actuator_CurrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *CurrLim_Max, uint8_t *CurrLim_Min);
HAL_StatusTypeDef Get_Actuator_VibrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *VibrLim_Max, uint8_t *VibrLim_Min);

void Init_Ctrys_Status_Table(void);
HAL_StatusTypeDef Get_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus *Act_Status);
HAL_StatusTypeDef Get_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus *Ctry_Status);
HAL_StatusTypeDef Get_CTRY_All_Actuators_Status(uint8_t Ctry_Id, uint8_t *All_Act_Status);
HAL_StatusTypeDef Set_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus Act_Status);
HAL_StatusTypeDef Set_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus Ctry_Status);
HAL_StatusTypeDef Set_CTRY_All_Actuators_Status_FL(uint8_t Ctry_Id);
e_MonitTableStatus Monitor_Status_Table(void);
HAL_StatusTypeDef Get_Actuator_Speed(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *Act_Speed, e_Actuator_OperType Oper_Type);
HAL_StatusTypeDef Get_Equipped_Actuators (uint8_t ctry_id, uint8_t *equipped_act);
HAL_StatusTypeDef Get_CTRY_DIAC_Ch(uint8_t Ctry_Id, e_DIAC_Num *Ctry_Diac_Ch);
HAL_StatusTypeDef Get_Actuator_Id(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint16_t *Act_Id);
#endif /*SYSTEM_PARAMS_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
