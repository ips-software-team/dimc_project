/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : cbit_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 04-07-2021
**
** Description: Header for cbit_dimc.c file
** 		        This file contains procedure that preform the CBIT test procedure.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2	: 25-04-2021 -- Update the file description (header) due to verification remarks.
** Rev 1.3  : 04-07-2021 -- Change CBIT return value to void.
** 						  Removed test result enum.
**
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CBIT_DIMC_H
#define CBIT_DIMC_H


/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/


/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
void Perform_CBIT_Test(void);

#endif /*CBIT_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
