 /************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : mcu_config_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.0
** Updated   : 28-10-2020
**
** Description: Header for mcu_config_dimc.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 28-10-2020 -- Original version created.
**
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MCU_CONFIG_DIMC_H
#define MCU_CONFIG_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
#include "gipsy_hal_i2c_dimc.h"
#include "gipsy_hal_uart.h"
#include "gipsy_hal_gpio.h"
/* -----Definitions ----------------------------------------------------------------*/



/*Analog Input Pins definition*/
#define V5VDCLVL_PIN	   	    GPIO_PIN_0
#define ADC1_PORT	            GPIOB

/*Debug Led Output Pins definition*/
#define SW_RUN_LED_PIN	   	GPIO_PIN_4 /*LED_1*/
#define FLT_LED_PIN   	    GPIO_PIN_5 /*LED_2*/
#define LED_PORT	        GPIOC

/*Cut Of Power Output Pins definition*/
#define COP_TTL_1_PIN     GPIO_PIN_6
#define COP_TTL_2_PIN     GPIO_PIN_7
#define COP_TTL_3_PIN     GPIO_PIN_9
#define COP_TTL_4_PIN     GPIO_PIN_12
#define COP_TTL_PORT      GPIOC

/*Fault Channels Output Pins definition*/
#define CHANNEL_A_FAULT_TTL_PIN         GPIO_PIN_8
#define CHANNEL_C_FAULT_TTL_PIN         GPIO_PIN_9
#define CHANNEL_B_FAULT_TTL_1_PIN       GPIO_PIN_4
#define CHANNEL_B_FAULT_TTL_2_PIN       GPIO_PIN_1
#define CHANNEL_FAULT_TTL_OUT_PORT      GPIOB

/*BIT Operate Output Pins definition*/
#define CPU_BIT_OPERATE_1_PIN         GPIO_PIN_11
#define CPU_BIT_OPERATE_2_PIN         GPIO_PIN_12
#define BIT_OPERATE_PORT      		  GPIOD

/*External Watch-Dog strobe Output Pin definition*/
#define WDI_PIN	   	    GPIO_PIN_15
#define WDI_PORT	        GPIOA

/*Fault Channels Input Pins definition*/
#define CHANNEL_A_FAULT_TTL_1_PIN         GPIO_PIN_0 /* diac ch 1 */
#define CHANNEL_A_FAULT_TTL_2_PIN         GPIO_PIN_1 /* diac ch 2 */
#define CHANNEL_A_FAULT_TTL_3_PIN         GPIO_PIN_3 /* diac ch 4 */
#define CHANNEL_A_FAULT_TTL_4_PIN         GPIO_PIN_4 /* diac ch 5 */

#define CHANNEL_C_FAULT_TTL_1_PIN         GPIO_PIN_6
#define CHANNEL_C_FAULT_TTL_2_PIN         GPIO_PIN_7
#define CHANNEL_C_FAULT_TTL_3_PIN         GPIO_PIN_8
#define CHANNEL_C_FAULT_TTL_4_PIN         GPIO_PIN_9
#define CHANNEL_C_FAULT_TTL_5_PIN         GPIO_PIN_10
#define CHANNEL_C_FAULT_TTL_6_PIN         GPIO_PIN_11
#define CHANNEL_FAULT_TTL_IN_PORT         GPIOE

/*OPER Input Pins definition*/
#define OPER_TTL_1_PIN         GPIO_PIN_12
#define OPER_TTL_2_PIN         GPIO_PIN_13
#define OPER_1_2_TTL_PIN       GPIO_PIN_15
#define OPER_TTL_PORT          GPIOE

/*IBIT Input Pin definition*/
#define IBIT_TTL_PIN           GPIO_PIN_14
#define IBIT_TTL_PORT          GPIOE

/*Fault Sense Input Pins definition*/
#define FAIL_A_SENS_TTL_PIN         GPIO_PIN_2
#define FAIL_B_SENS_TTL_PIN         GPIO_PIN_3
#define FAIL_C_SENS_TTL_PIN         GPIO_PIN_4
#define FAIL_SENS_TTL_PORT          GPIOD


/*Peripheral Pins definition*/
#define TXCAN_1_PIN         GPIO_PIN_1
#define RXCAN_1_PIN         GPIO_PIN_0
#define CAN_PORT            GPIOD

#define U3_TX_PIN           GPIO_PIN_10
#define U3_RX_PIN           GPIO_PIN_11
#define DEBUG_UART_PORT     GPIOC

#define RS422_TX0_PIN       GPIO_PIN_9
#define RS422_RX0_PIN       GPIO_PIN_10
#define RS422_DE0_PIN       GPIO_PIN_11
#define RS422_RE0_PIN       GPIO_PIN_12
#define RS422_SER_PORT      GPIOA

#define EX_FLASH_SCL_PIN    GPIO_PIN_10
#define EX_FLASH_SDA_PIN    GPIO_PIN_11
#define EX_FLASH_WP_PIN     GPIO_PIN_5
#define EX_FLASH_PORT       GPIOB

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/



/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

extern I2C_HandleTypeDef     g_HandleI2C2_ExFlash;
extern UART_HandleTypeDef    g_HandleUart1Irs;
#ifdef _DEBUG_
extern UART_HandleTypeDef g_HandleUart3Debug;
#endif

/* -----Function prototypes --------------------------------------------------------*/
void MCU_Periph_Config(void);
HAL_StatusTypeDef MCU_CAN_Interface_Init(void);

#endif /*MCU_CONFIG_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
