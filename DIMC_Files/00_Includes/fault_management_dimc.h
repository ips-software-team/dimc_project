/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : fault_management_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.1
** Updated   : 04-07-2021
**
** Description: Header for fault_management_dimc.c file
** 		        This file contains procedures that define possible DIMC faults
** 		        and perform fault management on fault occurrence.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 04-07-2021 -- Removed the function and global variable that were responsible for counting SW erros.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef FAULT_MANAGEMENT_DIMC_H
#define FAULT_MANAGEMENT_DIMC_H


/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/




/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

void Fault_Management(void);
#endif /*FAULT_MANAGEMENT_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
