/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : pbit_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 11-07-2021
**
** Description: - .h file for pbit_dimc.c file.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 02-10-2019 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2	: 18.04-2021 -- Adding the "send_operation_limits" function and supported variable.
** Rev 1.3	: 30.06-2021 -- Removed the external varible "st_Ctry_Lim_Resp[]".
** Rev 1.4  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef PBIT_DIMC_H
#define PBIT_DIMC_H


/* -----Includes ---------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
#include "system_params_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/

/*! Enumerator for Limits types description*/
typedef enum
{
	E_OPER_LIM = 0U,					/*!<Val:0 -  OPER Limits			 */
	E_IBIT_LIM,							/*!<Val:1 -  IBIT Limits			 */
}e_Oper_Lim_Type;

/*! Structure for actuators limits response parameters*/
typedef struct
{
	e_Oper_Lim_Type oper_lim_type;		/*!< Specifies limits type (Range: 0 - 1 | Unit: None)							*/
	uint8_t max_current;				/*!< Specifies maximal current limits value (Range: var type | Unit: 0.1 Amp)	*/
	uint8_t min_current;				/*!< Specifies minimal current limits value (Range: var type | Unit: 0.1 Amp)	*/
	uint8_t max_vibration;				/*!< Specifies maximal vibration limits value (Range: var type | Unit: G)		*/
	uint8_t min_vibration;				/*!< Specifies minimal vibration limits value (Range: var type | Unit: G)		*/
	uint8_t resp;						/*!< Specifies response status (Range: 0 - 1 | Unit: None)						*/
}st_Act_Lim_Resp;

/*! Structure for CTRY limits response parameters*/
typedef struct
{
	st_Act_Lim_Resp act_resp[E_ACT_MAX_NUM];	/*!< Specifies structure for actuators limits response parameters (Range: var type | Unit: See type description)*/
}st_Ctry_Lim_Resp;

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes -----------------------------------------------------*/
void Perform_DIMC_PBIT_Test(void);
void Perform_System_PBIT_Test(void);


#endif /*PBIT_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
