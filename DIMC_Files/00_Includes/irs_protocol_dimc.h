/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : irs_protocol_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 17-08-2023
**
** Description: Header for irs_protocol_dimc.c file
** 		        This file contains definition of RS-422 interface protocol used by
** 		        DIMC unit.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 02-01-2020 -- Added Safe Mode Entry IRS command.
** Rev 1.2  : 11-07-2021 -- Added doxygen comments.
** Rev 1.3  : 07-05-2023 -- Added IPS Reset Request command.
** Rev 1.4	: 17-08-2023 -- Fixing 'e_Last_Rec_Msg_Status' values.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef IRS_PROTOCOL_DIMC_H
#define IRS_PROTOCOL_DIMC_H





/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
#include "gipsy_hal_uart.h"
/* -----Definitions ----------------------------------------------------------------*/
#define SOM_VAL       0x5533U
#define SOM_VAL_HIGH  0x55U
#define SOM_VAL_LOW   0x33U

#define MSG_HEADER_BASE_PTR           0U
#define MSG_HEADER_SOM_VAL_HIGH_PTR   MSG_HEADER_BASE_PTR
#define MSG_HEADER_SOM_VAL_LOW_PTR    (MSG_HEADER_BASE_PTR + 1U)
#define MSG_HEADER_MSG_LENGTH_PTR	  (MSG_HEADER_BASE_PTR + 2U)
#define MSG_HEADER_MSG_COUNTER_PTR	  (MSG_HEADER_BASE_PTR + 3U)
#define MSG_HEADER_MSG_STATUS_PTR	  (MSG_HEADER_BASE_PTR + 4U)

#define MSG_TYPE_PTR	              (MSG_HEADER_BASE_PTR + 5U)
#define MSG_DATA_BYTE_PTR	          (MSG_HEADER_BASE_PTR + 6U)

#define MSG_HEADER_LENGTH_BYTES       5U
#define MSG_MINIMAL_LENGTH_BYTES      6U
#define MSG_CCSUM_LENGTH_BYTES        2U

/*IRS Request Commands from UAV*/
#define IRS_DATA_REQUEST_REQ           0x10U
#define IRS_ACTUATOR_ACTIVATION_REQ    0x20U
#define IRS_LOG_ERASE_REQ              0x30U
#define IRS_SET_TIME_DATE_REQ          0x40U
#define IRS_LOG_DATA_REQ               0x50U
#define IRS_IPS_RESET_REQ              0x60U

/*IRS Response Commands to UAV*/
#define IRS_IBIT_RESULT_RESP           0x13U
#define IRS_VERSION_RESP               0x11U
#define IRS_PBIT_RESULT_RESP           0x12U
#define IRS_ACT_MAINT_RESP             0x14U
#define IRS_SYS_MAINT_RESP             0x15U
#define IRS_LOG_DATA_RESP              0x55U

/*IRS Periodical Command to UAV*/
#define IRS_SYS_OPER_STATUS_RESP       0x01U
#define SYS_OPER_STATUS_RESP_DATA_LENGTH_BYTES       30U

#define DATA_REQUEST_VER_REPORT_ID            0x01U
#define VER_REPORT_DATA_LENGTH_BYTES          43U

#define DATA_REQUEST_PBIT_RESULTS_REPORT_ID   0x02U
#define PBIT_RESULTS_DATA_LENGTH_BYTES        22U

#define DATA_REQUEST_IBIT_RESULTS_REPORT_ID   0x03U
#define IBIT_RESULTS_DATA_LENGTH_BYTES        23U

#define DATA_REQUEST_ACT_MAINT_REPORT_ID      0x04U
#define ACT_MAINT_DATA_LENGTH_BYTES           8U

#define DATA_REQUEST_MAINT_REPORT_ID          0x05U
#define MAINT_REPORT_DATA_LENGTH_BYTES        14U

#define IRS_TIME_DATE_REQ_START_PTR            6U

#define IRS_LOG_DATA_REQ_START_HIGH_PTR        6U
#define IRS_LOG_DATA_REQ_START_LOW_PTR         (IRS_LOG_DATA_REQ_START_HIGH_PTR + 1U)

#define ACT_MAINT_RESP_DATA_LENGTH_BYTES       8U

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/*! Enumerator for Last IRS message status description*/
typedef enum
{
	E_LAST_MSG_OK              = 0x00U,               /*!<Val:0x00 - Last message OK 			*/
	E_LAST_MSG_CNT_ERR         = 0x01U,               /*!<Val:0x01 - Last message counter error	*/
	E_LAST_MSG_TYPE_ERR        = 0x02U,               /*!<Val:0x02 - Last message type error	*/
	E_LAST_MSG_LENGTH_ERR      = 0x03U,               /*!<Val:0x03 - Last message length error	*/
	E_LAST_MSG_CRC_ERR         = 0x04U                /*!<Val:0x04 - Last message CRC error		*/
}e_Last_Rec_Msg_Status;

/*! Enumerator for Mode status description for OPER Command*/
typedef enum
{
	E_OPER_CMD_STBY   = 1U,                           /*!<Val:1 - Mode STBY			*/
	E_OPER_CMD_OPERATION,                             /*!<Val:2 - Mode OPER 		*/
	E_OPER_CMD_IBIT_START,                            /*!<Val:3 - Mode IBIT Started */
	E_OPER_CMD_IBIT_ABORT                             /*!<Val:4 - Mode IBIT Aborted */
}e_Oper_Command_Msg;

/*! Enumerator for command type description for Data Request Message*/
typedef enum
{
	E_DATA_REQ_VERSION   = 1U,                         /*!<Val:1 - Version 						*/
	E_DATA_REQ_PBIT_RESULT,                            /*!<Val:2 - PBIT Result 					*/
	E_DATA_REQ_IBIT_RESULT,                            /*!<Val:3 - IBIT Result 					*/
	E_DATA_REQ_ACT_MAINT_REPORT,                       /*!<Val:4 - Actuator Maintenance Report  */
	E_DATA_REQ_MAINT_DATA                              /*!<Val:5 - Actuator Maintenance Data 	*/
}e_Data_Request_Msg;

/* -----Function prototypes --------------------------------------------------------*/
void HAL_UART1_RxCpltCallback(UART_HandleTypeDef *huart);
void IRS_UART1_Serial_Init(void);
void Parse_IRS_Command(void);

void IRS_Send_Msg_Maintenance_Report(void);
void IRS_Send_Msg_Log_Data_Report(uint8_t* log_data, uint8_t size);
void IRS_Send_Msg_Maintanance_Actuator_Data(void);
void IRS_Send_Msg_Operational_Status_Report(void);
#endif /*IRS_PROTOCOL_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
