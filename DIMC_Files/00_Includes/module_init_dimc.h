/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : module_init_dimc.h
** Author    : Leonid Savchenko
** Revision  : 2.2
** Updated   : 28-05-2023
**
** Description: Header for module_init_dimc.c file
** 		        This file contains procedures for DIMC unit initialization
** 		        and Power-Up tests
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 02-09-2020 -- Extending st_System_Operational_Status struct.
** Rev 1.2	: 21-09-2020 -- Adding the following defines:
** 													1. DIMC_ID.
** 													2. IPS_ID.
** Rev 1.3  : 25-10-2020 -- Added respond_time and transmit_time variables to the CTRY_Status struct.
**
** Rev 1.4	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.5	: 18-04-2021 -- Added over_temp and under_temp varibles to CTRY_Status struct.
** Rev 1.6	: 25-04-2021 -- Delete unsued variable "DIMC_RTC_Test_Status".
** Rev 1.7	: 06-07-2021 -- Added the data field to log CTRY's report of abnormal actuator activation.
** 						  Added the  data field "pbit_resp" that will be used to identify duplicate CTRY response.
** 						  Added the data field "Power_Switch".
** 						  Delete 'Enter_Safe_Mode_Req' varible. not supported anymore.
** 						  Added the data field "over_current_rep_time".
** 						  Added the data field "over_current_fl".
** 						  Added SW error data field to 'g_SysStatus'.
** 						  Update the IBIT test result structure.
** Rev 1.8  : 11-07-2021 -- Added doxygen comments.
** 							Removed unused variable "STBY_Entry_Time".
** 							Removed unused variable "ChannelB_FaultsStatus".
** 							Removed unused variable "Temp_Sens_Status".
** Rev 1.9	: 03-02-2022 -- Update the struct 'st_CTRY_Status'.
** Rev 2.0	: 10-08-2022 -- Disable IBIT Retry capability.
** Rev 2.1  : 07-05-2023 -- Removing unused variables:
** 								- 'over_current_detect_time'
** 								- 'over_current_flt'
** 								- fl_act_number
** Rev 2.2  : 28-05-2023 --	Change variable name from 'act_de_activation_fl' to 'com_lost_during_act_activation'
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MODULE_INIT_DIMC_H
#define MODULE_INIT_DIMC_H

/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
#include "system_params_dimc.h"
/* -----Definitions ----------------------------------------------------------------*/

#define CTRY_SW_ERR_VAL  (uint16_t)0xFFFFU

#define CTRY_ERR_ID   99U

#define CTRY_ID_REF_VALUE  			0x7E
#define CTRY_ACTUATORS_MAX_NUM   	6U
#define MAX_TEMP_SENSORS_ON_BOARD  	4U
#define HEATER_ON          			1U
#define HEATER_OFF         			0U


/* Rev 1.2 - start */
#define DIMC_ID				1U
#define IPS_ID				1U



/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/


/*! Enumerator for CTRY board temperature status types*/
typedef enum
{
	E_BOARD_TEMP_OK     = 0U,		/*!<Val:0 -  Temperature OK			 				*/
	E_BOARD_UNDER_TEMP,             /*!<Val:1 -  Under-Temperature			 			*/
	E_BOARD_OVER_TEMP,              /*!<Val:2 -  Over-Temperature		 				*/
	E_BOARD_TEMP_FL                 /*!<Val:3 -  All board's temperature sensors are FL	*/
}e_CtryBoardTempStatus;

/*! Enumerator for CTRY IBIT status types*/
typedef enum
{
	E_IBIT_PASSED  = 1U,             /*!<Val:1 -  IBIT Passed	 */
	E_IBIT_FAILED,                   /*!<Val:2 -  IBIT Failed	 */
	E_IBIT_NOT_DONE,                 /*!<Val:3 -  IBIT Not Done	 */
	E_IBIT_IN_PROCESS,               /*!<Val:4 -  IBIT In Process*/
	E_IBIT_ABORTED                   /*!<Val:5 -  IBIT Aborted	 */
}e_IBIT_Status;

/*! Enumerator for system modes types*/
typedef enum
{
	E_INIT_MODE   = 1U,              /*!<Val:1 -  INIT Mode	*/
	E_STBY_MODE,                     /*!<Val:2 -  STBY Mode	*/
	E_OPER_MODE,                     /*!<Val:3 -  OPER Mode */
	E_IBIT_MODE,                     /*!<Val:4 -  IBIT Mode */
	E_SAFE_MODE                      /*!<Val:5 -  SAFE Mode */
}e_SystemModesTypes;

/*! Enumerator for CTRY boards types*/
typedef enum CTRY_Boards
{
	E_CPU_BOARD          = 0U,       /*!<Val:0 -  CPU Board			 				*/
	E_DRV_BOARD,                     /*!<Val:1 -  Driver Board		 				*/
	E_CTRY_NUM_OF_BOARDS             /*!<Val:2 -  Total number of CTRY's boards		*/
}e_CTRY_Boards;




/*! Structure for CTRY's board temperature parameters*/
typedef struct
{
	uint8_t  Sensors_Status;                                  /*!< Specifies board's temp. sensors status (Range: var type | Unit: None)                  */
	uint8_t  Senors_Temperature[MAX_TEMP_SENSORS_ON_BOARD];   /*!< Specifies board's temp. sensors temperature value (Range: var type | Unit: Celsius Deg)*/
	uint8_t  Heater_Status;                                   /*!< Specifies board's heater status  (Range: 0 - 1 | Unit: None)                           */
}st_CTRY_Board_Temperature;

/*! Structure for CTRY's status parameters*/
typedef struct
{
	uint8_t  Ctry_ID_Test_Status;                                 /*!< Specifies CTRY's ID test result (Range: 0 -  | Unit: None)*/
	uint8_t  Ctry_Mot_Oper_In_Test_Status;                        /*!< Specifies CTRY's motor oper input test result (Range: 0 - 1 | Unit: None)*/
	uint8_t  Ctry_Status;                                         /*!< Specifies CTRY's status ('OK' \ 'FL') (Range: 0 - 1 | Unit: None)*/
	uint16_t Ctry_Failure_Desc;                                   /*!< Specifies CTRY's CBIT failure description (Range: var type | Unit: None)*/
	uint8_t  CH_C_Status;                                         /*!< Specifies CTRY's channel C state (as monitored by the CTRY) (Range: 0 - 1 | Unit: None)*/
	uint8_t  Mot_Oper_In_State;                                   /*!< Specifies CTRY's motor oper input state (as monitored by the CTRY) (Range: 0 - 1 | Unit: None)*/
	uint8_t	 Power_Switch;                                        /*!< Specifies CTRY's power switch report (Range: 0 - 1 | Unit: None)*/
	uint8_t  VibrationValue;                                      /*!< Specifies CTRY's vibration value (Range: var type | Unit: G)*/
	uint8_t  CurrentValue;                                        /*!< Specifies CTRY's current consumption value (Range: var type | Unit: 0.1Amp)*/
	uint8_t  ActuatorsStatus;                                     /*!< Specifies CTRY's actuator statuses (Range: 0 - 0x3F | Unit: None)*/
	uint8_t  Abnormal_Actuator_Report;                            /*!< Specifies detection of abnormal actuator (Range: 0 - 1 | Unit: None)*/
	st_CTRY_Board_Temperature Brd_Temp[E_CTRY_NUM_OF_BOARDS];     /*!< Specifies Temperatures report from the CTRY (Range: var type | Unit: None)*/
	uint32_t transmit_time;                                       /*!< Specifies transmit time of the last message request by the DIMC to the CTRY (Range: var type | Unit: None)*/
	volatile uint32_t respond_time;                               /*!< Specifies respond time of the last message received from the CTRY (Range: var type | Unit: None)*/
	uint8_t failed_to_resp;                                       /*!< Specifies CTRY's failure of responding to CAN message (Range: 0 - 1 | Unit: None)*/
	uint8_t com_lost_during_act_activation;                       /*!< Specifies communication lost during actuator activation (Range: 0 - 1 | Unit: None)*/
	uint8_t fl_act_id;                                            /*!< Specifies ID of the failed actuator (Range: 0 - 92 | Unit: None)*/
	uint8_t dup_resp_fl;                                          /*!< Specifies more than one CTRY respond to PBIT request with the same id  (Range: 0 - 1 | Unit: None)*/
	uint8_t over_temp;                                            /*!< Specifies CTRY's report of over temperature (Range: 0 - 1 | Unit: None)*/
	uint8_t pbit_resp;                                            /*!< Specifies response of the CTRY to PBIT request (Range: 0 - 1 | Unit: None)*/
	uint8_t over_current_rep;                                     /*!< Specifies CTRY's report of over current consumption (Range: 0 - 1 | Unit: None)*/
}st_CTRY_Status;

/*! Structure for CTRY's PBIT status parameters*/
typedef struct
{
	 uint8_t  PBIT_Status;                            /*!< Specifies CTRY's PBIT status (Range: 0 - 1 | Unit: None)                */
	 uint16_t PBIT_Failure_Desc;                      /*!< Specifies CTRY's PBIT failures descriptor (Range: var type | Unit: None)*/
}st_CTRY_PBIT_Status;

/*! Structure for system's operational status parameters*/
typedef struct
{
	e_SystemModesTypes Sys_Mode;                           /*!< Specifies system's current mode (Range: 1 - 5 | Unit: None)						*/
	uint8_t DiscretesStatus;                               /*!< Specifies discretes status (Range: 0 - 0x3F | Unit: None)						*/
	uint8_t Can_Bus_Error;                                 /*!< Specifies CAN BUS error (Range: 0 - 1 | Unit: None)								*/
	uint8_t Can_Bus_Tr_error;                              /*!< Specifies CAN BUS transmitting error (Range: 0 - 1 | Unit: None)				*/
	uint8_t Can_Bus_Msg_Type_Error;                        /*!< Specifies CAN BUS message type error (Range: 0 - 1 | Unit: None)				*/
	uint8_t Can_bus_Address_Error;                         /*!< Specifies CAN BUS address error (Range: 0 - 1 | Unit: None)						*/
	uint8_t Can_Bus_Ctry_Id_Error;                         /*!< Specifies CAN BUS CTRY ID error (Range: 0 - 1 | Unit: None)						*/
	uint8_t ChannelA_FaultsStatus;                         /*!< Specifies CH_A faults status (Range: 0 - 0x0F | Unit: None)						*/
	uint8_t ChannelC_FaultsStatus;                         /*!< Specifies CH_C faults status (Range: 0 - 0x3F | Unit: None)						*/
	uint8_t DIMC_CBIT_Status;                              /*!< Specifies DIMC CBIT status (Range: 0 - 1 | Unit: None)							*/
	uint8_t DIMC_PBIT_Status;                              /*!< Specifies DIMC PBIT status (Range: 0 - 1 | Unit: None)							*/
	uint8_t DIMC_CBIT_Failure_Desc;                        /*!< Specifies DIMC CBIT failure description (Range: var type | Unit: None)			*/
	uint8_t DIMC_PBIT_Failure_Desc;                        /*!< Specifies DIMC PBIT failure description (Range: var type | Unit: None)			*/
	uint32_t CTRYs_Status;                                 /*!< Specifies all CTRYs statuses (Range: 0x00000000 - 0x000FFFFF | Unit: None)		*/
	uint32_t All_CTRYs_PBIT_Status;                        /*!< Specifies all CTRYs PBIT statuses (Range: 0x00000000 - 0x000FFFFF | Unit: None)	*/
	st_CTRY_PBIT_Status CTRY_PBIT_Status[CTRY_MAX_NUM];    /*!< Specifies CTRY PBIT status(Range: var type | Unit: See struct description)		*/
	st_CTRY_Status CTRY_Status[CTRY_MAX_NUM];              /*!< Specifies CTRY statuses (Range: var type | Unit: See struct description)		*/
	uint8_t Logger_FL;                                     /*!< Specifies logger failure (Range: 0 - 1 | Unit: None)							*/
	uint8_t SW_Erros;                                      /*!< Specifies detected software errors (Range: 0 - 1 | Unit: None)										*/
}st_System_Operational_Status;

/*! Structure for tests statuses performed by DIMC*/
typedef struct
{
	uint8_t DIMC_Volt_Test_Status;                          /*!< Specifies voltage test status (Range: 0 - 1 | Unit: None)					*/
	uint8_t DIMC_Can_Bus_Init_Test_Status;                  /*!< Specifies CAN init test status (Range: 0 - 1 | Unit: None)					*/
	uint8_t DIMC_Ch_A_Test_Status;                          /*!< Specifies Channel_A test status (Range: 0 - 1 | Unit: None)				*/
	uint8_t DIMC_Ch_B_Test_Status;                          /*!< Specifies Channel_B test status (Range: 0 - 1 | Unit: None)				*/
	uint8_t DIMC_Ch_C_Test_Status;                          /*!< Specifies Channel_C test status (Range: 0 - 1 | Unit: None)				*/
	uint8_t DIMC_Logger_Test_Status;                        /*!< Specifies logger test status (Range: 0 - 1 | Unit: None)					*/
	uint8_t DIMC_Monitor_Status_Table_Test_Status;          /*!< Specifies status table monitoring test status (Range: 0 - 1 | Unit: None)	*/
	uint8_t DIMC_Oper_Inputs_Test_Status;                   /*!< Specifies OPER inputs test status (Range: 0 - 1 | Unit: None)				*/
	uint8_t CTRYs_SW_Ver_Mismatch_Test_Status;              /*!< Specifies CTRYs' SW mismatch test status (Range: 0 - 1 | Unit: None)     	*/
}st_System_Tests_Status;

/*! Structure for IBIT result parameters*/
typedef struct
{
	e_IBIT_Status IBIT_status;                             /*!< Specifies IBIT status (Range: 1 - 5 | Unit: None)							*/
	uint8_t DIMC_IBIT_Res;                                 /*!< Specifies IBIT result (Range: 0 - 0x1F | Unit: None)						*/
	uint8_t CTRYs_IBIT_Results[CTRY_MAX_NUM];              /*!< Specifies CTRYs' IBIT result (Range: 0 - 0x0F | Unit: None)					*/
}st_IBIT_Res;


/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_System_Operational_Status g_SysStatus;
extern st_System_Tests_Status g_SysTestsStatus;
extern st_IBIT_Res g_IBIT_Result;
extern uint16_t g_CTRYs_SW_Ver[CTRY_MAX_NUM];

/* -----Function prototypes --------------------------------------------------------*/
void DIMC_Module_Init(void);

#endif /* MODULE_INIT_DIMC_H */
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
