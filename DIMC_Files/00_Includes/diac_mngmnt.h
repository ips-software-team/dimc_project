/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : diac_mngmnt.h
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 11-07-2021
**
** Description: Header for diac_mngmnt.c file
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 25-09-2019 -- Updated ENUM e_DIAC_Num.
** Rev 1.2	: 05-02-2021 -- Integration changes.
** Rev 1.3  : 11-07-2021 -- Added doxygen comments
**************************************************************************************
*/
#ifndef DIAC_MNGMNT_H
#define DIAC_MNGMNT_H


/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for DIACs types description*/
typedef enum
{
	E_DIAC_PS_1  = 0U,          /*!<Val:0 -  DIAC Power Supply 1 			*/
	E_DIAC_PS_2,                /*!<Val:1 -  DIAC Power Supply 2 			*/
	E_DIAC_PS_3,                /*!<Val:2 -  DIAC Power Supply 3 			*/
	E_DIAC_PS_4,                /*!<Val:3 -  DIAC Power Supply 4 			*/
	E_DIAC_PS_ALL,              /*!<Val:4 -  DIAC All Power Supplies  		*/
	E_DIAC1,                    /*!<Val:5 -  DIAC 1							*/
	E_DIAC2,                    /*!<Val:6 -  DIAC 2 						*/
	E_DIAC_ENUM_MAX_NUM         /*!<Val:7 -  DIAC Maximal Enum Value 		*/
}e_DIAC_Num;

/*! Enumerator for DIAC state*/
typedef enum
{
	E_DIAC_OFF  = 0U,	/*!<Val:0 -  DIAC OFF 		*/
	E_DIAC_ON           /*!<Val:1 -  DIAC ON 		*/
}e_DIAC_State;

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
uint8_t Test_CH_A_Inputs(void);
void Diac_Control(e_DIAC_Num diac_num, e_DIAC_State diac_state);
#endif /*DIAC_MNGMNT_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
