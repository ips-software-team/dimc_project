/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : can_protocol_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.2
** Updated   : 29-04-2021
**
** Description: Header for can_protocol_dimc.c file
** 		        This file contains definition of CAN BUS interface protocol used by
** 		        DIMC unit.
*/


/* Revision Log:
**
** Rev 1.0  : 05-01-2020 -- Original version created.
** Rev 1.1  : 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2	: 29-04-2021 -- Adding new can messages.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef CAN_PROTOCOL_DIMC_H
#define CAN_PROTOCOL_DIMC_H





/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

#define DIMC_CAN_ADDR  30U
#define CAN_BROADCAST_ADDR 0x00U



#define ID_ALL_ACTUATORS_OFF_REQ      (uint8_t)0x00U
#define ID_CTRY_PBIT_RESULT_REQ       (uint8_t)0x01U
#define ID_ACTUATOR_ON_REQ            (uint8_t)0x02U
#define ID_CTRY_STATUS_REQ            (uint8_t)0x03U
#define ID_CTRYS_STATUS_UPDATE_REQ    (uint8_t)0x04U
#define ID_CPU_BRD_TEMP_REQ           (uint8_t)0x05U
#define ID_DRV_BRD_TEMP_REQ           (uint8_t)0x06U
#define ID_CTRY_CHANNEL_C_CTRL_REQ    (uint8_t)0x07U

/* Rev 1.2 */
#define ID_ACTUATORS_LIM_UPD_REQ	  (uint8_t)0x08U



#define ID_CTRY_PBIT_RESULT_RESP      (uint8_t)0x20U
#define ID_CTRY_STATUS_RESP           (uint8_t)0x21U
#define ID_CPU_BRD_TEMP_RESP          (uint8_t)0x22U
#define ID_DRV_BRD_TEMP_RESP          (uint8_t)0x23U

/* Rev 1.2 */
#define ID_ACTUATORS_LIM_UPD_RESP	  (uint8_t)0x24U

#define ACTUATOR_ON_REQ_DATA_LENGTH   0x07U

#define CAN_UNIT_ADDR_LENGTH      5U
#define CAN_UNIT_ADDR_MASK      0x1FU

#ifdef _DEBUG_

#include "diac_mngmnt.h"
#include "gpio_control_dimc.h"
#define DBG_DISC_IN_STATE_RESP        0x30U
#define DBG_DIAC_CTRL_REQ             0x31U
#define DBG_FLT_IN_STATE_REQ          0x32U
#define DBG_FTL_CH_B_CTRL_REQ         0x33U
#define DBG_IBIT_RESULT_RESP          0x34U
#define DBG_FLT_IN_STATE_RESP         0x35U

#endif
/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
 void HAL_CAN_RxFifo0MsgPendingCallback(void);
 void CAN_Message_Transmit(uint8_t message_type, uint8_t reciever_id, uint8_t *data_buff);
#endif /*CAN_PROTOCOL_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/






