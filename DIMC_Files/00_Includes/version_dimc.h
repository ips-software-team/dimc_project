/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : version_dimc.h
** Author    : Leonid Savchenko
** Revision  : 2.1
** Updated   : 11-09-2023
**
** Description: This file contains definition of SW version of DIMC unit
**
** Notes: This file represents a versions ID.
** The version is formatted as Y.X where according  to AIF convention
**
** Y is the major_version(00 - 99).
** X is the minor_version (00 - 99).
*/

/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 24-08-2021 -- Updated due to verification remarks.
** 							Update project version to 01_04.
** Rev 1.2	: 09-09-2021 -- Update project version to 01_05.
** Rev 1.3	: 05-10-2021 -- Update project version to 01_06.
** Rev 1.4	: 15-11-2021 -- Update project version to 01_07.
** Rev 1.5	: 30-01-2022 -- Update project version to 01_09.
** Rev 1.6	: 24-05-2022 -- Update project version to 01_10.
** Rev 1.7	: 29-05-2022 -- Update project version to 02_01.
** Rev 1.8	: 20-07-2022 -- Update project version to 02_02.
** Rev 1.9	: 17-08-2022 -- Update project version to 02_03.
** Rev 2.0	: 07-05-2023 -- Update project version to 02_04.
** Rev 2.1	: 11-09-2023 -- Update project version to 02_05.
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef VERSION_DIMC_H
#define VERSION_DIMC_H

/* -----Includes -------------------------------------------------------------------*/

/* -----Definitions ----------------------------------------------------------------*/

#define DEVICE	        DIMC
#define _VER_MAJOR_ 	(uint8_t)2U
#define _VER_MINOR_ 	(uint8_t)5U





/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/

#endif /*_VERSION_DIMC_H_*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
