/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gpio_control_dimc.h
** Author    : Leonid Savchenko
** Revision  : 1.4
** Updated   : 19-07-2022
**
** Description: Header for gpio_control_dimc.c file
** 		        This file contains function for control and monitoring GPIOs.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 13-03-2020 -- Updated e_FAULT_CH_Out
** Rev 1.2	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.3  : 11-07-2021 -- Added doxygen comments.
** Rev 1.4	: 19-07-2022 -- Fixing comment typo error.
**
**************************************************************************************
*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GPIO_CONTROL_DIMC_H
#define GPIO_CONTROL_DIMC_H





/* -----Includes -------------------------------------------------------------------*/
#include "gipsy_hal_def.h"
/* -----Definitions ----------------------------------------------------------------*/

/* -----Macros ---------------------------------------------------------------------*/

/* -----Type Definitions -----------------------------------------------------------*/
/*! Enumerator for Fault Channels types description*/
typedef enum
{
	E_FLT_CH_A_OUT  = 0U,             /*!<Val:0 -  Fault Channel A			 */
	E_FLT_CH_C_OUT,                   /*!<Val:1 -  Fault Channel C 			 */
	E_FLT_CH_B_OUT,                   /*!<Val:2 -  Fault Channel B 			 */
	E_FLT_CH_ENUM_NUM                 /*!<Val:3 -  Number of Fault Channels  */
}e_FAULT_CH_Out;


/*! Enumerator for Fault Channels states description*/
typedef enum
{
	E_FLT_CH_OK  = 0U,                /*!<Val:0 -  Fault Channel OK		 */
	E_FLT_CH_FAULT                    /*!<Val:1 -  Fault Channel FL		 */
}e_FAULT_CH_State;

/*! Enumerator for Fault Channels Sense input types description*/
typedef enum
{
	E_FLT_CH_A_SENSE_IN  = 0U,        /*!<Val:0 -  Sense input for CH_A		*/
	E_FLT_CH_B_SENSE_IN,              /*!<Val:1 -  Sense input for CH_B 	*/
	E_FLT_CH_C_SENSE_IN               /*!<Val:2 -  Sense input for CH_C 	*/
}e_FAULT_SenseIn;

/*! Enumerator for OPER inputs states description*/
typedef enum
{
	E_OPER_OFF = 0U,                  /*!<Val:0 -  OPER input ON		 */
	E_OPER_ON                         /*!<Val:1 -  OPER input OFF 		 */
}e_OPER_InputState;

/*! Enumerator for IBIT input states description*/
typedef enum
{
	E_IBIT_OFF = 0U,                        	/*!<Val:0 -  IBIT input OFF	*/
	E_IBIT_ON		                  			/*!<Val:1 -  IBIT input ON	*/
}e_IBIT_InputState;
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Function prototypes --------------------------------------------------------*/
uint8_t Test_Flt_CH_B(void);
uint8_t Test_OPER_Inputs(void);
uint8_t Test_CH_C_Inputs(void);
void Fault_Channel_OUT_Control(e_FAULT_CH_Out flt_ch_name, e_FAULT_CH_State flt_ch_state);
uint8_t Read_FLT_Sense_Inputs(void);
e_IBIT_InputState Read_IBIT_Input(void);
HAL_StatusTypeDef Read_OPER_Inputs(e_OPER_InputState *oper_state ,uint8_t *oper_ch_val, uint8_t *oper_1_2_val);
void Read_FLT_Channel_A_C_Inputs(uint8_t *flt_ch_a_in_val, uint8_t *flt_ch_c_in_val);
void Manage_OnBoard_Leds(void);
#endif /*GPIO_CONTROL_DIMC_H*/
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
