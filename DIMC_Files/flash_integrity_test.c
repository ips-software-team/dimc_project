/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC\CTRY
**
** Filename  : flash_integrity_test.c
** Author    : Leonid Savchenko
** Revision  : 1.3
** Updated   : 03-02-2022
**
** Description: This file contains procedures for MCUs flash integrity test(CRC).
*/


/* Revision Log:
**
** Rev 1.0  : 25-06-2020 -- Original version created.
** Rev 1.1  : 05-11-2020 -- Flash_Integrity_Test() removed CRC HW enable
** Rev 1.2  : 25-06-2021 -- Static analysis update
** Rev 1.3	: 03-02-2022 -- Update due to LDRA remarks.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "flash_inegrity_test.h"

/* -----Definitions ----------------------------------------------------------------*/
#define ROM_START      ( uint32_t  ) ( 0x08000000U )
#define ROM_END        ( uint32_t  ) ( 0x0803FFFBU )
#define CHECKSUM       ( uint8_t * ) ( 0x0803FFFCU )
#define ROM_LEN        ( uint32_t ) ( ROM_END  -  ROM_START  +  1U )
#define ROM_LEN_WORD   ( uint32_t ) ( ROM_LEN  /  4U )

#define RCC_AHB1ENR_CRCEN_BIT_MASK  (uint32_t)0x00001000U
/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
**
** Flash_Integrity_Test - Test MCU's Flash integrity by calculating CRC value and comparing
** 						calculated to pre-burned CRC values.
**
** Params : None.
**
** Returns: eCRC_Test_Result - Flash integrity test result (E_CRC_TEST_OK | E_CRC_TEST_FAIL)
** Notes:
*************************************************************************************/
eCRC_Test_Result Flash_Integrity_Test(void)
{
	CRC_TypeDef   *crc_instance = CRC;   /*CRC = CRC Registers base address */
	uint32_t crc = 0U;
	uint32_t checksum = 0U;
	eCRC_Test_Result crc_test_result = E_CRC_TEST_OK;

	uint32_t ptr_addr_val = (uint32_t)ROM_START;
	uint32_t *ptr_buffer = (uint32_t*)ptr_addr_val;
	uint32_t crc_ena = READ_BIT(RCC->AHB1ENR, RCC_AHB1ENR_CRCEN_BIT_MASK);

	/* Check that CRC HW peripheral enabled */
	if(0U != crc_ena)
	{
		/*Reset CRC Data Register by setting CR bit#0 to 1 (CR cleared automatically by HW)*/
		crc_instance->CR |= 0x01U;

		/*Calculate CRC by passing Flash data words to CRC-DR register*/
		for (uint32_t index = 0U; index < ROM_LEN_WORD; index++)
		{
			crc_instance->DR = (volatile uint32_t)(*ptr_buffer);
			ptr_addr_val +=4U;
			ptr_buffer = (uint32_t*)ptr_addr_val;
		}
		/*Get calculated CRC value*/
		crc = crc_instance->DR;
		/*Get pre-burned CRC value*/
		checksum = *(uint32_t*)CHECKSUM;
		/*Compare calculated and pre-burned CRC values*/
		if(crc == checksum)
		{
			crc_test_result = E_CRC_TEST_OK;
		}
		else
		{
			crc_test_result = E_CRC_TEST_FAIL;
		}
	}
	else
	{
		/*CRC HW init failed*/
		crc_test_result = E_CRC_TEST_FAIL;
	}


	return crc_test_result;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
