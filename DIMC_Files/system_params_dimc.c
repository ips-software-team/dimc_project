/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : system_params_dimc.c
** Author    : Leonid Savchenko
** Revision  : 2.8
** Updated   : 01-06-2023
**
** Description: Definiton of system parameters and their values.
**
*/


/* Revision Log:
**
** Rev 1.0  : 16-03-2020 -- Original version created.
** Rev 1.1  : 19-10-2020 --  Updated Set_CTRY_All_Actuators_Status(),Set_CTRY_Status(), Init_Ctrys_Status_Table()-CTRY init tatus changed to "OK"
** Rev 1.2  : 11-11-2020 -- Updated SysCtryParamTable fields, Updated Monitor_Status_Table()
** Rev 1.3	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.4	: 16-02-2021 -- Update Updated SysCtryParamTable fields.
** 						  Acutator's Id added to 'SysCtryParamTable' and a 'getter' function for this field.
** Rev 1.5	: 25-04-2021 -- Updated due to verification remarks.
** Rev 1.6  : 27-05-2021 -- Update the actuators table according to system changes.
** 						  Update due to verification remarks.
** Rev 1.7	: 05-07-2021 -- Update the function "Get_Equipped_Actuators".
** 						  Update the actuators speeds.
** 						  Update the function "Set_CTRY_All_Actuators_Status".
** 						  Added IBIT speed parameter for each actuator.
** 						  Update the function "Get_Actuator_Speed".
** 						  Update Actuators vibration and current consumption limits.
** Rev 1.8  : 25-07-2021 -- Update the implementation of the function "Set_CTRY_All_Actuators_Status".
** Rev 1.9  : 23-08-2021 -- Updated due to verification remarks.
** Rev 2.0  : 01-09-2021 -- Updated due to verification remarks.
** Rev 2.1	: 05-10-2021 -- Update PWM values.
** 							Integration updates.
** Rev 2.2	: 19-10-2021 -- Remove unused function "Get_Actuator_State".
** 							Limits Updates.
** 							Update due to LDRA remarks.
** Rev 2.3	: 08-11-2021 -- Update limits and PWM values.
** 							Update due to LDRA remarks.
** Rev 2.4	: 19-01-2022 -- Update ibit pwm values.
** Rev 2.5	: 03-02-2022 -- Update input parameters for the functions 'Set_CTRY_Status' & 'Get_CTRY_Status'.
** Rev 2.6	: 24-05-2022 -- Update operational limits due to results of flight tests.
** Rev 2.7	: 23-04-2023 -- Update IBIT PWM values.
** Rev 2.8  : 01-06-2023 -- Update IBIT PWM values.
**************************************************************************************
*/


/* Includes ------------------------------------------------------------------*/
#include "system_params_dimc.h"
#include "module_init_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define FL_CTRY_MAX_NUM        0U
#define FL_ACTUATORS_MAX_NUM   1U


/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

static st_SysCtryActStatus SysCtryStatusTable[CTRY_MAX_NUM] = {0U};

static const e_DIAC_Num ctrys_diac_ch [CTRY_MAX_NUM] =
{
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 00, CTRY_ID 01 */
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 02, CTRY_ID 03 */
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 04, CTRY_ID 05 */
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 06, CTRY_ID 07 */
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 08, CTRY_ID 09 */
		E_DIAC_PS_3, E_DIAC_PS_1,	/* CTRY_ID 10, CTRY_ID 11 */
		E_DIAC_PS_4, E_DIAC_PS_2,	/* CTRY_ID 12, CTRY_ID 13 */
		E_DIAC_PS_4, E_DIAC_PS_2,	/* CTRY_ID 14, CTRY_ID 15 */
		E_DIAC_PS_4, E_DIAC_PS_2,	/* CTRY_ID 16, CTRY_ID 17 */
		E_DIAC_PS_4, E_DIAC_PS_2,	/* CTRY_ID 18, CTRY_ID 19 */
};

static const st_SysCtryActParams SysCtryParamTable[CTRY_MAX_NUM] =
{
		/*----------------------CTRY_01 Actuators' Parameters----------------*/
		{
					/* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		60U		, 0U	  ,	60U		, 55U	  , 0U		, 55U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x204U	, 0xFFFFU , 0x206U	, 0x200U  , 0xFFFFU , 0x202U  },
			{
					{   250U,       0U,     250U,     250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,      21U,      21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,       8U,       8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_02 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		60U		, 0U	  ,	60U		, 55U	  , 0U		, 55U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x207U	, 0xFFFFU , 0x205U	, 0x203U  , 0xFFFFU , 0x201U  },
			{
					{   250U,       0U,     250U,     250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,      21U,	   21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,       8U,       8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_03 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		0U		, 0U	  ,	0U		, 50U	  , 0U		 , 55U	  },
			{		0U		, 0U	  ,	0U		, 40U	  , 0U		 , 40U	  }, /* Actuator IBIT speed */
			{		0xFFFFU	, 0xFFFFU , 0xFFFFU	, 0x210U  , 0xFFFFU  , 0x208U },
			{
					{     0U,       0U,     0U,       250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,     0U,		   21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,     0U,        70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,     0U,         8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_04 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_NA, E_ACT_NA, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		0U		, 0U	  ,	0U		, 55U	  , 0U		, 50U	  },
			{		0U		, 0U	  ,	0U		, 40U	  , 0U		 ,40U	  }, /* Actuator IBIT speed */
			{		0xFFFFU	, 0xFFFFU , 0xFFFFU	, 0x209U , 0xFFFFU  , 0x211U  },
			{
					{     0U,       0U,     0U,       250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{     0U,       0U,     0U,		   21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{     0U,       0U,     0U,        70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     0U,       0U,     0U,         8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_05 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_NA}, /*Actuator State*/
			{		45U		, 0U	  ,	45U		, 45U	  , 0U		, 0U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 0U		, 0U	  }, /* Actuator IBIT speed */
			{		0x212U	, 0xFFFFU , 0x216U	, 0x214U  , 0xFFFFU , 0xFFFFU },
			{
					{   250U,      0U,     250U,       250U,       0U,       0U}, /*Actuator current upper limit in OPER mode*/
					{    21U,      0U,      21U,		21U,	   0U,       0U}, /*Actuator current lower limit in OPER mode*/
					{    70U,      0U,      70U,        70U,       0U,       0U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,      0U,       8U,         8U,       0U,       0U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_06 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_NA, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		45U		, 0U	  ,	45U		, 0U	  , 0U		, 45U	  },
			{		40U		, 0U	  ,	40U		, 0U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x217U	, 0xFFFFU , 0x213U	, 0xFFFFU , 0xFFFFU , 0x215U  },
			{
					{   250U,      0U,      250U,     0U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,      0U,       21U,     0U,       0U,	     21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,      0U,       70U,     0U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,      0U,        8U,     0U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_07 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		55U		, 0U	  ,	65U		, 50U	  , 0U		, 60U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x218U	, 0xFFFFU , 0x220U	, 0x222U  , 0xFFFFU , 0x224U  },
			{
					{   250U,       0U,     250U,     250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,      21U,      21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,       8U,       8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_08 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		65U		, 0U	  ,	55U		, 60U	  , 0U		, 50U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x221U	, 0xFFFFU , 0x219U	, 0x225U  , 0xFFFFU , 0x223U  },
			{
					{   250U,       0U,     250U,     250U,       0U,     250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,      21U,	   21U,		  0U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,      70U,      70U,       0U,      70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,       8U,       8U,       0U,       8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_09 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		70U		, 0U	  ,	50U		, 50U	  , 75U		, 60U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x228U	, 0xFFFFU , 0x226U	, 0x232U  , 0x230U  , 0x234U  },
			{
					{   250U,       0U,     250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,      21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,      70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,       8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_10 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_NA, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		50U		, 0U	  ,	70U		, 60U	  , 75U		, 50U	  },
			{		40U		, 0U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x227U	, 0xFFFFU , 0x229U	, 0x235U  , 0x231U  , 0x233U  },
			{
					{   250U,       0U,   250U,       250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,       0U,    21U,		   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,       0U,    70U,        70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,       0U,     8U,         8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_11 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		70U		, 55U	  ,	55U		, 80U	  , 0U		, 65U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x236U	, 0x240U  , 0x238U	, 0x244U , 0xFFFFU  , 0x242U  },
			{
					{   250U,       250U,     250U,       250U,       0U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,      21U,		   21U,		  0U,    21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,      70U,        70U,       0U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,       8U,         8U,       0U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_12 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_NA, E_ACT_EQ}, /*Actuator State*/
			{		55U		, 55U	  ,	70U		, 65U	  , 0U		, 80U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 0U		, 40U	  }, /* Actuator IBIT speed */
			{		0x239U  , 0x241U  , 0x237U  , 0x243U  , 0xFFFFU , 0x245U  },
			{
					{     250U,       250U,   250U,     250U,       0U,    250U}, /*Actuator current upper limit in OPER mode*/
					{      21U,        21U,    21U,		 21U,		0U,	    21U}, /*Actuator current lower limit in OPER mode*/
					{      70U,        70U,    70U,      70U,       0U,     70U}, /*Actuator vibration power upper limit in OPER mode*/
					{       8U,         8U,     8U,       8U,       0U,      8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_13 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		60U		, 70U	  ,	60U		, 65U	  , 65U		, 65U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x246U	, 0x250U  , 0x248U	, 0x254U  , 0x252U  , 0x256U   },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_14 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		60U		, 70U	  ,	60U		, 65U	  , 65U		, 65U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x249U	, 0x251U  , 0x247U	, 0x257U , 0x253U  , 0x255U   },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_15 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		55U		, 60U	  ,	50U		, 55U	  , 60U		, 55U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x258U	, 0x262U  , 0x260U	, 0x266U  , 0x264U  , 0x268U   },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_16 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		50U		, 60U	  ,	55U		, 55U	  , 60U		, 55U	  },
			{		40U		, 40U	  ,	40U		, 40U	  , 40U		, 40U	  }, /* Actuator IBIT speed */
			{		0x261U	, 0x263U  , 0x259U	, 0x269U  , 0x265U  , 0x267U   },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_17 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		80U		, 65U	  ,	90U		, 80U	  , 90U		, 70U	  },
			{		45U		, 45U	  ,	45U		, 45U	  , 45U		, 45U	  }, /* Actuator IBIT speed */
			{		0x270U	, 0x274U  , 0x272U	, 0x278U  , 0x276U  , 0x280U  },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_18 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		90U		, 65U	  ,	80U		, 70U	  , 90U		, 80U	  },
			{		45U		, 45U	  ,	45U		, 45U	  , 45U		, 45U	  }, /* Actuator IBIT speed */
			{		0x273U	, 0x275U  , 0x271U	, 0x281U  , 0x277U  , 0x279U  },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_19 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		90U		, 70U	  ,	60U		, 70U	  , 70U		, 70U	  },
			{		60U		, 60U	  ,	60U		, 60U	  , 60U		, 60U	  }, /* Actuator IBIT speed */
			{		0x282U	, 0x286U  , 0x284U	, 0x290U  , 0x288U  , 0x292U  },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
		/*----------------------CTRY_20 Actuators' Parameters----------------*/
		{
				    /* Act_1     Act_2     Act_3     Act_4     Act_5     Act_6*/
			{       E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ, E_ACT_EQ}, /*Actuator State*/
			{		60U		, 70U	  ,	90U		, 70U	  , 70U		, 70U	  },
			{		60U		, 60U	  ,	60U		, 60U	  , 60U		, 60U	  }, /* Actuator IBIT speed */
			{		0x285U	, 0x287U  , 0x283U	, 0x293U  , 0x289U  , 0x291U   },
			{
					{   250U,       250U,   250U,     250U,       250U,   250U}, /*Actuator current upper limit in OPER mode*/
					{    21U,        21U,    21U,	   21U,		   21U,	   21U}, /*Actuator current lower limit in OPER mode*/
					{    70U,        70U,    70U,      70U,        70U,    70U}, /*Actuator vibration power upper limit in OPER mode*/
					{     8U,         8U,     8U,       8U,         8U,     8U}  /*Actuator vibration power lower limit in OPER mode*/
			}
		},
};
/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Get_Actuator_CurrLimits - Gets required actuator's current limits values from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 uint8_t *CurrLim_Max - pointer to actuator's max speed value (0-255)
** 			 uint8_t *CurrLim_Min - pointer to actuator's min speed value (0-255)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_CurrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *CurrLim_Max, uint8_t *CurrLim_Min)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != CurrLim_Max) && (NULL != CurrLim_Min))
	{
		/*Pass actuator's current limits as output parameter*/
		*CurrLim_Max = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.CurrentMax[(uint8_t)ActNum];
		*CurrLim_Min = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.CurrentMin[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Get_Actuator_VibrLimits - Gets required actuator's vibration limits values from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 uint8_t *VibrLim_Max - pointer to actuator's max vibration value (0-255)
** 			 uint8_t *VibrLim_Min - pointer to actuator's min vibration value (0-255)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_VibrLimits(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *VibrLim_Max, uint8_t *VibrLim_Min)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != VibrLim_Max) && (NULL != VibrLim_Min))
	{
		/*Pass actuator's vibration limits as output parameter*/
		*VibrLim_Max = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.AccelPowMax[(uint8_t)ActNum];
		*VibrLim_Min = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].stActLimits.AccelPowMin[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Init_Ctrys_Status_Table - Initialization of CTRYs' initial status.
**
** Params : None.
**
** Returns: None.
** Notes:  Init CTRY status as E_CTRY_OK, each CTRY's actuators status as E_ACT_OK
** 			and all CTRY's actuators' status as 0x00.
*************************************************************************************/
void Init_Ctrys_Status_Table(void)
{
	for(uint8_t i = 0U; i < CTRY_MAX_NUM; i++)
	{
		SysCtryStatusTable[i].CtryStatus = E_CTRY_NA;
		SysCtryStatusTable[i].CtryActuatorsStatus = 0x00U;
		for(uint8_t act_num = 0U; act_num < ((uint8_t)E_ACT_MAX_NUM); act_num++)
		{
			SysCtryStatusTable[i].ActStatus[act_num] = E_ACT_OK;
		}
	}
}
/*************************************************************************************
** Get_Actuator_Status - Gets required actuator's status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 e_ActuatorStatus *Act_Status - pointer to actuator's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus *Act_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != Act_Status))
	{
		/*Pass actuator's status as output parameter*/
		*Act_Status = SysCtryStatusTable[(uint8_t)Ctry_Id].ActStatus[(uint8_t)ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Get_CTRY_Status - Gets required CTRY's status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_CtryStatus *Ctry_Stat - pointer to CTRY's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus *Ctry_Stat)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  (NULL != Ctry_Stat))
	{
		/*Pass CTRY's status as output parameter*/
		*Ctry_Stat = SysCtryStatusTable[(uint8_t)Ctry_Id].CtryStatus;
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Get_CTRY_All_Actuators_Status - Gets required CTRY's all actuators' status from system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 uint8_t *All_Act_Status - pointer to CTRY's all actuators status (0x00-0x3F)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_CTRY_All_Actuators_Status(uint8_t Ctry_Id, uint8_t *All_Act_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  (NULL != All_Act_Status))
	{
		/*Pass CTRY's all actuators' status as output parameter*/
		*All_Act_Status = SysCtryStatusTable[(uint8_t)Ctry_Id].CtryActuatorsStatus;
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Set_Actuator_Status - Sets required actuator's status into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			 e_ActuatorStatus Act_Status - actuator's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Set_Actuator_Status(uint8_t Ctry_Id, e_ActuatorsNum ActNum, e_ActuatorStatus Act_Status)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t act_num_mask = 0x01U;

	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && ((E_ACT_OK == Act_Status) || (E_ACT_FL == Act_Status)))
	{
		/*Set actuator's status into status table*/
		SysCtryStatusTable[(uint8_t)Ctry_Id].ActStatus[(uint8_t)ActNum] = Act_Status;

		/*Update All CTRY's actuators status value into status table*/
		act_num_mask = act_num_mask << (uint8_t)ActNum;

		if(E_ACT_OK == Act_Status)
		{
			SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus = SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus  & (uint8_t)(~act_num_mask);
		}
		else
		{
			SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus  = SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus  | act_num_mask;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Set_CTRY_Status - Sets required CTRY's status into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_CtryStatus Ctry_Stat - CTRY's status (OK/FL)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Set_CTRY_Status(uint8_t Ctry_Id, e_CtryStatus Ctry_Stat)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  ((E_CTRY_OK == Ctry_Stat) || (E_CTRY_FL == Ctry_Stat)))
	{
		/*Update CTRY's status only to FL status (latch FL status)*/
		if(E_CTRY_FL != SysCtryStatusTable[Ctry_Id].CtryStatus)
		{
			/*Set CTRY's status into status table*/
			SysCtryStatusTable[(uint8_t)Ctry_Id].CtryStatus = Ctry_Stat;
		}
		/*In case that CTRY is FL - mark all CTRY's actuators as FL*/
		if(E_CTRY_FL == Ctry_Stat)
		{
			hal_status = Set_CTRY_All_Actuators_Status_FL(Ctry_Id);

			/* Update all CTRYs status accordingly */
			g_SysStatus.CTRYs_Status |= (uint32_t)((uint32_t)E_CTRY_FL << Ctry_Id);
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/*************************************************************************************
** Set_CTRY_All_Actuators_Status_FL - Sets required CTRY's all actuators' status into system status table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Set_CTRY_All_Actuators_Status_FL(uint8_t Ctry_Id)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t equipped_acts = 0U;


	/*Check input parameters*/
	if(CTRY_MAX_NUM > Ctry_Id)
	{
		/*Set CTRY's all actuators' status into status table*/
		hal_status |= Get_Equipped_Actuators(Ctry_Id,&equipped_acts);
		SysCtryStatusTable[Ctry_Id].CtryActuatorsStatus = (uint8_t)(((uint8_t)(~equipped_acts)) & 0x3FU);

		for(uint8_t act_num = 0U; act_num < ((uint8_t)E_ACT_MAX_NUM); act_num++)
		{
			/* Update the status of each actuator */
			if(E_ACT_EQ == SysCtryParamTable[Ctry_Id].ActState[act_num])
			{
				SysCtryStatusTable[Ctry_Id].ActStatus[act_num] = E_ACT_FL;
			}
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Monitor_Status_Table - Scans status table values
**
** Params : None
**
** Returns: e_MonitTableStatus - Table monitoring status
** Notes:
*************************************************************************************/
e_MonitTableStatus Monitor_Status_Table(void)
{
	e_MonitTableStatus monit_table_status = E_MON_TBL_OK;
	uint8_t fl_ctry_counter = 0U;
	uint8_t fl_act_counter = 0U;

	/*Scan entire status table*/
	for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM; ctry_num++)
	{
		/*Check CTRY's status*/
		if(E_CTRY_FL == SysCtryStatusTable[ctry_num].CtryStatus)
		{
			/*Increment fail CTRY counter*/
			fl_ctry_counter++;
		}
		/*Check current CTRY's equipped actuators' status*/
		for(uint8_t act_num = 0U; act_num < ((uint8_t)E_ACT_MAX_NUM); act_num++)
		{
			if((E_ACT_FL == SysCtryStatusTable[ctry_num].ActStatus[act_num]) && (E_ACT_EQ == SysCtryParamTable[ctry_num].ActState[act_num]))
			{
				/*Increment fail actuator counter*/
				fl_act_counter++;
			}
		}
	}
	/*Check if number of failed CTRs/Actuators exceed the limit or reading status table failed*/
	if((FL_CTRY_MAX_NUM < fl_ctry_counter) || (FL_ACTUATORS_MAX_NUM < fl_act_counter))
	{
		monit_table_status = E_MON_TBL_FL;
	}
	return monit_table_status;
}
/*************************************************************************************
** Get_Actuator_Speed - Gets required actuator's speed value from system parameters table
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_ActuatorsNum ActNum 	- Actuator's number (0-5)
** 			 uint8_t *Act_Speed 	- pointer to actuator's speed value (0-100)
** 			 e_Actuator_OperType 	- Operation Type (OPER/IBIT)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_Speed(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint8_t *Act_Speed, e_Actuator_OperType Oper_Type)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) && (NULL != Act_Speed))
	{
		if(E_ACT_OPER == Oper_Type)
		{
			/*Pass actuator's speed as output parameter*/
			*Act_Speed = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].ActDefaultSpeed[(uint8_t)ActNum];
		}
		else if ((E_ACT_IBIT == Oper_Type) || (E_ACT_MANUAL == Oper_Type))
		{
			/*Pass actuator's speed as output parameter*/
			*Act_Speed = (uint8_t)SysCtryParamTable[(uint8_t)Ctry_Id].ActDefaultIBITSpeed[(uint8_t)ActNum];
		}
		else
		{
			hal_status = HAL_ERROR;
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}
/*************************************************************************************
** Get_Equipped_Actuators - Get all the CTRY's equipped actuators.
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			uint8_t *equipped_act - pointer to the equipped actuators ('0' ACT_EQ)
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Equipped_Actuators (uint8_t ctry_id, uint8_t *equipped_act)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	if ((CTRY_MAX_NUM > ctry_id ) && (NULL != equipped_act))
	{
		for (uint8_t act_num = 0U ; act_num < ((uint8_t)E_ACT_MAX_NUM) ; act_num++)
		{
			*equipped_act |= ((uint8_t)(SysCtryParamTable[ctry_id].ActState[act_num]) << act_num);
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}

	return hal_status;
}/*************************************************************************************
** Get_CTRY_DIAC_Ch - Get the DIAC's channel number of the CTRY.
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			 e_DIAC_Num *Ctry_Diac_Ch - pointer to CTRY's DIAC channel
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_CTRY_DIAC_Ch(uint8_t Ctry_Id, e_DIAC_Num *Ctry_Diac_Ch)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) &&  (NULL != Ctry_Diac_Ch))
	{
		/*Pass CTRY's DIAC channel as output parameter*/
		*Ctry_Diac_Ch = ctrys_diac_ch[(uint8_t)Ctry_Id];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}/*************************************************************************************
** Get_Actuator_Id - Get the actuator's ID.
**
** Params : uint8_t Ctry_Id - CTRY's ID value (0-19)
** 			e_ActuatorsNum ActNum - Actuator's number (0-5)
** 			uint16_t *Act_Id - pointer to the actuator's id.
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Get_Actuator_Id(uint8_t Ctry_Id, e_ActuatorsNum ActNum, uint16_t *Act_Id)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	/*Check input parameters*/
	if((CTRY_MAX_NUM > Ctry_Id) && (E_ACT_MAX_NUM > ActNum) &&  (NULL != Act_Id))
	{
		/*Pass CTRY's DIAC channel as output parameter*/
		*Act_Id = (uint16_t)SysCtryParamTable[Ctry_Id].ActId[ActNum];
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	/*Return status*/
	return hal_status;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
