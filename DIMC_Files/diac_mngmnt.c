/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : diac_mngmnt.c
** Author    : Leonid Savchenko
** Revision  : 1.9
** Updated   : 17-07-2022
**
** Description: This file contains function control, monitor and test DIACs.
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 05-02-2020 -- Updated both functions
** Rev 1.2  : 01-11-2020 -- Updated Test status states
** Rev 1.3  : 03-01-2021 -- Updated due to verification remarks.
** Rev 1.4  : 07-07-2021 -- Update the method of delay in the test function in order to trigger the WDG during the test.
** 						  Update "Diac_Control" function.
** Rev 1.5	: 16-08-2021 -- Update the function "Diac_Control".
** Rev 1.6	: 22-08-2021 -- Added Include statements.
** Rev 1.7	: 23-08-2021 -- Update the function "Diac_Control".
** Rev 1.8	: 24-10-2021 -- Update the function "Diac_Control".
** Rev 1.9	: 17-07-2022 -- Update the function "Test_CH_A_Inputs".
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include <mcu_config_dimc.h>
#include "diac_mngmnt.h"
#include "module_init_dimc.h"
#include "logger_dimc.h"
#include "gpio_control_dimc.h"
#include "gipsy_hal_rcc.h"
#include "wdog.h"



/* -----Definitions ----------------------------------------------------------------*/
#define DIAC_PS_DELAY_MILIS       1800U
#define DELAY_CYCLE				  600U
#define DIAC_NUM_OF_TEST_STEPS    4U
#define SENSE_DELAY_TIME		  5U

#define NUM_OF_DIACS_PS     4U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
const st_PinDescriptor ST_DIAC_PS_PIN[NUM_OF_DIACS_PS] =
{
		{COP_TTL_1_PIN, COP_TTL_PORT},
		{COP_TTL_2_PIN, COP_TTL_PORT},
		{COP_TTL_3_PIN, COP_TTL_PORT},
		{COP_TTL_4_PIN, COP_TTL_PORT},
};


/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Test_CH_A_Inputs - Performs Fault Channel_A inputs test by activating DIACs in
** 					  pre-defined order.
**
** Params : None
**
** Returns: uint8_t - Test result(TEST_OK - Test Passed | TEST_FL - Test Failed)
** Notes:
*************************************************************************************/
uint8_t Test_CH_A_Inputs(void)
{
	const uint8_t diacs_states[DIAC_NUM_OF_TEST_STEPS] 			=       {0x00U, 0x0AU, 0x05U, 0x0FU};
	const uint8_t ch_a_exp_result[DIAC_NUM_OF_TEST_STEPS] 		=      	{0x00U, 0x0AU, 0x05U, 0x0FU};
	const uint8_t ch_a_sens_exp_result[DIAC_NUM_OF_TEST_STEPS] 	= 		{0x00U, 0x00U, 0x00U, 0x01U};
	uint8_t ch_a_state = 0U;
	uint8_t ch_c_state = 0U;
	uint8_t sens_state = 0U;
	uint8_t ch_a_test_ok = TEST_OK;


	Fault_Channel_OUT_Control(E_FLT_CH_A_OUT,E_FLT_CH_OK);/*Reset FL Channel_A output  before the test*/
	for(uint8_t test_step = 0U; test_step < DIAC_NUM_OF_TEST_STEPS; test_step++)
	{
		if (TEST_OK == ch_a_test_ok)
		{
			/*Set DIACs' Power supplies according to test sequence*/
			for(uint8_t ps_num = 0U; ps_num < NUM_OF_DIACS_PS; ps_num++)
			{
				Diac_Control((e_DIAC_Num)ps_num,(diacs_states[test_step] >> ps_num) & 0x01U);
			}

			/*Wait for DIAC turn ON/OFF*/
			for (uint16_t delay_count = 0U; delay_count < DIAC_PS_DELAY_MILIS; delay_count += DELAY_CYCLE)
			{
				WDOG_Trigger();
				HAL_Delay(DELAY_CYCLE);
			}
			Read_FLT_Channel_A_C_Inputs(&ch_a_state, &ch_c_state);/*Read input Flt Channels GPIO inputs*/
			ch_a_state &= 0x0FU;                                   /*4 lower bits of ch_a_state represent 4 Ch_A inputs*/
			sens_state = (Read_FLT_Sense_Inputs() & 0x01U);       /*sens_state Lsb bit represents Sense Ch_A input state*/

			/*Check Channel_A*/
			if((ch_a_exp_result[test_step] != ch_a_state) ||(ch_a_sens_exp_result[test_step] != (sens_state & 0x01U)))
			{
#ifndef _TEST_BENCH_CONFIGURATION_
				/*Channel_A test failed*/
				Fault_Channel_OUT_Control(E_FLT_CH_A_OUT,E_FLT_CH_FAULT);/*Set Ch_A output*/
				Fault_Channel_OUT_Control(E_FLT_CH_B_OUT,E_FLT_CH_FAULT);/*Set Ch_B output*/
#endif
				ch_a_test_ok = TEST_FL;                         /*Set flag that indicates Channel_A test failure*/
			}
		}
	}

	/* Test DIMC channel A output control */
	if(TEST_OK == ch_a_test_ok)
	{
		Fault_Channel_OUT_Control(E_FLT_CH_A_OUT,E_FLT_CH_FAULT);
		HAL_Delay(SENSE_DELAY_TIME);
		sens_state = (Read_FLT_Sense_Inputs() & 0x01U);       /*sens_state Lsb bit represents Sense Ch_A input state*/
		if(0x00U == sens_state)
		{
			/* Reset fault channel */
			Fault_Channel_OUT_Control(E_FLT_CH_A_OUT,E_FLT_CH_OK);
			HAL_Delay(SENSE_DELAY_TIME);
			sens_state = (Read_FLT_Sense_Inputs() & 0x01U);       /*sens_state Lsb bit represents Sense Ch_A input state*/

			/* Verify that the channel was reset correctly */
			if(0x01 != sens_state)
			{
				ch_a_test_ok = TEST_FL;
			}
		}
		else
		{
			ch_a_test_ok = TEST_FL;
		}
	}
	return ch_a_test_ok;
}
/*************************************************************************************
** Diac_Control - Control of DIAC's Power supplies Enable inputs
**
**
** Params : e_DIAC_Num diac_num - Number of required DIAC's PS(E_DIAC_PS_ALL --> apply required state to all Power Supplies) or
** 						          DIAC modules(each module includes 2 Power Supplies
** 			e_DIAC_State diac_state - Required DIAC's or DIAC PS's state (ON/OFF)
**
** Returns: None
** Notes:
*************************************************************************************/
void Diac_Control(e_DIAC_Num diac_num, e_DIAC_State diac_state)
{
	uint8_t spare_bytes[3U] = {0U};

	if(E_DIAC_ENUM_MAX_NUM > diac_num)/*Check if diac_num is in correct range*/
	{
		if(E_DIAC_PS_ALL == diac_num)/*Set all PSs (NUM_OF_DIACS_PS) with required state (diac_state)*/
		{
			for(uint8_t i = 0U; i < NUM_OF_DIACS_PS; i++)
			{
				HAL_GPIO_WritePin(ST_DIAC_PS_PIN[i].Pin_port,ST_DIAC_PS_PIN[i].Pin_num,(GPIO_PinState)diac_state);
			}
		}

		else if((E_DIAC_PS_1 <= diac_num) && (E_DIAC_PS_4 >= diac_num))
		{
			HAL_GPIO_WritePin(ST_DIAC_PS_PIN[diac_num].Pin_port,ST_DIAC_PS_PIN[diac_num].Pin_num,(GPIO_PinState)diac_state);

			if ((E_DIAC_OFF == diac_state) && (E_INIT_MODE != g_SysStatus.Sys_Mode))
			{
				g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_FAULT, E_FC_POWER_SUP_CUT_OFF, 0x00U , E_UNIT_TYPE_DIAC, diac_num+1U, spare_bytes);/* Rev 1.8 */
			}
		}
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
