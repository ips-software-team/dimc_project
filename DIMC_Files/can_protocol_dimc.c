/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : can_protocol_dimc.c
** Author    : Leonid Savchenko
** Revision  : 1.9
** Updated   : 03-02-2022
**
** Description: This file contains definition of CAN BUS interface protocol used by
** 		        DIMC unit.
*/


/* Revision Log:
**
** Rev 1.0  : 05-01-2020 -- Original version created.
** Rev 1.1  : 03-02-2020 -- Updated HAL_CAN_RxFifo0MsgPendingCallback()
** Rev 1.2  : 24-02-2020 -- Added input parameters check to CAN_Message_Transmit()
** Rev 1.3  : 31-10-2020 -- set HAL_GetTick() value when the CTRAY response to a request from the DIMC.
** 						  Added CTRY'sMotor Operate field parsing.
** Rev 1.4  : 03-01-2021 -- Updated due to verification remarks.
** Rev 1.5	: 07-01-2021 -- Added casting according to verification updates.
** Rev 1.6  : 18-04-2021 -- Added support for over temperature bit in status resp handling.
** Rev 1.7  : 31-05-2021 -- Parsing CTRY's report of abnormal actuator activation.
** 						  Parsing CTRY's report of power switch status.
** 						  Adding a check of DIMC voltage status before transmitting CAN msg.
** 						  Adding a check of loopback test result before transmitting CAN msg.
** 						  change CTRY PBIT respond parsing. now duplicate responde will be identify.
** Rev 1.8	: 27-06-2021 -- Update the implementation of ID_CTRYS_STATUS_UPDATE_REQ message transmit.
** 						  Added handling of CTRY over current bit.
** 						  Added the external variable "g_CTRY_Resp[]".
** Rev 1.9	: 03-02-2022 -- Update global variable  name according to changes in module_init.h-Rev 1.9.
** Rev 2.0	: 13-09-2023 -- Fix static analysis issue in the function 'CAN_Message_Transmit'.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include <mcu_config_dimc.h>
#include "can_protocol_dimc.h"
#include "module_init_dimc.h"
#include "gipsy_hal_can.h"
#include "gipsy_hal_rcc.h"
#include "pbit_dimc.h"
/* -----Definitions ----------------------------------------------------------------*/
#define CAN_MSG_DATA_LENGH_BYTES     8U            /*CAN message DLC*/



/* -----Macros ---------------------------------------------------------------------*/
#define COMBINE_CAN_HEADER(msg_id,rec_id)  (uint32_t)(((msg_id) << CAN_UNIT_ADDR_LENGTH) | (rec_id))
#define GET_CAN_MSG_ID_FROM_HEADER(header)  (uint8_t)((header) >> CAN_UNIT_ADDR_LENGTH)
#define GET_CAN_UNIT_ADDR_FROM_HEADER(header)  (uint8_t)((header) & CAN_UNIT_ADDR_MASK)
/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/
extern st_Ctry_Lim_Resp g_CTRY_Resp[CTRY_MAX_NUM];

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/



/************************************************************************************
** HAL_CAN_RxFifo0MsgPendingCallback - Rx Fifo 0 message pending callback
**
** The method is executed after CAN RX Interrupt occurrence, which indicated that
** a message is available in CAN HW buffer.
** The method reads received CAN message and handles it :
** 1. Checks that the message was addressed to DIMC.
** 2. Gets sender's(CTRY's) Id.
** 3. Gets the type of received message.
** 4. Parses message and updates variables according to received data.
**
** Params : None.
**
** Returns: None.
** Notes:
*************************************************************************************/
 void HAL_CAN_RxFifo0MsgPendingCallback(void)
 {
	CAN_RxHeaderTypeDef   can_rx_header = {0U};
	uint8_t               can_msg_rx_data[CAN_MSG_DATA_LENGH_BYTES] = {0U};
	uint8_t        		  ctry_id = 0U;
	uint8_t				  act_numer = 0x00U; /* Rev 1.7 */
   /* Get RX message */
   if (HAL_OK == HAL_CAN_GetRxMessage(&can_rx_header, can_msg_rx_data))
   {
	   if(DIMC_CAN_ADDR == GET_CAN_UNIT_ADDR_FROM_HEADER(can_rx_header.StdId))/*Handle messages addressed to DIMC*/
	   {
		   ctry_id = can_msg_rx_data[0U] & 0x1FU; /*Get CTRY's ID (5 lower bits of can_msg_rx_data[0]*/
		   /*CTRY's ID validity check (range 1 - 20)*/
		   if((ctry_id > 0U) && ( ctry_id <= CTRY_MAX_NUM))
		   {
			   ctry_id--; /*Decrement CTRY's ID to fit range 0 - 19 */
			   switch (GET_CAN_MSG_ID_FROM_HEADER(can_rx_header.StdId))/*Check message type*/
			   {
					case ID_CTRY_PBIT_RESULT_RESP:
						if (FALSE == g_SysStatus.CTRY_Status[ctry_id].pbit_resp)
						{
							g_SysStatus.CTRY_PBIT_Status[ctry_id].PBIT_Status = can_msg_rx_data[1U];
							g_SysStatus.All_CTRYs_PBIT_Status |= ((uint32_t)can_msg_rx_data[1U] << ctry_id);
							g_CTRYs_SW_Ver[ctry_id] = (can_msg_rx_data[2U] * 256U) + can_msg_rx_data[3U];
							g_SysStatus.CTRY_PBIT_Status[ctry_id].PBIT_Failure_Desc = (can_msg_rx_data[4U] * 256U) + can_msg_rx_data[5U];
							g_SysStatus.CTRY_Status[ctry_id].respond_time = HAL_GetTick();
							g_SysStatus.CTRY_Status[ctry_id].pbit_resp = TRUE;
						}
						else
						{
							g_SysStatus.CTRY_Status[ctry_id].dup_resp_fl = TRUE;
						}


					   break;
					case ID_CTRY_STATUS_RESP:
						g_SysStatus.CTRY_Status[ctry_id].Ctry_Status = (can_msg_rx_data[0U] >> 7U) & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].CH_C_Status = (can_msg_rx_data[0U] >> 6U) & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].Mot_Oper_In_State = (can_msg_rx_data[0U] >> 5U) & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].VibrationValue = can_msg_rx_data[1U];
						g_SysStatus.CTRY_Status[ctry_id].CurrentValue = can_msg_rx_data[3U];
						g_SysStatus.CTRY_Status[ctry_id].Power_Switch = ((can_msg_rx_data[4] >> 3U) & 0x01U);
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Sensors_Status = can_msg_rx_data[4U] & 0x0FU;
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Sensors_Status = (can_msg_rx_data[4U] >> 4U) & 0x0FU;
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Heater_Status =  can_msg_rx_data[5U] & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Heater_Status =  (can_msg_rx_data[5U] >> 1U) & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].ActuatorsStatus =  (can_msg_rx_data[5U] >> 2U) & 0x3FU;
						g_SysStatus.CTRY_Status[ctry_id].over_temp = ((can_msg_rx_data[6U] >> 6U) & 0x01U);		/* Rev 1.6 */
						g_SysStatus.CTRY_Status[ctry_id].Ctry_Failure_Desc = (can_msg_rx_data[6U] * 256U) + can_msg_rx_data[7U];
						g_SysStatus.CTRY_Status[ctry_id].respond_time = HAL_GetTick();
						g_SysStatus.CTRY_Status[ctry_id].Abnormal_Actuator_Report = (can_msg_rx_data[6U] >> 5U) & 0x01U;
						g_SysStatus.CTRY_Status[ctry_id].over_current_rep = (can_msg_rx_data[7U] >> 6U) & 0x01U;
					   break;
					case ID_CPU_BRD_TEMP_RESP:
						for( uint8_t i = 0U; i < MAX_TEMP_SENSORS_ON_BOARD; i++ )
						{
							g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Senors_Temperature[i] = can_msg_rx_data[i+1U];
						}
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Sensors_Status = can_msg_rx_data[5U] & 0x0FU;
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_CPU_BOARD].Heater_Status =  can_msg_rx_data[6U] & 0x01U;
						break;
					case ID_DRV_BRD_TEMP_RESP:
						for( uint8_t i = 0U; i < MAX_TEMP_SENSORS_ON_BOARD; i++ )
						{
							g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Senors_Temperature[i] = can_msg_rx_data[i+1U];
						}
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Sensors_Status = (can_msg_rx_data[5U] >> 4U) & 0x0FU;
						g_SysStatus.CTRY_Status[ctry_id].Brd_Temp[E_DRV_BOARD].Heater_Status =  (can_msg_rx_data[6U] >> 1U) & 0x01U;
					   break;

					/* Rev 1.7 */
					case ID_ACTUATORS_LIM_UPD_RESP:
						act_numer = (can_msg_rx_data[1U] & 0x07U);
						g_CTRY_Resp[ctry_id].act_resp[act_numer].oper_lim_type = ((can_msg_rx_data[1U] >> 7U) & 0x01U);
						g_CTRY_Resp[ctry_id].act_resp[act_numer].max_vibration = can_msg_rx_data[2U];
						g_CTRY_Resp[ctry_id].act_resp[act_numer].min_vibration = can_msg_rx_data[3U];
						g_CTRY_Resp[ctry_id].act_resp[act_numer].max_current = can_msg_rx_data[4U];
						g_CTRY_Resp[ctry_id].act_resp[act_numer].min_current = can_msg_rx_data[5U];
						g_CTRY_Resp[ctry_id].act_resp[act_numer].resp = TRUE;
						break;
					/* --- */
#ifdef _DEBUG_
					case DBG_FTL_CH_B_CTRL_REQ:
						Fault_Channel_OUT_Control(E_FLT_CH_B_OUT,(e_FAULT_CH_State)can_msg_rx_data[1U]);
						break;
					case DBG_DIAC_CTRL_REQ:
						Diac_Control((e_DIAC_Num)can_msg_rx_data[1U], (e_DIAC_State)can_msg_rx_data[2U]);
						break;
					case DBG_FLT_IN_STATE_REQ:
						Diac_Control((e_DIAC_Num)can_msg_rx_data[1U], (e_DIAC_State)can_msg_rx_data[2U]);
						uint8_t ch_a_flt =0U;
						uint8_t ch_c_flt=0U;
						Read_FLT_Channel_A_C_Inputs(&ch_a_flt, &ch_c_flt);
						can_msg_rx_data[0U]=ch_a_flt;
						can_msg_rx_data[1U]=ch_c_flt;
						CAN_Message_Transmit(DBG_FLT_IN_STATE_RESP, 1U, can_msg_rx_data);
						break;
#endif
					default:
						g_SysStatus.Can_Bus_Msg_Type_Error = ctry_id + 1U; /*set CTRY id range to 1-20*/
					   break;
			   }
		   }
		   /* CTRY id error */
		   else
		   {
			   if (0x00U == ctry_id)
			   {
				   ctry_id = 0xFFU;
			   }
			   g_SysStatus.Can_Bus_Ctry_Id_Error = ctry_id;
		   }
	   }
	   /* Invalid address error */
	   else
	   {
		   if (0x00U == GET_CAN_UNIT_ADDR_FROM_HEADER(can_rx_header.StdId))
		   {
			   g_SysStatus.Can_bus_Address_Error = 0xFFU;
		   }
		   else
		   {
			   g_SysStatus.Can_bus_Address_Error = GET_CAN_UNIT_ADDR_FROM_HEADER(can_rx_header.StdId);
		   }
	   }
   }
   else
   {
	   g_SysStatus.Can_Bus_Error = 0x01U;
   }
 }



 /************************************************************************************
 ** CAN_Message_Transmit - This function transmits CAN message via CAN BUS interface
 **
 ** Params : uint8_t message_type - Type of message to transmit
 ** 		 uint8_t reciever_id  - The ID of the CTRY which will be the receiver of the message
 ** 		 uint8_t *data_buff   - Pointer to data array that contains the data which will be
 ** 		 						transmitted.
 **
 ** Returns: None.
 ** Notes:
 *************************************************************************************/
void CAN_Message_Transmit(uint8_t message_type, uint8_t reciever_id, uint8_t *data_buff)
{
	uint8_t valid_message = TRUE;
	uint8_t               can_msg_tx_data[CAN_MSG_DATA_LENGH_BYTES] = {0U};
	uint32_t              msg_std_id = 0U;

	/*Check range o input parameters*/
	if((NULL != data_buff) && (((CTRY_MAX_NUM >=reciever_id) && (reciever_id > 0U)) || (CAN_BROADCAST_ADDR == reciever_id)))
	{
		/*Combine TX message according to it's type*/
		switch(message_type)
		{
		case ID_ALL_ACTUATORS_OFF_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			break;
		case ID_CTRY_PBIT_RESULT_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			break;
		case ID_CTRY_STATUS_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			break;
		case ID_CPU_BRD_TEMP_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			break;
		case ID_DRV_BRD_TEMP_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			break;
		case ID_ACTUATOR_ON_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			for(uint8_t i = 0U; i < ACTUATOR_ON_REQ_DATA_LENGTH; i++)
			{
				can_msg_tx_data[i] = data_buff[i];
			}
			break;
		case ID_CTRYS_STATUS_UPDATE_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			can_msg_tx_data[1U] = data_buff[1U];
			can_msg_tx_data[2U] = data_buff[2U];
			break;
		case ID_CTRY_CHANNEL_C_CTRL_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			break;

		/* Rev 1.7 */
		case ID_ACTUATORS_LIM_UPD_REQ:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			can_msg_tx_data[1U] = data_buff[1U];
			can_msg_tx_data[2U] = data_buff[2U];
			can_msg_tx_data[3U] = data_buff[3U];
			can_msg_tx_data[4U] = data_buff[4U];
			break;
		/* --- */

#ifdef _DEBUG_
		case DBG_FLT_IN_STATE_RESP:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			can_msg_tx_data[1U] = data_buff[1U];
			can_msg_tx_data[2U] = data_buff[2U];
			can_msg_tx_data[3U] = data_buff[3U];
			can_msg_tx_data[4U] = data_buff[4U];
			break;
		case DBG_IBIT_RESULT_RESP:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			can_msg_tx_data[1U] = data_buff[1U];
			can_msg_tx_data[2U] = data_buff[2U];
			can_msg_tx_data[3U] = data_buff[3U];
			can_msg_tx_data[4U] = data_buff[4U];
			break;
		case DBG_DISC_IN_STATE_RESP:
			msg_std_id = COMBINE_CAN_HEADER( message_type, reciever_id);
			can_msg_tx_data[0U] = data_buff[0U];
			can_msg_tx_data[1U] = data_buff[1U];
			can_msg_tx_data[2U] = data_buff[2U];
			can_msg_tx_data[3U] = data_buff[3U];
			can_msg_tx_data[4U] = data_buff[4U];
			break;
#endif
		default:
			valid_message = FALSE;
			break;
		}

		/*Check validity of Message's Type and the DIMC voltage status*/
		if((TRUE == valid_message) && (TEST_OK == g_SysTestsStatus.DIMC_Volt_Test_Status) && (TEST_OK == g_SysTestsStatus.DIMC_Can_Bus_Init_Test_Status))
		{
			/* Start the Transmission process */
			if (HAL_OK != HAL_CAN_AddTxMessage(msg_std_id, can_msg_tx_data))
			{
				/* Failed to transmit message */
				g_SysStatus.Can_Bus_Tr_error = TRUE;
			}
		}
	}
}


 /************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
