/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC,CTRY
**
** Filename  : wdog.c
** Author    : Leonid Savchenko
** Revision  : 1.5
** Updated   : 23-08-2021
**
** Description: This file contains procedures for Watch-Dog control and test.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 28-07-2020 -- Original version created.
** Rev 1.1  : 21-11-2020 -- Updated due to verification remarks
** Rev 1.2  : 27-06-2021 -- Updated due to integration remarks
** Rev 1.3  : 08-07-2021 -- Updated due to integration remarks
** Rev 1.4  : 14-07-2021 -- Updated due to integration remarks
** Rev 1.5  : 23-08-2021 -- Updated due to verification remarks
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "wdog.h"
#include "gipsy_hal_rcc.h"
#include "mcu_config_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define WDOG_PULSE_DELAY  200U /*"for loop" end value for making short pulse to strobe WDOG*/

/*Watch-dog Timeout Period tWD defined in data sheet is:
 * minimal = 1.12 second
 * typical = 1.60 second
 * maximal = 2.40 second
 * */
#define WDOG_TEST_LOOP_STEP_INTERVAL_MILIS 100U
#define WDOG_TEST_LOOP_MAX_STEPS 28U /*Loop maximal period : 2.8 second*/
#define WDOG_TEST_RESULT_STEPS_MAX_LIMIT 25U /*WDOG Test maximal "kick" time interval limit 2.5 second*/

/* Backup register 0 address */
#define BCKUP_REG_0_ADDRESS		0x40002850U
#define BCKUP_REG_1_ADDRESS		0x40002854U

/* Reset Source */
#define NRST_RESET				0xAAAAAAAAU

/* WDG wait time */
#define NUM_LOOPS_BEFORE_REG_1_UPDATE	5U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
static e_WDOG_TestResult WDOG_Test_Result = E_WDOG_TEST_FAILED;

/* -----External variables ---------------------------------------------------------*/
/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** WDOG_Perform_Test - Performs on-board WDOG test. Test check correct WDOG expiration period.
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void WDOG_Perform_Test(void)
{
	e_WDOG_TestResult test_result = E_WDOG_TEST_PASSED;
	uint32_t wdog_test_loop_cnt = 0U;
	uint32_t *bckup_reg_0 = (uint32_t *)BCKUP_REG_0_ADDRESS;
	uint32_t *bckup_reg_1 = (uint32_t *)BCKUP_REG_1_ADDRESS;
	uint8_t update_reg_1 = TRUE;

	WDOG_Trigger();/*refresh watch-dog before the test to eliminate INIT time period*/

	/*In case of power up reset, perform wdog test*/
	if (NRST_RESET != *bckup_reg_1)/*These bits are set by hardware when a power reset occurs*/
	{
		for(uint16_t loop_counter = 0U; loop_counter < WDOG_TEST_LOOP_MAX_STEPS; loop_counter++)
		{
			HAL_Delay(WDOG_TEST_LOOP_STEP_INTERVAL_MILIS);
			*bckup_reg_0 = loop_counter;

			/* Set Reg 1 */
			if((TRUE == update_reg_1) && (NUM_LOOPS_BEFORE_REG_1_UPDATE<loop_counter))
			{
				*bckup_reg_1 = NRST_RESET;
				update_reg_1 = FALSE;
			}
		}

		/*WDOG did not reseted the MCU within required time interval -> mark test as FAILED*/
		test_result = E_WDOG_TEST_FAILED;
	}

	/*In case of external NRST reset, perform check of WDOG expiration time period*/
	else
	{
		wdog_test_loop_cnt = *bckup_reg_0; /*Read loop interval value stored in RTC back domain(non volatile) register*/

		/*Check if WDOG expiration time period is within the limits*/
		if(WDOG_TEST_RESULT_STEPS_MAX_LIMIT >= wdog_test_loop_cnt)
		{
			test_result = E_WDOG_TEST_PASSED;

		}
		else
		{
			test_result = E_WDOG_TEST_FAILED;
		}
	}

	/* Reset back up register before next reset */
	*bckup_reg_1 = 0x00U;
	*bckup_reg_0 = 0x00U;

	/*Save WFOG test result*/
	WDOG_Test_Result = test_result;
}
/*************************************************************************************
** WDOG_Get_Test_Result - Gets Watch-Dog Test Result
**
** Params : None
**
** Returns: e_WDOG_TestResult - Watch-Dog Test result.
** Notes:
*************************************************************************************/
e_WDOG_TestResult WDOG_Get_Test_Result(void)
{
	return WDOG_Test_Result;
}
/*************************************************************************************
** WDOG_Trigger - Triggers Watch Dog by strobing WDI pin
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void WDOG_Trigger(void)
{

	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_RESET); /*Make sure WDI_PIN is LOW*/
	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_SET);   /*Set WDI_PIN to HIGH state*/
	/*Wait to form short pulse*/
	for(uint16_t delay = 0U; delay < WDOG_PULSE_DELAY ; delay++)
	{
		__NOP();
	}
	HAL_GPIO_WritePin(WDI_PORT,WDI_PIN,E_GPIO_PIN_RESET); /*Set WDI_PIN to LOW steady state*/
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
