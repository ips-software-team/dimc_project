/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : exflash_dimc.c
** Author    : Omer Geron
** Revision  : 2.2
** Updated   : 20-03-2022
**
** Description: This file contains External flash interface functions.
**
** Notes:
**
*/


/* Revision Log:
**
** Rev 1.0  : 23-08-2020 -- Original version created.
** Rev 1.1	: 26.08-2020 -- Update "Read_EEPROM" function.
** 						  Delete "pre_read_data_buffer_hal" function (unnecessary).
** Rev 1.2	: 22-09-2020 -- Init value of WrBuffer changed from {0U} -> to {0,0,0,0,0,0,0,0x55U} in order to make sure that the
** 						  Safety value will always be written to the memory.
** Rev 1.3	: 03-01-2021 -- Updated due to verification remarks.
**
**
** Rev 1.5	: 17-02-2021 -- Send '0' when there is no data to read (after erasing memory).
**
** Rev 1.6	: 15-03-2021 -- Set "g_Logger_Status" variable during INIT.
**
** Rev 1.7	: 25-04-2021 -- See LDRA comment inside the "verify_mem_hal_status_in_EEPROM"
**
** Rev 1.8  : 02-06-2021 -- Update the mem_hal verification process in order to avoid recurssion (verify <--> Erase functions).
** 						  Added 2 golbal variable (global to this file) Flash_Status: Mark EEPROM as FL in case that 2 verification failure detected in a raw.
** Rev 1.9	: 15-08-2021 -- Added define values, and update the function "Read_EEPROM".
** Rev 2.0	: 01-09-2021 -- Updated due to verification remarks.
** Rev 2.1	: 24-10-2021 -- Update due to LDRA remarks.
** Rev 2.2	: 20-03-2022 -- Update the 'POST_WRITE_DELAY_MILIS' defined value.
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "mcu_config_dimc.h"
#include "exflash_dimc.h"
#include "gipsy_hal_rcc.h"
#include "logger_dimc.h"



/* -----Definitions ----------------------------------------------------------------*/

	/* EEPROM DEVICE DATA */
#define TIMEOUT_DURATION			100U
#define EXTRACT_TIMEOUT_DURATION	150U
#define MEMORY_SIZE_IN_BYTES		0x3E7F0U	/* 255,984 BYTES */
#define DEV_SEL_ADD_WR				0xA0U		/* "10100000, WR bit =0; Chip Enable= 0; bit16=0; bit17=0 */
#define DEV_SEL_ADD_RD				0xA1U		/* "10100001, WR bit =1; Chip Enable= 0; bit16=0; bit17=0 */

	/* MEM_HAL CONSTANTS DATA */
#define MEMORY_HAL_ADDRESS			0X3E7F8U 	/* Address of the last 8 bytes in memory */
#define SAFETY_VALUE				0X55U		/* SAFETY_VALUE = 01010101 */
#define BUFFER_SIZE_IN_BYTES		8U 			/* Mem_HAL buffer size - 8 bytes */

#define	DATA_BUFF_LEN				160U

	/* 32-bits masks */
#define MASK_16_BITS				0x0000FFFFU
#define MASK_8_BITS 				0x000000FFU
#define MASK_2_BITS					0x00000003U

	/* Size of bits to shift (in bytes unit) - for data parsing */
#define MOVE_TWO_BYTES				0x10U
#define MOVE_ONE_BYTE				0x08U

#define POST_WRITE_DELAY_MILIS     10U
#define POST_READ_DELAY_MILIS      5U

#define NO_DATA_TO_READ 0xFFU


/* EEPROM Status */
#define EEPROM_OK					0U
#define EEPROM_FL					1U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*Memory status handler definition*/
struct Mem_HAL_st
{
	uint32_t current_address;
	uint32_t free_space_in_bytes;
}Mem_HAL =
{	.current_address = 0U,
	.free_space_in_bytes = 0U
};


uint8_t WrBuffer[8U] = {0,0,0,0,0,0,0,0x55U};           /* Write mem_hal_status byte buffer. */
uint8_t RdBuffer[8U] = {0U};                     		/* Read mem_hal_status byte buffer. */

/* EEPROM DEVICE local variables */
uint8_t I2C_devSelect_address_WR = 0U;
uint8_t I2C_devSelect_address_RD = 0U;
volatile uint16_t I2C_mem_address = 0U;

uint8_t EEPROM_Status = EEPROM_OK;

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

	/* Mem_HAL support functions */
static HAL_StatusTypeDef update_mem_hal(uint16_t size_of_data);
static HAL_StatusTypeDef read_mem_hal_status_from_EEPROM(void);
static HAL_StatusTypeDef write_mem_hal_status_to_EEPROM(void);
static HAL_StatusTypeDef verify_mem_hal_status_in_EEPROM(void);


/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Write_EEPROM -
** 		- Write the data in pData byte-buffer to the top of the memory.
** 		- Update the memory handler (mem_hal).
**
**
** Params : 1. uint8_t *pData 						- Pointer to the data byte buffer.
** 			2. uint8_t number_of_bytes_to_write 	- Number of bytes to write to memory.
**
** Returns: HAL_StatusTypeDef - Returns the result of the write operation.
**
** Notes:
** 		@<pre>: None.
**
** 		@<post>: 1. pData values are placed in memory.
** 				 2. mem_hal status is update after writing.
*************************************************************************************/
HAL_StatusTypeDef Write_EEPROM(uint8_t *pData, uint8_t number_of_bytes_to_write)
{
	/* Local result variable */
	HAL_StatusTypeDef write_result = HAL_OK;
	uint8_t address_bits_A17_A16 = 0U;

	/* If memory is full */
	if(0x00U == Mem_HAL.free_space_in_bytes)
	{
		write_result = Erase_EEPROM();
	}

	if(HAL_OK == write_result)
	{
		/* Set address */
		I2C_mem_address = (uint16_t)(Mem_HAL.current_address & MASK_16_BITS);

		/*Set A17 and A16 bits */
		I2C_devSelect_address_WR = DEV_SEL_ADD_WR;	/* reset device select code to its init value */
		address_bits_A17_A16 = (uint8_t)((Mem_HAL.current_address >> MOVE_TWO_BYTES ) & MASK_2_BITS);
		I2C_devSelect_address_WR |= ((address_bits_A17_A16 << 1U ) & 0x06U); /* mask A17 & A16 bits */

		/* Writing to EEPROM */
		write_result = HAL_I2C_Mem_Write(&g_HandleI2C2_ExFlash, I2C_devSelect_address_WR,I2C_mem_address, 0x10U,pData,number_of_bytes_to_write ,TIMEOUT_DURATION);
		HAL_Delay(POST_WRITE_DELAY_MILIS);
	}

	if(HAL_OK == write_result)
	{
		/* Update memory handler */
		write_result = update_mem_hal(number_of_bytes_to_write);
	}

	return write_result;
}

/*************************************************************************************
** Read_EEPROM -
** 		- Read data from a specific place in memory and place it at pData byte-buffer.
** 		- If the size of data in the memory is less than number_of_bytes_to_read,
** 			than, the data buffer extra space will be filled with "NO_DATA_TO_READ" special value (0xFFU).
** 			(see example below).
**

**
** Params : 1. uint8_t *pData 						- Pointer to the data byte buffer.
** 			2. uint16_t number_of_bytes_to_read 	- Number of bytes to read from the memory.
** 			3. uint32_t address						- Address of *the end* of the required data in memory.
** 														(i.e. address of the top of the required data block).
**
** Returns: HAL_StatusTypeDef - Returns the result of the read operation.
** Notes:
**
** 		@<pre>:  1. number_of_bytes_to_read is equal to the size of pData buffer.
**
** 		@<post>: 1. None.
**
** 		example: The memory contains only 16 bytes of data:		Read_EEPROM(byte_buffer, 20, READ_FROM_TOP_OF_MEMORY)
** 					the byte buffer will be as followed:
** 					byte_buffer[0] - byte_buffer[15] = some_data.
**					byte_buffer[16] - byte_buffer[19] = NO_DATA_TO_READ (0xFFU).
**
*************************************************************************************/
HAL_StatusTypeDef Read_EEPROM(uint8_t *pData, uint16_t number_of_bytes_to_read, uint32_t address)
{
	/* Local result variable */
	HAL_StatusTypeDef read_result = HAL_OK;
	uint8_t address_bits_A17_A16 = 0U;
	uint16_t emty_space_size = 0U;

	/* check that pData is not null */
	if (NULL == pData)
	{
		read_result = HAL_ERROR;
	}
	else
	{
		/* if address is equal to special value "READ_FROM_TOP_OF_MEMORY" */
		if(READ_FROM_TOP_OF_MEMORY == address)
		{
			/* check that the address value is no negative */
			if(number_of_bytes_to_read <= Mem_HAL.current_address)
			{
				address = Mem_HAL.current_address - number_of_bytes_to_read;
			}
			else
			{
				address = 0x00U;
			}
		}

		/* address is a specific location in memory */
		else
		{
			address -= number_of_bytes_to_read;
		}

		/* Set address */
		I2C_mem_address = (uint16_t)(address & MASK_16_BITS);

		/* Set A17 and A16 bits */
		I2C_devSelect_address_RD = DEV_SEL_ADD_RD; /* reset device select code to init value */
		address_bits_A17_A16 = (uint8_t)((address >> 16U ) & MASK_2_BITS);
		I2C_devSelect_address_RD |= ((address_bits_A17_A16 << 1U ) & 0x06U); /* mask A17 & A16 bits */

		/* Reading from EEPROM */
		read_result = HAL_I2C_Mem_Read(&g_HandleI2C2_ExFlash,I2C_devSelect_address_RD, I2C_mem_address, 0x10U, pData, number_of_bytes_to_read, TIMEOUT_DURATION);

		if(HAL_OK == read_result)
		{
			HAL_Delay(POST_READ_DELAY_MILIS);
		}

		/* Filling Empty space with '0' */
		if((address+DATA_BUFF_LEN) > Mem_HAL.current_address)/* Rev 2.1*/
		{
			emty_space_size = DATA_BUFF_LEN+address-Mem_HAL.current_address;

			if (emty_space_size > DATA_BUFF_LEN)
			{
				emty_space_size = DATA_BUFF_LEN;
			}
			for(uint8_t i = 0x00U ; i < emty_space_size ; i++)
			{
				pData[(DATA_BUFF_LEN - 1U)-i] = 0x00U;
			}
		}
	}
	return read_result;
}



/*************************************************************************************
** Init_Mem_Hal -
** 			- Reads data from special place in memory (defined as MEMORY_HAL_ADDRESS).
** 			- Check if memory has been in used before (by checking if it contains SAFETY_VALUE).
** 			- If memory was used than mem_hal is set according to the retrieved data from MEMORY_HAL_ADDRESS.
** 			- Else, mem_hal is reset (address is set to 0, and free_space is set to default value - MEMORY_SIZE_IN_BYTES).
** 			- If SAFETY_VALUE is not contained in memory than it will be placed there.
**
** Params: None
**
** Return: HAL_StatusTypeDef - Status
** Notes:
**
** 	   @<post>: 1. mem_hal holds the current address in memory.
**	 			2. mem_hal holds the current number of free bytes in memory.
** 				3. SAFETY_VALUE is placed in memory.
**
*************************************************************************************/
HAL_StatusTypeDef Init_Mem_Hal(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Reads data form special place in memory */
	hal_status = read_mem_hal_status_from_EEPROM();

	if(HAL_OK == hal_status)
	{
		/* Check if memory contains SAFETY_VALUE */
		if(SAFETY_VALUE == RdBuffer[7U])				/* if true -> memory was used and contains prev data */
		{
			/* Set current address */
			Mem_HAL.current_address = ((uint32_t)(RdBuffer[2U]) & MASK_2_BITS);			/* A17 & A16 bits */
			Mem_HAL.current_address <<= 8U;
			Mem_HAL.current_address |= ((uint32_t)(RdBuffer[1U]) & MASK_8_BITS); 		/* MIDDLE byte */
			Mem_HAL.current_address <<= 8U;
			Mem_HAL.current_address |= ((uint32_t)(RdBuffer[0U]) & MASK_8_BITS);		/* LSB */

			/* Set current free space */
			Mem_HAL.free_space_in_bytes = ((uint32_t)(RdBuffer[5U]) & MASK_2_BITS);		/* 17 & 16 data bits */
			Mem_HAL.free_space_in_bytes <<= 8U;
			Mem_HAL.free_space_in_bytes |= ((uint32_t)(RdBuffer[4U]) & MASK_8_BITS);	/* MIDDLE byte */
			Mem_HAL.free_space_in_bytes <<= 8U;
			Mem_HAL.free_space_in_bytes |= ((uint32_t)(RdBuffer[3U]) & MASK_8_BITS);	/* LSB */

			/* Mark that the memory contains previous data */
			g_Logger_Status = LOGGER_NOT_EMPTY;
		}
		else
		{
			/* Write SAFETY_VALUE to memory and reset mem_hal */
			WrBuffer[7U] = SAFETY_VALUE;

			/* Reset mem_hal */
			hal_status = Erase_EEPROM();
		}
	}
	else
	{
		hal_status = HAL_ERROR;
	}
	return hal_status;
}

/*************************************************************************************
** Erase_EEPROM -
**			- Set mem_hal.current_address to 0x00U.
**			- Set mem_hal.free_sapce_in_bytes to MEMORY_SIZE_IN_BYTES.
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
** 		@<pre>: None
**
** 		@<post>: 	1. mem_hal.current_address is 0x00U.
** 			    	2. mem_hal.free_sapce_in_bytes is MEMORY_SIZE_IN_BYTES.
*************************************************************************************/
HAL_StatusTypeDef Erase_EEPROM(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	Mem_HAL.current_address = 0x00U;
	Mem_HAL.free_space_in_bytes = MEMORY_SIZE_IN_BYTES;
	hal_status = write_mem_hal_status_to_EEPROM();
	return hal_status;
}

/*************************************************************************************
** update_mem_hal -
** 			- Increment mem_hal address by size_of_data.
** 			- Decrement mem_hal free_space by size_of_data.
**
** Params : uint16_t size_of_data - Size of data in bytes that added to memory.
**
** Returns: HAL_StatusTypeDef - Status
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef update_mem_hal(uint16_t size_of_data)
{
	/* Update mem_hal values */
	Mem_HAL.current_address += size_of_data;
	Mem_HAL.free_space_in_bytes -= size_of_data;

	/* In current implementation mem_hal status is written to the memory after every update */
	return write_mem_hal_status_to_EEPROM();
}

/*************************************************************************************
** Cbit_Mem_Test -
** 			- Check if connection to memory is OK by looking at the value of the last byte in EEPROM.
** 			- If it contains SAFETY_VALUE than it means that the 'read' function succeed.
**
** Params : None.
**
** Returns: e_ExFlash_BIT_RESULT
**
** Notes:
**		@<pre>: SAFETY_VALUE is already placed in the last byte of the EEPROM. (Done by init function)
*************************************************************************************/
e_ExFlash_BIT_RESULT Cbit_Mem_Test(void)
{
	/* Set local result vars */
	e_ExFlash_BIT_RESULT cbit_result = E_EXFLASH_BIT_FAIL;
	HAL_StatusTypeDef read_result = HAL_OK;

	/* Read data from special place in memory */
	read_result = read_mem_hal_status_from_EEPROM();

	if((HAL_OK == read_result) && (SAFETY_VALUE == RdBuffer[7U]) && (EEPROM_OK == EEPROM_Status))
	{
		cbit_result =  E_EXFLASH_BIT_PASS;
	}

	return cbit_result;
}

/*************************************************************************************
** Pbit_Mem_Test -
** 			- Check if connection to memory is OK at power-up.
** 			- Check is done by reading the last byte in EEPROM (i.e. SAFETY_VALUE).
** 			- If the last byte is equal to SAFETY_VALUE the test PASSED
** 			- Else, write SAFETY_VALUE to EEPROM and than check again by calling CBIT().
**
** Params : None.
**
** Returns: e_ExFlash_BIT_RESULT
** Notes:
*************************************************************************************/
e_ExFlash_BIT_RESULT Pbit_Mem_Test(void)
{
	/* Set local result vars */
	e_ExFlash_BIT_RESULT pbit_result = E_EXFLASH_BIT_FAIL;
	HAL_StatusTypeDef hal_status = HAL_OK;

	/* Read data from special place in memory */
	hal_status = read_mem_hal_status_from_EEPROM();

	if((HAL_OK == hal_status) && (SAFETY_VALUE == RdBuffer[7U]))
	{
		pbit_result =  E_EXFLASH_BIT_PASS;
	}
	else
	{
		/* Place safety value in write-buffer */
		WrBuffer[7U] = SAFETY_VALUE;

		/* Write safety value and current mem_hal status to memory */
		hal_status = write_mem_hal_status_to_EEPROM();
		if((HAL_OK == hal_status) && (EEPROM_OK == EEPROM_Status))
		{
			/* Check if write succeed by call Cbit function */
			pbit_result = Cbit_Mem_Test();
		}
	}
	return pbit_result;
}

/*************************************************************************************
**
** read_mem_hal_status_from_EEPROM(void)
** 			- Read data from MEMORY_HAL_ADDRESS in EEPROM.
** 			- The retrieved data is placed in "RdBuffer" byte buffer.
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - Returns the result of the read operation.
** Notes:
**
*************************************************************************************/
static HAL_StatusTypeDef read_mem_hal_status_from_EEPROM(void)
{
	/* Init local operation result variable */
	HAL_StatusTypeDef read_result = HAL_OK;

	/* Init local address variables */
	uint32_t mem_hal_address = 0x00U;
	uint8_t address_bits_A17_A16 = 0U;

	/* Read from EEPROM only if no faults detected */
	if (EEPROM_OK == EEPROM_Status)
	{
		/* Parse address of MEMORY_HAL_ADDRESS */
		mem_hal_address = MEMORY_HAL_ADDRESS;
		I2C_mem_address = (uint16_t)(mem_hal_address & MASK_16_BITS);

		/* Parse A17 & A16 bits */
		I2C_devSelect_address_RD = DEV_SEL_ADD_RD;	/* reset device select code to init value */
		address_bits_A17_A16 = (uint8_t)((mem_hal_address >> MOVE_TWO_BYTES ) & MASK_2_BITS);
		I2C_devSelect_address_RD |= ((address_bits_A17_A16 << 1U ) & 0x06U); /* mask A17 & A16 bits */

		/* Read from EEPROM */
		read_result = HAL_I2C_Mem_Read(&g_HandleI2C2_ExFlash,I2C_devSelect_address_RD, I2C_mem_address, 0x10U, RdBuffer, BUFFER_SIZE_IN_BYTES, TIMEOUT_DURATION);
		if(HAL_OK == read_result)
		{
			HAL_Delay(POST_READ_DELAY_MILIS);
		}
	}
	return read_result;
}

/*************************************************************************************
** write_mem_hal_status_to_EEPROM -
** 			- Parse and place mem_hal data in WrBuffer byte-buffer.
** 			- Write WrBuffer byte-buffer to EEPROM in MEMORY_HAL_ADDRESS.
** 			- Verify that the write succeed and the data written to EEPROM
** 				is match to mem_hal current status.
**
** Params : None.
**
** Returns: HAL_StatusTypeDef - Returns the result of the write operation.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef write_mem_hal_status_to_EEPROM(void)
{
	/* Init local operation result variable */
	HAL_StatusTypeDef write_result = HAL_OK;

	/* Init local address variables */
	uint32_t mem_hal_address = MEMORY_HAL_ADDRESS;
	uint8_t address_bits_A17_A16 = 0U;

	/* Write to EEPROM only if no faults detected */
	if (EEPROM_OK == EEPROM_Status)
	{
		/* Parse mem_hal current address */
		WrBuffer[0U] = (uint8_t)(Mem_HAL.current_address & MASK_8_BITS); 						/* current_address LSB */
		WrBuffer[1U] = (uint8_t)((Mem_HAL.current_address >> MOVE_ONE_BYTE) & MASK_8_BITS);		/* current_address MIDDLE byte */
		WrBuffer[2U] = (uint8_t)((Mem_HAL.current_address>> MOVE_TWO_BYTES) & MASK_2_BITS);		/* A17 & A16 bits */

		/* Parse mem_hal free space */
		WrBuffer[3U] = (uint8_t)(Mem_HAL.free_space_in_bytes & MASK_8_BITS);					/* free_space LSB */
		WrBuffer[4U] = (uint8_t)((Mem_HAL.free_space_in_bytes >> 8U) & MASK_8_BITS);			/* free_space MIDDLE byte */
		WrBuffer[5U] = (uint8_t)((Mem_HAL.free_space_in_bytes >> 16U) & MASK_2_BITS);			/* free_space 17 & 16 data bits */

		/* Parse address of MEMORY_HAL_ADDRESS */
		I2C_mem_address = (uint16_t)(mem_hal_address & MASK_16_BITS);

		/* Parse A17 & A16 bits */
		I2C_devSelect_address_WR = DEV_SEL_ADD_WR;	/* reset device select code to init value */
		address_bits_A17_A16 = (uint8_t)((mem_hal_address >> MOVE_TWO_BYTES) & MASK_2_BITS);
		I2C_devSelect_address_WR |= ((address_bits_A17_A16 << 1U) & 0x06U); 	/* mask A17 & A16 bits */

		/* Write to EEPROM */
		write_result = HAL_I2C_Mem_Write(&g_HandleI2C2_ExFlash, I2C_devSelect_address_WR,I2C_mem_address, 0x010U,WrBuffer,BUFFER_SIZE_IN_BYTES ,TIMEOUT_DURATION);
		if(HAL_OK == write_result)
		{
			HAL_Delay(POST_WRITE_DELAY_MILIS);
		}

		if(HAL_OK == write_result)
		{
			/* Wait for write to complete and verify that the write succeed */
			write_result = verify_mem_hal_status_in_EEPROM();
		}
	}
	return write_result;
}

/*************************************************************************************
** verify_mem_hal_status_in_EEPROM -
** 			- Verify that the write succeed and the data written to EEPROM
** 				is match to mem_hal current status.
** 			- If mem_hal status is different from memory status written in
** 				memory than mem_hal will be reset and re-write to memory.
**
** Params : None.
**
** Returns: HAL_StatusTypeDef.
** Notes:
*************************************************************************************/
static HAL_StatusTypeDef verify_mem_hal_status_in_EEPROM(void)
{
	/* Init local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t verify_pass = FALSE;
	uint8_t num_fl = 0x00U;
	uint32_t address_value = 0U;
	uint32_t free_space_value = 0U;

	while ((num_fl < 2U) && (FALSE == verify_pass))
	{
		/* Read data from MEMORY_HAL_ADDRESS */
		hal_status = read_mem_hal_status_from_EEPROM();

		/* Parse current address */
		address_value = ((uint32_t)(RdBuffer[2U]) & MASK_2_BITS);		/* A17 & A16 bits */
		address_value <<= MOVE_ONE_BYTE;
		address_value |= ((uint32_t)(RdBuffer[1U]) & MASK_8_BITS); 		/* MIDLLE byte */
		address_value <<= MOVE_ONE_BYTE;
		address_value |= ((uint32_t)(RdBuffer[0U]) & MASK_8_BITS);		/* LSB */

		/* Parse current free space */
		free_space_value = ((uint32_t)(RdBuffer[5U]) & MASK_2_BITS);	/* 17 & 16 data bits */
		free_space_value <<= MOVE_ONE_BYTE;
		free_space_value |= ((uint32_t)(RdBuffer[4U]) & MASK_8_BITS);	/* MIDDLE byte */
		free_space_value <<= MOVE_ONE_BYTE;
		free_space_value |= ((uint32_t)(RdBuffer[3U]) & MASK_8_BITS);	/* LSB */

		if((address_value != Mem_HAL.current_address) || (free_space_value != Mem_HAL.free_space_in_bytes) || (HAL_OK != hal_status))
		{
			/* Results don't match, update the logger FL counter*/
			num_fl++;

			/* Short delay between reading sessions */
			HAL_Delay(POST_READ_DELAY_MILIS);
		}

		else
		{
			verify_pass = TRUE;
		}
	}

	/* Failed to verify eeprom status 2 times, mark EEPROM as 'FL' */
	if (FALSE == verify_pass)
	{
		EEPROM_Status = EEPROM_FL;
	}

	return hal_status;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
