/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : gpio_control_dimc.c
** Author    : Leonid Savchenko
** Revision  : 2.5
** Updated   : 30-05-2023
**
** Description: This file contains function for control and monitoring GPIOs.
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 14-02-2020 -- Changed Read_FLT_Channel_A_C_Inputs()
** Rev 1.2  : 17-02-2020 -- Changed Test_CH_C_Inputs()
** Rev 1.3  : 25-02-2020 -- Changed Read_FLT_Channel_A_C_Inputs() IDR reg port.
** Rev 1.4  : 13-03-2020 -- Updated Fault_Channel_OUT_Control(),Test_OPER_Inputs(),Test_CH_C_Inputs()
** Rev 1.5  : 05-05-2020 -- Updated Read_FLT_Channel_A_C_Inputs()
** Rev 1.6  : 31-10-2020 -- Updated Read_OPER_Inputs() function
** Rev 1.7  : 03-12-2020 -- Update 'Test_Flt_CH_B' function
** Rev 1.8	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.9	: 25-04-2021 -- Updated due to verification remarks.
** Rev 2.0	: 25-04-2021 -- Update test_ch_b function.
** Rev 2.1	: 30-06-2021 -- Update test_ch_B function - test stops when a failure is detected.
** 						  Update test_ch_C function
** 						  Update the "Test_OPER_Inputs" function. Remove 'break' statment and add 'if' instead.
** Rev 2.2	: 05-10-2021 -- Updated due to integration remarks.
** Rev 2.3	: 03-02-2022 -- Update due to LDRA remarks.
** Rev 2.4	: 17-07-2022 -- Update the function "test_ch_C".
** Rev 2.5	: 30-05-2023 -- Fixing typos in the code's comments.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/

#include <mcu_config_dimc.h>
#include "gpio_control_dimc.h"
#include "module_init_dimc.h"
#include "can_protocol_dimc.h"
#include "gipsy_hal_rcc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define BOTH_OPER_ON               0x03U
#define NUM_OF_FLT_CH_OUTPUTS		4U
#define CTRY_CH_C_RESP_DELAY_MILIS  10U
#define WAIT_READ_SENS_MILLIS		5U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

const st_PinDescriptor ST_FAULT_CH_OUT_PIN[NUM_OF_FLT_CH_OUTPUTS] =
{
		{CHANNEL_A_FAULT_TTL_PIN,   CHANNEL_FAULT_TTL_OUT_PORT},
		{CHANNEL_C_FAULT_TTL_PIN,   CHANNEL_FAULT_TTL_OUT_PORT},
		{CHANNEL_B_FAULT_TTL_1_PIN, CHANNEL_FAULT_TTL_OUT_PORT},
		{CHANNEL_B_FAULT_TTL_2_PIN, CHANNEL_FAULT_TTL_OUT_PORT},
};
/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/



/*************************************************************************************
** Read_FLT_Channel_A_C_Inputs - Reads Fault Channels A/C  inputs value
**
**
** Params : uint8_t *flt_ch_a_in_val - state of 4 channel_a fault inputs
** 			uint8_t *flt_ch_c_in_val - state of 6 channel_c fault inputs
**
** Returns: None.
** Notes:
*************************************************************************************/
void Read_FLT_Channel_A_C_Inputs(uint8_t *flt_ch_a_in_val, uint8_t *flt_ch_c_in_val)
{
	uint32_t port_val = CHANNEL_FAULT_TTL_IN_PORT->IDR; /*Read input port value*/
	uint8_t tmp = 0U;
	if((NULL != flt_ch_a_in_val) && (NULL != flt_ch_c_in_val))
	{
		port_val = port_val & 0x00000FDBU;       /*CH_A --> Pins 0-1 and 3-4 | CH_C --> Pins 6-11*/
		tmp = (uint8_t)((port_val & 0x00000018U) >> 1U); /*set pins 3-4 to position 2-3*/
		*flt_ch_a_in_val = (uint8_t)(port_val & 0x00000003U) | tmp; /*combine pins 0-1 and 2-3 as 4bit result*/
		*flt_ch_c_in_val = (uint8_t)((port_val >> 6U) & 0x0000003FU);
	}
}


/*************************************************************************************
** Read_OPER_Inputs - Reads OPER inputs value and return (by pointer) operation state and the channels value
**
** Params : e_OPER_InputState oper_state 	- Pointer to oper state.
** 			uint8_t oper_ch_val 			- Pointer that will hold the channels values.
** 			uint8_t oper_1_2_val 			- Pointer that will hold the value of channel oper_1_2
**
** Returns: HAL_StatusTypeDef - Method's status
** Notes:
*************************************************************************************/
HAL_StatusTypeDef Read_OPER_Inputs(e_OPER_InputState *oper_state ,uint8_t *oper_ch_val, uint8_t *oper_1_2_val)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	*oper_state = E_OPER_OFF;
	uint32_t port_val = 0U;

	if ((NULL != oper_ch_val) && (NULL != oper_state) && (NULL != oper_1_2_val))
	{
		port_val = OPER_TTL_PORT->IDR; /*Read input port value*/
		*oper_ch_val = (uint8_t)((port_val >> 12U) & 0x00000003U);/*Get state of OPER_1,OPER_2 Pins 12-13*/
		*oper_1_2_val = (uint8_t)HAL_GPIO_ReadPin(OPER_TTL_PORT,OPER_1_2_TTL_PIN);/*Get OPER_1_2_TTL input value*/

		/*Check if both OPER inputs are '1' (ON)*/
		if(BOTH_OPER_ON == *oper_ch_val)
		{
			*oper_state = E_OPER_ON;
		}
	}

	else
	{
		hal_status = HAL_ERROR;
	}

	return hal_status;
}


/*************************************************************************************
** Read_IBIT_Input - Reads IBIT input value
**
**
** Params : None.
**
** Returns: e_IBIT_InputState - Return value of IBIT input state
** Notes:
*************************************************************************************/
e_IBIT_InputState Read_IBIT_Input(void)
{
	e_IBIT_InputState ibit_state = E_IBIT_OFF;

	if(E_GPIO_PIN_RESET == HAL_GPIO_ReadPin(IBIT_TTL_PORT,IBIT_TTL_PIN))/*Read IBIT input pin*/
	{
		ibit_state = E_IBIT_ON;
	}
	return ibit_state;
}
/*************************************************************************************
** Test_OPER_Inputs - Testing OPER1/2 and OPER combined inputs circuit according to pre-defined states
**
** Params : None.
**
** Returns: uint8_t Test result(TEST_OK - Test Passed | TEST_FL - Test Failed)
** Notes:
*************************************************************************************/
uint8_t Test_OPER_Inputs(void)
{
	uint8_t oper_test_ok = TEST_OK;/*Test passed*/
	uint32_t port_val = 0U;
	uint8_t oper_inputs_val = 0U;
	uint8_t oper_1_2_ttl_val = 0U;
	const uint8_t CPU_OPER_1_STATE = 0U;
	const uint8_t CPU_OPER_2_STATE = 1U;
	const uint8_t OPER_TTL_INPUTS_STATE = 2U;
	const uint8_t OPER_1_2_TTL_INPUT_STATE = 3U;
	const uint8_t OPER_NUM_OF_TEST_CASES = 7U;
	uint8_t OPER_TEST_PARAMS[4U][7U] =
	{
			{1U, 0U, 1U, 1U, 1U, 0U, 1U}, /*CPU_OPER_1_STATE*/
			{1U, 1U, 1U, 0U, 1U, 0U, 1U}, /*CPU_OPER_2_STATE*/
			{0U, 1U, 0U, 2U, 0U, 3U, 0U},  /*OPER_TTL_INPUTS_STATE*/
			{0U, 0U, 0U, 0U, 0U, 1U, 0U}  /*OPER_1_2_TTL_INPUT_STATE*/
	};

	for(uint8_t i = 0U; i < OPER_NUM_OF_TEST_CASES; i++)
	{
		if (TEST_OK == oper_test_ok)
		{
			/*Set CPU_OPER_1/2 pins state*/
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_1_PIN,(GPIO_PinState)(OPER_TEST_PARAMS[CPU_OPER_1_STATE][i]));
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_2_PIN,(GPIO_PinState)(OPER_TEST_PARAMS[CPU_OPER_2_STATE][i]));
			HAL_Delay(5U); /*Delay for HW transition*/

			/*Read OPER_TTL input and compare with required test result state*/
			port_val = OPER_TTL_PORT->IDR; /*Read input port value*/
			oper_inputs_val = (uint8_t)((port_val >> 12U) & 0x00000003U);/*Get state of OPER_1,OPER_2 Pins 12-13*/
			oper_1_2_ttl_val = (uint8_t)HAL_GPIO_ReadPin(OPER_TTL_PORT,OPER_1_2_TTL_PIN);/*Get OPER_1_2_TTL input value*/

	/*Rev 1.7: change statement order to match coding standards (was "oper_inputs_val != OPER_TEST_PARAMS[OPER_TTL_INPUTS_STATE][i]") */
			if((OPER_TEST_PARAMS[OPER_TTL_INPUTS_STATE][i] != oper_inputs_val) || (OPER_TEST_PARAMS[OPER_1_2_TTL_INPUT_STATE][i] != oper_1_2_ttl_val))
			{
				/*Test Failed*/
				Fault_Channel_OUT_Control(E_FLT_CH_B_OUT,E_FLT_CH_FAULT);
				oper_test_ok = TEST_FL;
			}
		}
	}

	/*Reset CPU_OPER_TTL1/2 pins to default state */
	HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_1_PIN,E_GPIO_PIN_SET);
	HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_2_PIN,E_GPIO_PIN_SET);
	return oper_test_ok;
}
/*************************************************************************************
** Test_Flt_CH_B - Testing CH_B circuit according to pre-defined states
** DIMC_HLR_1260,1270,1280
**
** Params : None.
**
** Returns: uint8_t Test result(TEST_OK - Test Passed | TEST_FL - Test Failed)
** Notes:
*************************************************************************************/
uint8_t Test_Flt_CH_B(void)
{
	uint8_t ch_b_test_ok = TEST_OK;/*Test passed*/
	uint8_t ch_b_sens = 0U;
	const uint8_t CH_B_TTL_1 = 2U;
	const uint8_t CH_B_TTL_2 = 3U;
	const uint8_t CH_B_1_STATE    = 0U;
	const uint8_t CH_B_2_STATE    = 1U;
	const uint8_t CH_B_SENS_STATE = 2U;
	const uint8_t CH_B_NUM_OF_TEST_CASES = 7U;
	uint8_t CH_B_TEST_PARAMS[3U][7U] =
	{
			/* Rev 1.7 - Update test sequence */
			{1U, 0U, 1U, 1U, 1U, 0U, 1U}, /*CH_B_1_STATE*/
			{1U, 1U, 1U, 0U, 1U, 0U, 1U}, /*CH_B_2_STATE*/
			{0U, 0U, 0U, 0U, 0U, 1U, 0U}  /*CH_B_SENS_STATE*/
			/* --- */
	};
	/*Read and save CH_B_TTL1/2 pins state*/
	GPIO_PinState ch_b_ttl_1_state = HAL_GPIO_ReadPin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_num);
	GPIO_PinState ch_b_ttl_2_state = HAL_GPIO_ReadPin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_num);

	for(uint8_t i = 0U; i < CH_B_NUM_OF_TEST_CASES; i++)
	{
		if(TEST_OK == ch_b_test_ok)
		{
			/*Set CH_B_TTL1/2 pins state*/
			HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_num,(GPIO_PinState)(CH_B_TEST_PARAMS[CH_B_1_STATE][i]));
			HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_num,(GPIO_PinState)(CH_B_TEST_PARAMS[CH_B_2_STATE][i]));
			HAL_Delay(5U); /*Delay for HW transition*/

			/*Read CH_B Sens input and compare with required test result state*/
			ch_b_sens = ((Read_FLT_Sense_Inputs() >> 1U) & 0x01U);
			if(CH_B_TEST_PARAMS[CH_B_SENS_STATE][i] != ch_b_sens)
			{
				/* Rev 1.7 - was '&=' now '|=' */
				ch_b_test_ok = TEST_FL;	/* Rev 1.9.2 */
				Fault_Channel_OUT_Control(E_FLT_CH_C_OUT,E_FLT_CH_FAULT);/* Rev 1.9.2 */
			}
		}
	}

	/*Set CH_B_TTL1/2 pins state with initial saved values*/
	HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_1].Pin_num,ch_b_ttl_1_state);
	HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_port,ST_FAULT_CH_OUT_PIN[CH_B_TTL_2].Pin_num,ch_b_ttl_2_state);
	return ch_b_test_ok;
}
/*************************************************************************************
** Test_CH_C_Inputs - Performs Fault Channel_C inputs test by sending Ch_C activation
**                    commands to CTRYs in pre-defined order.
**
** Params : None
**
** Returns: uint8_t - Test result(TEST_OK - Test Passed | TEST_FL - Test Failed)
** Notes:
*************************************************************************************/
uint8_t Test_CH_C_Inputs(void)
{
    const uint8_t ch_c_ttl_num[CTRY_MAX_NUM] = {0x03U, 0x00U, 0x03U, 0x00U, 0x04U, 0x01U, 0x04U, 0x01U, 0x04U, 0x01U, \
												0x04U, 0x01U, 0x05U, 0x02U, 0x05U, 0x02U, 0x05U, 0x02U, 0x05U, 0x02U};
	uint8_t ch_c_state = 0U;
	uint8_t ch_a_state = 0U;
	uint8_t sens_state = 0U;
	uint8_t ch_c_test_ok = TEST_OK;
	uint8_t ch_c_required_state[1U] ={0U};
	uint8_t test_sens = TRUE;

	/* Read current channel C state */
	Read_FLT_Channel_A_C_Inputs(&ch_a_state, &ch_c_state);
	sens_state = Read_FLT_Sense_Inputs();

	/* Pre-test check */
	if (0x3FU != ch_c_state)
	{
		if(1U != ((sens_state >>2U) & 0x01U))
		{
			/* channel C ttl is ON therefore channel c sens will always stay ON */
			test_sens = FALSE;
		}

		else
		{
			/* Mismatch between channel c ttl and channel c sens */
			ch_c_test_ok = TEST_FL;
		}
	}

	/* Perform test */
	for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM; ctry_num++)
	{
		if (TEST_OK == ch_c_test_ok)
		{
			/* Monitor Channel_C input  that is not equal to �0�( i.e. no failure) before the test execution  */
			if (1U == ((ch_c_state >> ch_c_ttl_num[ctry_num]) & 0x01U))
			{
				/*Test Channel_C HIGH state */
				ch_c_required_state[0U] = 1U;
				CAN_Message_Transmit(ID_CTRY_CHANNEL_C_CTRL_REQ, (ctry_num + 1U), ch_c_required_state);/*Command CTRY to set CH_C output to high */
				HAL_Delay(CTRY_CH_C_RESP_DELAY_MILIS);/*Wait for CTRYs to set their output CH_C*/
				Read_FLT_Channel_A_C_Inputs(&ch_a_state, &ch_c_state);
				sens_state = Read_FLT_Sense_Inputs();

				/* Test channel c ttl value */
				if (0U != ((ch_c_state >> ch_c_ttl_num[ctry_num]) & 0x01U))
				{
					ch_c_test_ok = TEST_FL;
				}

				/* Test channel c sense value */
				if ((TRUE == test_sens) && (0U != ((sens_state >> 2U) & 0x01U)))
				{
					ch_c_test_ok = TEST_FL;
				}

				/*Test Channels_C LOW state */
				ch_c_required_state[0U] = 0U;
				CAN_Message_Transmit(ID_CTRY_CHANNEL_C_CTRL_REQ, CAN_BROADCAST_ADDR, ch_c_required_state);/*Command all CTRYs to set CH_C output to low */
				HAL_Delay(CTRY_CH_C_RESP_DELAY_MILIS);/*Wait for CTRYs to set their output CH_C*/
				Read_FLT_Channel_A_C_Inputs(&ch_a_state, &ch_c_state);
				sens_state = Read_FLT_Sense_Inputs();

				/* Test channel c ttl value */
				if (1U != ((ch_c_state >> ch_c_ttl_num[ctry_num]) & 0x01U))
				{
					ch_c_test_ok = TEST_FL;
				}

				/* Test channel c sense value */
				if ((TRUE == test_sens) && (0U == ((sens_state >> 2U) & 0x01U)))
				{
					ch_c_test_ok = TEST_FL;
				}
			}
		}
	}

	/* Test DIMC channel C output control */
	if((TRUE == test_sens) && (TEST_OK == ch_c_test_ok))
	{
		Fault_Channel_OUT_Control(E_FLT_CH_C_OUT,E_FLT_CH_FAULT);
		HAL_Delay(WAIT_READ_SENS_MILLIS);
		sens_state = Read_FLT_Sense_Inputs();
		sens_state = ((sens_state >> 2U) & 0x01U); /*sens_state 3rd bit represents Sense Ch_C input state*/

		if(0x00U == sens_state)
		{
			/* Reset fault channel */
			Fault_Channel_OUT_Control(E_FLT_CH_C_OUT,E_FLT_CH_OK);
			HAL_Delay(WAIT_READ_SENS_MILLIS);
			sens_state = Read_FLT_Sense_Inputs();
			sens_state = ((sens_state >> 2U) & 0x01U); /*sens_state 3rd bit represents Sense Ch_C input state*/

			/* Verify that the channel was reset correctly */
			if(0x01 != sens_state)
			{
				ch_c_test_ok = TEST_FL;
			}
		}
		else
		{
			ch_c_test_ok = TEST_FL;
		}
	}

#ifndef _TEST_BENCH_CONFIGURATION_
	/*Check if test marked as Fail*/
	if(TEST_FL == ch_c_test_ok)
	{
		Fault_Channel_OUT_Control(E_FLT_CH_C_OUT,E_FLT_CH_FAULT);
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT,E_FLT_CH_FAULT);
	}
#endif

	return ch_c_test_ok;
}
/*************************************************************************************
** Read_FLT_Sense_Inputs - Reads all 3 Sense inputs value directly from port input register.
**
**
** Params : None.
**
** Returns: uint8_t return value of 3 sense inputs
** 					-Bit#0 - channel A
** 					-Bit#1 - channel B
** 					-Bit#2 - channel C
** Notes:
*************************************************************************************/
uint8_t Read_FLT_Sense_Inputs(void)
{
	uint8_t flt_sens_val = 0U;
	uint32_t port_val = FAIL_SENS_TTL_PORT->IDR; /*Read input port value*/
	flt_sens_val = (uint8_t)((port_val >> 2U) & 0x00000007U);/*Pins 2-4*/

	return flt_sens_val;
}

/*************************************************************************************
** Fault_Channel_OUT_Control - Control of Fault Channel Output pins
**
**
** Params : e_FAULT_CH_Out flt_ch_name - Name of required Fault Channel(E_FAULT_CH_ALL --> apply required state to all Fault Channel's pins)
** 			e_FAULT_CH_State flt_ch_state - Required Fault channel's state (OK/Fault)
**
** Returns: None
** Notes:
*************************************************************************************/
void Fault_Channel_OUT_Control(e_FAULT_CH_Out flt_ch_name, e_FAULT_CH_State flt_ch_state)
{
	if(E_FLT_CH_ENUM_NUM > flt_ch_name)
	{
		/*Set single fault channel*/

		HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[flt_ch_name].Pin_port,ST_FAULT_CH_OUT_PIN[flt_ch_name].Pin_num,(GPIO_PinState)flt_ch_state);
		/*Channel B has 2 output pins*/
		if(E_FLT_CH_B_OUT == flt_ch_name)/*For CH_B set both outputs*/
		{
			HAL_GPIO_WritePin(ST_FAULT_CH_OUT_PIN[flt_ch_name+1U].Pin_port,ST_FAULT_CH_OUT_PIN[flt_ch_name+1U].Pin_num,(GPIO_PinState)flt_ch_state);
		}
	}
}

/*************************************************************************************
** Manage_OnBoard_Leds - On-Board Staus Leds management.
** 						1. Toggle SW_RUN_LED (LED1)
** 						2. Manage FLT_LED Status (LED2).
**
** Params : None
**
** Returns: None
** Notes: OG: change discrete input read: was '!= 0x00U' now -> '!= 0x07U'
*************************************************************************************/
void Manage_OnBoard_Leds(void)
{
	HAL_GPIO_TogglePin(LED_PORT,SW_RUN_LED_PIN);/*Toggle SW_RUN Led*/
#ifdef _EVBOARD_
	HAL_GPIO_TogglePin(LD3_GPIO_Port,LD3_Pin);/*Toggle SW_RUN Led - ONLY IN EVALUATION BOARD*/
#endif
	/*Set state of FLT_LED*/
	if((0U != g_SysStatus.DIMC_PBIT_Status) || (0U != g_SysStatus.DIMC_CBIT_Status)  ||\
	  (0U != g_SysStatus.CTRYs_Status) || (0U != g_SysStatus.All_CTRYs_PBIT_Status) ||\
	  (7U != (Read_FLT_Sense_Inputs() & 0x07U)))
	{
		HAL_GPIO_WritePin(LED_PORT,FLT_LED_PIN,E_GPIO_PIN_RESET);/*Turn FLT_LED - ON*/
	}
	else
	{
		HAL_GPIO_WritePin(LED_PORT,FLT_LED_PIN,E_GPIO_PIN_SET);/*Turn FLT_LED - OFF*/
	}
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
