/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : cbit_dimc.c
** Author    : Leonid Savchenko
** Revision  : 1.7
** Updated   : 07-05-2023
**
** Description: This file contains the CBIT procedure.
**
** Notes:
**
** 			g_SysStatus.DIMC_CBIT_Failure_Desc BIT Layout:
** 			Bit #1  -  CTRAY monitor table result.
** 			Bit #3  -  Channel A status.
** 			Bit #4  -  Channel C status.
**
*/


/* Revision Log:
**
** Rev 1.0  : 02-09-2020 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2	: 14-03-2021 -- Update the implementation of the OPER inputs test.
** Rev 1.3  : 25-04-2021 -- Update the file description (header) due to verification remarks.
** Rev 1.4  : 04-07-2021 -- OPER test will fail only if fault is detected twice in a raw.
** 						  Change CBIT return value to void.
** Rev 1.5	: 23-08-2021 -- Updated due to verification remarks.
** Rev 1.6	: 20-03-2022 -- Update failure value of the voltage test to 'TEST_FL'.
** Rev 1.7	: 07-05-2023 -- Removed the 5VDC test.
** 							Removed the OPER signals integrity test.
** 							Update 'Notes' comment section according to the changes.
** 							Update function's header.
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "system_params_dimc.h"
#include "gpio_control_dimc.h"
#include "voltage_meas_dimc.h"
#include "logger_dimc.h"
#include "cbit_dimc.h"
#include "fault_management_dimc.h"
#include "module_init_dimc.h"
#include "can_protocol_dimc.h"
#include "periodic_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define STATUS_TABLE_FLT		0x02U
#define CHANNEL_A_FLT			0x08U
#define	CHANNEL_C_FLT			0x10U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
**
** Perform_CBIT_Test(void)
** 		- Perform Cyclic-BIT as specify in HLR: DIMC CBIT Capability.
** 		- Monitor the status of the CTRAYs and the actuators.
** 		- Test fault channels A, C.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Perform_CBIT_Test(void)
{
	/* Init CBIT test variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	uint8_t channel_A_value = 0x00U;
	uint8_t channel_C_value = 0x00U;

	/* Test CTRY's status */
	if(E_MON_TBL_FL == Monitor_Status_Table())
	{
		/* Set test_result */
		g_SysStatus.DIMC_CBIT_Failure_Desc |= STATUS_TABLE_FLT;
		g_SysTestsStatus.DIMC_Monitor_Status_Table_Test_Status = (uint8_t)E_MON_TBL_FL;
	}

	/* Read the values of channels A & C */
	Read_FLT_Channel_A_C_Inputs(&channel_A_value, &channel_C_value);

	/* Check if channel A status */
	if(0x0FU != channel_A_value)
	{
		/* Set test_result */
		g_SysStatus.DIMC_CBIT_Failure_Desc |= CHANNEL_A_FLT;
		g_SysStatus.ChannelA_FaultsStatus = channel_A_value;
	}

	/* Check if channel C status */
	if(0x3FU != channel_C_value)
	{
		/* Set test_result */
		g_SysStatus.DIMC_CBIT_Failure_Desc |= CHANNEL_C_FLT;
		g_SysStatus.ChannelC_FaultsStatus = channel_C_value;
	}

	/* SW Error found */
	if (HAL_OK != hal_status)
	{
		g_SysStatus.SW_Erros = TRUE;
	}

	/* CBIT failed */
	if(0x00U != g_SysStatus.DIMC_CBIT_Failure_Desc)
	{
		/* Handle faults */
		Fault_Management();
	}
}


/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
