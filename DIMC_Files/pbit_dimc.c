/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : pbit_dimc.c
** Author    : Leonid Savchenko
** Revision  : 1.9
** Updated   : 10-05-2023
**
** Description: - PBIT � Power-Up BIT � Is a module testing procedure which performed during module HW initialization phase.
** 				  PBIT tests the module as standalone entity, no in\out interfaces being tested.
**
** Notes:
**
**
*/


/* Revision Log:
**
** Rev 1.0  : 02-09-2020 -- Original version created.
** Rev 1.1	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.2	: 18-04-2021 -- Adding the "send_operation_limits" function and supported variable.
** Rev 1.3  : 04-07-2021 -- Update the value of "CTRY_MAX_RESP_DELAY_MILIS" to 250 (miliseconds).
** 						  Removed the 'else' statment in the channel A test section. See comment below.
** 						  Update the SW_Error varible in case that a bug occurs.
** Rev 1.4  : 11-07-2021 -- Added doxygen comments
** Rev 1.5  : 18-07-2021 -- Update "send_operation_limits" function implementation. CTRY will be marked as FL in case that the received limits do not match.
** Rev 1.6	: 25-07-2021 -- Update the call to the function "Set_CTRY_All_Actuators_Status_FL".
** Rev 1.7	: 30-01-2022 -- Update the function "Perform_System_PBIT_Test" - fixing PR: DIMC-26.
** 							Update the function "send_operation_limits".
** 							Removed the defined value 'NA_ACT_ID'.
** 							Fixing comments typo error.
** Rev 1.8	: 24-07-2022 -- Update the function "Perfoem_System_PBIT_Test".
** Rev 1.9	: 10-05-2023 -- Update the function "send_operation_limits" (Sending IBIT limits as well).
** 							Update the function "Perform_System_PBIT_Test".
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "logger_dimc.h"
#include "exflash_dimc.h"
#include "voltage_meas_dimc.h"
#include "diac_mngmnt.h"
#include "gpio_control_dimc.h"
#include "fault_management_dimc.h"
#include "pbit_dimc.h"
#include "system_params_dimc.h"
#include "module_init_dimc.h"
#include "can_protocol_dimc.h"
#include "gipsy_hal_gpio.h"
#include "mcu_config_dimc.h"
#include "gipsy_hal_rcc.h"
#include "wdog.h"

/* Include the supported functions for the DIMC Test-Bench */
/* For HLT Execution Only */
#ifdef _TEST_BENCH_CONFIGURATION_
#include "../Test_Bench_Config/dimc_test_bench_config.h"
#define CAN_TX_DELAY						(uint8_t)150U	/* Match the delay value to the CTRY Simulator SW */
#endif

/* -----Definitions ----------------------------------------------------------------*/
#define PBIT_FLT_EXFLASH   					(uint8_t)0x01U
#define PBIT_FLT_POWER_5V  					(uint8_t)0x02U
#define PBIT_FLT_CANBUS    					(uint8_t)0x04U
#define PBIT_FLT_CTRY_SW_MISMATCH  			(uint8_t)0x08U
#define PBIT_FLT_OPER_INPUTS    			(uint8_t)0x10U
#define PBIT_FLT_CH_A_TEST    				(uint8_t)0x20U
#define PBIT_FLT_CH_B_TEST    				(uint8_t)0x40U
#define PBIT_FLT_CH_C_TEST    				(uint8_t)0x80U

#define CTRY_MAX_RESP_DELAY_MILIS           (uint8_t)250U
#define CTRY_MSG_RECEPTION_TIME_ERR_VAL     (uint32_t)1U
#define CTRY_WAIT_RESP_DELAY_MILIS          (uint32_t)10U

#define CTRYS_INIT_WAIT_PERIOD_MILIS        (uint16_t)2600U
#define DELAY_INTERVAL_MILLI				(uint16_t)650U

#define OPER_LIM							(uint8_t)0x80U
#define IBIT_LIM							(uint8_t)0x00U

#define DISCRETE_STABLE_TIME				(uint8_t)10U
#define CAN_RETRY_TIME						(uint8_t)50U

#ifndef _TEST_BENCH_CONFIGURATION_
#define CAN_TX_DELAY						(uint8_t)10U
#endif
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
/*Global Peripherals handlers*/
/*!Specifies respond of the CTRY to actuator limits update message (Range: var type | Units: see var declaration)*/
/*!\sa Set function: HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Get function: send_operation_limits()*/
st_Ctry_Lim_Resp g_CTRY_Resp[CTRY_MAX_NUM] = {0U};

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/
static void send_operation_limits (e_Oper_Lim_Type oper_type);

/* -----Modules implementation -----------------------------------------------------*/



/*************************************************************************************
** Perform_DIMC_PBIT_Test - Perform Power-Up BIT as specify in HLR: DIMC PBIT Capability.
** 		- Test external flash memory interface.
** 		- Test 5VDC power.
** 		- Read CAN-BUS loopback test result.
** 		- Test fault channels A, B,
** 		- OPER inputs test
**
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Perform_DIMC_PBIT_Test(void)
{
	/* Perform 5VDC power source test */
	if (E_VOLT_TEST_OK != Test_OnBoard_Voltage())
	{
		g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_POWER_5V;
		g_SysTestsStatus.DIMC_Volt_Test_Status = TEST_FL;
	}

	/* Test external flash memory interface */
	if(TEST_OK == g_SysTestsStatus.DIMC_Logger_Test_Status)
	{
		if (E_EXFLASH_BIT_FAIL == Pbit_Mem_Test())
		{
			g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_EXFLASH;
			g_SysTestsStatus.DIMC_Logger_Test_Status = TEST_FL;
		}
	}

	/* Check CAN BUS init and loop-back test result */
	if(TEST_OK != g_SysTestsStatus.DIMC_Can_Bus_Init_Test_Status)
	{
		g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_CANBUS;
	}

	/* Test channel B */
	if (TEST_FL == Test_Flt_CH_B())
	{
		/*Fault_CH_C activated inside the test procedure*/
		g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_CH_B_TEST;
		g_SysTestsStatus.DIMC_Ch_B_Test_Status = TEST_FL;
	}

	/* Test OPER channels */
	if (TEST_FL == Test_OPER_Inputs())
	{
		/*Fault_CH_B activated inside the test procedure*/
		g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_OPER_INPUTS;
		g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status = TEST_FL;
	}

	/* Test channel A */
	if (TEST_FL == Test_CH_A_Inputs())
	{
		g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_CH_A_TEST;
		g_SysTestsStatus.DIMC_Ch_A_Test_Status = TEST_FL;
		Diac_Control(E_DIAC_PS_ALL, E_DIAC_OFF);/*Make sure all DIACS deactivated*/
	}

	/* If channel A test pass, then all DIACs will be activated by the test procedure */

/* For HLT Execution Only */
#ifdef _TEST_BENCH_CONFIGURATION_
	DIMC_Pbit_Recovery();
#endif

	/*Evaluate DIMC PBIT status*/
	if(0x00U != g_SysStatus.DIMC_PBIT_Failure_Desc)
	{
		g_SysStatus.DIMC_PBIT_Status = TEST_FL;

		/* Handle faults */
		Fault_Management();
	}
}

/*************************************************************************************
** Perform_System_PBIT_Test - Perform System Power up tests that require CTRYs response.
**							  The tests are being executed after CTRY identification procedure.
**
** Param : 	None.
**
** Returns: None.
** Notes:
*************************************************************************************/
void Perform_System_PBIT_Test(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef logger_status = HAL_OK;
	uint8_t tx_can_data_buf[7U] = {0U};		/* Data Buffer - used to send data to the CTRY's via CAN BUS */
	uint32_t wait_ms = 0x00U;
	uint8_t wait_for_resp = FALSE;
	uint8_t retry = FALSE;
	uint8_t spare_data[3U] = {0U}; 			/* for fault logging */


	if(E_INIT_MODE == g_SysStatus.Sys_Mode)/*Perform System PBIT if during DIMC internal PBIT no critical failure detected(SAFE mode) */
	{
		/* Wait for CTRY to exit from INIT mode */
		for (uint16_t delay_count = 0U; delay_count < CTRYS_INIT_WAIT_PERIOD_MILIS; delay_count += DELAY_INTERVAL_MILLI)
		{
			WDOG_Trigger();
			HAL_Delay(DELAY_INTERVAL_MILLI);
		}

		/*Perform:
		 * 1. CTRY's identification
		 * 2. Receive PBIT result/SW version
		 * 3. Perform Motor Operate test - High State
		 * 4. Test channel C.
		 * */

		if(TEST_OK == g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status)
		{
			/*Simulate CPU_OPER_1/2 pins state - '0' (DIMC Motor_Operate output is HIGH)*/
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_1_PIN,E_GPIO_PIN_RESET);
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_2_PIN,E_GPIO_PIN_RESET);
			HAL_Delay(DISCRETE_STABLE_TIME);
		}

		for(uint8_t ctry_num = 0U; CTRY_MAX_NUM > ctry_num; ctry_num++)
		{
			/* Trigger WDG */
			WDOG_Trigger();

			/*Perform CTRY's identification*/
			/*Set default fail values*/
			g_CTRYs_SW_Ver[ctry_num] = CTRY_SW_ERR_VAL;
			g_SysStatus.CTRY_PBIT_Status[ctry_num].PBIT_Status = TEST_FL;
			g_SysStatus.CTRY_PBIT_Status[ctry_num].PBIT_Failure_Desc = 0xFFFFU;
			g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status = TEST_FL;
			g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status = TEST_FL;
			g_SysStatus.CTRY_Status[ctry_num].Mot_Oper_In_State = 0U;
			g_SysStatus.CTRY_Status[ctry_num].respond_time  = CTRY_MSG_RECEPTION_TIME_ERR_VAL;

			/*Save CAN command transmit time*/
			g_SysStatus.CTRY_Status[ctry_num].transmit_time = HAL_GetTick();
			wait_ms = HAL_GetTick();

			/* Send status request to the current CTRY */
			CAN_Message_Transmit(ID_CTRY_STATUS_REQ, (ctry_num+1U), tx_can_data_buf);
			wait_for_resp = TRUE;
			retry = FALSE;

			/* Wait for CTRY's ID response */
			while ((CTRY_MAX_RESP_DELAY_MILIS > (HAL_GetTick() - wait_ms)) && (TRUE == wait_for_resp))
			{
				if((CTRY_MSG_RECEPTION_TIME_ERR_VAL != g_SysStatus.CTRY_Status[ctry_num].respond_time) && \
				  (g_SysStatus.CTRY_Status[ctry_num].transmit_time <= g_SysStatus.CTRY_Status[ctry_num].respond_time))
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status = TEST_OK;
					wait_for_resp = FALSE;/*CTRY successfully identified before maximal response time interval elapsed - Exit wait loop!*/
					HAL_Delay(CAN_TX_DELAY);
				}

				/* Retry */
				else if ((FALSE == retry) && (CAN_RETRY_TIME <= (HAL_GetTick() - wait_ms)))
				{
					CAN_Message_Transmit(ID_CTRY_STATUS_REQ, (ctry_num+1U), tx_can_data_buf);
					retry = TRUE;
				}
			}

			/* CTRY failed to respond to message request */
			if(TRUE == wait_for_resp)
			{
				g_SysStatus.CTRY_Status[ctry_num].failed_to_resp = TRUE;
			}

			/* Send PBIT request */
			g_SysStatus.CTRY_Status[ctry_num].respond_time  = CTRY_MSG_RECEPTION_TIME_ERR_VAL;
			g_SysStatus.CTRY_Status[ctry_num].transmit_time = HAL_GetTick();
			CAN_Message_Transmit(ID_CTRY_PBIT_RESULT_REQ, (ctry_num+1U), tx_can_data_buf);
			wait_ms = HAL_GetTick();
			wait_for_resp = TRUE;
			retry = FALSE;

			while ((CTRY_MAX_RESP_DELAY_MILIS > (HAL_GetTick() - wait_ms)) && (TRUE == wait_for_resp))
			{
				if((CTRY_MSG_RECEPTION_TIME_ERR_VAL != g_SysStatus.CTRY_Status[ctry_num].respond_time) && \
				  (g_SysStatus.CTRY_Status[ctry_num].transmit_time <= g_SysStatus.CTRY_Status[ctry_num].respond_time))
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status |= TEST_OK;
					wait_for_resp = FALSE; /* CTRY successfully identified before maximal response time interval elapsed - Exit wait loop! */
					HAL_Delay(CAN_TX_DELAY);
				}

				/* Retry */
				else if ((FALSE == retry) && (CAN_RETRY_TIME <= (HAL_GetTick() - wait_ms)))
				{
					CAN_Message_Transmit(ID_CTRY_PBIT_RESULT_REQ, (ctry_num+1U), tx_can_data_buf);
					retry = TRUE;
				}
			}

			/* CTRY failed to respond to message request */
			if(TRUE == wait_for_resp)
			{
				g_SysStatus.CTRY_Status[ctry_num].failed_to_resp = TRUE;
			}

			if (TEST_OK == g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status)
			{
				/*Check CTRY's Motor Operate Input state - mark as fail if not equal to '0'*/
				if(0U == g_SysStatus.CTRY_Status[ctry_num].Mot_Oper_In_State)
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status = TEST_OK;
				}
				else
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status = TEST_FL;
				}
			}
		}

		/*Perform second step of CTRYs' Motor Operate test*/
		if(TEST_OK == g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status)
		{
			/*Simulate CPU_OPER_1/2 pins state - '1' (DIMC Motor_Operate output is LOW)*/
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_1_PIN,E_GPIO_PIN_SET);
			HAL_GPIO_WritePin(BIT_OPERATE_PORT,CPU_BIT_OPERATE_2_PIN,E_GPIO_PIN_SET);
			HAL_Delay(DISCRETE_STABLE_TIME);
		}

		for(uint8_t ctry_num = 0U; CTRY_MAX_NUM > ctry_num; ctry_num++)
		{
			/* Trigger WDG */
			WDOG_Trigger();

			/*Perform second step of CTRYs' Motor Operate test only if previous step('1') not failed*/
			if(TEST_OK == g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status)
			{
				/* Send status request to the current CTRY */
				g_SysStatus.CTRY_Status[ctry_num].transmit_time = HAL_GetTick();
				wait_ms = g_SysStatus.CTRY_Status[ctry_num].transmit_time;
				g_SysStatus.CTRY_Status[ctry_num].respond_time  = CTRY_MSG_RECEPTION_TIME_ERR_VAL;
				CAN_Message_Transmit(ID_CTRY_STATUS_REQ, (ctry_num+1U), tx_can_data_buf);
				wait_for_resp = TRUE;
				retry = FALSE;

				while ((CTRY_MAX_RESP_DELAY_MILIS > (HAL_GetTick() - wait_ms)) && (TRUE == wait_for_resp))
				{
					if((CTRY_MSG_RECEPTION_TIME_ERR_VAL != g_SysStatus.CTRY_Status[ctry_num].respond_time) && \
					  (g_SysStatus.CTRY_Status[ctry_num].transmit_time <= g_SysStatus.CTRY_Status[ctry_num].respond_time))
					{
						g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status |= TEST_OK;
						wait_for_resp = FALSE;/*CTRY successfully identified before maximal response time interval elapsed - Exit wait loop!*/
						HAL_Delay(CAN_TX_DELAY);
					}

					/* Retry */
					else if ((FALSE == retry) && (CAN_RETRY_TIME <= (HAL_GetTick() - wait_ms)))
					{
						CAN_Message_Transmit(ID_CTRY_STATUS_REQ, (ctry_num+1U), tx_can_data_buf);
						retry = TRUE;
					}
				}

				/* CTRY failed to respond to mesage request */
				if(TRUE == wait_for_resp)
				{
					g_SysStatus.CTRY_Status[ctry_num].failed_to_resp = TRUE;
				}

				/*Check CTRY's Motor Operate Input state - mark as fail if not equal to '1'*/
				if(1U == g_SysStatus.CTRY_Status[ctry_num].Mot_Oper_In_State)
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status |= TEST_OK;
				}
				else
				{
					g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status = TEST_FL;
				}
			}
		}


		/*CTRY's SW version mismatch test and CTRYs' status update in System Status Table*/
		g_SysTestsStatus.CTRYs_SW_Ver_Mismatch_Test_Status = TEST_OK;
		for(uint8_t ctry_num = 1U; ctry_num < CTRY_MAX_NUM; ctry_num++)
		{
			/*CTRY's SW version mismatch test and CTRYs' status update in System Status Table*/
			if((CTRY_SW_ERR_VAL == g_CTRYs_SW_Ver[ctry_num - 1U]) ||(g_CTRYs_SW_Ver[ctry_num] != g_CTRYs_SW_Ver[ctry_num - 1U]))
			{
				g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_CTRY_SW_MISMATCH;
				g_SysTestsStatus.CTRYs_SW_Ver_Mismatch_Test_Status = TEST_FL;
				logger_status |= Write_Event_To_Memory(E_FAULT,E_FC_CTRYS_SW_VERSION_TEST_FAIL,g_CTRYs_SW_Ver[ctry_num - 1U] ,E_UNIT_TYPE_CTRY,ctry_num,spare_data);
			}
		}

#ifdef _TEST_BENCH_CONFIGURATION_
		DIMC_System_PBIT_Recovery();
#endif

		/* Mark CTRYs as OK if all test passed */
		for(uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM ; ctry_num++)
		{
			if ((TEST_OK == g_SysStatus.CTRY_Status[ctry_num].Ctry_ID_Test_Status) 	&&\
				(0x00U == g_SysStatus.CTRY_PBIT_Status[ctry_num].PBIT_Status) 		&&\
				(CTRY_SW_ERR_VAL != g_CTRYs_SW_Ver[ctry_num]) 						&&\
				(TEST_OK == g_SysStatus.CTRY_Status[ctry_num].Ctry_Mot_Oper_In_Test_Status))
			{
				hal_status |= Set_CTRY_Status(ctry_num, E_CTRY_OK);
			}
		}

		/* Trigger WDG */
		WDOG_Trigger();


		/*Perform CH_C test*/
		if(TEST_FL == Test_CH_C_Inputs())
		{
/* Executed only in operatioal version */
#ifndef _TEST_BENCH_CONFIGURATION_
			/*Fault_CH_B/C activated inside the test procedure*/
			g_SysStatus.DIMC_PBIT_Failure_Desc |= PBIT_FLT_CH_C_TEST;
			g_SysTestsStatus.DIMC_Ch_C_Test_Status = TEST_FL;
#endif
		}

		/* Send vibration and current limits */
		send_operation_limits(E_OPER_LIM);
		send_operation_limits(E_IBIT_LIM);

		/* SW Error found */
		if (HAL_OK != hal_status)
		{
			g_SysStatus.SW_Erros = TRUE;
		}

		/* logger faults */
		if (HAL_OK != logger_status)
		{
			g_SysStatus.Logger_FL = TRUE;
		}

		/* Handle faults discovered during CTRYs INIT phase */
		Fault_Management();
	}
}

/*************************************************************************************
** send_operation_limits - Send the current cunsomption and vibration limits of each actuator to the relevant CTRY
** 							  for OPERATION mode and IBIT mode.
** 							  Verify the reception of the limits by the CTRYs.
**
** Param : 	e_Oper_Lim_Type oper_type - specify the type of limits needed to be sent (IBIT / OPER).
**
** Returns: None.
** Notes:
*************************************************************************************/
static void send_operation_limits (e_Oper_Lim_Type oper_type)
{
	/* Local variables */
	HAL_StatusTypeDef hal_status = HAL_OK;
	HAL_StatusTypeDef logger_status = HAL_OK;
	uint8_t max_vib = 0x00U;
	uint8_t min_vib = 0x00U;
	uint8_t max_curr = 0x00U;
	uint8_t min_curr = 0x00U;
	uint8_t data_buff[8U] = {0U};
	uint8_t logger_spare_bytes[3U] = {0U};
	e_CtryStatus ctry_st = E_CTRY_OK;
	uint16_t flt_act_id = 0x00U;
	uint32_t all_ctry_statuses = 0x00U;

	/* Iterate over the CTRYs */
	for (uint8_t ctry_num = 0U; ctry_num < CTRY_MAX_NUM ; ctry_num++)
	{
		/* Trigger WDG */
		WDOG_Trigger();

		/* Get CTRY status */
		hal_status |= Get_CTRY_Status(ctry_num, &ctry_st);

		if	(E_CTRY_OK == ctry_st)
		{
			/* Send operation limits to all the CTRY's actuators */
			for (uint8_t act_num = 0U; act_num < (uint8_t)E_ACT_MAX_NUM ; act_num++)
			{

				/* Get actuator's operation limits */
				if(E_OPER_LIM == oper_type)
				{
					hal_status |= Get_Actuator_VibrLimits(ctry_num, act_num, &max_vib, &min_vib);
					hal_status |= Get_Actuator_CurrLimits(ctry_num, act_num, &max_curr, &min_curr);
				}
				else
				{
					max_vib = IBIT_TEST_UPPER_VIBRATION_LIMIT;
					min_vib = IBIT_TEST_LOWER_VIBRATION_LIMIT;
					max_curr = IBIT_TEST_UPPER_CURRENT_LIMIT;
					min_curr = IBIT_TEST_LOWER_CURRENT_LIMIT;
				}

				/* Send operation limits */
				data_buff[0U] = act_num | ((uint8_t)oper_type << 7U);
				data_buff[1U] = max_vib;
				data_buff[2U] = min_vib;
				data_buff[3U] = max_curr;
				data_buff[4U] = min_curr;
				CAN_Message_Transmit(ID_ACTUATORS_LIM_UPD_REQ, (ctry_num + 1U), data_buff);
				HAL_Delay(CAN_TX_DELAY);

				/* Retry */
				if(TRUE	!=  g_CTRY_Resp[ctry_num].act_resp[act_num].resp)
				{
					CAN_Message_Transmit(ID_ACTUATORS_LIM_UPD_REQ, (ctry_num + 1U), data_buff);
					HAL_Delay(CAN_TX_DELAY);
				}

				/* Verify data reception by the CTRY */
				if ((TRUE		!=  g_CTRY_Resp[ctry_num].act_resp[act_num].resp) 			||\
					(oper_type 	!= 	g_CTRY_Resp[ctry_num].act_resp[act_num].oper_lim_type) 	||\
					(max_curr 	!= 	g_CTRY_Resp[ctry_num].act_resp[act_num].max_current) 	||\
					(min_curr 	!=  g_CTRY_Resp[ctry_num].act_resp[act_num].min_current) 	||\
					(max_vib 	!=  g_CTRY_Resp[ctry_num].act_resp[act_num].max_vibration)	||\
					(min_vib 	!=  g_CTRY_Resp[ctry_num].act_resp[act_num].min_vibration))
				{
					/* Limits not match */
					hal_status |= Set_CTRY_Status(ctry_num, E_CTRY_FL); 		/* Set the CTRY's status to 'FL' */
					hal_status |= Set_CTRY_All_Actuators_Status_FL(ctry_num);	/* Set all the equipped actuators to 'FL' */

					/* Get actuaor's ID and write an event to the logger */
					hal_status |= Get_Actuator_Id(ctry_num,act_num,&flt_act_id);
					flt_act_id &= 0xFF;
					logger_status |= Write_Event_To_Memory(E_FAULT,E_FC_WRONG_OPERATION_LIMITS,(uint16_t)oper_type,E_UNIT_TYPE_ACTUATOR,((uint8_t) flt_act_id),logger_spare_bytes);

					/* Update all ctrys statuses */
					all_ctry_statuses |= (uint32_t)(0x01U << ctry_num);
				}

			}
		}
	}

	/* Transmit all CTRYs statuses */
	data_buff[0U] = (uint8_t)((all_ctry_statuses >> 16U) & 0xFFU);
	data_buff[1U] = (uint8_t)((all_ctry_statuses >> 8U) & 0xFFU);
	data_buff[2U] = (uint8_t)(all_ctry_statuses & 0xFFU);
	data_buff[3U] = 0U;
	data_buff[4U] = 0U;
	CAN_Message_Transmit(ID_CTRYS_STATUS_UPDATE_REQ, CAN_BROADCAST_ADDR, data_buff);
	HAL_Delay(CAN_TX_DELAY);

	/* SW Error found */
	g_SysStatus.SW_Erros |= hal_status;

	/* Logger Error found */
	g_SysStatus.Logger_FL |= logger_status;
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
