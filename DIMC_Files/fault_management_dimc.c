/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : fault_management_dimc.c
** Author    : Leonid Savchenko
** Revision  : 2.7
** Updated   : 25-10-2023
**
** Description:	This file contains procedures that define possible faults of the IPS system
** 		    	and perform fault management on fault occurrence.
** Notes:
**
**
**
**
*/


/* Revision Log:
**
** Rev 1.0  : 07-09-2020 -- Original version created.
**
** Rev 1.1	: 01-11-2020 -- Each function is returning hal_status, and check the hal status of each function it uses.
** 						  Added Added support to CAN error fault
**
** Rev 1.2	: 24-11-2020 -- removed unused functions and variables.
**
** Rev 1.3	: 03-12-2020 -- update ctry's actuators status monitoring
**
** Rev 1.4  : 03-01-2021 -- Updated due to verification remarks.
**
** Rev 1.5  : 02-02-2021 -- Addad actuator de-activation verification function.
**
** Rev 1.6	: 25-04-2021 -- Update few fault handling and update the file description.
** 						  Added support for handling CTRY over temperature fault.
** 						  Updates due to verification remarks.
** 						  Updated due to LDRA remarks.
**
** Rev 1.7  : 07-07-2021 -- Enter safe mode upon voltage fault detected.
** 						  Remove the activation of channel B when handling 'DIMC CAN BUS wrong CTRY id Error detected' fault. (20.05.21).
** 						  Update the handling of invalid ctry id (HLR 260).
** 						  Disables CAN RX interrupt when 5-volt failure detected.
** 						  Handling duplicate CTRY_ID PBIT response. (HLR 270).
** 						  Update the handling of channel A faults. (HLR 690).
** 						  Monitor CTRYs over current report.
** 						  Enter SAFE mode when all CTRYs reported 'FL'.
** 						  Monitor discrete sens channels.
** 						  Enter SAFE modde upon internal SW error.
** 						  Removed the function and global variable that were responsible for counting SW erros.
** Rev 1.8	: 25-07-2021 -- Update the call to function "Set_CTRY_All_Actuators_Status_FL".
** Rev 1.9	: 26-09-2021 -- Integration updates.
** Rev 2.0	: 15-11-2021 -- Integration updates.
** Rev 2.1	: 30-01-2022 -- Added a flag (variable) to prevent repetitve logging of abnormal actuator activation flt - "Abnormal_Act_Activation_FLT_Detected".
** 							Update the implementation of the function "volt_mes_to_fault_value" and clearify the function's desctiption.
** 							In the function 'ctry_fault_mang', update the code section 'CTRY detect over current consumption'.
** Rev 2.2	: 13-03-2022 -- Update the initial value of the variables "Prev_Sys_Status.ChannelA_FaultsStatus" and "Prev_Sys_Status.ChannelC_FaultsStatus".
** 							Monitoring channel A sense.
** Rev 2.3	: 17-07-2022 -- Update the implementation of channel A sense monitoring.
** Rev 2.4	: 07-05-2023 -- Remove the actuators performance failure handling code:
** 								The DIMC will not monitor the performance of tha actuatos.
** 							Updating the CTRYs over current report.
** 							Update the handling of actuator failure report received from the CTRYs.
** Rev 2.5	: 28-05-2023 -- Update varaible name from 'act_de_activation_fl' to 'com_lost_during_act_activation':
** 								- Update fault code related to this failure.
** Rev 2.6	: 17-08-2023 -- Log the activation of discrete fault channels by the DIMC.
** Rev 2.7	: 25-10-2023 -- Fix static analysis errors.
**************************************************************************************
*/

/* -----Includes ---------------------------------------------------------------------*/
#include "fault_management_dimc.h"
#include "gpio_control_dimc.h"
#include "system_params_dimc.h"
#include "logger_dimc.h"
#include "voltage_meas_dimc.h"
#include "irs_protocol_dimc.h"
#include "diac_mngmnt.h"
#include "module_init_dimc.h"
#include "can_protocol_dimc.h"
#include "periodic_dimc.h"
#include "gipsy_hal_rcc.h"
#include "gipsy_hal_can.h"

/* -----Definitions ----------------------------------------------------------------*/

/* DIACs ID for logging failures */
#define DIAC1_ID	0x01U
#define DIAC2_ID	0x02U
#define DIAC3_ID	0x03U
#define DIAC4_ID	0x04U

/* When all CTRYS are reported as 'FL' */
#define ALL_CTRYS_FL	0xFFFFFU

#define WAIT_SEN_MILLIS		5U

#define DISC_FLT_SENSE_A 0x08U
#define DISC_FLT_SENSE_C 0x20U

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/
uint32_t Monitor_CTRY_Time = 0U;
uint8_t  Number_Abnormal_Act_report = 0U;		/* Used to monitor the number of CTRYs that reported abnormal actuator operation sequence */

/* Event variables for data logger records */
uint16_t Fault_Value = 0x00U;
uint8_t Spare[3U] = {0U};

/* FLT channels activation states */
uint8_t FLT_CH_Turn_On[E_FLT_CH_ENUM_NUM] = {0U};
uint8_t Flt_Ch_Activate = FALSE;

uint32_t Current_FL_Ctrays = 0x00U;		/* Holds the fault CTRYs */
uint32_t Prev_FL_Ctrays = 0x00U; 		/* used to identify only new 'FL' CTRYs */


/* Previous state of system-statuses - used to identify and handle only new faults */

uint8_t Abnormal_Act_Activation_FLT_Detected = FALSE; /* Detection flag to prevent repetitive logging of the failure */
uint8_t Prev_Sens_A_FLT = FALSE;
uint8_t Prev_Sens_C_FLT = FALSE;

st_System_Operational_Status Prev_Sys_Status =
{
		.Sys_Mode = E_INIT_MODE,
		.DiscretesStatus = 0U,
		.Can_Bus_Error = 0U,
		.Can_Bus_Tr_error = 0U,
		.Can_Bus_Msg_Type_Error = 0U,
		.Can_bus_Address_Error = 0U,
		.Can_Bus_Ctry_Id_Error = 0U,
		.ChannelA_FaultsStatus = 0xFFU,
		.ChannelC_FaultsStatus = 0xFFU,
		.DIMC_CBIT_Status = 0U,
		.DIMC_PBIT_Status = 0U,
		.DIMC_CBIT_Failure_Desc = 0U,
		.DIMC_PBIT_Failure_Desc = 0U,
		.CTRYs_Status = 0U,
		.All_CTRYs_PBIT_Status = 0U,
		.CTRY_PBIT_Status = {{0U}},
		.CTRY_Status = {{0U}},
		.Logger_FL = 0U,
		.SW_Erros = 0U,



};

st_System_Tests_Status Prev_Test_Status =
{
		.DIMC_Volt_Test_Status = TEST_OK,
		.DIMC_Can_Bus_Init_Test_Status = TEST_OK,
		.DIMC_Ch_A_Test_Status = TEST_OK,
		.DIMC_Ch_B_Test_Status = TEST_OK,
		.DIMC_Ch_C_Test_Status = TEST_OK,
		.DIMC_Logger_Test_Status = TEST_OK,
		.DIMC_Monitor_Status_Table_Test_Status = TEST_OK,
		.DIMC_Oper_Inputs_Test_Status = TEST_OK,
		.CTRYs_SW_Ver_Mismatch_Test_Status = TEST_OK
};




/* -----External variables ---------------------------------------------------------*/
extern uint32_t g_Sys_OPER_Time_Sec;
extern uint32_t g_Sys_Acc_OPER_Time_Sec;

/* -----Static Function prototypes -------------------------------------------------*/
static void ctry_fault_mang(uint8_t ctry_id);
static void enter_safe_mode(void);
static void volt_mes_to_fault_value(uint16_t *fault_value);




/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** Fault_Management - Check for new faults in IPS.
** 						Hnadle new faults according to the HLR specification.
** Params:  None.
** Returns: None.
*************************************************************************************/
void Fault_Management(void)
{
	HAL_StatusTypeDef logger_errors = HAL_OK;
	uint8_t data_buf[6U] = {0U};
	uint8_t sens_state = 0U;

	/* SW version test failed */
	if ((TEST_FL == g_SysTestsStatus.CTRYs_SW_Ver_Mismatch_Test_Status) && (TEST_OK == Prev_Test_Status.CTRYs_SW_Ver_Mismatch_Test_Status))
	{
		/* Update Prev_System_test_status */
		Prev_Test_Status.CTRYs_SW_Ver_Mismatch_Test_Status = TEST_FL;

		/* Write fault event to memory */
		/* Event will be written in PBIT */

		/* Enter safe mode */
		enter_safe_mode();
	}


	/* IPS channel A ON */
	if ((0x0FU != g_SysStatus.ChannelA_FaultsStatus) && (g_SysStatus.ChannelA_FaultsStatus != Prev_Sys_Status.ChannelA_FaultsStatus))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.ChannelA_FaultsStatus = g_SysStatus.ChannelA_FaultsStatus;

		/* Set channel A to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_A_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_A_OUT] = TRUE;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* DIAC shut-off */
		for (uint8_t diac_num = 0x00U; diac_num < (uint8_t)E_DIAC_PS_ALL; diac_num++)
		{
			if(0U == ((g_SysStatus.ChannelA_FaultsStatus >> diac_num) & 0x01U))
			{
				Diac_Control(diac_num, E_DIAC_OFF);
			}
		}

		/* Enter safe mode */
		enter_safe_mode();

		/* Write fault event to memory */
		Fault_Value = g_SysStatus.ChannelA_FaultsStatus;
		if (0x00U == (g_SysStatus.ChannelA_FaultsStatus & 0x01U))
		{
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_POWER_SUP_1_CHANNEL_A_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIAC,DIAC1_ID,Spare);
		}

		if (0x00U == (g_SysStatus.ChannelA_FaultsStatus & 0x02U))
		{
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_POWER_SUP_2_CHANNEL_A_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIAC,DIAC2_ID,Spare);
		}

		if (0x00U == (g_SysStatus.ChannelA_FaultsStatus & 0x04U))
		{
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_POWER_SUP_3_CHANNEL_A_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIAC,DIAC3_ID,Spare);
		}

		if (0x00U == (g_SysStatus.ChannelA_FaultsStatus & 0x08U))
		{
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_POWER_SUP_4_CHANNEL_A_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIAC,DIAC4_ID,Spare);
		}

		/* Reset fault value */
		Fault_Value = 0x00U;
	}


	/* IPS channel C ON */
	if ((0x3FU != g_SysStatus.ChannelC_FaultsStatus) && (g_SysStatus.ChannelC_FaultsStatus != Prev_Sys_Status.ChannelC_FaultsStatus))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.ChannelC_FaultsStatus = g_SysStatus.ChannelC_FaultsStatus;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set channel C to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_C_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_C_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write fault event to memory */
		Fault_Value = g_SysStatus.ChannelC_FaultsStatus;
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CHANNEL_C_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_CTRY,IPS_ID,Spare);
		Fault_Value = 0x00U;
	}


	/* DIMC Channel A test fail */
	if ((TEST_FL == g_SysTestsStatus.DIMC_Ch_A_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Ch_A_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Ch_A_Test_Status = TEST_FL;

		/* Set channel A to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_A_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_A_OUT] = TRUE;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CHANNEL_A_TEST_FLT,Fault_Value,E_UNIT_TYPE_IPS,IPS_ID,Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC Channel B test fail */
	if ((TEST_FL == g_SysTestsStatus.DIMC_Ch_B_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Ch_B_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Ch_B_Test_Status = TEST_FL;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set channel C to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_C_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_C_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CHANNEL_B_TEST_FLT, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC Channel C test fail */
	if ((TEST_FL == g_SysTestsStatus.DIMC_Ch_C_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Ch_C_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Ch_C_Test_Status = TEST_FL;

		/* Channels 'B' & 'C' are set to ON by the testing procedure */
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;
		FLT_CH_Turn_On[E_FLT_CH_C_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CHANNEL_C_TEST_FLT, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC 5volt fault */
	if((TEST_FL == g_SysTestsStatus.DIMC_Volt_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Volt_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Volt_Test_Status = TEST_FL;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Resets CAN handle status and disables CAN RX interrupt */
		HAL_CAN_Stop();

		/* Write fault event to memory */
		volt_mes_to_fault_value(&Fault_Value);
		logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_DIMC_POWER_MONITOR_5VDC_FAILED, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);
		Fault_Value = 0x00U;

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC logger fault */
	if ((TEST_FL == g_SysTestsStatus.DIMC_Logger_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Logger_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Logger_Test_Status = TEST_FL;
	}

	/* DIMC Can_Bus init fault */
	if ((TEST_FL == g_SysTestsStatus.DIMC_Can_Bus_Init_Test_Status) && (TEST_FL != Prev_Test_Status.DIMC_Can_Bus_Init_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Can_Bus_Init_Test_Status = TEST_FL;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Resets CAN handle status and disables CAN RX interrupt */
		HAL_CAN_Stop();

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CAN_BUS_INIT_FAILED, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC CAN BUS Error detected */
	if (TRUE == g_SysStatus.Can_Bus_Error)
	{
		/* Reset the fault indicator */
		g_SysStatus.Can_Bus_Error = FALSE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CAN_BUS_ERROR_DETECTED,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);
	}

	/* DIMC CAN BUS Transmit error detected */
	if (TRUE == g_SysStatus.Can_Bus_Tr_error)
	{
		/* Update Prev_System_status */
		g_SysStatus.Can_Bus_Tr_error = FALSE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CAN_BUS_TRANSMIT_ERROR,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);
	}

	/* DIMC CAN BUS Msg type error */
	if (0x00U != g_SysStatus.Can_Bus_Msg_Type_Error)
	{
		/* Write fault event to memory */
		Fault_Value = (uint8_t)g_SysStatus.Can_Bus_Msg_Type_Error - 1U;
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CAN_BUS_ERROR_DETECTED,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);

		/* Update System_status */
		g_SysStatus.Can_Bus_Msg_Type_Error = 0x00U;

	}

	/* DIMC CAN BUS wrong address Error detected */
	if ((0x00U != g_SysStatus.Can_bus_Address_Error) && (g_SysStatus.Can_bus_Address_Error != Prev_Sys_Status.Can_bus_Address_Error))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.Can_bus_Address_Error = g_SysStatus.Can_bus_Address_Error;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_INVALID_CAN_ADDRESS,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);
	}

	/* DIMC CAN BUS wrong CTRY id Error detected */
	if ((0x00U != g_SysStatus.Can_Bus_Ctry_Id_Error) && (g_SysStatus.Can_Bus_Ctry_Id_Error != Prev_Sys_Status.Can_Bus_Ctry_Id_Error))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.Can_Bus_Ctry_Id_Error = g_SysStatus.Can_Bus_Ctry_Id_Error;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_INVALID_CAN_ID,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);

		if(E_INIT_MODE == g_SysStatus.Sys_Mode)
		{
			/* Set channel B (1&2) to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

			/* Set flt activation */
			Flt_Ch_Activate = TRUE;

			/* Enter safe mode */
			enter_safe_mode();
		}
	}

	/* DIMC OPER channels hardware fault */
	if ((TRUE == g_SysTestsStatus.DIMC_Oper_Inputs_Test_Status) && (TRUE != Prev_Test_Status.DIMC_Oper_Inputs_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Oper_Inputs_Test_Status = TRUE;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write fault event to memory */
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_OPER_SIGNALS_INTEGRITY_TEST_FAILED,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID,Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* DIMC Monitor CTRYs Status Table */
	if ((0x00U != g_SysTestsStatus.DIMC_Monitor_Status_Table_Test_Status) && (0x00U == Prev_Test_Status.DIMC_Monitor_Status_Table_Test_Status))
	{
		/* Update Prev_System_status */
		Prev_Test_Status.DIMC_Monitor_Status_Table_Test_Status = g_SysTestsStatus.DIMC_Monitor_Status_Table_Test_Status;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;
	}

	/* DIMC PBIT fail */
	if ((0x00U != g_SysStatus.DIMC_PBIT_Failure_Desc)  && (g_SysStatus.DIMC_PBIT_Failure_Desc != Prev_Sys_Status.DIMC_PBIT_Failure_Desc))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.DIMC_PBIT_Failure_Desc = g_SysStatus.DIMC_PBIT_Failure_Desc;

		/* Write PBIT fail event to memory */
		Fault_Value = 0xFFU & g_SysStatus.DIMC_PBIT_Failure_Desc;
		logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_DIMC_PBIT_FAILED, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID,Spare);
		Fault_Value = 0x00U;

	}

	/* DIMC CBIT fail */
	if ((0x00U != g_SysStatus.DIMC_CBIT_Failure_Desc)  && (g_SysStatus.DIMC_CBIT_Failure_Desc != Prev_Sys_Status.DIMC_CBIT_Failure_Desc))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.DIMC_CBIT_Failure_Desc = g_SysStatus.DIMC_CBIT_Failure_Desc;

		/* Write CBIT fail event to memory */
		Fault_Value = 0xFFU & g_SysStatus.DIMC_CBIT_Failure_Desc;
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_CBIT_FAILED, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID,Spare);
		Fault_Value = 0x00U;

		/* Set CBIT fail */
		g_SysStatus.DIMC_CBIT_Status = TEST_FL;
	}

	/* DIMC SW errors */
	if ((TRUE == g_SysStatus.SW_Erros) && (FALSE == Prev_Sys_Status.SW_Erros))
	{
		/* Update Prev_System_status */
		Prev_Sys_Status.SW_Erros = TRUE;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write to logger */
		Fault_Value = 0x00U;
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_SW_ERROR, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID,Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* Handle CTRYs faults */
	for (uint8_t ctry_id = 0U; ctry_id < CTRY_MAX_NUM ; ctry_id++)
	{
		/* Monitor CTRYs report of abnormal actuator activation */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].Abnormal_Actuator_Report) && (FALSE == Prev_Sys_Status.CTRY_Status[ctry_id].Abnormal_Actuator_Report))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].Abnormal_Actuator_Report = TRUE;

			/* Increment the counter of CTRYs that report abnormal actuator activation */
			Number_Abnormal_Act_report++;

			/* Write Event to the Memory */
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_SANITY_CHECK_FAILURE, 0x00U, E_UNIT_TYPE_CTRY, ctry_id,Spare);
		}

		/* Handle CTRY internal faults */
		ctry_fault_mang(ctry_id);
	}

	/* Handle Abnormal Actuator Activation Fault */
	if ((2U <= Number_Abnormal_Act_report) && (FALSE == Abnormal_Act_Activation_FLT_Detected))
	{
		/* Update detection state */
		Abnormal_Act_Activation_FLT_Detected = TRUE;

		/* Set channel B (1&2) to '1' */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

		/* Set flt activation */
		Flt_Ch_Activate = TRUE;

		/* Write Abnormal Actuator Activation Sequence event to memory */
		Fault_Value = 0x00U;
		logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_SANITY_CHECK_FAILURE, Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID,Spare);

		/* Enter safe mode */
		enter_safe_mode();
	}

	/* Send the updated CTRYs status */
	if ((0x00U != Current_FL_Ctrays) && (Current_FL_Ctrays != Prev_FL_Ctrays))
	{
		Prev_FL_Ctrays = Current_FL_Ctrays;
		data_buf[0U] = (uint8_t)((Current_FL_Ctrays >> 16U) & 0xFFU);
		data_buf[1U] = (uint8_t)((Current_FL_Ctrays >> 8U) & 0xFFU);
		data_buf[2U] = (uint8_t)(Current_FL_Ctrays & 0xFFU);

		CAN_Message_Transmit(ID_CTRYS_STATUS_UPDATE_REQ, CAN_BROADCAST_ADDR, data_buf);

		/* Enter SAFE mode if all CTRY are reported as 'FL' */
		if(ALL_CTRYS_FL == Current_FL_Ctrays)
		{
			enter_safe_mode();
		}
	}

	/* Verify FLT channels activation by monitor the sens channels */
	if (TRUE == Flt_Ch_Activate)
	{
		/* Reset FLT_Ch_Activate flag */
		Flt_Ch_Activate = FALSE;

		/* Wait for the signal to stabilized */
		HAL_Delay(WAIT_SEN_MILLIS);

		/* Read sens channels */
		sens_state = Read_FLT_Sense_Inputs();

		/* Reset logger fault value */
		Fault_Value = 0U;


		/* Channel A */
		if(TRUE == FLT_CH_Turn_On[E_FLT_CH_A_OUT])
		{
			/* Log channel activation */
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_ACTIVATE_FLT_CH_A,Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

			/* Verify Sense state */
			if(0U != (sens_state & 0x01U))
			{
				/* Write failure to logger */
				logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CHANNEL_A_SENS_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID, Spare);

				/* Enter safe mode */
				enter_safe_mode();
			}

			/* Reset channel A activation's flag */
			FLT_CH_Turn_On[E_FLT_CH_A_OUT] = FALSE;
		}

		/* Channel B */
		if(TRUE == FLT_CH_Turn_On[E_FLT_CH_B_OUT])
		{
			/* Log channel activation */
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_ACTIVATE_FLT_CH_B,Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

			/* Verify Sense state */
			if(0U != ((sens_state >> 1U) & 0x01U))
			{
				/* Write failure to logger */
				logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CHANNEL_B_SENS_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID, Spare);

				/* Enter safe mode */
				enter_safe_mode();
			}

			/* Reset channel B activation's flag */
			FLT_CH_Turn_On[E_FLT_CH_B_OUT] = FALSE;
		}

		/* Channel C */
		if(TRUE == FLT_CH_Turn_On[E_FLT_CH_C_OUT])
		{
			/* Log channel activation */
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_DIMC_ACTIVATE_FLT_CH_C,Fault_Value, E_UNIT_TYPE_DIMC, DIMC_ID, Spare);

			/* Verify Sense state */
			if(0U != ((sens_state >> 2U) & 0x01U))
			{
				/* Write failure to logger */
				logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CHANNEL_C_SENS_FAULT_REPORT,Fault_Value,E_UNIT_TYPE_DIMC,DIMC_ID, Spare);

				/* Enter safe mode */
				enter_safe_mode();
			}

			/* Reset channel C activation's flag */
			FLT_CH_Turn_On[E_FLT_CH_C_OUT] = FALSE;
		}
	}

	g_SysStatus.Logger_FL |= logger_errors;
}



/*************************************************************************************
** ctry_fault_mang - 	Check for new faults in CTRY.
** 						If a new fault was found, then the fault will be processed.
**
** Params:  uint8_t ctry_id	-	The ctry id.
** Returns: None.
** Notes:
*************************************************************************************/
static void ctry_fault_mang(uint8_t ctry_id)
{
	/* Local variables */
	HAL_StatusTypeDef logger_errors = HAL_OK;
	HAL_StatusTypeDef sw_errors = HAL_OK;
	uint8_t update_ctry_st = FALSE;
	uint8_t fl_actuator_id = 0x00U;
	uint16_t act_id = 0x00U;
	uint8_t all_act_St = 0x00U;
	uint8_t equipped_actuators = 0x00U;
	uint8_t current_act_st = 0x00U;
	uint8_t fl_act = 0x00U;
	e_DIAC_Num diac_ch = 0x00U;

	if (ctry_id < CTRY_MAX_NUM)
	{
		/* CTRY COM lost during actuator activation */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].com_lost_during_act_activation) && (TRUE != Prev_Sys_Status.CTRY_Status[ctry_id].com_lost_during_act_activation))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].com_lost_during_act_activation = TRUE;

			/* Set channel B (1&2) to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

			/* Set flt activation */
			Flt_Ch_Activate = TRUE;

			/* Set the DIAC control signal responsible for the failed CTRY to '0' */
			sw_errors |= Get_CTRY_DIAC_Ch(ctry_id,&diac_ch);
			Diac_Control(diac_ch, E_DIAC_OFF);

			/* Enter safe mode */
			enter_safe_mode();

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;

			/* Write Event to the Memory */
			logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CTRY_COM_LOST_DURING_ACT_ACTIVATION, Fault_Value, E_UNIT_TYPE_CTRY,ctry_id,Spare);
		}

		/* CTRY failed to respond to can bus message request */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].failed_to_resp) && (TRUE != Prev_Sys_Status.CTRY_Status[ctry_id].failed_to_resp))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].failed_to_resp = TRUE;

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;

			/* Write Event to the Memory */
			logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CTRY_NOT_RESP, 0x00U , E_UNIT_TYPE_CTRY, ctry_id, Spare);
		}

		/* CTRY duplicated PBIT response */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].dup_resp_fl) && (FALSE == Prev_Sys_Status.CTRY_Status[ctry_id].dup_resp_fl))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].dup_resp_fl = TRUE;

			/* Set channel B (1&2) to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

			/* Set flt activation */
			Flt_Ch_Activate = TRUE;

			/* Write Event to the Memory */
			logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CTRYS_PBIT_DUPLICATE_ST_RESP, 0x00U , E_UNIT_TYPE_CTRY, ctry_id, Spare);

			/* Enter safe mode */
			enter_safe_mode();
		}

		/* CTRY identification test failed */
		if ((TEST_FL == g_SysStatus.CTRY_Status[ctry_id].Ctry_ID_Test_Status) && (TEST_FL != Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_ID_Test_Status))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_ID_Test_Status = TEST_FL;

			/* Write Event to the Memory */
			Fault_Value = 0x00U;
			logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CTRY_CAN_BUS_COMM_FAIL, 0x00U , E_UNIT_TYPE_CTRY, ctry_id, Spare);

			/* Enter safe mode */
			enter_safe_mode();
		}

		/* CTRY MOTOR OPER test failed */
		if ((TEST_FL == g_SysStatus.CTRY_Status[ctry_id].Ctry_Mot_Oper_In_Test_Status) && (TEST_OK == Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_Mot_Oper_In_Test_Status))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_Mot_Oper_In_Test_Status = TEST_FL;

			/* Write Event to the Memory */
			Fault_Value = 0x00U;
			logger_errors |= Write_Event_To_Memory(E_FAULT, E_FC_CTRY_MOT_OPER_IN, 0X00u, E_UNIT_TYPE_CTRY, ctry_id, Spare);

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;
		}

		/* CTRY over temperature handling */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].over_temp) && (FALSE == Prev_Sys_Status.CTRY_Status[ctry_id].over_temp))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].over_temp = TRUE;

			/* Set the DIAC control signal responsible for the failed CTRY to '0' */
			sw_errors |= Get_CTRY_DIAC_Ch(ctry_id, &diac_ch);
			Diac_Control(diac_ch, E_DIAC_OFF);

			/* Write Event to the Memory */
			Fault_Value = 0x00U;
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_OVER_TEMP, Fault_Value ,E_UNIT_TYPE_CTRY,ctry_id,Spare);

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;

			/* Set channel A to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_A_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_A_OUT] = TRUE;

			/* Set channel C to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_C_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_C_OUT] = TRUE;

			/* Set flt activation */
			Flt_Ch_Activate = TRUE;

			/* Enter safe mode */
			enter_safe_mode();
		}

		/* CTRY PBIT fault */
		if ((0x01U == g_SysStatus.CTRY_PBIT_Status[ctry_id].PBIT_Status) && (0x01U != Prev_Sys_Status.CTRY_PBIT_Status[ctry_id].PBIT_Status))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_PBIT_Status[ctry_id].PBIT_Status = 0x01U;

			/* Write Event to the Memory */
			Fault_Value = g_SysStatus.CTRY_PBIT_Status[ctry_id].PBIT_Failure_Desc;
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_PBIT_FAILED,Fault_Value ,E_UNIT_TYPE_CTRY,ctry_id,Spare);

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;
		}

		/* CTRY report over current consumption */
		if ((TRUE == g_SysStatus.CTRY_Status[ctry_id].over_current_rep) && (FALSE == Prev_Sys_Status.CTRY_Status[ctry_id].over_current_rep))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].over_current_rep = TRUE;

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;

			/* Set channel B (1&2) to '1' */
			Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
			FLT_CH_Turn_On[E_FLT_CH_B_OUT] = TRUE;

			/* Set flt activation */
			Flt_Ch_Activate = TRUE;

			/* Set the DIAC control signal responsible for the failed CTRY to '0' */
			sw_errors |= Get_CTRY_DIAC_Ch(ctry_id,&diac_ch);
			Diac_Control(diac_ch, E_DIAC_OFF);

			/* Write Event to the Memory */
			Fault_Value = g_SysStatus.CTRY_Status[ctry_id].CurrentValue;
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_OVER_CURRENT_DETECTED,Fault_Value ,E_UNIT_TYPE_CTRY,ctry_id,Spare);
		}

		/* CTRY CBIT fail */
		if ((0x00U != g_SysStatus.CTRY_Status[ctry_id].Ctry_Status) && (g_SysStatus.CTRY_Status[ctry_id].Ctry_Status != Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_Status))
		{
			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].Ctry_Status = g_SysStatus.CTRY_Status[ctry_id].Ctry_Status;

			/* Write fault event to memory */
			Fault_Value = g_SysStatus.CTRY_Status[ctry_id].Ctry_Failure_Desc;
			logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_CTRY_CBIT_FAILED, Fault_Value, E_UNIT_TYPE_CTRY, ctry_id, Spare);

			/* Update CTRY's status to 'FL' */
			update_ctry_st = TRUE;

			/* Update IBIT results */
			if(E_IBIT_MODE == g_SysStatus.Sys_Mode)
			{
				g_IBIT_Result.CTRYs_IBIT_Results[ctry_id] |= 0x01U;
				g_IBIT_Result.CTRYs_IBIT_Results[ctry_id] |= ((g_SysStatus.CTRY_Status[ctry_id].Power_Switch << 2U) & 0x04U);
			}

		}

		/* CTRY's actuator fail report */
		if ((0x00U != g_SysStatus.CTRY_Status[ctry_id].ActuatorsStatus) && (g_SysStatus.CTRY_Status[ctry_id].ActuatorsStatus != Prev_Sys_Status.CTRY_Status[ctry_id].ActuatorsStatus))
		{
			/* Get the new 'FL' actuators */
			fl_act = (g_SysStatus.CTRY_Status[ctry_id].ActuatorsStatus - Prev_Sys_Status.CTRY_Status[ctry_id].ActuatorsStatus);

			for (uint8_t act_number = 0U; act_number < (uint8_t)E_ACT_MAX_NUM; act_number++)
			{
				current_act_st = (fl_act >> act_number) & 0x01U;
				if (0x01U == current_act_st)
				{
					/* Set actuator as 'FL' */
					sw_errors |= Set_Actuator_Status(ctry_id,act_number,E_ACT_FL);

					/* Get the actuator's ID */
					sw_errors |= Get_Actuator_Id(ctry_id, act_number, &act_id);
					fl_actuator_id = (uint8_t)(act_id - 0x200U);

					/* Log measured values */
					Fault_Value = ((uint16_t)g_SysStatus.CTRY_Status[ctry_id].VibrationValue);
					Fault_Value <<= 8U;
					Fault_Value |= (0xFFU & g_SysStatus.CTRY_Status[ctry_id].CurrentValue);

					/* Write fault event to memory */
					logger_errors |= Write_Event_To_Memory(E_FAULT,E_FC_ACTUATOR_DISABLED_BY_THE_CTRY,Fault_Value,E_UNIT_TYPE_ACTUATOR,fl_actuator_id,Spare);
				}
			}

			/* Update IBIT Results */
			if(E_IBIT_MODE == g_SysStatus.Sys_Mode)
			{
				g_IBIT_Result.CTRYs_IBIT_Results[ctry_id] |= 0x08U;		/* Actuator test */
				g_IBIT_Result.CTRYs_IBIT_Results[ctry_id] |= 0x02U;		/* vibration monitor result*/
				g_IBIT_Result.CTRYs_IBIT_Results[ctry_id] |= 0x10U;		/* current  monitor result*/
			}

			/* Update Prev_System_status */
			Prev_Sys_Status.CTRY_Status[ctry_id].ActuatorsStatus = g_SysStatus.CTRY_Status[ctry_id].ActuatorsStatus;
		}

		/* Get the equipped actuators */
		sw_errors |= Get_Equipped_Actuators(ctry_id, &equipped_actuators);
		equipped_actuators = (uint8_t)(~equipped_actuators) & 0x3FU;

		/* Get all actuators statuses from sys_param */
		sw_errors |= Get_CTRY_All_Actuators_Status(ctry_id, &all_act_St);

		/* Check if all the equipped actuators are 'FL' */
		if ((0U == sw_errors) && (equipped_actuators == all_act_St))
		{
			/* Set CTRY's status to 'FL' */
			update_ctry_st = TRUE;
		}

		/* Set Ctray's status to 'FL' */
		if (TRUE == update_ctry_st)
		{
			Current_FL_Ctrays |= (uint32_t)(0x01U << ctry_id);
			sw_errors |= Set_CTRY_Status(ctry_id, E_CTRY_FL); 	/* Set the CTRY's status to 'FL' */
			sw_errors |= Set_CTRY_All_Actuators_Status_FL(ctry_id);	/* Set all the equipped actuators to 'FL' */
		}

		/* Update SW errors and logger status */
		g_SysStatus.Logger_FL |= logger_errors;

		if (HAL_OK != sw_errors)
		{
			g_SysStatus.SW_Erros = TRUE;
		}
	}

	/* ctry_id is out of range */
	else
	{
		g_SysStatus.SW_Erros = TRUE;
	}
}







/*************************************************************************************
** enter_safe_mode - Enter safe mode.
** 						Abort current system mode.
** 						Turn off running actuators.
** 						Log safe mode entry event.
** Params:  None.
** Returns: None.
** Notes:
*************************************************************************************/
static void enter_safe_mode(void)
{
	if (E_SAFE_MODE != g_SysStatus.Sys_Mode)
	{
		HAL_StatusTypeDef logger_errors = 0x00U;
		uint8_t data_buf[6U] = {0U};

		/* Abort operation mode */
		if(E_OPER_MODE == g_SysStatus.Sys_Mode)
		{
			/* Turn off all actuators */
			CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, data_buf);

			/* Stop operation time counter */
			g_Sys_OPER_Time_Sec = ((HAL_GetTick() - g_Sys_OPER_Time_Sec) / 1000U);

			/* Add to the accumulated operation time */
			g_Sys_Acc_OPER_Time_Sec += g_Sys_OPER_Time_Sec;

			/* Write event */
			logger_errors |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_OPERATION_MODE_TERMINATED, 0X00U, E_UNIT_TYPE_IPS, IPS_ID, Spare);
		}

		/* Abort Ibit mode */
		if(E_IBIT_MODE == g_SysStatus.Sys_Mode)
		{
			/* Turn off all actuators */
			CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, data_buf);

			/* Write event */
			logger_errors |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_IBIT_ABORTED, 0X00U, E_UNIT_TYPE_IPS, IPS_ID, Spare);
		}

		/* Abort manual actuator activation */
		if(E_ACTIVE_REQ == g_Actuator_Man_Req.request_status)
		{
			/* Turn off all actuators */
			CAN_Message_Transmit(ID_ALL_ACTUATORS_OFF_REQ, CAN_BROADCAST_ADDR, data_buf);
			g_Actuator_Man_Req.request_status = E_NO_REQ;
		}

		/* Set system operation mode to "SAFE MODE" */
		g_SysStatus.Sys_Mode = E_SAFE_MODE;

		/* Write "SAFE_MODE_ENTRY" event to memory */
		logger_errors |= Write_Event_To_Memory(E_SYSTEM_EVENT, E_EC_SAFE_MODE_ENTRY, Fault_Value, E_UNIT_TYPE_IPS, DIMC_ID, Spare);

		if (HAL_OK != logger_errors)
		{
			g_SysStatus.Logger_FL = TRUE;
		}
	}
}



/*************************************************************************************
** volt_mes_to_fault_value - Converts the voltage value to the required representation
** 							 according to the logger standarts (see note below).
**
** Params:  uint16_t *fault_value - Pointer to fault_value variable.
**
** Returns: None.
**
** Notes:
** 		According to "IPS-DIMC Faults Events Store Management" Document,
** 		the voltage value will be represented by 2 bytes (uin16_t). The first byte will hold the integer part
** 		and the second byte will hold the decimal part.
*************************************************************************************/
static void volt_mes_to_fault_value(uint16_t *fault_value)
{
    if (NULL != fault_value)
    {
        *fault_value = (g_VoltageMeasurement.VoltageValue / 10U);
        *fault_value <<= 8U;
        *fault_value |= (uint16_t)((g_VoltageMeasurement.VoltageValue % 10) & 0xff);
    }
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
