/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : irs_protocol_dimc.c
** Author    : Leonid Savchenko
** Revision  : 2.2
** Updated   : 24-10-2023
**
** Description: This file contains definition of RS-422 interface protocol used by
** 		        DIMC unit.
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 02-01-2020 -- Added Safe Mode Entry IRS command.
** Rev 1.2  : 22-02-2020 -- Updated IRS_Send_Msg_Operational_Status_Report()
** Rev 1.3  : 28-10-2020 -- Base line updates
** Rev 1.4  : 03-12-2020 -- Update Ibit result variable to struct 'g_IBIT_Result'.
** Rev 1.5  : 03-01-2021 -- Updated due to verification remarks.
** rev 1.6	: 24-02-2021 -- Set 'Enter_Safe_Mode_Req' to 'TRUE' when receiving msg "IRS_SAFE_MODE_REQ".
** Rev 1.7	: 25-04-2021 -- 1. Metohd of building msg arrays changed.
** 						  	2. Update the "IRS_Send_Msg_Operational_Status_Report" function (the discrete values part).
** 						  	3. delete unused global variavle "Maint_ActuatorData".
** 						  	4. remove the prefix "g_" for variables "ucCyclic_Msg_Counter" and "IRS_Errors_Counter"
** Rev 1.8	: 08-07-2021 -- Remove support for safe mode entry request.
**						  	Update due to integration remarks.
** Rev 1.9	: 30-01-2022 -- Removed the unused code block "#ifdef _IAI_INTEGRATION"
** 							Removed the following unused external variables:
** 								- `g_Perform_Logger_Erase`
**								- `g_stReadFromLogger`
**								- `g_IBIT_Command_from_IRS`
**								- 'g_Mission_OPER_Time_Sec'
**							Update code comments in function 'HAL_UART1_RxCpltCallback'.
** Rev 2.0	: 07-05-2023 -- IPS Reset command was added.
** Rev 2.1	: 11-09-2023 -- Log the reception of IRS commands.
** 							Log reception of IRS message with wrong CRC value.
** Rev 2.2	: 24-10-2023 -- Update the function 'IRS_Send_Msg_Operational_Status_Report'.
** 							Fix static analysis errors.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "mcu_config_dimc.h"
#include "main.h"
#include "irs_protocol_dimc.h"
#include "periodic_dimc.h"
#include "gipsy_hal_uart.h"
#include "gipsy_hal_rtc.h"
#include "crc16.h"
#include "version_dimc.h"
#include "module_init_dimc.h"
#include "logger_dimc.h"
#include "gipsy_hal_rcc.h"
#include "fault_management_dimc.h"

/* -----Definitions ----------------------------------------------------------------*/

#define RS422_BUF_SIZE  30U
#define TX_MSG_BUF_SIZE 120U
#define IRS_TX_MSG_TIMEOUT_MILIS 50U
#define MSG_COUNTER_MAX_VALUE   255U
#define MSG_COUNTER_MIN_VALUE   1U
#define WDOG_LOOP_MAX_STEPS 	6U
#define WDOG_LOOP_STEP_INTERVAL_MILIS 500U
/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

uint32_t IRS_Errors_Counter = 0U;
uint8_t Ser_Rx_Buffer[RS422_BUF_SIZE]= {0U};
uint8_t Received_Msg[RS422_BUF_SIZE] = {0U};
uint8_t bCmd_Ready_For_Parse = FALSE;
uint8_t RX_Byte = 0xFFU;
uint8_t Buff_index = 0U;
uint8_t ucCyclic_Msg_Counter = 1U; /*Cyclic message counter starts from 1*/
uint8_t ucLast_Message_Status = (uint8_t)E_LAST_MSG_OK;
uint8_t Cmd_Data_length_Bytes = 0U;
uint8_t Logger_Spare_Bytes[3U] = {0U};					/* Byte array that used for writing events to the data logger */

/* -----External variables ---------------------------------------------------------*/

extern uint32_t g_Sys_OPER_Time_Sec;
extern uint32_t g_Sys_Acc_OPER_Time_Sec;
extern st_IBIT_Res g_IBIT_Result;
extern uint8_t g_Logger_Status;

/* -----Static Function prototypes -------------------------------------------------*/

static void irs_execute_recieved_command(void);
static void irs_message_transmit(uint8_t *msg_data, uint8_t msg_length);
static void irs_send_msg_sw_version_report(void);
static void irs_send_msg_pbit_results_report(void);
static void irs_send_msg_ibit_results_report(void);
static void set_time_date_from_irs_msg(void);

/* -----Modules implementation -----------------------------------------------------*/




/*************************************************************************************
** HAL_UART1_RxCpltCallback - IRS UART RX Interrupt callback
** 							  Receives single Byte from IRS RS-422 serial port and stores it in the buffer
** 							  Checks if valid message fully received and raises global flag for message parsing process.
**
**
** Params : UART_HandleTypeDef *huart - IRS UART handle.
**
** Returns: None
** Notes:
*************************************************************************************/
void HAL_UART1_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t cmd_index = 0U;

	if(HAL_OK == HAL_UART_Receive_IT(huart,&RX_Byte,1U))
	{
		/*If input buffer is full -> Flush to avoid overflow*/
		if(Buff_index >= RS422_BUF_SIZE)
		{
			Buff_index = 0U; /*Reset counter*/

			/*Clear buffer*/
	        for (uint8_t index = 0U; index < RS422_BUF_SIZE ;index++)
	        {
	        	Ser_Rx_Buffer[index] = 0U;
	        }
		}
		/*Receive single Byte*/
		Ser_Rx_Buffer[Buff_index]= RX_Byte;
		Buff_index++;
		/*Check if valid Header Received and stored in 2 first Bytes
		 * For invalid Header -> Flush buffer and reset buff_index counter*/
		if((Buff_index - 1U) == MSG_HEADER_SOM_VAL_LOW_PTR)
		{
			if((Ser_Rx_Buffer[MSG_HEADER_SOM_VAL_HIGH_PTR] != SOM_VAL_HIGH) || (Ser_Rx_Buffer[MSG_HEADER_SOM_VAL_LOW_PTR] != SOM_VAL_LOW))
			{
				Buff_index = 0U;                                /*Reset counter*/

				/*Clear buffer*/
		        for (uint8_t index = 0U; index < RS422_BUF_SIZE ;index++)
		        {
		        	Ser_Rx_Buffer[index] = 0U;
		        }
			}
		}

		/*Get Message's data length
		 * Using data length, total message length will be calculated*/
		if((Buff_index - 1U)  == MSG_HEADER_MSG_LENGTH_PTR)
		{
			Cmd_Data_length_Bytes = Ser_Rx_Buffer[MSG_HEADER_MSG_LENGTH_PTR];
		}
		/*Check if all message bytes received*/
		if((Buff_index ) == (MSG_HEADER_LENGTH_BYTES + Cmd_Data_length_Bytes + MSG_CCSUM_LENGTH_BYTES))
		{
			/*Copy message bytes to Received_Msg array for later parsing*/
			for(cmd_index = 0U; cmd_index < (/*MSG_MINIMAL_LENGTH_BYTES*/ MSG_HEADER_LENGTH_BYTES + Cmd_Data_length_Bytes + MSG_CCSUM_LENGTH_BYTES); cmd_index++)
			{
					Received_Msg[cmd_index] = Ser_Rx_Buffer[cmd_index];
			}
			Cmd_Data_length_Bytes = 0U;
			Buff_index = 0U;                                /*Reset counter*/

			/*Clear buffer*/
	        for (uint8_t index = 0U; index < RS422_BUF_SIZE ;index++)
	        {
	        	Ser_Rx_Buffer[index] = 0U;
	        }

		    bCmd_Ready_For_Parse = TRUE;                    /*Raise a flag that full message is ready for parsing*/
		}
	}
}

/*************************************************************************************
** Parse_IRS_Command - Procedure for parsing received message from IRS UART port
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void Parse_IRS_Command(void)
{
	uint8_t cmd_data_length_bytes = 0U;
	if(TRUE == bCmd_Ready_For_Parse)
	{
		ucLast_Message_Status = (uint8_t)E_LAST_MSG_OK;
		cmd_data_length_bytes = Received_Msg[MSG_HEADER_MSG_LENGTH_PTR];
		if(0U == Calculate_CRC16(Received_Msg,MSG_MINIMAL_LENGTH_BYTES + cmd_data_length_bytes + MSG_CCSUM_LENGTH_BYTES))
		{
			ucCyclic_Msg_Counter++; /*Increment Msg counter  -> should be equal to received Message Counter value*/
			/*Check Cyclic Message Counter value*/
			if(ucCyclic_Msg_Counter != Received_Msg[MSG_HEADER_MSG_COUNTER_PTR])
			{
				ucLast_Message_Status |= (uint8_t)E_LAST_MSG_CNT_ERR;
			}

			irs_execute_recieved_command();
			ucLast_Message_Status = (uint8_t)E_LAST_MSG_OK; /*Reset Last Message Status after message transmission*/
			/*Update Message counter value(increment or reset to MSG_COUNTER_MIN_VALUE */
			if(MSG_COUNTER_MAX_VALUE == ucCyclic_Msg_Counter)
			{
				ucCyclic_Msg_Counter = MSG_COUNTER_MIN_VALUE;
			}
			else
			{
				ucCyclic_Msg_Counter++;
			}
		}
		else
		{
			ucLast_Message_Status |= (uint8_t)E_LAST_MSG_CRC_ERR;
			g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_MSG_RECEIVED_WITH_CRC_FLT,0x00,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
		}

		/*Clear buffer*/
        for (uint8_t index = 0U; index < RS422_BUF_SIZE ;index++)
        {
        	Received_Msg[index] = 0U;
        }
		Buff_index = 0U;                              /*Reset counter*/
		bCmd_Ready_For_Parse = FALSE; /*Clear flag*/
	}
}
/*************************************************************************************
** irs_execute_recieved_command - Procedure for execution of the received message
**                                from IRS UART port
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void irs_execute_recieved_command(void)
{
	uint16_t event_number = 0U;
	uint16_t act_id = ACTUATOR_ID_NONE;

	/*Check Message Type*/
	switch(Received_Msg[MSG_TYPE_PTR])
	{
		case IRS_DATA_REQUEST_REQ:
			switch(Received_Msg[MSG_DATA_BYTE_PTR])
			{
				case DATA_REQUEST_VER_REPORT_ID:
					irs_send_msg_sw_version_report();
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_SW_VER_REPORT_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
				case DATA_REQUEST_PBIT_RESULTS_REPORT_ID:
					irs_send_msg_pbit_results_report();
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_PBIT_RESULTS_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
				case DATA_REQUEST_IBIT_RESULTS_REPORT_ID:
					irs_send_msg_ibit_results_report();
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_IBIT_RESULTS_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
				case DATA_REQUEST_ACT_MAINT_REPORT_ID:
					IRS_Send_Msg_Maintanance_Actuator_Data();
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_ACT_MAINTENANCE_REPORT_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
				case DATA_REQUEST_MAINT_REPORT_ID:
					IRS_Send_Msg_Maintenance_Report();
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_MAINTENANCE_REPORT_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
				default:
					/*Invalid message type -> Discard message*/
					ucLast_Message_Status = (uint8_t)E_LAST_MSG_TYPE_ERR;
					g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_INVALID_DATA_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
					break;
			}
		break;

		case IRS_ACTUATOR_ACTIVATION_REQ:
			/*Check Actuator ID is in range*/
			act_id = ((uint16_t)(Received_Msg[MSG_DATA_BYTE_PTR]) << 8U) | (uint16_t)(Received_Msg[MSG_DATA_BYTE_PTR + 1U]);
			if((ACTUATOR_ID_MIN > act_id) || (ACTUATOR_ID_MAX < act_id))
			{
				ucLast_Message_Status = (uint8_t)E_LAST_MSG_TYPE_ERR;
			}
			else
			{
				/* Send request only if there is no other request active */
				if ((E_NO_REQ == g_Actuator_Man_Req.request_status) && (E_STBY_MODE == g_SysStatus.Sys_Mode))
				{
					g_Actuator_Man_Req.actuator_id = act_id;
					g_Actuator_Man_Req.request_status = E_NEW_REQ;
				}
				/* --- */
			}
			g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_ACTUATOR_ACTIVATION_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
			break;


		case IRS_SET_TIME_DATE_REQ:
			g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_SET_TIME_DATE_REQ, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
			set_time_date_from_irs_msg();
			IRS_Send_Msg_Maintenance_Report();
			break;
		case IRS_LOG_ERASE_REQ:
			/*The logger will be erased in periodic task due to long execution time*/
			Set_Logger_Erase_Flag();
			break;
		case IRS_LOG_DATA_REQ:
			/*Reading data from logger will be executed in periodic task due to long execution time*/
			event_number = ((uint16_t)(Received_Msg[IRS_LOG_DATA_REQ_START_HIGH_PTR]) << 8U) | (uint16_t)(Received_Msg[IRS_LOG_DATA_REQ_START_LOW_PTR]);
			Set_Read_Request(TRUE, event_number);
			break;
		case IRS_IPS_RESET_REQ:
			if((E_IBIT_MODE != g_SysStatus.Sys_Mode) && (E_OPER_MODE != g_SysStatus.Sys_Mode))
			{
				/* Log reset request */
				g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_FC_IPS_RESET_REQUEST_RECEIVED, 0x00U,E_UNIT_TYPE_IPS,IPS_ID,Logger_Spare_Bytes);

				/* Wait for WDOG Reset */
				for(uint8_t wdog_loop_counter = 0U; wdog_loop_counter < WDOG_LOOP_MAX_STEPS; wdog_loop_counter++)
				{
					HAL_Delay(WDOG_LOOP_STEP_INTERVAL_MILIS);
				}

				/* Reset request failed */
				g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_FC_IPS_RESET_FAILED, 0x00U,E_UNIT_TYPE_IPS,IPS_ID,Logger_Spare_Bytes);
			}
			else
			{
				/* Log reset request ignored */
				g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_FC_IPS_RESET_REQUEST_IGNORED, 0x00U,E_UNIT_TYPE_IPS,IPS_ID,Logger_Spare_Bytes);
			}
			break;
		default:
			/*Invalid message type -> Discard message*/
			ucLast_Message_Status = (uint8_t)E_LAST_MSG_TYPE_ERR;
			g_SysStatus.Logger_FL |= Write_Event_To_Memory(E_SYSTEM_EVENT,E_EC_IRS_INVALID_COMMAND, 0x00U,E_UNIT_TYPE_DIMC, DIMC_ID, Logger_Spare_Bytes);
			break;
	}
}

/*************************************************************************************
** IRS_Send_Msg_Operational_Status_Report - Sends System's operational status IRS message
**                                          via RS-422 serial port
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void IRS_Send_Msg_Operational_Status_Report(void)
{
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t index = 0U; /* Used to iterate over the CTRYs */
	uint8_t i = 0U;
	uint8_t ctry_st = 0x00U;
	uint8_t all_act_st = 0x00U;


	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = SYS_OPER_STATUS_RESP_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_SYS_OPER_STATUS_RESP;

	tx_message[i++] = g_SysStatus.Sys_Mode;
	tx_message[i++] = g_SysStatus.DIMC_PBIT_Failure_Desc;
	tx_message[i++] = (uint8_t)(g_SysStatus.All_CTRYs_PBIT_Status & 0x000000FFU);
	tx_message[i++] = (uint8_t)((g_SysStatus.All_CTRYs_PBIT_Status >> 8U) & 0x000000FFU);
	tx_message[i++] = (uint8_t)((g_SysStatus.All_CTRYs_PBIT_Status >> 16U) & 0x0000000FU);
	tx_message[i++] = g_SysStatus.DiscretesStatus;
	tx_message[i++] = ((uint8_t)(~g_SysStatus.ChannelA_FaultsStatus) & 0x0FU);
	tx_message[i++] = ((uint8_t)(~g_SysStatus.ChannelC_FaultsStatus) & 0x3FU);
	tx_message[i++] = g_SysStatus.DIMC_CBIT_Failure_Desc;

	/* CTRYs 130H, 132H, ..., 140H */
	for(index = 0U; index < 12U; index += 2U)
	{
		(void)Get_CTRY_Status(index,&ctry_st);
		(void)Get_CTRY_All_Actuators_Status(index,&all_act_st);
		tx_message[i++] = ((ctry_st << 6U) | (all_act_st & 0x3FU));
	}

	/* CTRYs 131H, 133H, ..., 141H */
	for(index = 1U; index < 12U; index += 2U)
	{
		(void)Get_CTRY_Status(index,&ctry_st);
		(void)Get_CTRY_All_Actuators_Status(index,&all_act_st);
		tx_message[i++] = ((ctry_st << 6U) | (all_act_st & 0x3FU));
	}

	/* CTRYs 142H, 144H, ..., 148H */
	for(index = 12U; index < 20U; index += 2U)
	{
		(void)Get_CTRY_Status(index,&ctry_st);
		(void)Get_CTRY_All_Actuators_Status(index,&all_act_st);
		tx_message[i++] = ((ctry_st << 6U) | (all_act_st & 0x3FU));
	}

	/* CTRYs 143H, 145H, ..., 149H */
	for(index = 13U; index < 20U; index += 2U)
	{
		(void)Get_CTRY_Status(index,&ctry_st);
		(void)Get_CTRY_All_Actuators_Status(index,&all_act_st);
		tx_message[i++] = ((ctry_st << 6U) | (all_act_st & 0x3FU));
	}

	/* Send Msg */
	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + SYS_OPER_STATUS_RESP_DATA_LENGTH_BYTES);
}

/*************************************************************************************
** IRS_Send_Msg_Maintanance_Actuator_Data - Sends Last Operated Actuator's data as IRS message
**                                          via RS-422 serial port
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void IRS_Send_Msg_Maintanance_Actuator_Data(void)
{
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t i = 0U;

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = ACT_MAINT_RESP_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_ACT_MAINT_RESP;

	tx_message[i++] = (uint8_t)((g_Maint_ActuatorData.Actuator_ID & 0xFF00U) >> 8U);
	tx_message[i++] = (uint8_t)(g_Maint_ActuatorData.Actuator_ID & 0x00FFU);
	tx_message[i++] = g_Maint_ActuatorData.TestsResults;
	tx_message[i++] = g_Maint_ActuatorData.VibrationValue;
	tx_message[i++] = g_Maint_ActuatorData.CurrConsumption;
	tx_message[i++] = (uint8_t)g_Maint_ActuatorData.TemperatureCpu; /* need to check that the casting is not cause data loss */
	tx_message[i]   = (uint8_t)g_Maint_ActuatorData.TemperatureDrv;

	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + ACT_MAINT_RESP_DATA_LENGTH_BYTES);
}
/*************************************************************************************
** IRS_Send_Msg_Log_Data_Report - Sends Log Data via IRS serial port
**
** Params : uint8_t* log_data - Pointer to Log Data array
**          uint8_t size - Size of the Log Data(number of bytes)
**
** Returns: None
** Notes:
*************************************************************************************/
void IRS_Send_Msg_Log_Data_Report(uint8_t *log_data, uint8_t size)
{
	uint8_t tx_message[170U] = {0U};
	uint8_t i = 0U;
	uint8_t index = 0U;

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = (size + 1U);
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_LOG_DATA_RESP;
	for(index = 0U; index < size; index++,*log_data++)
	{
		tx_message[i++] = *log_data;

	}
	irs_message_transmit(tx_message,(uint8_t)(MSG_MINIMAL_LENGTH_BYTES + size));
}
/*************************************************************************************
** set_time_date_from_irs_msg - Gets Time/Date value from received IRS message
**                              Writes Time/Date values to RTC
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void set_time_date_from_irs_msg(void)
{
	uint8_t i = IRS_TIME_DATE_REQ_START_PTR;
	RTC_TimeDate_t stTimeDateIRS = {0U};
	stTimeDateIRS.Year    = Received_Msg[i++];
	stTimeDateIRS.Month   = Received_Msg[i++];
	stTimeDateIRS.Day     = Received_Msg[i++];
	stTimeDateIRS.Hours   = Received_Msg[i++];
	stTimeDateIRS.Minutes = Received_Msg[i++];
	stTimeDateIRS.Seconds = Received_Msg[i++];
	(void)RTC_SetDateTime(&stTimeDateIRS);/*Write received Time/Date to RTC*/
	RTC_GetDateTime(&stTimeDateIRS);/*Update stTimeDateIRS struct by reading Time/Date from RTC*/
}

/*************************************************************************************
** irs_send_msg_pbit_results_report - Sends PBIT Results IRS message via RS-422 serial port
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void irs_send_msg_pbit_results_report(void)
{
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t i = 0U;
	uint8_t index = 0U;		/* Used to iterate over the CTRYs */
	uint8_t ctry_pbit_result = 0x00U;

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = PBIT_RESULTS_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_PBIT_RESULT_RESP;
	tx_message[i++] = g_SysStatus.DIMC_PBIT_Failure_Desc;
	for(index = 0U; index < CTRY_MAX_NUM; index++)
	{
		ctry_pbit_result |= (g_SysStatus.CTRY_PBIT_Status[index].PBIT_Failure_Desc >> 12U) & 0x01U; 			/* Current cons test */
		ctry_pbit_result |= ((g_SysStatus.CTRY_PBIT_Status[index].PBIT_Failure_Desc >> 13U) << 1U) & 0x2U; 		/* Accelerometer test */
		ctry_pbit_result |= ((g_SysStatus.CTRY_PBIT_Status[index].PBIT_Failure_Desc) << 2U) & 0x4U; 			/* Power supply test (V_in) */
		ctry_pbit_result |= ((g_SysStatus.CTRY_PBIT_Status[index].PBIT_Failure_Desc >> 2U) << 4U) & 0x08U; 		/* Temp. sensor test */
		ctry_pbit_result |= ((g_SysStatus.CTRY_PBIT_Status[index].PBIT_Failure_Desc >> 3U) << 5U) & 0x10U; 		/* Motor drivers test */
		ctry_pbit_result |= ((g_SysStatus.CTRY_Status[index].Ctry_Mot_Oper_In_Test_Status) << 6U) & 0x20U; 		/* CTRY cutoff test */
		tx_message[i++] = ctry_pbit_result;
		ctry_pbit_result = 0U; /* Reset variable before next iteration */
	}
	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + PBIT_RESULTS_DATA_LENGTH_BYTES);
}
/*************************************************************************************
** irs_send_msg_ibit_results_report - Sends IBIT Results IRS message via RS-422 serial port
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void irs_send_msg_ibit_results_report(void)
{
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t i = 0U;
	uint8_t index = 0U;		/* Used to iterate over the CTRYs */

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = IBIT_RESULTS_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_IBIT_RESULT_RESP;
	tx_message[i++] = g_IBIT_Result.IBIT_status;
	tx_message[i++] = g_IBIT_Result.DIMC_IBIT_Res;
	for(index = 0U; index < CTRY_MAX_NUM; index++)
	{
		tx_message[i++] = g_IBIT_Result.CTRYs_IBIT_Results[index];
	}
	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + IBIT_RESULTS_DATA_LENGTH_BYTES);
}

/*************************************************************************************
** irs_send_msg_sw_version_report - Sends SW version IRS message via RS-422 serial port
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
static void irs_send_msg_sw_version_report(void)
{
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t i = 0U;
	uint8_t index = 0U;		/* Used to iterate over the CTRYs */

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = VER_REPORT_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = (uint8_t)IRS_VERSION_RESP;
	tx_message[i++] = _VER_MAJOR_;
	tx_message[i++] = _VER_MINOR_;
	for(index = 0U; index < CTRY_MAX_NUM; index++)
	{
		tx_message[i++] = (uint8_t)((g_CTRYs_SW_Ver[index] >> 8U) & 0x00FFU);
		tx_message[i++] = (uint8_t)(g_CTRYs_SW_Ver[index] & 0x00FFU);
	}
	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + VER_REPORT_DATA_LENGTH_BYTES);
}

/*************************************************************************************
** IRS_Send_Msg_Maintenance_Report - Sends Maintenance Report IRS message via RS-422 serial port
**
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void IRS_Send_Msg_Maintenance_Report(void)
{
	RTC_TimeDate_t stTimeDateIRS = {0U};
	uint8_t tx_message[TX_MSG_BUF_SIZE] = {0U};
	uint8_t i = 0U;
/* Rev 1.5 - Update operation time counter implementation */
	uint32_t sys_acc_oper_time_sec = 0x00U;
	uint16_t sys_mission_oper_time_sec = 0x00U;
	if (E_OPER_MODE == g_SysStatus.Sys_Mode)
	{
		/* Set current operation time  */
		sys_mission_oper_time_sec = (uint16_t)((HAL_GetTick() - g_Sys_OPER_Time_Sec) / 1000U);

		/* Add to the accumulated operation time */
		sys_acc_oper_time_sec = (g_Sys_Acc_OPER_Time_Sec +sys_mission_oper_time_sec);
	}
	else
	{
		sys_acc_oper_time_sec = g_Sys_Acc_OPER_Time_Sec;
	}

	RTC_GetDateTime(&stTimeDateIRS);/*Read current Time/Date from RTC*/

	tx_message[i++] = SOM_VAL_HIGH;
	tx_message[i++] = SOM_VAL_LOW;
	tx_message[i++] = MAINT_REPORT_DATA_LENGTH_BYTES;
	tx_message[i++] = ucCyclic_Msg_Counter;
	tx_message[i++] = ucLast_Message_Status;
	tx_message[i++] = IRS_SYS_MAINT_RESP;
	tx_message[i++] = (uint8_t)((sys_acc_oper_time_sec >> 24U) & 0x000000FFU);
	tx_message[i++] = (uint8_t)((sys_acc_oper_time_sec >> 16U) & 0x000000FFU);
	tx_message[i++] = (uint8_t)((sys_acc_oper_time_sec >> 8U) & 0x000000FFU);
	tx_message[i++] = (uint8_t)( sys_acc_oper_time_sec  & 0x000000FFU);
	tx_message[i++] = (uint8_t)((sys_mission_oper_time_sec >> 8U) & 0x00FFU);
	tx_message[i++] = (uint8_t)( sys_mission_oper_time_sec  & 0x00FFU);
	tx_message[i++] = stTimeDateIRS.Year;
	tx_message[i++] = stTimeDateIRS.Month;
	tx_message[i++] = stTimeDateIRS.Day;
	tx_message[i++] = stTimeDateIRS.Hours;
	tx_message[i++] = stTimeDateIRS.Minutes;
	tx_message[i++] = stTimeDateIRS.Seconds;
	tx_message[i++] = Get_Logger_Status();
	irs_message_transmit(tx_message,MSG_HEADER_LENGTH_BYTES + MAINT_REPORT_DATA_LENGTH_BYTES);
}
/*************************************************************************************
** irs_message_transmit - Transmits IRS composed message
**                        Adds calculated message's CRC as a trailer
**
** Params : uint8_t *msg_data - pointer to message array
**          uint8_t msg_length- length of IRS message(number of bytes) without 2 trailer bytes
**
** Returns: None
** Notes:
*************************************************************************************/
static void irs_message_transmit(uint8_t *msg_data, uint8_t msg_length)
{
	uint8_t *start_msg_ptr = 0x00U;
	uint16_t msg_crc_value = 0U;

	if (NULL != msg_data)
	{
		start_msg_ptr = msg_data;
		msg_crc_value = Calculate_CRC16(msg_data, msg_length);
		msg_data += msg_length;

		if (NULL != msg_data)
		{
			*msg_data = (uint8_t)((msg_crc_value >> 8U) & 0x00FFU);
			*msg_data++;
			*msg_data = (uint8_t)(msg_crc_value & 0x00FFU);

			if (NULL != start_msg_ptr)
			{
				(void)HAL_UART_Transmit(&g_HandleUart1Irs,start_msg_ptr,(uint16_t)(msg_length + MSG_CCSUM_LENGTH_BYTES) ,IRS_TX_MSG_TIMEOUT_MILIS);
			}
		}
	}
}

/*************************************************************************************
** IRS_UART1_Serial_Init - Attaches RX_Byte variable to serial port receive buffer
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void IRS_UART1_Serial_Init(void)
{
	HAL_StatusTypeDef hal_status = HAL_OK;

	hal_status = HAL_UART_Receive_IT(&g_HandleUart1Irs,&RX_Byte,1U);

	if(HAL_OK != hal_status)
	{
		IRS_Errors_Counter++;
	}
}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
