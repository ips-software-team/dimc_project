/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : module_init_dimc.c
** Author    : Leonid Savchenko
** Revision  : 2.2
** Updated   : 30-05-2023
**
** Description: This file contains procedures for DIMC unit initialization
** 				and initial tests(Power Up BIT)
**
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1	: 07-09-2020 -- Updating g_SysStatus init according to the .h file changes.
** Rev 1.2  : 03-01-2021 -- Updated due to verification remarks.
** Rev 1.3  : 24-02-2021 -- Added 'Enter_Safe_Mode_Req' variable to g_SysStatus for handling UAV enter safe mode request.
** Rev 1.4	: 25-04-2021 -- Updated due to verification remark (handling the return value of the "RTC_StartUp_Initialization" procedure).
** Rev 1.5	: 06-07-2021 -- Update INIT procedure.
** 						  Added SW error data field to 'g_SysStatus'.
** 						  Update the IBIT test result structure.
** Rev 1.6  : 11-07-2021 -- Added doxygen comments.
** Rev 1.7  : 26-08-2021 -- Updated WDOG and FLASH tests enable.
** Rev 1.8	: 09-09-2021 -- Updated due to verification remarks.
** Rev 1.9	: 22-03-2022 -- Update initial values of 'g_SysStatus'.
** Rev 2.0	: 24-07-2022 -- Update comments.
** 							Add casting to defined values.
** Rev 2.1	: 10-08-2022 -- Disable IBIT Retry capability.
** Rev 2.2	: 30-05-2023 -- Update Doxygen comments.
**************************************************************************************
*/


/* -----Includes ---------------------------------------------------------------------*/
#include "module_init_dimc.h"
#include "voltage_meas_dimc.h"
#include "gipsy_hal_can.h"
#include "mcu_config_dimc.h"
#include "gipsy_hal_rtc.h"
#include "exflash_dimc.h"
#include "pbit_dimc.h"
#include "main.h"
#include "pbit_dimc.h"
#include "wdog.h"
#include "fault_management_dimc.h"
#include "flash_inegrity_test.h"
#include "gpio_control_dimc.h"
#include "gipsy_hal_rcc.h"

/* -----Definitions ----------------------------------------------------------------*/
#define FLT_LED_CRC_FAILURE_TOGGLE_PERIOD_MILIS           (uint16_t)400U  /*msec*/
#define FLT_LED_WDG_FAILURE_TOGGLE_PERIOD_MILIS           (uint16_t)1500U /*msec*/

#define  WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS    			  (uint8_t)100U

/* -----Macros ---------------------------------------------------------------------*/

/* -----Global variables -----------------------------------------------------------*/

/*!Specifies CTRYs' software version values (Range: var type | Units: None)*/
/*!\sa Set function: HAL_CAN_RxFifo0MsgPendingCallback(), Perform_System_PBIT_Test()*/
/*!\sa Get function: irs_send_msg_sw_version_report(), Perform_System_PBIT_Test()*/
uint16_t g_CTRYs_SW_Ver[CTRY_MAX_NUM] = {0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU,\
										 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU, 0xFFFFU};

/*!Specifies current OPER time (Range: var type | Units: Second)*/
/*!\sa Set function: monitor_discrete_channels(), enter_safe_mode(), operation_manager()*/
/*!\sa Get function: IRS_Send_Msg_Maintenance_Report(), enter_safe_mode(), operation_manager()*/
uint32_t g_Sys_OPER_Time_Sec = 0x00U;

/*!Specifies accumulated OPER time (Range: var type | Units: Second)*/
/*!\sa Set function: enter_safe_mode(), operation_manager()*/
/*!\sa Get function: IRS_Send_Msg_Maintenance_Report(), enter_safe_mode(), operation_manager()*/
uint32_t g_Sys_Acc_OPER_Time_Sec = 0x00U;

/*!Specifies operation system's status parameters (Range: var type | Units: see var declaration)*/
/*!\sa Sys_Mode - Set function: enter_safe_mode(), ibit_test(), monitor_discrete_channels(), operation_manager(), Periodic_Tasks_Scheduler()*/
/*!\sa Sys_Mode - Get function: enter_safe_mode(), Fault_Management(), handle_irs_actuator_activation_command(), ibit_test(), irs_execute_recieved_command(), IRS_Send_Msg_Maintenance_Report(), IRS_Send_Msg_Operational_Status_Report(), monitor_actuators(), monitor_discrete_channels(), operation_manager(), Perform_System_PBIT_Test(), Periodic_Tasks_Scheduler(), Proccess_Logger_Request_from_IRS(), Diac_Control()*/
/*!\sa DiscretesStatus - Set function: monitor_discrete_channels()*/
/*!\sa DiscretesStatus - Get function: IRS_Send_Msg_Operational_Status_Report()*/
/*!\sa Can_Bus_Error - Set function: Fault_Management(), HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Can_Bus_Error - Get function: Fault_Management()*/
/*!\sa Can_Bus_Tr_error - Set function: CAN_Message_Transmit(), Fault_Management()*/
/*!\sa Can_Bus_Tr_error - Get function: Fault_Management()*/
/*!\sa Can_Bus_Msg_Type_Error - Set function: Fault_Management(), HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Can_Bus_Msg_Type_Error - Get function: Fault_Management()*/
/*!\sa Can_bus_Address_Error - Set function: Fault_Management(), HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Can_bus_Address_Error - Get function: Fault_Management()*/
/*!\sa Can_Bus_Ctry_Id_Error - Set function: Fault_Management(), HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa Can_Bus_Ctry_Id_Error - Get function: Fault_Management()*/
/*!\sa ChannelA_FaultsStatus - Set function: Fault_Management(), monitor_discrete_channels(), Perform_CBIT_Test()*/
/*!\sa ChannelA_FaultsStatus - Get function: Fault_Management(), IRS_Send_Msg_Operational_Status_Report()*/
/*!\sa ChannelC_FaultsStatus - Set function: Fault_Management(), monitor_discrete_channels(), Perform_CBIT_Test()*/
/*!\sa ChannelC_FaultsStatus - Get function: Fault_Management(), IRS_Send_Msg_Operational_Status_Report()*/
/*!\sa DIMC_CBIT_Status - Set function: Fault_Management()*/
/*!\sa DIMC_CBIT_Status - Get function: Fault_Management(), IRS_Send_Msg_Operational_Status_Report(), Manage_OnBoard_Leds()*/
/*!\sa DIMC_PBIT_Status - Set function: Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_PBIT_Status - Get function: Manage_OnBoard_Leds()*/
/*!\sa DIMC_PBIT_Failure_Desc - Set function: Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_PBIT_Failure_Desc - Get function: IRS_Send_Msg_Operational_Status_Report(), irs_send_msg_pbit_results_report(), Perform_DIMC_PBIT_Test(), Fault_Management()*/
/*!\sa DIMC_CBIT_Failure_Desc - Set function: Perform_CBIT_Test()*/
/*!\sa DIMC_CBIT_Failure_Desc - Get function: Perform_CBIT_Test(),Fault_Management()*/
/*!\sa CTRYs_Status - Set function: Set_CTRY_Status()*/
/*!\sa CTRYs_Status - Get function: Manage_OnBoard_Leds()*/
/*!\sa All_CTRYs_PBIT_Status - Set function: HAL_CAN_RxFifo0MsgPendingCallback()*/
/*!\sa All_CTRYs_PBIT_Status - Get function: IRS_Send_Msg_Operational_Status_Report(), Manage_OnBoard_Leds()*/
/*!\sa CTRY_PBIT_Status - Set function: HAL_CAN_RxFifo0MsgPendingCallback(), Perform_System_PBIT_Test()*/
/*!\sa CTRY_PBIT_Status - Get function: ctry_fault_mang(), irs_send_msg_pbit_results_report(), Perform_System_PBIT_Test()*/
/*!\sa CTRY_Status - Set function: ctry_fault_mang(), Fault_Management(), get_ctrys_status(), HAL_CAN_RxFifo0MsgPendingCallback(), monitor_actuators(), Perform_System_PBIT_Test()*/
/*!\sa CTRY_Status - Get function: compute_ctry_temp_avrg(), ctry_fault_mang(), Fault_Management(), get_ctrys_status(),HAL_CAN_RxFifo0MsgPendingCallback(), irs_send_msg_pbit_results_report(), monitor_actuators(), Perform_System_PBIT_Test()*/
/*!\sa Logger_FL - Set function: ctry_fault_mang(), enter_safe_mode(), Fault_Management(), ibit_test(), operation_manager(), Perform_System_PBIT_Test(), send_operation_limits(), Diac_Control()*/
/*!\sa Logger_FL - Get function: ()*/
/*!\sa SW_Erros - Set function: ctry_fault_mang(), DIMC_Module_Init(), Fault_Management(), Perform_CBIT_Test(), Perform_System_PBIT_Test(), Periodic_Tasks_Scheduler(), send_operation_limits()*/
/*!\sa SW_Erros - Get function: Fault_Management()*/
st_System_Operational_Status g_SysStatus =
{
		.Sys_Mode = E_INIT_MODE,
		.DiscretesStatus = 0U,
		.Can_Bus_Error = 0U,
		.Can_Bus_Tr_error = 0U,
		.Can_Bus_Msg_Type_Error = 0U,
		.Can_bus_Address_Error = 0U,
		.Can_Bus_Ctry_Id_Error = 0U,
		.ChannelA_FaultsStatus = 0xFFU,
		.ChannelC_FaultsStatus = 0xFFU,
		.DIMC_CBIT_Status = 0U,
		.DIMC_PBIT_Status = 0U,
		.DIMC_CBIT_Failure_Desc = 0U,
		.DIMC_PBIT_Failure_Desc = 0U,
		.CTRYs_Status = 0U,
		.All_CTRYs_PBIT_Status = 0U,
		.CTRY_PBIT_Status = {0U},
		.CTRY_Status = {0U},
		.Logger_FL = 0U,
		.SW_Erros = 0U
};


/*!Specifies system's tests statuses (Range: var type | Units: see var declaration)*/
/*!\sa DIMC_Volt_Test_Status - Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Volt_Test_Status - Get function: CAN_Message_Transmit(), Fault_Management(), ibit_test()*/
/*!\sa DIMC_Can_Bus_Init_Test_Status - Set function: DIMC_Module_Init()*/
/*!\sa DIMC_Can_Bus_Init_Test_Status - Get function: CAN_Message_Transmit(), Fault_Management(), ibit_test(), Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Ch_A_Test_Status - Set function: Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Ch_A_Test_Status - Get function: Fault_Management()*/
/*!\sa DIMC_Ch_B_Test_Status - Set function: Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Ch_B_Test_Status - Get function: Fault_Management()*/
/*!\sa DIMC_Ch_C_Test_Status - Set function: ibit_test(), Perform_System_PBIT_Test()*/
/*!\sa DIMC_Ch_C_Test_Status - Get function: Fault_Management()*/
/*!\sa DIMC_Logger_Test_Status - Set function: DIMC_Module_Init(), Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Logger_Test_Status - Get function: Fault_Management(), ibit_test(), Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Monitor_Status_Table_Test_Status - Set function: Perform_CBIT_Test()*/
/*!\sa DIMC_Monitor_Status_Table_Test_Status - Get function: Fault_Management()*/
/*!\sa DIMC_Oper_Inputs_Test_Status - Set function: Perform_DIMC_PBIT_Test()*/
/*!\sa DIMC_Oper_Inputs_Test_Status - Get function: Fault_Management(), ibit_test(), Perform_System_PBIT_Test()*/
/*!\sa CTRYs_SW_Ver_Mismatch_Test_Status - Set function: Perform_System_PBIT_Test()*/
/*!\sa CTRYs_SW_Ver_Mismatch_Test_Status - Get function: Fault_Management(), ibit_test()*/

st_System_Tests_Status g_SysTestsStatus =
{
		.DIMC_Volt_Test_Status = TEST_OK,
		.DIMC_Can_Bus_Init_Test_Status = TEST_OK,
		.DIMC_Ch_A_Test_Status = TEST_OK,
		.DIMC_Ch_B_Test_Status = TEST_OK,
		.DIMC_Ch_C_Test_Status = TEST_OK,
		.DIMC_Logger_Test_Status = TEST_OK,
		.DIMC_Monitor_Status_Table_Test_Status = TEST_OK,
		.DIMC_Oper_Inputs_Test_Status = TEST_OK,
		.CTRYs_SW_Ver_Mismatch_Test_Status = TEST_OK
};


/*!Specifies IBIT tests statuses (Range: var type | Units: see var declaration)*/
/*!\sa IBIT_status - Set function: monitor_discrete_channels(), ibit_test(), monitor_actuators()*/
/*!\sa IBIT_status - Get function: irs_send_msg_ibit_results_report(), ibit_test()*/
/*!\sa DIMC_IBIT_Res - Set function: ibit_test()*/
/*!\sa DIMC_IBIT_Res - Get function: irs_send_msg_ibit_results_report()*/
/*!\sa CTRYs_IBIT_Results - Set function: ibit_test(), monitor_actuators()*/
/*!\sa CTRYs_IBIT_Results - Get function: irs_send_msg_ibit_results_report(), ibit_test(), monitor_actuators()*/
st_IBIT_Res g_IBIT_Result =
{
		.IBIT_status = E_IBIT_NOT_DONE,
		.DIMC_IBIT_Res = 0U,
		.CTRYs_IBIT_Results = {0U},
};

/* -----External variables ---------------------------------------------------------*/

/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/


/*************************************************************************************
** DIMC_Module_Init - Initialization mode of DIMC. Performs MCU configuration, DIMC's
** 					  peripherals configuration, WDOG/CRC tests, PBIT.
**
** Params : None
**
** Returns: None
** Notes:
*************************************************************************************/
void DIMC_Module_Init(void)
{

#ifndef _DEBUG_
	uint32_t crc_failure_last_toggle_exec = 0U;
	uint32_t wdg_failure_last_toggle_exec = 0U;
	uint32_t failure_wdg_trigger_exec     = 0U;
#endif

	/*Initialize MCU*/
	MCU_Periph_Config();


	/*Perform Flash integrity test*/
	if(E_CRC_TEST_OK != Flash_Integrity_Test())
	{
#ifndef _FLASH_TEST_DISABLE_
		/*In case of CRC Test failure:
		 * 1.) Activate CH_B outputs
		 * 2.) Toggle Failure LED (LED2)
		 * 3.) Stuck the operational code execution
		 */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		while(TRUE)
		{
			if(WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS <= (HAL_GetTick() - failure_wdg_trigger_exec))
			{
				WDOG_Trigger();
				failure_wdg_trigger_exec = HAL_GetTick();
			}
			if(FLT_LED_CRC_FAILURE_TOGGLE_PERIOD_MILIS <= (HAL_GetTick() - crc_failure_last_toggle_exec))
			{
				HAL_GPIO_TogglePin(LED_PORT,FLT_LED_PIN);
				crc_failure_last_toggle_exec = HAL_GetTick();
			}
		}
#endif
	}

#ifndef _WDOG_TEST_DISABLE_
	/*Perform Watch-Dog test*/
	WDOG_Perform_Test();
	if(E_WDOG_TEST_PASSED != WDOG_Get_Test_Result())
	{
		/*In case of WDOG Test failure:
		 * 1.) Activate CH_B outputs
		 * 2.) Toggle Failure LED (LED2)
		 * 3.) Stuck the operational code execution
		 */
		Fault_Channel_OUT_Control(E_FLT_CH_B_OUT, E_FLT_CH_FAULT);
		while(TRUE)
		{
			if(WDOG_TRIGGER_ON_FAULT_DELAY_MILLIS <= (HAL_GetTick() - failure_wdg_trigger_exec))
			{
				WDOG_Trigger();
				failure_wdg_trigger_exec = HAL_GetTick();
			}
			if(FLT_LED_WDG_FAILURE_TOGGLE_PERIOD_MILIS <= (HAL_GetTick() - wdg_failure_last_toggle_exec))
			{
				HAL_GPIO_TogglePin(LED_PORT,FLT_LED_PIN);
				wdg_failure_last_toggle_exec = HAL_GetTick();
			}
		}
	}
#endif
	Init_Ctrys_Status_Table(); /*Initialize status table with default parameters*/

	/*Initialize DIMC's peripherals*/
	g_SysTestsStatus.DIMC_Logger_Test_Status = (uint8_t)Init_Mem_Hal(); /* Logger initialization*/

	if (HAL_OK != RTC_StartUp_Initialization())
	{
		g_SysStatus.SW_Erros = TRUE;
	}

	/* Save can bus loopback test result to g_SysStatus global variable */
	g_SysTestsStatus.DIMC_Can_Bus_Init_Test_Status = (uint8_t)MCU_CAN_Interface_Init();

	/* Preform DIMC PBIT test */
	Perform_DIMC_PBIT_Test();

	/* Preform IPS PBIT test */
	Perform_System_PBIT_Test();

}

/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
