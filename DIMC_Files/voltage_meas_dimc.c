/************************ (C) COPYRIGHT AEROICEFREE *********************************/
/*
**************************************************************************************
** Project   : GIPSY
** Subsystem : DIMC
**
** Filename  : voltage_meas_dimc.c
** Author    : Leonid Savchenko
** Revision  : 1.6
** Updated   : 13-09-2021
**
** Description: This file provides procedures for voltage tests by measurement
**              using  internal ADC.
**
** Notes:
*/


/* Revision Log:
**
** Rev 1.0  : 25-09-2019 -- Original version created.
** Rev 1.1  : 12-02-2020 -- Updated Test_OnBoard_Voltage()
** Rev 1.2  : 31-10-2020 -- Updated Test_OnBoard_Voltage()
** Rev 1.3  : 03-12-2020 -- Change 'g_VoltageMeasurement.VoltageTestResult' init value from 'E_VOLT_TEST_FL' to: 'E_VOLT_TEST_OK'
** Rev 1.4	: 03-01-2021 -- Updated due to verification remarks.
** Rev 1.5  : 11-07-2021 -- Added doxygen comments.
** Rev 1.6	: 13-09-2021 -- Updated due to verification remarks.
**************************************************************************************
*/

/* -----Includes -------------------------------------------------------------------*/
#include "mcu_config_dimc.h"
#include "voltage_meas_dimc.h"
#include "gipsy_hal_adc.h"

/* -----Definitions ----------------------------------------------------------------*/

#define VOLT_MEAS_NUM_OF_SAMPLES       3U


#define VOLTAGE_5V_AMP_FACTOR 		 2.0
#define VOLTAGE_5V_LIM_MAX			 5.5
#define VOLTAGE_5V_LIM_MIN			 4.5
#define VOLTAGE_ERROR_VAL            99.0
#define VOLTAGE_TEST_ERROR_VAL       (uint8_t)0xFFU

/* -----Macros ---------------------------------------------------------------------*/

/* -----External variables ---------------------------------------------------------*/


/* -----Global variables -----------------------------------------------------------*/
/*!Specifies voltage measurement parameters (Range: var type | Units: see var declaration)*/
/*!\sa VoltageValue - Set function: Test_OnBoard_Voltage()*/
/*!\sa VoltageValue - Get function: CAN_Message_Transmit(), volt_mes_to_fault_value()*/
/*!\sa VoltageTestResult - Set function: Test_OnBoard_Voltage()*/
/*!\sa VoltageTestResult - Get function: CAN_Message_Transmit(), Test_OnBoard_Voltage()*/
st_VoltageMeasParameters g_VoltageMeasurement=
{
		.VoltageValue = VOLTAGE_ERROR_VAL,
		/* Rev 1.3 - 03-12-20 */
		 .VoltageTestResult = E_VOLT_TEST_OK,
		 /* -- */
};

struct st_VoltageMeasHwParameters
{
	const uint32_t ADC_Channel;
	const float AmplificationFactor;
}VoltHwParams =
{
		ADC_CHANNEL_8, VOLTAGE_5V_AMP_FACTOR
};


/* -----Static Function prototypes -------------------------------------------------*/

/* -----Modules implementation -----------------------------------------------------*/

/*************************************************************************************
** Test_OnBoard_Voltage - This function measures and tests on board voltage sources.
**
** Params : None
**
** Returns: e_VoltageTestStatus - Status of voltage measurement test.
** Notes:
*************************************************************************************/
e_VoltageTestStatus Test_OnBoard_Voltage(void)
{
	uint32_t adc_converted_value = 0U;
	uint16_t sum_of_samples = 0U;
	uint16_t voltage_samples[VOLT_MEAS_NUM_OF_SAMPLES] = {0U};
	uint8_t valid_samples_counter = 0U;
	float voltage_value = 0.0;

    g_VoltageMeasurement.VoltageValue = VOLTAGE_TEST_ERROR_VAL;
	/*Perform Voltage measurement*/
	for(uint8_t sample_num = 0U; sample_num < VOLT_MEAS_NUM_OF_SAMPLES; sample_num++)
	{
		/*Start the conversion process*/
		if(HAL_OK == HAL_ADC_Perform_Conversion(VoltHwParams.ADC_Channel, &adc_converted_value))
		{
			/*Get the converted value */
			voltage_samples[sample_num] = (uint16_t)(adc_converted_value & 0x00000FFFU);/*make sure to get 12 bit of data*/
		}
		else
		{
			/*Mark sample as ERROR in case of conversion start failed*/
			voltage_samples[sample_num] = VOLTAGE_ERROR_VAL;
		}
	}
	/*Compute average samples value from all valid samples*/
	for(uint8_t sample = 0U; sample < VOLT_MEAS_NUM_OF_SAMPLES; sample++)
	{
/* Was: if (VOLTAGE_ERROR_VAL != voltage_samples[sample]) */
		if ((voltage_samples[sample] - VOLTAGE_ERROR_VAL) > 1.0)
		{
			valid_samples_counter++;
			sum_of_samples+=voltage_samples[sample];
		}
	}
	/*Compute measured current value in case that valid samples available*/
	if(valid_samples_counter > 0U)
	{
		/*Calculate average value from measured voltage samples and convert it to measured voltage*/
		voltage_value = (sum_of_samples/valid_samples_counter) * ACD_RESOLUTION_VOLT;
		/*Get real  voltage value by removing External Amplification factor*/
		voltage_value = voltage_value * VoltHwParams.AmplificationFactor;

		if(voltage_value < 0)
		{
			voltage_value = 0.0;
		}
		/*Save measured voltage value*/
		g_VoltageMeasurement.VoltageValue = (uint8_t)(voltage_value * 10U);
	}
	else
	{
		/*Set error measurement in case that no valid samples produced*/
		g_VoltageMeasurement.VoltageValue = VOLTAGE_TEST_ERROR_VAL;
		g_VoltageMeasurement.VoltageTestResult = E_VOLT_MEAS_FL;

	}
	/*Perform Test of measured voltage*/
	/*Check whether the voltage measurement succeeded*/
	if(E_VOLT_TEST_OK == g_VoltageMeasurement.VoltageTestResult)
	{
		/*Check whether the measured voltage is inside limits*/
		if((VOLTAGE_5V_LIM_MIN < voltage_value ) && (VOLTAGE_5V_LIM_MAX > voltage_value))
		{
			g_VoltageMeasurement.VoltageTestResult = E_VOLT_TEST_OK;
		}
		else
		{
			g_VoltageMeasurement.VoltageTestResult = E_VOLT_TEST_FL;
		}
	}
	return g_VoltageMeasurement.VoltageTestResult;
}
/************************ (C) COPYRIGHT AEROICEFREE *****END OF FILE*****************/
